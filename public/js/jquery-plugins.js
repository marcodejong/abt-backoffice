

(function ($) {

    function textCompare(strA, strB) {
        if (strA.toLowerCase() === strB.toLowerCase()) {
            return 0;
        } else if (strA.toLowerCase() < strB.toLowerCase()) {
            return -1;
        } else {
            return 1;
        }
    }

    $.fn.insertOption = function (optionElementToInsert) {

        var selectElement = this;
        optionElementToInsert = $(optionElementToInsert);

        if ('SELECT' !== selectElement.prop("tagName")) {
            throw 'jQuery insertOption can only be performed on a select element';
        }

        if ('OPTION' !== optionElementToInsert.prop("tagName")) {
            throw 'jQuery insertOption can only insert an option element';
        }

        var inserted = false;
        $('option', selectElement).each(function () {
            var currentOptionElement = $(this);
            if ('' !== currentOptionElement.val()
                && -1 === textCompare(optionElementToInsert.prop('text'), currentOptionElement.prop('text'))
            ) {
                    currentOptionElement.before(optionElementToInsert);
                    inserted = true;
                    return false;
            }
        });

        if (!inserted) {
            selectElement.append(optionElementToInsert);
        }

    }

}(jQuery));
