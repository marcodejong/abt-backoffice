FROM hashicorp/envconsul AS envconsul
FROM r.sqills.com/sqills-projects-php-nginx-symfony:7.2-alpine

ENV NGINX_ROOT_FOLDER "/app/public"
ENV NGINX_PUBLIC_PHP_FILE_PATTERN "index\\.php"
ENV NGINX_PUBLIC_PHP_FILE "index.php"
ENV PATH "$PATH:/app/bin"
ENV LOG_PATH=/app/var/log/application.log

ENV S6_KEEP_ENV "1"

# Default consul settings
ENV CONSUL_HOST ""
ENV CONSUL_PREFIX ""
ENV CONSUL_TOKEN ""
ENV CONSUL_LOG_LEVEL "info"

# Copy envconsul from hashicorp/envconsul image
COPY --from=envconsul /bin/envconsul /bin/envconsul

COPY ./docker/source_env.sh /usr/local/bin/source_env

RUN apk update \
    && apk add -q --no-cache libxml2-dev icu-dev

RUN docker-php-ext-install soap intl bcmath sockets

COPY ./docker/cont-init.d /etc/cont-init.d
COPY ./docker/fix-attrs.d /etc/fix-attrs.d
COPY ./docker/config/nginx /etc/nginx/conf.d
COPY ./docker/config/php /usr/local/etc/php/conf.d

COPY ./docker /app/docker
COPY ./composer.json /app/composer.json
COPY ./composer.lock /app/composer.lock
COPY ./symfony.lock /app/symfony.lock
COPY ./public /app/public
COPY ./bin /app/bin
COPY ./config /app/config
COPY ./translations /app/translations
COPY ./templates /app/templates
COPY ./var /app/var
COPY ./vendor /app/vendor
COPY ./src /app/src
COPY ./tests /app/tests
COPY ./build.xml /app/build.xml
COPY ./phpunit.xml.dist /app/phpunit.xml.dist

RUN rm -rf /app/vendor/friendsofsymfony/ckeditor-bundle/src/Resources/public && \
    mkdir -p /tmp/ckeditor && \
    wget -O /tmp/ckeditor.tar.gz https://github.com/ckeditor/ckeditor-releases/archive/full/4.12.x.tar.gz && \
    tar -zxf /tmp/ckeditor.tar.gz -C /tmp/ckeditor && \
    mkdir -p /app/vendor/friendsofsymfony/ckeditor-bundle/src/Resources/public && \
    mv /tmp/ckeditor/ckeditor4-releases-full-4.12.x/* /app/vendor/friendsofsymfony/ckeditor-bundle/src/Resources/public && \
    rm -rf /tmp/ckeditor /tmp/ckeditor.tar.gz

HEALTHCHECK --interval=5s --start-period=10s --timeout=5s \
    CMD curl -I -s -f -o /dev/null http://localhost/_healthcheck || exit 1

ENTRYPOINT [ "source_env", "/init" ]
