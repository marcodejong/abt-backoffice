<?php

namespace App\Tests\Functional;

use App\Test\FixtureKernel;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;

abstract class WebTestCase extends BaseWebTestCase
{
    /** @inheritdoc */
    protected static function createClient(array $options = [], array $server = []): Client
    {
        $client = parent::createClient(
            array_merge(['environment' => 'ci'], $options),
            $server
        );

        $kernel = $client->getKernel();

        if ($kernel instanceof FixtureKernel) {
            $kernel->resetFixtures();
        }

        return $client;
    }
}
