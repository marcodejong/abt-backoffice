<?php

namespace App\Tests\Unit\Service\IpTools;

use App\Service\IpTools\IpUtilService;
use PHPUnit\Framework\TestCase;

class IpUtilServiceTest extends TestCase
{
    /**
     * @dataProvider getCasesForIpMatchesAny
     *
     * @param string $ip
     * @param string $range
     * @param bool $expectedResult
     */
    public function testIpMatchesAny(string $ip, array $range, bool $expectedResult): void
    {
        $this->assertEquals($expectedResult, $this->createIpUtilService()->ipMatchesAny($ip, $range));
    }

    public function getCasesForIpMatchesAny(): array
    {
        return [
            'exact match' => ['127.0.0.1', ['127.0.0.1'], true],
            'exact mismatch' => ['127.0.0.1', ['127.0.0.2'], false],
            'complete mismatch' => ['192.168.1.10', ['127.0.0.1'], false],
            '/24 match' => ['127.0.0.10', ['127.0.0.1/24'], true],
            '/24 mismatch' => ['127.0.1.10', ['127.0.0.1/24'], false],
            '/16 match' => ['192.168.2.10', ['192.168.1.1/16'], true],
            '/16 mismatch' => ['192.167.2.10', ['129.168.1.1/16'], false],
            '/8 match' => ['192.100.2.10', ['192.168.1.1/8'], true],
            '/8 mismatch' => ['193.168.1.1', ['129.168.1.1/8'], false],
            'two ranges, match first' => ['10.0.0.10', ['10.0.0.1/8', '192.168.1.1/24'], true],
            'two ranges, match second' => ['192.168.1.10', ['10.0.0.1/8', '192.168.1.1/24'], true],
            'two ranges, match both' => ['192.168.1.10', ['192.168.1.1/24', '192.168.1.2/24'], true],
            'two ranges, match none' => ['127.0.0.1', ['10.0.0.1/8', '192.168.1.1/24'], false],
            'many ranges, match one' => [
                '127.1.2.3',
                ['127.0.0.1', '127.0.0.1/24', '192.168.1.1/24', '127.1.0.0/16'],
                true,
            ],
        ];
    }

    private function createIpUtilService(): IpUtilService
    {
        return new IpUtilService();
    }
}