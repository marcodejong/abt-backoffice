<?php

namespace App\Tests\Unit\Monolog;

use App\Monolog\ActivationStrategy;
use App\Service\IpTools\IpUtilService;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ActivationStrategyTest extends TestCase
{
    /** @var RequestStack */
    private $requestStack;

    /**
     * @dataProvider getCasesForIsHandlerActivated
     *
     * @param string $actionLevel
     * @param array $ignoredHttpStatuses
     * @param null|Request $request
     * @param array $record
     * @param bool $expectedResult
     */
    public function testIsHandlerActivated(
        string $actionLevel,
        array $ignoredHttpStatuses,
        ?Request $request,
        array $record,
        bool $expectedResult
    ): void {
        Request::setTrustedProxies(['127.0.0.1'], Request::HEADER_X_FORWARDED_ALL);

        $activationStrategy = $this->createActivationStrategy($actionLevel, $ignoredHttpStatuses);

        if ($request) {
            $this->requestStack->push($request);
        }

        $isHandlerActivated = $activationStrategy->isHandlerActivated($record);
        $this->assertEquals(
            $expectedResult,
            $isHandlerActivated,
            sprintf(
                'Handler %s expected to be activated, but %s',
                $expectedResult ? 'was' : 'wasn\'t',
                $isHandlerActivated ? 'was' : 'wasn\'t'
            )
        );
    }

    public function getCasesForIsHandlerActivated(): iterable
    {
        foreach ($this->getRequests() as $requestName => [$method, $path, $clientIpAddress, $isHealthCheck]) {
            foreach (
                $this->getLogLevelPairs()
                as $logLevelPairName => [$activationLevel, $recordLevel, $isActivateLevel]
            ) {
                foreach (
                    $this->getHttpExceptions()
                    as $httpExceptionName => [$ignoredHttpStatuses, $httpStatus, $isIgnoredHttpStatus]
                ) {
                    $expectedResult = $isActivateLevel && !$isHealthCheck && !$isIgnoredHttpStatus;

                    yield "$requestName (loglevel $logLevelPairName, status $httpExceptionName)" => [
                        $activationLevel,
                        $ignoredHttpStatuses,
                        $this->createSimpleRequest($path, $method, $clientIpAddress),
                        $this->createLogRecord($recordLevel, $httpStatus),
                        $expectedResult,
                    ];
                }
            }
        }
    }

    private function getRequests(): array
    {
        return [
            'health check from trusted ip' => ['HEAD', '/_healthcheck', '127.0.0.1', true],
            'health check from untrusted ip' => ['HEAD', '/_healthcheck', '1.3.3.7', false],
            'get on index from trusted ip' => ['GET', '/', '127.0.0.1', false],
            'get on index from untrusted ip' => ['GET', '/', '1.3.3.7', false],
        ];
    }

    private function getLogLevelPairs(): iterable
    {
        $levels = ['debug', 'info', 'warning', 'error'];

        foreach ($levels as $activationSeverity => $activationLevel) {
            foreach ($levels as $recordSeverity => $recordLevel) {
                yield "${activationLevel}/${recordLevel}" => [
                    $activationLevel,
                    $recordLevel,
                    $recordSeverity >= $activationSeverity,
                ];
            }
        }
    }

    private function getHttpExceptions(): iterable
    {
        return [
            '200, no ignored statuses' => [[], 200, false],
            '200, ignore 200' => [[200], 200, false],
            '200, ignore 400' => [[400], 200, false],
            '400, ignore 400' => [[400], 400, true],
            '400, ignore 400/404' => [[400, 404], 400, true],
            '500, ignore 400/404' => [[400, 404], 500, false],
        ];
    }

    private function createActivationStrategy(string $actionLevel, array $ignoredHttpStatuses)
    {
        return new ActivationStrategy(
            $this->requestStack,
            new IpUtilService(),
            $actionLevel,
            $ignoredHttpStatuses
        );
    }

    private function createLogRecord(string $level, string $httpStatus): array
    {
        return [
            'level' => Logger::toMonologLevel($level),
            // 2xx responses are not represented as exceptions
            'context' => $httpStatus && ($httpStatus < 200 || $httpStatus >= 300) ?
                ['exception' => new HttpException($httpStatus)] :
                [],
        ];
    }

    private function createSimpleRequest($path, $method = 'GET', $remoteAddr = '127.0.0.1'): Request
    {
        return new Request(
            [], [], [], [], [], [
                'REQUEST_URI' => $path,
                'REQUEST_METHOD' => $method,
                'REMOTE_ADDR' => $remoteAddr,
            ]
        );
    }

    protected function setUp()
    {
        $this->requestStack = new RequestStack();
    }
}