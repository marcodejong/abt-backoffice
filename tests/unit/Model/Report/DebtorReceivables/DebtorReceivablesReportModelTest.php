<?php

namespace App\Tests\unit\Model\Report\DebtorReceivables;

use App\Model\Report\DebtorReceivables\DebtorReceivablesReportInput;
use App\Model\Report\DebtorReceivables\DebtorReceivablesReportModel;
use App\Repository\CustomerIdentityRepositoryInterface;
use App\Repository\InvoiceRepositoryInterface;
use App\Service\CustomerIdService\CustomerIdentityService;
use App\Test\Serializer\Traits\JsonSerializerTrait;
use Arriva\Abt\Entity\Card;
use Arriva\Abt\Entity\Configuration\PropositionTerm\FlexiblePropositionTerm;
use Arriva\Abt\Entity\Configuration\RegularConfiguration;
use Arriva\Abt\Entity\Customer;
use Arriva\Abt\Entity\CustomerGroup;
use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\InvoiceItem;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\Entity\PropositionInstance;
use Arriva\Abt\Entity\PropositionOrderItem;
use Arriva\Abt\Entity\Receivable\Receivable;
use Arriva\Abt\Entity\Receivable\SubscriptionTermReceivable;
use Arriva\Abt\Entity\SalePeriod;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Entity\ValidityPeriod;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\DateRange;
use Arriva\Abt\Value\Duration\IntervalDuration;
use Arriva\Abt\Value\EngravedId;
use Arriva\Abt\Value\Financial\Journal;
use Arriva\Abt\Value\InvoiceStatus;
use Arriva\Abt\Value\ProductConfiguration\RegularProductConfiguration;
use Arriva\Abt\Value\TAccount;
use Arriva\Reporting\Entity\CustomerIdentity;
use DateTimeImmutable;
use Exception;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Spatie\Snapshots\MatchesSnapshots;

class DebtorReceivablesReportModelTest extends TestCase
{
    use MatchesSnapshots;
    use JsonSerializerTrait;

    /**
     * @param Invoice[]                    $invoices
     * @param DebtorReceivablesReportInput $input
     *
     * @dataProvider getCasesForGenerateDebtorReceivablesReport
     * @throws Exception
     */
    public function testGenerateDebtorReceivablesReport(array $invoices, DebtorReceivablesReportInput $input): void
    {
        $model = new DebtorReceivablesReportModel(
            Journal::of(1),
            TAccount::of(1337),
            TAccount::of(1234),
            CostUnitCode::of('1020'),
            IntervalDuration::of('P1D'),
            $this->createInvoiceRepository($invoices),
            $this->createCustomerIdentityService()
        );

        $actualOutput = $model->generateDebtorReceivablesReport($input);

        $this->assertMatchesJsonSnapshot($this->serializeToJson($actualOutput));
    }

    public function getCasesForGenerateDebtorReceivablesReport(): array
    {
        $customer = new Customer();
        $customer->setIdmId(1);

        return [
            'empty report' => [
                [],
                new DebtorReceivablesReportInput(DateRange::of('2019-01-01', '2019-01-01')),
            ],
            'draft invoice' => [
                [InvoiceBuilder::create($customer)->build()],
                new DebtorReceivablesReportInput(DateRange::of('2019-01-01', '2019-01-01')),
            ],
            'open invoice not in period' => [
                [InvoiceBuilder::create($customer)
                               ->withSubscriptionTermCollection(VatMoney::fromAmountWithVat(Currency::EUR()->amount(10), 50))
                               ->open(Date::of('2018-01-01'))
                               ->build()],
                new DebtorReceivablesReportInput(DateRange::of('2019-01-01', '2019-01-01')),
            ],
            'open invoice in period' => [
                [
                    InvoiceBuilder::create($customer)
                                  ->withSubscriptionTermCollection(VatMoney::fromAmountWithVat(Currency::EUR()->amount(10), 50))
                    ->open(Date::of('2019-01-01'))
                    ->build()
                ],
                new DebtorReceivablesReportInput(DateRange::of('2019-01-01', '2019-01-01')),
            ]
        ];
    }

    private function createInvoiceRepository(array $invoices)
    {
        return new class($invoices) implements InvoiceRepositoryInterface
        {
            /** @var Invoice[] */
            private $invoices;

            public function __construct(array $invoices)
            {
                $this->invoices = $invoices;
            }

            public function findInvoiceById(int $invoiceId): ?Invoice
            {
                return Iterator::create($this->invoices)->first(
                    static function (Invoice $invoice) use ($invoiceId) {
                        return $invoice->getId() === $invoiceId;
                    }
                );
            }

            public function findDraftInvoicesByIdentifiers(array $invoiceIdentifiers): array
            {
                return Iterator::create($this->invoices)->filter(
                    static function (Invoice $invoice) use ($invoiceIdentifiers) {
                        return $invoice->getStatus() === InvoiceStatus::DRAFT()
                            && in_array($invoice->getId(), $invoiceIdentifiers, true);
                    }
                )->getArray(false);
            }

            /**
             * @param DateRange $period
             * @return Iterator&iterable<Invoice>
             */
            public function findInvoicesByInvoiceDate(DateRange $period): Iterator
            {
                $invoicesInPeriod = Iterator::create($this->invoices)
                    ->filter(
                        static function (Invoice $invoice) use ($period) {
                            return ($invoiceDate = $invoice->getInvoiceDate()) && $period->includes($invoiceDate);
                        }
                    )->getArray(false);

                return Iterator::create($invoicesInPeriod);
            }

            public function bulkUpdateInvoiceStatus(array $invoices, InvoiceStatus $invoiceStatus): int
            {
                throw new RuntimeException(
                    sprintf('Method %s not implemented yet.', __METHOD__)
                );
            }

            public function getNumberOfInvoicesInDeliveringState(): int
            {
                throw new RuntimeException(
                    sprintf('Method %s not implemented yet.', __METHOD__)
                );
            }

            public function getGroupedSubscriptionTermReceivables(Invoice $invoice, string $groupBy): array
            {
                throw new RuntimeException(
                    sprintf('Method %s not implemented yet.', __METHOD__)
                );
            }
        };
    }

    private function createCustomerIdentityService(): CustomerIdentityService
    {
        return new CustomerIdentityService(
            new class() implements CustomerIdentityRepositoryInterface {

                public function findByIdmId(int $idmId): ?CustomerIdentity
                {
                    return new CustomerIdentity(
                        $idmId,
                        new DateTimeImmutable('2019-01-01'),
                    new DateTimeImmutable('2019-01-01'),
                        1000 + $idmId,
                        2000 + $idmId,
                        '',
                        '',
                        '',
                        '',
                        new DateTimeImmutable('2019-01-01'),
                        sprintf('customer%d@test.com', $idmId),
                        '+31612345678',
                        'P',
                        '',
                        '1234AB',
                        1,
                        null,
                        'Mytown',
                        '1234',
                        false,
                        false
                    );
                }
            }
        );
    }
}

final class InvoiceBuilder
{
    public static function create(Customer $customer): self
    {
        return new self($customer);
    }

    /** @var Customer */
    private $customer;
    /** @var InvoiceItemBuilder[] */
    private $invoiceItemBuilders = [];
    /** @var Date|null */
    private $invoicedOn;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function withItem(InvoiceItemBuilder $itemBuilder): self
    {
        $this->invoiceItemBuilders[] = $itemBuilder;

        return $this;
    }

    public function withSubscriptionTermCollection(VatMoney $money): self
    {
        $proposition = new Proposition(
            'proposition',
            true,
            new PropositionGroup('proposition-group'),
            new FlexiblePropositionTerm(IntervalDuration::of('1 month'), false),
            new ValidityPeriod('validity-period', Date::of('2019-01-01'), Date::of('2019-01-01')),
            new SalePeriod('sale-period', Date::of('2019-01-01'), Date::of('2019-01-01'))
        );

        $card = new Card(EngravedId::of('3528111111111111'));

        return $this->withItem(
            InvoiceItemBuilder::collection(
                new SubscriptionTermReceivable(
                    $this->customer,
                    $money,
                    new Subscription(
                        'subscription',
                        $this->customer,
                        new PropositionInstance(
                            $proposition,
                            RegularConfiguration::instance(),
                            $card
                        ),
                        new PropositionOrderItem(
                            'proposition-order-item',
                            Date::of('2019-01-01'),
                            RegularProductConfiguration::of($proposition, new CustomerGroup()),
                            $money,
                             $card,
                            Date::of('2019-01-01'),
                            false,
                            null
                        ),
                        Date::of('2019-01-01'),
                        Date::of('2019-01-01')
                    ),
                    DateRange::of('2019-01-01', '2019-01-31'),
                    null
                )
            )
        );
    }

    public function open(Date $invoicedOn): self
    {
        $this->invoicedOn = $invoicedOn;

        return $this;
    }

    public function build(): Invoice
    {
        $invoice = new Invoice($this->customer);

        foreach ($this->invoiceItemBuilders as $itemBuilder) {
            $invoice->addInvoiceItem($itemBuilder->setInvoice($invoice)->build());
        }

        if ($this->invoicedOn) {
            $invoice->setStatus(InvoiceStatus::OPEN());
            $invoice->setInvoiceDate($this->invoicedOn);
        } else {
            $invoice->setStatus(InvoiceStatus::DRAFT());
            $invoice->setInvoiceDate(null);
        }

        return $invoice;
    }
}

class InvoiceItemBuilder
{
    private $invoice;
    private $receivable;

    private function __construct()
    {
    }

    public static function collection(Receivable $receivable): self
    {
        $instance = new self();
        $instance->receivable = $receivable;

        return $instance;
    }

    public function setInvoice(Invoice $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    public function build(): InvoiceItem
    {
        if ($this->receivable) {
            $item = new Invoice\Item\Collection('Collection', $this->receivable);
            $item->refreshAmount();

            $item->setInvoice($this->invoice);

            return $item;
        }

        throw new RuntimeException('Not enough information to construct invoice item');
    }
}
