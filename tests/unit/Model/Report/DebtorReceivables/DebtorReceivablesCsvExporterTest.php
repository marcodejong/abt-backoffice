<?php

namespace App\Tests\unit\Model\Report\DebtorReceivables;

use App\Model\Export\Target\CsvFile;
use App\Model\Report\DebtorReceivables\CollectionSummary;
use App\Model\Report\DebtorReceivables\DebtorReceivablesCsvExporter;
use App\Model\Report\DebtorReceivables\DebtorReceivablesReport;
use App\Test\CsvSnapshotDriver;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\Financial\Journal;
use Arriva\Abt\Value\InMemoryFile;
use Arriva\Abt\Value\PaymentMethod;
use Arriva\Abt\Value\TAccount;
use Arriva\Assist\Value\CustomerNumber;
use PHPUnit\Framework\TestCase;
use Spatie\Snapshots\MatchesSnapshots;

class DebtorReceivablesCsvExporterTest extends TestCase
{
    use MatchesSnapshots;

    /**
     * @param DebtorReceivablesReport $report
     * @dataProvider getCasesForExport
     */
    public function testExport(DebtorReceivablesReport $report): void
    {
        $file = InMemoryFile::create();

        $exporter = new DebtorReceivablesCsvExporter();
        $exporter->export($report, new CsvFile($file));

        $this->assertMatchesSnapshot($file->read(), new CsvSnapshotDriver());
    }

    public function getCasesForExport(): array
    {
        return [
            'empty report'      => [new DebtorReceivablesReport([])],
            'single collection' => [
                new DebtorReceivablesReport(
                    [
                        new CollectionSummary(
                            Date::of('2019-01-01'),
                            Date::of('2019-01-14'),
                            Journal::of('100'),
                            TAccount::of('1000'),
                            TAccount::of('2000'),
                            CustomerNumber::fromCustomerId(1),
                            'Collection',
                            PaymentMethod::of('Partial Payment'),
                            CostUnitCode::of('1020'),
                            Currency::EUR()->amount(10)
                        ),
                    ]
                ),
            ],
            'refund' => [
                new DebtorReceivablesReport(
                    [
                        new CollectionSummary(
                            Date::of('2019-01-01'),
                            Date::of('2019-01-14'),
                            Journal::of('100'),
                            TAccount::of('1000'),
                            TAccount::of('2000'),
                            CustomerNumber::fromCustomerId(1),
                            'Refund',
                            PaymentMethod::of('Partial Payment'),
                            CostUnitCode::of('1020'),
                            Currency::EUR()->amount(-10)
                        ),
                    ]
                ),
            ],
        ];
    }
}
