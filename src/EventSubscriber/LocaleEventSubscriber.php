<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\EventSubscriber;

use App\Manager\LocaleManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @package App\EventSubscriber
 */
class LocaleEventSubscriber implements EventSubscriberInterface
{
    /** @var string */
    private const LOCALE_ATTRIBUTE_NAME = '_locale';

    /** @var LocaleManager */
    private $localeManager;

    public function __construct(LocaleManager $localeManager)
    {
        $this->localeManager = $localeManager;
    }

    public function onKernelRequest(GetResponseEvent $event): void
    {
        $isLocaleSwitched = $event->isMasterRequest() && $this->switchLocaleIfChanged($event->getRequest());
        if (!$isLocaleSwitched && $event->getRequest()->hasPreviousSession()) {
            $this->saveLocaleInSession($event->getRequest());
        }
    }

    private function switchLocaleIfChanged(Request $request): bool
    {
        $isLocaleSwitched = false;
        $locale = $this->getLocaleFromRequest($request);
        if (null !== $locale && null !== $request->getSession()) {
            $request->setLocale($locale);
            $request->getSession()->set(self::LOCALE_ATTRIBUTE_NAME, $locale);
            $request->attributes->set(self::LOCALE_ATTRIBUTE_NAME, $locale);
            $isLocaleSwitched = true;
        }

        return $isLocaleSwitched;
    }

    private function saveLocaleInSession(Request $request): void
    {
        if (null !== $request->getSession() && $request->attributes->has(self::LOCALE_ATTRIBUTE_NAME)) {
            $request->getSession()->set(
                self::LOCALE_ATTRIBUTE_NAME,
                $request->attributes->get(self::LOCALE_ATTRIBUTE_NAME)
            );
            $this->setLocale($request->attributes->get(self::LOCALE_ATTRIBUTE_NAME));
        } else {
            $locale = $request->getSession()->get(
                self::LOCALE_ATTRIBUTE_NAME,
                $this->localeManager->getDefaultLocale()
            );
            $this->setLocale($locale);
            $request->setLocale($locale);
        }
    }

    private function getLocaleFromRequest(Request $request): ?string
    {
        return $this->getLocaleFromRequestParams($request->request->all());
    }

    private function getLocaleFromRequestParams(array $params): ?string
    {
        foreach ($params as $key => $value) {
            if (\is_array($value)) {
                $locale = $this->getLocaleFromRequestParams($value);
                if (null !== $locale) {
                    return $locale;
                }
            }
            if (self::LOCALE_ATTRIBUTE_NAME === $key) {
                return \is_string($value) ? $value : null;
            }
        }

        return null;
    }

    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::REQUEST => ['onKernelRequest', 20]];
    }

    /**
     * @param string $languageCode
     * @throws \LogicException
     * @return void
     */
    public function setLocale(string $languageCode): void
    {
        \setlocale(LC_ALL, $this->localeManager->getLocaleForLanguageCode($languageCode));
    }
}
