<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\EventSubscriber;

use App\Entity\FakeUser;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @package App\EventSubscriber
 */
class RequestEventSubscriber implements EventSubscriberInterface
{
    public const LOCALE = '_zf_locale';
    public const MODULE = '_zf_module';
    public const CONTROLLER = '_zf_controller';
    public const ACTION = '_zf_action';
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest(GetResponseEvent $event): void
    {
        $user = new \Symfony\Component\Security\Core\User\User('fake', 'test', ['ROLE_ADMINISTRATOR']);
//        $this->tokenStorage->setToken(new UsernamePasswordToken($user, null, 'fake', ['ROLE_ADMINISTRATOR']));

        $path = $event->getRequest()->getPathInfo();
        $parts = \explode('/', \trim($path, '/'));
        if (\count($parts) >= 3) {
            if (!\array_key_exists(3, $parts)) {
                $parts[] = 'index';
            }
            $event->getRequest()->attributes->set(self::LOCALE, $parts[0]);
            $event->getRequest()->attributes->set(self::MODULE, $parts[1]);
            $event->getRequest()->attributes->set(self::CONTROLLER, $parts[2]);
            $event->getRequest()->attributes->set(self::ACTION, $parts[3]);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::REQUEST => 'onKernelRequest'];
    }
}
