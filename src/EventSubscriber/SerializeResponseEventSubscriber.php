<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\EventSubscriber;

use App\HttpFoundation\Response\SerializeResponse;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @package App\EventListener
 */
class SerializeResponseEventSubscriber implements EventSubscriberInterface
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::RESPONSE => 'onKernelResponse'];
    }

    /**
     * @param FilterResponseEvent $event
     * @throws \UnexpectedValueException
     * @return void
     */
    public function onKernelResponse(FilterResponseEvent $event): void
    {
        $response = $event->getResponse();
        if (!$response instanceof SerializeResponse) {
            return;
        }

        $content = $this->serializer->serialize($response->getData(), 'json');
        $response->setContent($content);
        $response->headers->set('Content-Type', $event->getRequest()->getMimeType('json'));
    }
}
