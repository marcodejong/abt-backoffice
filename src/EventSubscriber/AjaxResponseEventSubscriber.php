<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Translation\Exception\InvalidArgumentException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @package App\EventListener
 */
class AjaxResponseEventSubscriber implements EventSubscriberInterface
{
    private const EXCEPTION_MAP = [
        AccessDeniedException::class => [
            'key' => 'notAuthorized',
            'messageId' => 'general.access-denied',
            'responseStatusCode' => Response::HTTP_FORBIDDEN,
        ],
        AuthenticationException::class => [
            'key' => 'notAuthenticated',
            'messageId' => 'general.not-authenticated',
            'responseStatusCode' => Response::HTTP_UNAUTHORIZED,
        ],
    ];

    /** @var TranslatorInterface */
    private $translator;

    /** @var null|\Exception */
    private $exception;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 1000],
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        if (!$event->getRequest()->isXmlHttpRequest()) {
            return;
        }

        foreach (array_keys(self::EXCEPTION_MAP) as $class) {
            if ($event->getException() instanceof $class) {
                $this->exception = $event->getException();
                break;
            }
        }
    }

    /**
     * @param FilterResponseEvent $event
     * @throws InvalidArgumentException when TranslatorInterface::trans throws it
     * @return void
     */
    public function onKernelResponse(FilterResponseEvent $event): void
    {
        if (!$event->getRequest()->isXmlHttpRequest()) {
            return;
        }

        $originalResponse = $event->getResponse();
        $resultResponse = null;
        if ($this->exception && $data = $this->getDataForException($this->exception)) {
            $responseData = [
                'success' => false,
                'errors' => [
                    $data['key'] => $this->translator->trans($data['messageId']),
                ],
            ];

            if ($originalResponse instanceof RedirectResponse) {
                $responseData['redirectUrl'] = $originalResponse->getTargetUrl();
            }

            $resultResponse = new JsonResponse($responseData, $data['responseStatusCode']);
            $this->exception = null;
        }

        if ($resultResponse) {
            $event->setResponse($resultResponse);
        }
    }

    private function getDataForException(\Exception $exception): ?array
    {
        foreach (self::EXCEPTION_MAP as $class => $data) {
            if ($exception instanceof $class) {
                return $data;
            }
        }

        return null;
    }
}
