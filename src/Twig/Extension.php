<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Twig;

use App\EventSubscriber\RequestEventSubscriber;
use App\Manager\MenuManager;
use App\VersionResolver;
use Arriva\Assist\Entity\Menu;
use Arriva\Assist\Value\EngravedId;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\Exception\InvalidArgumentException;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

/**
 * @package App\Twig
 */
class Extension extends AbstractExtension
{
    /** @var RequestStack */
    private $requestStack;

    /** @var VersionResolver */
    private $versionResolver;

    /** @var MenuManager */
    private $menuManager;

    /** @var TranslatorInterface */
    private $translator;

    public function __construct(
        RequestStack $requestStack,
        MenuManager $menuManager,
        VersionResolver $versionResolver,
        TranslatorInterface $translator
    ) {
        $this->requestStack = $requestStack;
        $this->menuManager = $menuManager;
        $this->versionResolver = $versionResolver;
        $this->translator = $translator;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pageTitle', [$this, 'getPageTitle']),
            new TwigFunction('version', [$this, 'getVersion']),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('engravedId', [$this, 'filterEngravedId']),
            new TwigFilter('currency', [$this, 'filterCurrency']),
            new TwigFilter('environment', [$this, 'formatEnvironment'])
        ];
    }

    public function getTests()
    {
        return [
            new TwigTest('instanceof', [$this, 'isInstanceof'])
        ];
    }

    public function isInstanceof($var, string $instance): bool
    {
        return $var instanceof $instance;
    }

    /**
     * @param string|EngravedId $engravedId
     * @param int $size
     * @param string $separator
     * @return string
     */
    public function filterEngravedId($engravedId, int $size = 4, $separator = '.'): string
    {
        if ($engravedId instanceof EngravedId) {
            $engravedId = $engravedId->getEngravedIdString();
        }

        return \implode($separator, \str_split($engravedId, $size));
    }

    public function filterCurrency($value, string $currency = 'EUR'): string
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request) {
            return (string)$value;
        }

        if (false === \is_float($value)) {
            $value = (float)$value;
        }

        return (new \NumberFormatter($request->getLocale(), \NumberFormatter::CURRENCY))->formatCurrency(
            $value,
            $currency
        );
    }

    public function formatEnvironment(string $environment): string
    {
        switch($environment) {
            case 'dev':
            case 'development':
                return 'development';
            case 'test':
                return 'test';
            case 'accept':
            case 'acc':
                return 'accept';
            case 'production':
            case 'prod':
                return 'production';
            case 'ci':
                return 'ci';
            default:
                return '';
        }
    }

    /**
     * @throws InvalidArgumentException
     * @return string
     */
    public function getPageTitle(): string
    {
        $pageTitle = null;
        $request = $this->requestStack->getCurrentRequest();
        if ($request && $request->attributes->has(RequestEventSubscriber::ACTION)) {
            $menuItem = $this->menuManager->findOneByModuleControllerAction(
                $request->attributes->get(RequestEventSubscriber::MODULE),
                $request->attributes->get(RequestEventSubscriber::CONTROLLER),
                $request->attributes->get(RequestEventSubscriber::ACTION)
            );
            if ($menuItem instanceof Menu) {
                $pageTitle = $menuItem->getTitle();
            }
        }

        return null !== $pageTitle ? $this->translator->trans($pageTitle) : '';
    }

    public function getVersion(): string
    {
        return $this->versionResolver->getVersion();
    }
}
