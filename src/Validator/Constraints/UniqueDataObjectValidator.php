<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use App\Form\DataObject\DataObjectStateInterface;
use App\Manager\FindByManager;
use App\Util\IterableFunctions;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @package App\Validator\Constraints
 */
class UniqueDataObjectValidator extends ConstraintValidator
{
    /** @var FindByManager */
    private $findByManager;

    /** @var PropertyAccessorInterface */
    private $propertyAccessor;

    public function __construct(FindByManager $findByManager, PropertyAccessorInterface $propertyAccessor)
    {
        $this->findByManager = $findByManager;
        $this->propertyAccessor = $propertyAccessor;
    }

    public function validate($dataObject, Constraint $constraint): void
    {
        if (!$constraint instanceof UniqueDataObject) {
            throw new UnexpectedTypeException($constraint, UniqueDataObject::class);
        }

        if (!\is_string($constraint->identifierField) || empty($constraint->identifierField)) {
            throw new UnexpectedTypeException($constraint->identifierField, 'not empty string');
        }

        if (!\is_array($constraint->fields) && !\is_string($constraint->fields)) {
            throw new UnexpectedTypeException($constraint->fields, 'array');
        }

        if (null !== $constraint->errorPath && !\is_string($constraint->errorPath)) {
            throw new UnexpectedTypeException($constraint->errorPath, 'string or null');
        }

        $fields = (array)$constraint->fields;

        if (0 === \count($fields)) {
            throw new ConstraintDefinitionException('At least one field has to be specified.');
        }

        if (null === $dataObject) {
            return;
        }

        if (!$dataObject instanceof DataObjectStateInterface) {
            throw new \LogicException(
                \sprintf(
                    'Only objects which implement: %s are allowed for this validator %s',
                    DataObjectStateInterface::class,
                    __CLASS__
                )
            );
        }

        $criteria = new Criteria();
        $hasNullValue = false;

        if (!\property_exists($dataObject, $constraint->identifierField)) {
            throw new ConstraintDefinitionException(
                \sprintf(
                    'The field "%s" is not available so a unique record can not be identified in case of a modified object',
                    $constraint->identifierField
                )
            );
        }

        foreach ($fields as $dataObjectPropertyName => $entityPropertyName) {
            if (is_int($dataObjectPropertyName)) {
                // if a key is not explicitly provided, use the value as key
                $dataObjectPropertyName = $entityPropertyName;
            }
            if (!$this->propertyAccessor->isReadable($dataObject, $dataObjectPropertyName)) {
                throw new ConstraintDefinitionException(
                    \sprintf(
                        'The property "%s" is not available, so it cannot be validated for uniqueness.',
                        $dataObjectPropertyName
                    )
                );
            }
            $fieldValue = $this->propertyAccessor->getValue($dataObject, $dataObjectPropertyName);

            if (null === $fieldValue) {
                $hasNullValue = true;
            }

            if ($constraint->ignoreNull && null === $fieldValue) {
                continue;
            }

            $criteria->andWhere(Criteria::expr()->eq($entityPropertyName, $fieldValue));
        }

        // validation doesn't fail if one of the fields is null and if null values should be ignored
        if ($hasNullValue && $constraint->ignoreNull) {
            return;
        }

        // skip validation if there are no criteria (this can happen when the
        // "ignoreNull" option is enabled and fields to be checked are null
        if (empty($criteria)) {
            return;
        }

        $results = IterableFunctions::iterable_to_array(
            $this->findByManager->findBy($constraint->entityClass, $criteria)
        );


        $numberOfResult = \count($results);

        if (0 === $numberOfResult) {
            return;
        }

        if ($numberOfResult === 1 && $dataObject->isModifiedObject()) {
            $modifiedObjectId = $this->propertyAccessor->getValue($dataObject, $constraint->identifierField);
            $objectToQuery = \reset($results);
            $entityIdentifierField = $constraint->identifierFieldEntity ?? $constraint->identifierField;
            $currentObjectId = $this->propertyAccessor->getValue($objectToQuery, $entityIdentifierField);
            if ($modifiedObjectId === $currentObjectId) {
                return;
            }
        }

        $this->context->buildViolation($constraint->message)
            ->atPath($constraint->errorPath ?? $fields[0])
            ->setCode(UniqueDataObject::IS_NOT_UNIQUE_ERROR)
            ->addViolation();
    }
}
