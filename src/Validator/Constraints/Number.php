<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @package App\Validator\Constraints
 */
class Number extends Constraint
{
    public $message = 'form.validator.notanumber';
}
