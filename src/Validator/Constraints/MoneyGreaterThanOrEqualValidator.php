<?php

namespace App\Validator\Constraints;

use Arriva\Abt\Utility\Money;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqualValidator;

class MoneyGreaterThanOrEqualValidator extends GreaterThanOrEqualValidator
{
    /**
     * {@inheritdoc}
     */
    protected function compareValues($value1, $value2)
    {
        return ($value1 instanceof Money ? $value1->getAmount() : $value1)
            >= ($value2 instanceof Money ? $value2->getAmount() : $value2);
    }
}
