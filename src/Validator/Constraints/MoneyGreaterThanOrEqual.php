<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class MoneyGreaterThanOrEqual extends GreaterThanOrEqual
{
}
