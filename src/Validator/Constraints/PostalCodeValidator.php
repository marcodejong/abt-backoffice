<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use App\Service\Postcode\RestPostcodeService;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @package App\Validator\Constraints
 */
class PostalCodeValidator extends ConstraintValidator
{
    /** @var PropertyAccessorInterface */
    private $propertyAccessor;

    /** @var RestPostcodeService */
    private $postcodeService;

    public function __construct(PropertyAccessorInterface $propertyAccessor, RestPostcodeService $postcodeService)
    {
        $this->propertyAccessor = $propertyAccessor;
        $this->postcodeService = $postcodeService;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PostalCode) {
            throw new UnexpectedTypeException($constraint, PostalCode::class);
        }

        $postalCodeValue = $this->propertyAccessor->getValue($value, $constraint->postalCodeField);
        if (empty($postalCodeValue)) {
            return;
        }

        $countryValue = $this->propertyAccessor->getValue($value, $constraint->countryField);
        if (!\in_array($countryValue, PostalCode::SUPPORTED_COUNTRY_CODES, false)) {
            $this->context
                ->buildViolation($constraint->canNotValidatePostalCodeMessage)
                ->setCode(PostalCode::NO_COUNTRY_ERROR)
                ->atPath($constraint->postalCodeField)
                ->addViolation();

            return;
        }

        $pattern = PostalCode::REGEX_PATTERNS[$countryValue];
        $violations = $this->context->getValidator()->validate($postalCodeValue, new Regex(['pattern' => $pattern]));
        if ($violations->count()) {
            $this->context
                ->buildViolation($constraint->invalidMessage)
                ->setCode(PostalCode::IS_INVALID_ERROR)
                ->setParameter('{{ example }}', PostalCode::PATTERN_EXAMPLES[$countryValue])
                ->atPath($constraint->postalCodeField)
                ->addViolation();

            return;
        }

        if (null !== $constraint->houseNumberField && PostalCode::COUNTRY_CODE_NLD === $countryValue) {
            $houseNumberValue = $this->propertyAccessor->getValue($value, $constraint->houseNumberField);
            if (null !== $houseNumberValue) {
                $houseNumberExtensionValue = $constraint->houseNumberExtensionField
                    ? $this->propertyAccessor->getValue($value, $constraint->houseNumberExtensionField)
                    : null;
                $addressInfo = $this->postcodeService->getAddressInfoByPostcode(
                    $postalCodeValue,
                    $houseNumberValue,
                    $houseNumberExtensionValue
                );

                if (null === $addressInfo) {
                    $this->context
                        ->buildViolation($constraint->invalidLookupMessage)
                        ->setParameter('{{ houseNumber }}', $houseNumberValue)
                        ->setParameter('{{ houseNumberExtension }}', $houseNumberExtensionValue)
                        ->setParameter('{{ postalCode }}', $postalCodeValue)
                        ->atPath($constraint->postalCodeField)
                        ->addViolation();
                }
            }
        }
    }
}
