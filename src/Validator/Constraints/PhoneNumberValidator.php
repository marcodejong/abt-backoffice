<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @package App\Validator\Constraints
 */
class PhoneNumberValidator extends ConstraintValidator
{

    /** @var PhoneNumberUtil */
    private $phoneNumberUtil;

    /** @var string */
    private $defaultRegion = "NL";

    /** @var string[]|array */
    private $allowedRegions = [
        'NL',
        'BE',
        'DE',
    ];

    /**
     * PhoneNumberValidator constructor.
     * @param PhoneNumberUtil $phoneNumberUtil
     * @param string $defaultRegion
     * @param array|string[] $allowedRegions
     */
    public function __construct(PhoneNumberUtil $phoneNumberUtil, string $defaultRegion = "NL", $allowedRegions = null)
    {
        $this->phoneNumberUtil = $phoneNumberUtil;
        $this->defaultRegion = $defaultRegion;
        if ($allowedRegions !== null) {
            $this->allowedRegions = $allowedRegions;
        }
    }


    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PhoneNumber) {
            throw new UnexpectedTypeException($constraint, PhoneNumber::class);
        }

        if (empty($value)) {
            return;
        }

        try {
            $phoneNumberProto = $this->phoneNumberUtil->parse(
                $value,
                $this->defaultRegion
            );

            $isValidNumber = $this->phoneNumberUtil->isValidNumber($phoneNumberProto);

            $regionCode = $this->phoneNumberUtil->getRegionCodeForNumber($phoneNumberProto);

            if (empty($regionCode)
                || !\in_array($regionCode, $this->allowedRegions)
            ) {
                // number is not in region white list
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(PhoneNumber::IS_INVALID_ERROR)
                    ->addViolation();
            } elseif (!$isValidNumber) {
                // number is invalid
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(PhoneNumber::IS_INVALID_ERROR)
                    ->addViolation();
            }
        } catch (NumberParseException $e) {
            // Assume number is invalid if it cannot be parsed
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(PhoneNumber::IS_INVALID_ERROR)
                ->addViolation();
        }
    }
}
