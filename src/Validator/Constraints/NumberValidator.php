<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @package App\Validator\Constraints
 */
class NumberValidator extends ConstraintValidator
{
    private const PATTERN = '/^[\d]+$/';

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof Number) {
            throw new UnexpectedTypeException($constraint, Number::class);
        }

        if (empty($value)) {
            return;
        }

        $this->context->getValidator()->inContext($this->context)->validate(
            $value,
            new Regex(
                [
                    'pattern' => self::PATTERN,
                    'message' => $constraint->message,
                ]
            )
        );
    }
}
