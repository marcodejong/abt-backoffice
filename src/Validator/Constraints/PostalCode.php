<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @package App\Validator\Constraints
 */
class PostalCode extends Constraint
{

    const NO_COUNTRY_ERROR = 'fb628b2f-4004-49e9-8510-098edb611bfc';
    const IS_INVALID_ERROR = '0ceaf91e-b61b-4ace-8f9c-a1e337aa798e';

    public const COUNTRY_CODE_NLD = 'NLD';
    public const COUNTRY_CODE_BEL = 'BEL';
    public const COUNTRY_CODE_DEU = 'DEU';

    public const REGEX_PATTERNS = [
        self::COUNTRY_CODE_NLD => '/^[0-9]{4}[A-Z]{2}$/',
        self::COUNTRY_CODE_BEL => '/^[1-9]{1}[0-9]{3}$/',
        self::COUNTRY_CODE_DEU => '/^[0-9]{4,5}$/',
    ];

    public const PATTERN_EXAMPLES = [
        self::COUNTRY_CODE_NLD => '9999AA',
        self::COUNTRY_CODE_BEL => '1234',
        self::COUNTRY_CODE_DEU => '12345',
    ];

    public const SUPPORTED_COUNTRY_CODES = [self::COUNTRY_CODE_NLD, self::COUNTRY_CODE_BEL, self::COUNTRY_CODE_DEU];

    public $canNotValidatePostalCodeMessage = 'Can not validate postal code because the country is not known';
    public $invalidMessage = 'Please enter a valid postcode ({{ example }})';
    public $invalidLookupMessage = 'Invalid address: {{ houseNumber }}{{ houseNumberExtension }} {{ postalCode }}';

    public $postalCodeField;

    public $countryField;

    public $houseNumberField;

    public $houseNumberExtensionField;

    public function getRequiredOptions(): array
    {
        return ['countryField', 'postalCodeField'];
    }

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    public function getDefaultOption(): string
    {
        return 'postalCodeField';
    }
}
