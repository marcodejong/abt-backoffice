<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class ValidPropositionRefund extends Constraint
{
    public $message = 'form.validator.proposition.refund-value-without-period';

    public $errorPath = 'refundPeriod';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}