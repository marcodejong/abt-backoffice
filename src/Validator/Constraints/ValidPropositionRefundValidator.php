<?php


namespace App\Validator\Constraints;


use App\Form\DataObject\PropositionDataObject;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ValidPropositionRefundValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ValidPropositionRefund) {
            throw new \LogicException(\sprintf(
                'Constraint with class %s expected; %s given',
                ValidPropositionRefund::class,
                get_class($constraint)
            ));
        }

        if (!$value instanceof PropositionDataObject) {
            throw new \LogicException(
                \sprintf(
                    'Only objects with class %s are allowed; %s given',
                    PropositionDataObject::class,
                    get_class($value)
                )
            );
        }

        if ($value->refundPeriodValue && !$value->refundPeriod) {
            $this->context->buildViolation($constraint->message)
                ->atPath($constraint->errorPath)
                ->addViolation();
        }
    }


}