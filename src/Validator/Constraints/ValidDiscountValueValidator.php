<?php


namespace App\Validator\Constraints;


use App\Form\DataObject\DiscountDataObject;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ValidDiscountValueValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ValidDiscountValue) {
            throw new \LogicException(\sprintf(
                'Constraint with class %s expected; %s given',
                ValidDiscountValue::class,
                get_class($constraint)
            ));
        }

        if (!$value instanceof DiscountDataObject) {
            throw new \LogicException(
                \sprintf(
                    'Only objects with class %s are allowed; %s given',
                    DiscountDataObject::class,
                    get_class($value)
                )
            );
        }

        if ($value->percentage && $value->amount > 100) {
            $this->context->buildViolation($constraint->message)
                ->atPath($constraint->errorPath)
                ->addViolation();
        }
    }
}