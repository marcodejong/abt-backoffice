<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class StopPropositionSubscription extends Constraint
{
    const ERROR_CODE_END_DATE_TOO_LATE = 'a95adc1a-915e-4903-b426-8106249f5245';
    const ERROR_CODE_SUBSCRIPTION_HAS_REFUND = '840e6e36-42d8-4f61-b431-e281dcde9c54';
    const ERROR_CODE_SUBSCRIPTION_ALREADY_EXPIRED = '5fcf5036-ff0e-4409-9a6f-056db6bf7386';

    const ERROR_MESSAGE_END_DATE_TOO_LATE = 'The new end date must be before the current subscription end date.';
    const ERROR_MESSAGE_SUBSCRIPTION_HAS_REFUND = 'The subscription has already been refunded.';
    const ERROR_MESSAGE_SUBSCRIPTION_ALREADY_EXPIRED = 'The subscription has already expired.';

    public $errorPath = 'endDate';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}