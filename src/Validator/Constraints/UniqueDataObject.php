<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @package App\Validator\Constraints
 */
class UniqueDataObject extends Constraint
{
    const IS_NOT_UNIQUE_ERROR = 'ab087187-e0de-451e-bab7-97b5bfb0bb20';

    protected static $errorNames = [
        self::IS_NOT_UNIQUE_ERROR => 'IS_NOT_UNIQUE_ERROR',
    ];

    public $message = 'form.validator.data-object-not-unique';
    public $entityClass;
    public $fields = [];
    public $identifierField;
    public $identifierFieldEntity;
    public $errorPath;
    public $ignoreNull = true;

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    public function getRequiredOptions(): array
    {
        return ['identifierField', 'fields', 'entityClass'];
    }
}
