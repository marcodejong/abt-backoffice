<?php


namespace App\Validator\Constraints;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class PobOrStreetAddressNotBlankValidator extends ConstraintValidator
{

    /** @var PropertyAccessorInterface */
    private $propertyAccessor;

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof PobOrStreetAddressNotBlank) {
            throw new UnexpectedTypeException($constraint, PobOrStreetAddressNotBlank::class);
        }

        $postOfficeBoxValue = $this->propertyAccessor->getValue($value, $constraint->postOfficeBoxProperty);
        $streetValue = $this->propertyAccessor->getValue($value, $constraint->streetProperty);
        $houseNumberValue = $this->propertyAccessor->getValue($value, $constraint->houseNumberProperty);

        $postOfficeBoxViolations = $this->context->getValidator()->validate($postOfficeBoxValue, new NotBlank());
        if ($postOfficeBoxViolations->count() > 0) {
            // if the postOfficeBox is blank, both the street and the housenumber should not be blank:
            $streetViolations = $this->context->getValidator()->validate($streetValue, new NotBlank());
            $houseNumberViolations = $this->context->getValidator()->validate($houseNumberValue, new NotBlank());
            if ($streetViolations->count() > 0 || $houseNumberViolations->count() > 0) {
                $this->context
                    ->buildViolation($constraint->message)
                    ->setCode(PobOrStreetAddressNotBlank::IS_INVALID_ERROR)
                    ->setParameters(
                        [
                            '{{ postOfficeBoxProperty }}' => $constraint->postOfficeBoxProperty,
                            '{{ streetProperty }}' => $constraint->streetProperty,
                            '{{ houseNumberProperty }}' => $constraint->houseNumberProperty,
                        ]
                    )
                    ->addViolation();
            }
        }
    }
}
