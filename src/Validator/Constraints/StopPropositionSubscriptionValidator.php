<?php

namespace App\Validator\Constraints;

use App\Form\DataObject\StopPropositionSubscriptionDataObject;
use Arriva\Abt\Value\Date;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class StopPropositionSubscriptionValidator extends ConstraintValidator
{
    public function validate($dataObject, Constraint $constraint)
    {
        if (!$constraint instanceof StopPropositionSubscription) {
            throw new \LogicException(\sprintf(
                'Constraint with class %s expected; %s given',
                StopPropositionSubscription::class,
                get_class($constraint)
            ));
        }

        if (!$dataObject instanceof StopPropositionSubscriptionDataObject) {
            throw new \LogicException(
                \sprintf(
                    'Only objects with class %s are allowed; %s given',
                    StopPropositionSubscriptionDataObject::class,
                    get_class($dataObject)
                )
            );
        }

        if ($dataObject->subscription->getRefund()) {
            $this->context->buildViolation(StopPropositionSubscription::ERROR_MESSAGE_SUBSCRIPTION_HAS_REFUND)
                ->atPath($constraint->errorPath)
                ->setCode(StopPropositionSubscription::ERROR_CODE_SUBSCRIPTION_HAS_REFUND)
                ->addViolation();
        }

        if (Date::fromDateTime(new \DateTimeImmutable()) >= $dataObject->subscription->getExpiryDate()) {
            $this->context->buildViolation(StopPropositionSubscription::ERROR_MESSAGE_SUBSCRIPTION_ALREADY_EXPIRED)
                ->atPath($constraint->errorPath)
                ->setCode(StopPropositionSubscription::ERROR_CODE_SUBSCRIPTION_ALREADY_EXPIRED)
                ->addViolation();
        }

        if (Date::fromDateTime($dataObject->endDate) >= $dataObject->subscription->getExpiryDate()) {
            $this->context->buildViolation(StopPropositionSubscription::ERROR_MESSAGE_END_DATE_TOO_LATE)
                ->atPath($constraint->errorPath)
                ->setCode(StopPropositionSubscription::ERROR_CODE_END_DATE_TOO_LATE)
                ->addViolation();
        }
    }
}