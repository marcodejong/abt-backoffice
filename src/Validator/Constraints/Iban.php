<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @package App\Validator\Constraints
 */
class Iban extends Constraint
{
    const IS_INVALID_ERROR = '6ed80616-da44-48f7-abf9-be4ef25c4ca3';

    protected static $errorNames = [
        self::IS_INVALID_ERROR => 'IS_INVALID_ERROR',
    ];

    public $message = 'This value should be a valid IBAN number';
}
