<?php


namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class PobOrStreetAddressNotBlank
 * A constraint for when the Post Office Box OR the street+house number must be not blank
 * @package App\Validator\Constraints
 */
class PobOrStreetAddressNotBlank extends Constraint
{
    const IS_INVALID_ERROR = '276fe750-0fb0-4030-9a8d-49d31b00294f';

    protected static $errorNames = [
        self::IS_INVALID_ERROR => 'IS_INVALID_ERROR',
    ];

    public $message = 'The field {{ postOfficeBoxProperty }} or both {{ streetProperty }} and {{ houseNumberProperty }} should not be blank.';

    public $postOfficeBoxProperty = '';

    public $streetProperty = '';

    public $houseNumberProperty = '';

    public function getRequiredOptions(): array
    {
        return ['postOfficeBoxProperty', 'streetProperty', 'houseNumberProperty'];
    }

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
