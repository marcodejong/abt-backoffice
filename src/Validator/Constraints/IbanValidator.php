<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @package App\Validator\Constraints
 */
class IbanValidator extends ConstraintValidator
{
    /** @var \IBAN\Validation\IBANValidator */
    private $ibanValidator;

    /**
     * IbanValidator constructor.
     * @param \IBAN\Validation\IBANValidator $ibanValidator
     */
    public function __construct(\IBAN\Validation\IBANValidator $ibanValidator)
    {
        $this->ibanValidator = $ibanValidator;
    }


    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof Iban) {
            throw new UnexpectedTypeException($constraint, Iban::class);
        }

        if (empty($value)) {
            return;
        }

        if (!$this->ibanValidator->validate($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(Iban::IS_INVALID_ERROR)
                ->addViolation();
        }
    }
}
