<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @package App\Validator\Constraints
 */
class PhoneNumber extends Constraint
{
    const IS_INVALID_ERROR = 'db0f156c-4f8d-40ff-9f88-2b8a93bfafb8';

    protected static $errorNames = [
        self::IS_INVALID_ERROR => 'IS_INVALID_ERROR',
    ];

    public $message = 'This value should be a valid phone number';
}
