<?php


namespace App\Validator\Constraints;


use App\Form\DataObject\PropositionTermDataObject;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ValidPropositionTermTypeValidator extends ConstraintValidator
{
    public function validate($dataObject, Constraint $constraint)
    {
        if (!$constraint instanceof ValidPropositionTermType) {
            throw new \LogicException(\sprintf(
                'Constraint with class %s expected; %s given',
                ValidPropositionTermType::class,
                get_class($constraint)
            ));
        }

        if (!$dataObject instanceof PropositionTermDataObject) {
            throw new \LogicException(
                \sprintf(
                    'Only objects with class %s are allowed; %s given',
                    PropositionTermDataObject::class,
                    get_class($dataObject)
                )
            );
        }

        $isDurationTerm = $dataObject->years || $dataObject->months || $dataObject->weeks || $dataObject->days;

        $isFixedEndDateTerm = !empty($dataObject->endDate);

        if ($isDurationTerm && $isFixedEndDateTerm) {
            $this->context->buildViolation($constraint->wrongCombinationMessage)
                ->atPath($constraint->errorPath)
                ->setCode(ValidPropositionTermType::IS_NOT_VALID_TYPE)
                ->addViolation();
        }

        if (!$isDurationTerm && !$isFixedEndDateTerm) {
            $this->context->buildViolation($constraint->noTypeMessage)
                ->atPath($constraint->errorPath)
                ->setCode(ValidPropositionTermType::IS_NOT_VALID_TYPE)
                ->addViolation();
        }
    }
}