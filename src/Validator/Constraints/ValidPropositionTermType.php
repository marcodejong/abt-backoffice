<?php


namespace App\Validator\Constraints;



use Symfony\Component\Validator\Constraint;

class ValidPropositionTermType extends Constraint
{
    const IS_NOT_VALID_TYPE = 'de6cfc1c-9d02-4932-bde9-8afe78fc1fe4';

    public $wrongCombinationMessage = 'form.validator.proposition-term.combination-not-valid';

    public $noTypeMessage = 'form.validator.proposition-term.no-type';

    public $errorPath = 'endDate';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

}