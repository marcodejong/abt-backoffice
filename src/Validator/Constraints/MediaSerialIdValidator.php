<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Validator\Constraints;

use Arriva\Abt\Utility\EncrypterInterface;
use Arriva\Abt\Utility\Exception\EncryptionException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @package App\Validator\Constraints
 */
class MediaSerialIdValidator extends ConstraintValidator
{
    /** @var EncrypterInterface */
    private $encrypter;

    public function __construct(EncrypterInterface $encrypter)
    {
        $this->encrypter = $encrypter;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     * @throws EncryptionException
     * @return void
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof MediaSerialId) {
            throw new UnexpectedTypeException($constraint, MediaSerialId::class);
        }

        if (empty($value)) {
            return;
        }

        $this->context->getValidator()->inContext($this->context)->validate(
            $this->encrypter->decrypt($value),
            new Length(
                [
                    'min' => 0,
                    'max' => 15,
                    'minMessage' => 'form.validator.media_serial_id.not_valid_length.min',
                    'maxMessage' => 'form.validator.media_serial_id.not_valid_length.max',
                    'exactMessage' => 'form.validator.media_serial_id.not_valid_length.exact',
                    'charsetMessage' => 'form.validator.media_serial_id.not_valid_length.charset',
                ]
            )
        );
    }
}
