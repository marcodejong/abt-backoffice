<?php


namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ValidDiscountValue extends Constraint
{
    public $message = 'form.validator.discount.productdiscountpercentnotvalid';

    public $errorPath = 'amount';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}