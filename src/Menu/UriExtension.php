<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Menu;

use Knp\Menu\Factory\ExtensionInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @package App\Menu
 */
class UriExtension implements ExtensionInterface
{
    /** @var RequestStack */
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function buildOptions(array $options): array
    {
        $request = $this->requestStack->getCurrentRequest();
        $keys = ['module', 'controller', 'action'];
        if ($request && !\array_key_exists('uri', $options) && $this->containsAllKeys($options, $keys)) {
            $request = $this->requestStack->getCurrentRequest();
            $options['uri'] = \sprintf(
                '/%s/%s/%s/%s',
                $request->getLocale(),
                $options['module'],
                $options['controller'],
                $options['action']
            );
        }

        return $options;
    }

    public function buildItem(ItemInterface $item, array $options): void
    {
        // Nothing to do here
    }

    private function containsAllKeys(array $options, array $keys): bool
    {
        foreach ($keys as $key) {
            if (!\array_key_exists($key, $options)) {
                return false;
            }
        }

        return true;
    }
}
