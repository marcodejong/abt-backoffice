<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Menu;

use App\EventSubscriber\RequestEventSubscriber;
use App\Manager\MenuManager;
use Arriva\Abt\Entity\Menu;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

/**
 * @package App\Menu
 */
class Builder
{
    /** @var FactoryInterface */
    private $factory;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var AccessDecisionManagerInterface */
    private $accessDecisionManager;

    /** @var RequestStack */
    private $requestStack;

    /** @var MenuManager */
    private $menuManager;

    public function __construct(
        FactoryInterface $factory,
        TokenStorageInterface $tokenStorage,
        AccessDecisionManagerInterface $accessDecisionManager,
        RequestStack $requestStack,
        MenuManager $menuManager
    ) {
        $this->factory = $factory;
        $this->tokenStorage = $tokenStorage;
        $this->accessDecisionManager = $accessDecisionManager;
        $this->requestStack = $requestStack;
        $this->menuManager = $menuManager;
    }

    /**
     * @throws \InvalidArgumentException
     * @return ItemInterface
     */
    public function createMainMenu(): ItemInterface
    {
        $menuItems = [];
        $request = $this->requestStack->getCurrentRequest();
        if ($request) {
            $location = $request->attributes->get(RequestEventSubscriber::MODULE, 'administration');
            $menuItems = $this->menuManager->findVisibleTopLevelMenuItemsByLocation($location);
        }

        $menu = $this->factory->createItem('root');
        /** @var Menu $menuItem */
        foreach ($menuItems as $menuItem) {
            $this->processMenuItem($menuItem, $menu);
        }

        return $menu;
    }

    /**
     * @param Menu $menuItem
     * @param ItemInterface $menu
     * @throws \InvalidArgumentException
     * @return void
     */
    private function processMenuItem(Menu $menuItem, ItemInterface $menu): void
    {
        if ($this->canShowMenuItem($menuItem)) {

            $options = [
                'label' => $menuItem->getLabel(),
                'module' => $menuItem->getModule(),
                'controller' => $menuItem->getController(),
                'action' => $menuItem->getAction(),
                'linkAttributes' => [
                    'id' => 'applicationmenu-' . $menuItem->getId(),
                    'title' => $menuItem->getTitle(),
                ],
                'extras' => [
                    'icon' => $menuItem->getIcon(),
                ],
            ];

            if (null !== $menuItem->getUri()) {
                $options['uri'] = $menuItem->getUri();
            }

            $parentMenu = $menu->addChild('menu-item-' . $menuItem->getId(), $options);
            foreach ($this->getSortedChildren($menuItem) as $subMenuItem) {
                $this->processMenuItem($subMenuItem, $parentMenu);
            }
        }
    }

    /**
     * @param Menu $menuItem
     * @return Menu[]
     */
    private function getSortedChildren(Menu $menuItem): array
    {
        $values = $menuItem->getChildren()->getValues();
        \usort(
            $values,
            function (Menu $menuItemA, Menu $menuItemB) {
                return $menuItemA->getOrder() <=> $menuItemB->getOrder();
            }
        );

        return $values;
    }

    private function canShowMenuItem(Menu $menuItem): bool
    {
        $token = $this->tokenStorage->getToken();
        if ($token) {
            return $this->accessDecisionManager->decide($token, ['view'], $menuItem);
        }

        return false;
    }
}
