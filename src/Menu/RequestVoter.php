<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Menu;

use App\EventSubscriber\RequestEventSubscriber;
use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @package App\Menu
 */
class RequestVoter implements VoterInterface
{
    /** @var RequestStack */
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function matchItem(ItemInterface $item): ?bool
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request) {
            return null;
        }

        if ($item->getUri() === $request->getPathInfo()) {
            return true;
        }

        if ($request->attributes->has(RequestEventSubscriber::MODULE)) {
            $startUriWith = \sprintf(
                '/%s/%s/%s/',
                $request->attributes->get(RequestEventSubscriber::LOCALE),
                $request->attributes->get(RequestEventSubscriber::MODULE),
                $request->attributes->get(RequestEventSubscriber::CONTROLLER)
            );

            if (0 === \strpos($item->getUri(), $startUriWith)) {
                return true;
            }
        }

        return false;
    }
}
