<?php

namespace App\Exception;

use SolidPhp\ValueObjects\Type\Type;

class UnsupportedSubtypeException extends \RuntimeException
{
    /** @var Type */
    private $type;

    /** @var object */
    private $unsupportedInstance;

    /**
     * @param Type            $type
     * @param object          $unsupportedInstance
     * @param \Exception|null $previous
     */
    public function __construct(Type $type, object $unsupportedInstance, ?\Exception $previous = null)
    {
        $message = sprintf('Unsupported subtype of %s: %s', $type->getShortName(), Type::of($unsupportedInstance)->getShortName());
        $code = 0;

        parent::__construct($message, $code, $previous);

        $this->type = $type;
        $this->unsupportedInstance = $unsupportedInstance;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getUnsupportedInstance(): object
    {
        return $this->unsupportedInstance;
    }
}
