<?php

namespace App\Exception;

class InvalidArgumentTypeException extends \InvalidArgumentException
{
    /** @var string */
    private $method;

    /** @var string */
    private $argumentName;

    /** @var string */
    private $expectedType;

    /** @var mixed */
    private $actualArgument;

    public function __construct(string $method, string $argumentName, string $expectedType, $actualArgument)
    {
        $this->method = $method;
        $this->expectedType = $expectedType;
        $this->actualArgument = $actualArgument;
        $this->argumentName = $argumentName;

        $message = \sprintf(
            'method "%s" expects argument "%s" to be of type "%s", but received type "%s"',
            $method,
            $argumentName,
            $expectedType,
            $this->getType($actualArgument)
        );

        parent::__construct($message);
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getArgumentName(): string
    {
        return $this->argumentName;
    }

    public function getExpectedType(): string
    {
        return $this->expectedType;
    }

    public function getActualArgument()
    {
        return $this->actualArgument;
    }

    public function getActualArgumentType(): string
    {
        return $this->getType($this->actualArgument);
    }

    private function getType($var): string
    {
        if (\is_object($var)) {
            return \get_class($var);
        }

        if (\is_array($var)) {
            return 'array';
        }

        if (\is_string($var)) {
            return 'string';
        }

        if (\is_int($var)) {
            return 'integer';
        }

        if (\is_bool($var)) {
            return 'boolean';
        }

        if (\is_float($var)) {
            return 'double';
        }

        if (\is_nan($var)) {
            return 'NaN';
        }

        if (null === $var) {
            return 'null';
        }

        return 'unknown';
    }
}
