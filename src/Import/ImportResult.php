<?php


namespace App\Import;


class ImportResult
{
    /** @var ItemResultInterface[] */
    private $imports = [];

    /** @var string[] */
    private $messages;

    /**
     * ImportResult constructor.
     * @param ItemResultInterface[] $imports
     * @param string[] $messages
     */
    public function __construct(array $imports, array $messages)
    {
        $this->imports = $imports;
        $this->messages = $messages;
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    public function hasMessages(): bool
    {
        return count($this->messages);
    }

    public function hasFailures(): bool
    {
        return (bool) count(array_filter($this->imports, function (ItemResultInterface $import) {
            return !$import->isSuccessful();
        }));
    }

    /**
     * @return ImportFileResult[]
     */
    public function getSuccessfulFileImports(): array
    {
        return array_filter($this->imports, function (ItemResultInterface $import) {
            return $import instanceof ImportFileResult && $import->isSuccessful();
        });
    }

    /**
     * @return ImportFileResult[]
     */
    public function getFailedFileImports(): array
    {
        return array_filter($this->imports, function (ItemResultInterface $import) {
            return $import instanceof ImportFileResult && !$import->isSuccessful();
        });
    }

    /**
     * @return RecordDeletionResult[]
     */
    public function getSuccessfulRecordDeletions(): array
    {
        return array_filter($this->imports, function (ItemResultInterface $import) {
            return $import instanceof RecordDeletionResult && $import->isSuccessful();
        });
    }

    public function hasFailedRecordDeletions(): bool
    {
        return count($this->getFailedRecordDeletions());
    }

    /**
     * @return RecordDeletionResult[]
     */
    public function getFailedRecordDeletions(): array
    {
        return array_filter($this->imports, function (ItemResultInterface $import) {
            return $import instanceof RecordDeletionResult && !$import->isSuccessful();
        });
    }
}