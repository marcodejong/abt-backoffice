<?php


namespace App\Import;


class ImportLogger
{
    /** @var ItemResultInterface[] */
    private $imports;

    /** @var string[] */
    private $messages;

    /**
     * ImportLogger constructor.
     */
    public function __construct()
    {
        $this->imports = [];
        $this->messages = [];
    }

    public function addResult(ItemResultInterface $result)
    {
        $this->imports[] = $result;
    }

    /**
     * @return ImportResult
     */
    public function getImportResults(): ImportResult
    {
        return new ImportResult($this->imports, $this->messages);
    }

    public function addMessage(string $message): void
    {
        $this->messages[] = $message;
    }
}