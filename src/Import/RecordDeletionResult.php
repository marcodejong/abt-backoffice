<?php

namespace App\Import;

class RecordDeletionResult implements ItemResultInterface
{
    /** @var string|null */
    private $error;

    /** @var string|null */
    private $message;

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(string $error): void
    {
        $this->error = $error;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function isSuccessful(): bool
    {
        return !$this->error;
    }
}
