<?php

namespace App\Import;

interface ItemResultInterface
{
    public function getError(): ?string;

    public function setError(string $error): void;

    public function getMessage(): ?string;

    public function setMessage(string $message): void;

    public function isSuccessful(): bool;
}