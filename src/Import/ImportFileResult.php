<?php


namespace App\Import;


class ImportFileResult implements ItemResultInterface
{
    /** @var \SplFileObject */
    private $file;

    /** @var string|null */
    private $error;

    /** @var string|null */
    private $message;

    /**
     * ImportResult constructor.
     * @param \SplFileObject $file
     */
    public function __construct(\SplFileObject $file)
    {
        $this->file = $file;
    }

    public function getFile(): \SplFileObject
    {
        return $this->file;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(string $error): void
    {
        $this->error = $error;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function isSuccessful(): bool
    {
        return !$this->error;
    }

}