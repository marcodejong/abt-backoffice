<?php


namespace App\Import\AbtTransaction;


use App\Import\ImportFileResult;
use App\Import\RecordDeletionResult;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\FileLocator;

class AbtTransactionImportProcessor
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var FileLocator */
    private $fileLocator;

    /** @var string */
    private $msidEncryptionKey;

    public function __construct(EntityManagerInterface $entityManager, FileLocator $fileLocator, string $msidEncryptionKey)
    {
        $this->entityManager = $entityManager;
        $this->fileLocator = $fileLocator;
        $this->msidEncryptionKey = $msidEncryptionKey;
    }

    public function import(\SplFileObject $file, \DateTimeImmutable $date): ImportFileResult
    {
        $transactionImportSql = str_replace(
            ['%FILE_NAME%', '%MEDIA_SERIAL_ID_ENCRYPTION_KEY%'],
            [$file->getPathname(), $this->msidEncryptionKey],
            file_get_contents(
                $this->fileLocator->locate('Import/AbtTransactions/import_temp_transactions.sql')
            )
        );

        $type = $this->getTypeFromFileName($file);
        $transactionInsertSql = str_replace(
            ['%TYPE%', '%DATE%'],
            [$type, $date->format('Ymd')],
            file_get_contents(
                $this->fileLocator->locate('Import/AbtTransactions/insert_temp_transactions.sql')
            )
        );

        $importResult = new ImportFileResult($file);
        try {
            $conn = $this->entityManager->getConnection();
            $conn->exec($transactionImportSql);
            $numberRowsImported = $conn->exec($transactionInsertSql);
            $importResult->setMessage(sprintf(
                'import successful: %d rows imported',
                $numberRowsImported
            ));
        } catch (DBALException $exception) {
            $importResult->setError($exception->getMessage());
        }

        return $importResult;
    }

    /**
     * @param \DateTimeImmutable $dateTime
     * @return RecordDeletionResult
     * @throws \Exception
     */
    public function removeRecordsOfPreviousDay(\DateTimeImmutable $dateTime): RecordDeletionResult
    {

        $transactionRemoveSql = file_get_contents(
            $this->fileLocator->locate('Import/AbtTransactions/remove_tif_records_by_date.sql')
        );
        $query = str_replace(
            '%DATE%',
            $dateTime->sub(new \DateInterval('P1D'))->format('Y-m-d'),
            $transactionRemoveSql
        );

        $recordDeletionResult = new RecordDeletionResult();
        try {
            $numberOfRecordsDeleted = $this->entityManager->getConnection()->exec($query);
            $recordDeletionResult->setMessage(sprintf('%d TIF records deleted', $numberOfRecordsDeleted));

        } catch (DBALException $exception) {
            $recordDeletionResult->setError($exception->getMessage());
        }

        return $recordDeletionResult;
    }

    private function getTypeFromFileName(\SplFileObject $file): string
    {
        return strpos($file->getFilename(), 'Travel_Settlement_File') ? 'TSF' : 'TIF';
    }
}
