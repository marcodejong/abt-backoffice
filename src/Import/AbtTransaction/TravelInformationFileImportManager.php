<?php


namespace App\Import\AbtTransaction;

class TravelInformationFileImportManager extends AbstractTransactionImportManager
{
    public function process(\DateTimeImmutable $date): void
    {
        $filesToProcess = $this->findApplicableFilesForImport($date);
        foreach ($filesToProcess as $file) {
            $importFileResult = $this->importProcessor->import($file, $date);
            $this->importLogger->addResult($importFileResult);
            $this->moveFileBasedOnResult($importFileResult, $file);
        }
        $this->importLogger->addResult(
            $this->importProcessor->removeRecordsOfPreviousDay($date)
        );
        $this->deleteLocalFiles($filesToProcess);
    }

    protected function supports($file, \DateTimeImmutable $date): bool
    {
        if ($file['type'] === 'file' && $file['extension'] === "csv") {
            $fileName = $file['filename'];

            $fileNameDateRegexPattern = sprintf('/^.*(_%s).*$/', $date->format('Ymd'));
            $fileNamePattern = 'Travel_Information_File';

            if (preg_match($fileNameDateRegexPattern, $fileName) === 1 && strpos($fileName, $fileNamePattern)) {
                return true;
            }
        }

        return false;
    }
}