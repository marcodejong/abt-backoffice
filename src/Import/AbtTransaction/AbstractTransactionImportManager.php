<?php


namespace App\Import\AbtTransaction;

use App\Import\ImportFileResult;
use App\Import\ImportLogger;
use App\Import\ImportResult;
use League\Flysystem\Adapter\Local;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use SolidPhp\ValueObjects\Type\Type;

abstract class AbstractTransactionImportManager
{
    private const IMPORT_SUCCEEDED_DIRECTORY = 'verwerkt/';
    private const IMPORT_FAILED_DIRECTORY = 'gefaald/';

    /** @var AbtTransactionImportProcessor */
    protected $importProcessor;

    /** @var FilesystemInterface */
    protected $localFileSystem;

    /** @var FilesystemInterface */
    protected $remoteFileSystem;

    /** @var ImportLogger */
    protected $importLogger;

    /** @var string */
    protected $pathPrefix;

    public function __construct(
        FilesystemInterface $localFileSystem,
        FilesystemInterface $remoteFileSystem,
        AbtTransactionImportProcessor $importProcessor
    ) {
        $this->localFileSystem = $localFileSystem;
        $this->importProcessor = $importProcessor;
        $this->remoteFileSystem = $remoteFileSystem;
    }

    public function setLogger(): void
    {
        $this->importLogger = new ImportLogger();
    }

    public function setPathPrefix(): void
    {
        $adapter = $this->localFileSystem->getAdapter();
        if (!$adapter instanceof Local) {
            throw new \DomainException(
                sprintf(
                    'adapter of localFileSystem must be instance of %s: instance of %s found',
                    Type::of(Local::class),
                    Type::ofInstance($adapter)
                )
            );
        }

        $this->pathPrefix = $adapter->getPathPrefix();
    }

    public function getImportResult(): ImportResult
    {
        return $this->importLogger->getImportResults();
    }

    /**
     * @param \DateTimeImmutable $date
     * @return \SplFileObject[]
     */
    protected function findApplicableFilesForImport(\DateTimeImmutable $date): array
    {
        $remoteFiles = array_filter($this->remoteFileSystem->listContents(), function ($file) use ($date) {
            return $this->supports($file, $date);
        });

        $localFiles = [];
        foreach ($remoteFiles as $file) {
            $this->copyRemoteFileToLocal($file);
            $localFiles[] = new \SplFileObject($this->pathPrefix.$file['path']);
        }

        return $localFiles;
    }

    abstract protected function supports($file, \DateTimeImmutable $date): bool;

    private function copyRemoteFileToLocal(array $file): void
    {
        try {
            $remote = $this->remoteFileSystem->readStream($file['path']);

            if ($this->localFileSystem->has($file['path'])) {
                $this->localFileSystem->delete($file['path']);
            }

            $this->localFileSystem->writeStream($file['basename'], $remote);
        } catch (\Exception $exception) {
        }
    }

    /**
     * @param \SplFileObject[] $files
     */
    protected function deleteLocalFiles(array $files): void
    {
        foreach ($files as $file) {
            try {
                $this->localFileSystem->delete($file->getFilename());
            } catch (FileNotFoundException $exception) {
            }
        }
    }

    protected function moveFileBasedOnResult(ImportFileResult $result, \SplFileObject $file): void
    {
        $fileName = $file->getFilename();
        try {
            if ($result->isSuccessful()) {
                $this->remoteFileSystem->copy($fileName, self::IMPORT_SUCCEEDED_DIRECTORY.$fileName);
            } else {
                $this->remoteFileSystem->copy($fileName, self::IMPORT_FAILED_DIRECTORY.$fileName);
            }
            $this->remoteFileSystem->delete($fileName);
        } catch (FileNotFoundException | FileExistsException $exception) {
            $result->setMessage($exception->getMessage());
        }
    }
}
