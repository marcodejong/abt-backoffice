<?php


namespace App\Import\AbtTransaction;


class TravelSettlementFileImportManager extends AbstractTransactionImportManager
{
    public function process(\DateTimeImmutable $date): void
    {
        $filesToProcess = $this->findApplicableFilesForImport($date);

        foreach ($filesToProcess as $file) {
            $result = $this->importProcessor->import($file, $date);
            $this->moveFileBasedOnResult($result, $file);
            $this->importLogger->addResult($result);
        }

        $this->deleteLocalFiles($filesToProcess);
    }

    protected function supports($file, \DateTimeImmutable $date): bool
    {
        if ($file['type'] === 'file' && $file['extension'] === "csv") {
            $fileName = $file['filename'];

            $fileNameDateRegexPattern = sprintf('/^.*(_%s).*$/', $date->format('Ymd'));
            $fileNamePattern = 'Travel_Settlement_File';

            if (preg_match($fileNameDateRegexPattern, $fileName) === 1 && strpos($fileName, $fileNamePattern) !== false) {
                return true;
            }
        }

        return false;
    }


}