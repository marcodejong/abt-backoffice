<?php

namespace App\Queue\Handler;

use App\Entity\User;
use App\Queue\Handler\Options\CreateInvoiceHandlerOptions;
use App\Queue\Handler\Options\HandlerOptionsInterface;
use App\Queue\Handler\Result\InvoiceQueueBatchResult;
use App\Queue\Handler\Result\HandlerResultInterface;
use App\Queue\Handler\Result\InvoiceQueueHandlerResult;
use App\Repository\InvoiceRepositoryInterface;
use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Value\InvoiceStatus;
use Exception;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

class InvoiceQueueHandler implements QueueHandlerInterface
{
    /** @var Producer */
    private $queueProducer;

    /** @var SerializerInterface */
    private $serializer;

    /** @var InvoiceRepositoryInterface */
    private $invoiceRepository;

    /**
     * InvoiceHandler constructor.
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param SerializerInterface $serializer
     * @param Producer $queueProducer
     */
    public function __construct(
        InvoiceRepositoryInterface $invoiceRepository,
        SerializerInterface $serializer,
        Producer $queueProducer
    ) {
        $this->queueProducer = $queueProducer;
        $this->serializer = $serializer;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @param HandlerOptionsInterface|CreateInvoiceHandlerOptions $options
     * @return HandlerResultInterface
     */
    public function handle(HandlerOptionsInterface $options): HandlerResultInterface
    {
        $result = new InvoiceQueueHandlerResult();

        $invoiceBatches = $this->getInvoiceBatches($options->getInvoiceIdentifiers(), $options->getMaxBatchSize());
        foreach ($invoiceBatches as $invoiceBatch) {
            $this->invoiceRepository->bulkUpdateInvoiceStatus($invoiceBatch, InvoiceStatus::DELIVERING());
            $queueResult = $this->publishInvoiceBatch($invoiceBatch, $options->getInitiator());

            $this->invoiceRepository->bulkUpdateInvoiceStatus($queueResult->getFailed(), InvoiceStatus::DRAFT());
            $result->setFailedInvoices($queueResult->getFailed());
        }

        return $result;
    }

    /**
     * @param Invoice[] $invoices
     * @param User|null $initiator
     *
     * @return InvoiceQueueBatchResult
     */
    private function publishInvoiceBatch(array $invoices, ?User $initiator): InvoiceQueueBatchResult
    {
        $batchResult = new InvoiceQueueBatchResult();
        foreach ($invoices as $invoice) {
            try {
                $this->queueProducer->publish($this->createDeliverInvoiceMessage($invoice, $initiator));
                $batchResult->addSucceeded($invoice);
            } catch (Exception $exception) {
                $batchResult->addFailed($invoice);
            }
        }

        return $batchResult;
    }

    /**
     * @param int[] $invoiceIdentifiers
     * @param int $batchSize
     * @return Invoice[]
     */
    private function getInvoiceBatches(array $invoiceIdentifiers, int $batchSize): array
    {
        return array_filter(array_map(function(array $invoiceIdentifiers) {
            return $this->invoiceRepository->findDraftInvoicesByIdentifiers($invoiceIdentifiers);
        }, array_chunk($invoiceIdentifiers, $batchSize)), function(array $batch) {
            return !empty($batch);
        });
    }

    private function createDeliverInvoiceMessage(Invoice $invoice, ?User $initiator): string
    {
        return $this->serializer->serialize(
            [
                'id' => $invoice->getId(),
                'initiatedBy' => $initiator ? $initiator->getUsername() : ''
            ],
            'json',
            (new SerializationContext())->setGroups(['invoiceDeliveryQueue'])
        );
    }
}
