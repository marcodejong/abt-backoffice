<?php

namespace App\Queue\Handler;

use App\Queue\Handler\Options\HandlerOptionsInterface;
use App\Queue\Handler\Result\HandlerResultInterface;

interface QueueHandlerInterface
{
    public function handle(HandlerOptionsInterface $options): HandlerResultInterface;
}