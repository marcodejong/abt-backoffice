<?php

namespace App\Queue\Handler\Result;

use Arriva\Abt\Entity\Invoice;

class InvoiceQueueHandlerResult implements HandlerResultInterface
{
    /** @var Invoice[] */
    private $failedInvoices;

    /**
     * InvoiceQueueHandlerResult constructor.
     * @param Invoice[] $failedInvoices
     */
    public function __construct(array $failedInvoices = [])
    {
        $this->failedInvoices = $failedInvoices;
    }

    public function isSuccessful(): bool
    {
        return true;
    }

    /**
     * @return Invoice[]
     */
    public function getFailedInvoices(): array
    {
        return $this->failedInvoices;
    }

    public function setFailedInvoices(array $invoices): void
    {
        $this->failedInvoices = $invoices;
    }

}