<?php

namespace App\Queue\Handler\Result;

interface HandlerResultInterface
{
    public function isSuccessful(): bool;
}