<?php


namespace App\Queue\Handler\Result;


use Arriva\Abt\Entity\Invoice;

class InvoiceQueueBatchResult
{
    /** @var array Invoice[] */
    private $succeeded = [];

    /** @var Invoice[] */
    private $failed = [];

    public function addSucceeded(Invoice $invoice): void
    {
        $this->succeeded[] = $invoice;
    }

    public function addFailed(Invoice $invoice): void
    {
        $this->failed[] = $invoice;
    }

    /**
     * @return Invoice[]
     */
    public function getSucceeded(): array
    {
        return $this->succeeded;
    }

    /**
     * @return Invoice[]
     */
    public function getFailed(): array
    {
        return $this->failed;
    }

}