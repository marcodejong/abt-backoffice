<?php

namespace App\Queue\Handler\Options;

use App\Entity\User;

class CreateInvoiceHandlerOptions implements HandlerOptionsInterface
{
    /** @var int[] */
    private $invoiceIdentifiers;

    /** @var User|null */
    private $initiator;

    /** @var int */
    private $maxBatchSize;

    public function __construct(array $invoiceIdentifiers, ?User $initiator, $maxBatchSize = 1000)
    {
        $this->invoiceIdentifiers = $invoiceIdentifiers;
        $this->initiator = $initiator;
        $this->maxBatchSize = $maxBatchSize;
    }

    /**
     * @return int[]
     */
    public function getInvoiceIdentifiers(): array
    {
        return $this->invoiceIdentifiers;
    }

    public function getInitiator(): ?User
    {
        return $this->initiator;
    }

    public function getMaxBatchSize(): int
    {
        return $this->maxBatchSize;
    }
}
