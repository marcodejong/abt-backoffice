<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App;

use App\DependencyInjection\CompilerPass\RegisterExportersCompilerPass;
use App\Model\Export\Exporter;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Exception\FileLoaderLoadException;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

/**
 * @package App
 */
class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    private const CONFIG_EXTENSIONS = '.{php,xml,yaml,yml}';

    public function getCacheDir(): string
    {
        return $this->getProjectDir() . '/var/cache/' . $this->environment;
    }

    public function getLogDir(): string
    {
        return $this->getProjectDir() . '/var/log';
    }

    public function registerBundles(): iterable
    {
        $contents = require $this->getProjectDir() . '/config/bundles.php';
        /** @var array[] $contents */
        foreach ($contents as $class => $environments) {
            if (isset($environments['all']) || isset($environments[$this->environment])) {
                yield new $class();
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     * @param LoaderInterface $loader
     * @throws \Exception
     * @return void
     */
    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader): void
    {
        $container->setParameter('container.autowiring.strict_mode', true);
        $container->setParameter('container.dumper.inline_class_loader', true);
        $confDir = $this->getProjectDir() . '/config';
        $loader->load($confDir . '/packages/*' . self::CONFIG_EXTENSIONS, 'glob');
        if (is_dir($confDir . '/packages/' . $this->environment)) {
            $loader->load($confDir . '/packages/' . $this->environment . '/**/*' . self::CONFIG_EXTENSIONS, 'glob');
        }
        $loader->load($confDir . '/services' . self::CONFIG_EXTENSIONS, 'glob');
        $loader->load($confDir . '/services_' . $this->environment . self::CONFIG_EXTENSIONS, 'glob');

        $this->loadConfigurationFolder($loader, $confDir . '/entities');
        $this->loadConfigurationFolder($loader, $confDir . '/services');
    }

    /**
     * @param LoaderInterface $loader
     * @param string $folder
     * @throws \Exception
     */
    private function loadConfigurationFolder(LoaderInterface $loader, string $folder): void
    {
        $envSpecificFolder = "{$folder}/{$this->environment}";
        $loader->load("{$folder}/*" . self::CONFIG_EXTENSIONS, 'glob');
        if (is_dir($envSpecificFolder)) {
            $loader->load($envSpecificFolder . '/*' . self::CONFIG_EXTENSIONS, 'glob');
        }
    }

    /**
     * @param RouteCollectionBuilder $routes
     * @throws FileLoaderLoadException
     * @return void
     */
    protected function configureRoutes(RouteCollectionBuilder $routes): void
    {
        $configDirectory = $this->getProjectDir() . '/config';
        if (is_dir($configDirectory . '/routes/')) {
            $routes->import($configDirectory . '/routes/*' . self::CONFIG_EXTENSIONS, '/', 'glob');
        }
        if (is_dir($configDirectory . '/routes/' . $this->environment)) {
            $routes->import(
                $configDirectory . '/routes/' . $this->environment . '/**/*' . self::CONFIG_EXTENSIONS,
                '/',
                'glob'
            );
        }
        $routes->import($configDirectory . '/routes' . self::CONFIG_EXTENSIONS, '/', 'glob');
    }

    protected function build(ContainerBuilder $container)
    {
        // Exporter auto-registration
        $exporterTagName = 'app.exporter';
        $container->registerForAutoconfiguration(Exporter::class)->addTag($exporterTagName);
        $container->addCompilerPass(new RegisterExportersCompilerPass($exporterTagName));
    }
}
