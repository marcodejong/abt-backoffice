<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Monolog;

use GuzzleHttp\MessageFormatter;
use Psr\Log\LogLevel;

/**
 * @package App\Monolog
 */
class HttpClientMessageFormatter extends MessageFormatter
{
    public function __construct($logLevel = LogLevel::INFO)
    {
        switch ($logLevel) {
            case LogLevel::EMERGENCY:
            case LogLevel::ALERT:
            case LogLevel::CRITICAL:
            case LogLevel::ERROR:
            case LogLevel::WARNING:
                $template = MessageFormatter::CLF;
                break;
            case LogLevel::NOTICE:
            case LogLevel::INFO:
            case LogLevel::DEBUG:
            default:
                $template = MessageFormatter::DEBUG;
        }

        parent::__construct($template);
    }
}
