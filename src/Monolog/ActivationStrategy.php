<?php

namespace App\Monolog;

use App\Service\IpTools\IpUtilServiceInterface;
use Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ActivationStrategy extends ErrorLevelActivationStrategy
{
    /** @var RequestStack */
    private $requestStack;

    /** @var IpUtilServiceInterface */
    private $ipUtilService;

    /** @var int[] */
    private $ignoredHttpStatuses;

    public function __construct(
        RequestStack $requestStack,
        IpUtilServiceInterface $ipUtilService,
        string $actionLevel,
        array $ignoredHttpStatuses
    ) {
        $this->requestStack = $requestStack;
        $this->ignoredHttpStatuses = $ignoredHttpStatuses;
        $this->ipUtilService = $ipUtilService;

        parent::__construct($actionLevel);
    }

    public function isHandlerActivated(array $record): bool
    {
        if (!parent::isHandlerActivated($record)) {
            return false;
        }

        $activated = !$this->isDuringHealthCheck($record) &&
            !$this->isIgnoredExceptionStatus($record);

        return $activated;
    }

    private function isIgnoredExceptionStatus(array $record): bool
    {
        $exception = $record['context']['exception'] ?? null;

        return ($exception instanceof HttpException) &&
            \in_array($exception->getStatusCode(), $this->ignoredHttpStatuses, true);
    }

    private function isDuringHealthCheck(array $record): bool
    {
        $request = $record['context']['request'] ?? $this->requestStack->getMasterRequest();

        return ($request instanceof Request) && $this->isHealthCheckRequest($request);
    }

    private function isHealthCheckRequest(Request $request): bool
    {
        return $request->isMethod(Request::METHOD_HEAD) &&
            $request->getPathInfo() === '/_healthcheck' &&
            $this->isRequestByTrustedProxy($request);
    }

    private function isRequestByTrustedProxy(Request $request): bool
    {
        return $this->ipUtilService->ipMatchesAny($request->getClientIp(), Request::getTrustedProxies());
    }
}
