<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Monolog;

use Monolog\Formatter\FormatterInterface;
use Sqills\Censor\CensorInterface;
use Sqills\Censor\Factory\CensorFactoryInterface;

/**
 * @package App\Monolog
 */
class CensorFormatter implements FormatterInterface
{
    /** @var FormatterInterface */
    private $formatter;

    /** @var CensorFactoryInterface */
    private $censorFactory;

    /** @var null|CensorInterface */
    private $censor;

    public function __construct(FormatterInterface $formatter, CensorFactoryInterface $censorFactory)
    {
        $this->formatter = $formatter;
        $this->censorFactory = $censorFactory;
    }

    public function format(array $record)
    {
        return $this->formatter->format($this->getCensoredData($record));
    }

    private function getCensoredData(array $record): array
    {
        $censoredMessage = $this->getCensor()->censor($record['message']);
        $censoredContext = $this->getCensor()->censor($record['context']);
        $record['message'] = $censoredMessage;
        $record['context'] = $censoredContext;

        return $record;
    }

    private function getCensor(): CensorInterface
    {
        if (!$this->censor) {
            $this->censor = $this->censorFactory->createCensor();
        }

        return $this->censor;
    }

    public function formatBatch(array $records)
    {
        return $this->formatter->formatBatch($records);
    }
}
