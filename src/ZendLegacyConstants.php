<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App;

/**
 * @package App
 */
final class ZendLegacyConstants
{
    public const MESSAGES_ROOT_KEY = 'FlashMessenger';
    public const MESSAGES_CATEGORY_KEY = 'default';
    public const ZEND_AUTH_SESSION_BAG = 'zend_auth';
    public const ZEND_AUTH_SESSION_BAG_ROOT = 'storage';
}
