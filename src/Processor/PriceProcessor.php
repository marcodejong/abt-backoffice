<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Processor;

use App\Manager\PriceManager;
use App\Manager\PricePeriodManager;
use App\Manager\PropositionElementManager;
use App\Manager\StationManager;
use App\Processor\Import\Builder\Factory\PricedItemBuilderFactory;
use App\Processor\Import\Result\ImportResult;
use Arriva\Abt\Entity\Price;
use Arriva\Abt\Entity\StarValue;
use Arriva\Abt\ORM\Repository\PriceRepositoryInterface;
use Arriva\Abt\Value\PricedItem\PricedItemCriteria;
use Arriva\Abt\Value\PricedItem\RegularItemCriteria;
use Arriva\Abt\Value\PricedItem\RouteItemCriteria;
use Arriva\Abt\Value\PricedItem\StarItemCriteria;
use League\Csv\Reader;
use League\Csv\Writer;
use SplTempFileObject;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @package App\Processor
 */
class PriceProcessor
{
    const IMPORT_COLUMN_NAMES = [
        'PEname',
        'pricePeriod',
        'fromStationCode',
        'toStationCode',
        'starValue',
        'amount'
    ];

    /** @var PriceRepositoryInterface */
    private $priceRepository;

    /** @var PropositionElementManager */
    private $propositionElementManager;

    /** @var PricePeriodManager */
    private $pricePeriodManager;

    /** @var StationManager */
    private $stationManager;

    /** @var PriceManager */
    private $priceManager;

    /** @var PricedItemBuilderFactory */
    private $priceImportBuilderFactory;

    public function __construct(
        PriceRepositoryInterface $priceRepository,
        PropositionElementManager $propositionElementManager,
        PricePeriodManager $pricePeriodManager,
        StationManager $stationManager,
        PriceManager $priceManager,
        PricedItemBuilderFactory $priceImportBuilderFactory
    ) {
        $this->priceRepository = $priceRepository;
        $this->propositionElementManager = $propositionElementManager;
        $this->pricePeriodManager = $pricePeriodManager;
        $this->stationManager = $stationManager;
        $this->priceManager = $priceManager;
        $this->priceImportBuilderFactory = $priceImportBuilderFactory;
    }


    /**
     * @param $fileName
     * @throws \League\Csv\Exception
     * @return ImportResult
     */
    public function importCsv(UploadedFile $fileName): ImportResult
    {
        $result = new ImportResult();

        if (! ($fileName->getClientOriginalExtension() === 'csv')) {
            $result->addMessage('form.import.wrong-file-extension');

            return $result;
        }

        $reader = Reader::createFromPath($fileName, 'r');
        $reader->setHeaderOffset(0);
        $reader->setDelimiter(';');

        if (!$this->validateColumnNames($reader->getHeader())) {
            $result->addMessage('form.import.wrong-column-names');

            return $result;
        }

        $validatedPrices = $this->getValidatedPricesArray($reader, $result);

        if (! $result->hasMessages()) {
            foreach ($validatedPrices as $price) {
                $this->priceManager->addPrice($price);
            }
        }

        return $result;
    }

    private function validateColumnNames(array $names): bool
    {
        return  count(array_diff(self::IMPORT_COLUMN_NAMES, $names)) === 0 &&
                count($names) === count(self::IMPORT_COLUMN_NAMES);
    }

    private function getValidatedPricesArray($reader, ImportResult $result): array
    {
        $validated = [];

        foreach ($reader as $rowNumber => $row) {
            if ($this->validateRow($row, $rowNumber, $result)) {
                $pricedItemCriteria = $this->getPricedItemCriteria($row);
                $pricedItem = $this->priceImportBuilderFactory->createPricedItemBuilder()
                    ->setPricedItemCriteria($pricedItemCriteria)
                    ->getPricedItem();

                $pricePeriod = $this->pricePeriodManager->getPricePeriodByName($row['pricePeriod']);
                $amount = floatval(\str_replace(',', '.', $row['amount']));

                if (null !== $price = $this->priceRepository->findPriceForItem($pricedItem, $pricePeriod)) {
                    $price->setAmount($amount);
                } else {
                    $price = new Price($amount, $pricePeriod, $pricedItem);
                }

                $validated[] = $price;
            }
        }

        return $validated;
    }

    private function validateRow(array $row, int $rowNumber, ImportResult $result): bool
    {
        $isValid = true;

        if (null === $this->propositionElementManager->getPropositionElementByDescription(
                $row['PEname']
            )) {
            $isValid = false;
            $result->addMessage('form.import.price.wrong-proposition-element-value', $rowNumber);
        }

        if (null === $this->pricePeriodManager->getPricePeriodByName($row['pricePeriod'])) {
            $isValid = false;
            $result->addMessage('form.import.price.wrong-price-period-value', $rowNumber);
        }

        if (! empty($row['fromStationCode']) || ! empty($row['toStationCode'])) {
            if (null === $this->stationManager->getStationByCode((int)$row['fromStationCode'])) {
                $isValid = false;
                $result->addMessage('form.import.price.wrong-from-station-value', $rowNumber);
            }
            if (null === $this->stationManager->getStationByCode((int)$row['toStationCode'])) {
                $isValid = false;
                $result->addMessage('form.import.price.wrong-to-station-value', $rowNumber);
            }
        }

        if (! empty($row['starValue']) && (! empty($row['fromStationCode']) || ! empty($row['toStationCode']))) {
            $isValid = false;
            $result->addMessage('form.import.price.wrong-configuration-combination', $rowNumber);
        }

        try {
            empty($row['starValue']) ?: StarValue::instance((int)$row['starValue']);
        } catch (\ConfigurationException $e) {
            $isValid = false;
            $result->addMessage('form.import.price.wrong-star-value-value', $rowNumber);
        }

        if (0 === floatval(\str_replace(',', '.', $row['amount']))) {
            $isValid = false;
            $result->addMessage('form.import.price.wrong-amount-value', $rowNumber);
        }

        return $isValid;
    }

    private function getPricedItemCriteria(array $criteria): ?PricedItemCriteria
    {
        $propositionElement = $this->propositionElementManager->getPropositionElementByDescription(
            $criteria['PEname']
        );

        if (! $criteria['fromStationCode'] && ! $criteria['toStationCode'] && ! $criteria['starValue']) {
            return new RegularItemCriteria($propositionElement);
        }
        if ($criteria['fromStationCode'] && $criteria['toStationCode'] && ! $criteria['starValue']) {
            $fromStation = $this->stationManager->getStationByCode((int)$criteria['fromStationCode']);
            $toStation = $this->stationManager->getStationByCode((int)$criteria['toStationCode']);

            return new RouteItemCriteria($propositionElement, $fromStation, $toStation);
        }
        if ($criteria['starValue'] && ! ($criteria['fromStationCode'] || $criteria['toStationCode'])) {
            $starValue = StarValue::instance((int)$criteria['starValue']);

            return new StarItemCriteria($propositionElement, $starValue);
        }

        return null;
    }

    public function exampleCsv(): Response
    {
        $list = [];
        $list[] = ['Vrij op weekdagen, volwassen tarief', 'Prijzen 2020', '', '', '', '1,25'];
        $list[] = ['Vrij op weekdagen, volwassen tarief', 'Prijzen 2020', '11', '12', '', '1.50'];
        $list[] = ['Vrij op weekdagen, volwassen tarief', 'Prijzen 2020', '', '', '1', '1,25'];

        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->setDelimiter(';');
        $csv->insertOne([
            'PEname',
            'pricePeriod',
            'fromStationCode',
            'toStationCode',
            'starValue',
            'amount',
        ]);
        $csv->insertAll($list);

        $response = new Response($csv);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'example-prices.csv'
        );
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');

        return $response;
    }
}
