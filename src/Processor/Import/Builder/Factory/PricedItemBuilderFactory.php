<?php


namespace App\Processor\Import\Builder\Factory;


use App\Processor\Import\Builder\PricedItemBuilder;
use Arriva\Abt\ORM\Repository\PricedItemRepositoryInterface;

class PricedItemBuilderFactory
{
    /** @var PricedItemRepositoryInterface */
    private $pricedItemRepository;

    public function __construct(PricedItemRepositoryInterface $pricedItemRepository)
    {
        $this->pricedItemRepository = $pricedItemRepository;
    }

    public function createPricedItemBuilder(): PricedItemBuilder
    {
        return new PricedItemBuilder($this->pricedItemRepository);
    }
}