<?php


namespace App\Processor\Import\Builder;


use Arriva\Abt\Entity\Configuration\PricedItem;
use Arriva\Abt\ORM\Repository\PricedItemRepositoryInterface;
use Arriva\Abt\Value\PricedItem\PricedItemCriteria;

class PricedItemBuilder
{
    /** @var PricedItemRepositoryInterface  */
    private $pricedItemRepository;

    /** @var PricedItemCriteria|null */
    private $pricedItemCriteria;

    public function __construct(PricedItemRepositoryInterface $pricedItemRepository)
    {
        $this->pricedItemRepository = $pricedItemRepository;
    }

    public function getPricedItem(): PricedItem
    {
        if (null === $this->pricedItemCriteria) {
            throw new \DomainException("Can't build PricedItem: PricedItemCriteria not set");
        }

        return $this->pricedItemRepository->findOrCreatePricedItemByCriteria($this->pricedItemCriteria);
    }

    public function setPricedItemCriteria(PricedItemCriteria $criteria)
    {
        $this->pricedItemCriteria = $criteria;

        return $this;
    }
}
