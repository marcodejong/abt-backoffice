<?php


namespace App\Processor\Import\Result;


class ImportResult
{
    private $errorMessages;

    public function __construct()
    {
        $this->errorMessages = [];
    }

    public function hasMessages(): bool
    {
        return count($this->errorMessages) !== 0;
    }

    public function addMessage($message, $rowNumber = ''): void
    {
        $this->errorMessages[] = ['message' => $message, 'row' => $rowNumber];
    }

    public function getMessages(): array
    {
        return $this->errorMessages;
    }
}