<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Processor;

use App\Manager\RegionManager;
use App\Manager\StationManager;
use App\Processor\Import\Result\ImportResult;
use Arriva\Abt\Entity\Region;
use Arriva\Abt\Entity\Station;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use League\Csv\Reader;
use League\Csv\Writer;
use SplTempFileObject;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @package App\Processor
 */
class StationProcessor
{
    const IMPORT_COLUMN_NAMES = [
        'name',
        'code',
        'regions'
    ];

    private $stationManager;

    private $regionManager;

    public function __construct(
        StationManager $stationManager,
        RegionManager $regionManager
    ) {
        $this->stationManager = $stationManager;
        $this->regionManager = $regionManager;
    }

    /**
     * @param UploadedFile $fileName
     * @throws \League\Csv\Exception
     * @return ImportResult
     */
    public function importCsv(UploadedFile $fileName): ImportResult
    {
        $result = new ImportResult();

        if (!($fileName->getClientOriginalExtension() === 'csv')) {
            $result->addMessage('form.import.wrong-file-extension');
            return $result;
        }

        $reader = Reader::createFromPath($fileName, 'r');
        $reader->setHeaderOffset(0);
        $reader->setDelimiter(';');

        if (!$this->validateColumnNames($reader->getHeader())) {
            $result->addMessage('form.import.wrong-column-names');

            return $result;
        }

        $validatedStations = $this->getValidatedStationArray($reader, $result);

        if (!$result->hasMessages()) {
            foreach ($validatedStations as $station) {
                $this->stationManager->addStation($station);
            }
        }

        return $result;
    }

    private function validateColumnNames(array $names): bool
    {
        return  count(array_diff(self::IMPORT_COLUMN_NAMES, $names)) === 0 &&
            count($names) === count(self::IMPORT_COLUMN_NAMES);
    }

    private function getValidatedStationArray($reader, ImportResult $result): array
    {
        $validated = [];

        foreach ($reader as $rowNumber => $row) {
            if($this->validateRow($row, $rowNumber, $result)) {
                $station = $this->stationManager->getStationByCode($row['code']) ?: new Station();
                $station->setName($row['name']);
                $station->setCode($row['code']);
                $station->setRegions($this->getRegions($row['regions']));

                $validated[] = $station;
            }
        }

        return $validated;
    }

    /**
     * @param string $codes
     * @return Response
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     */
    public function exportCsv(string $codes): Response
    {
        $stations = [];
        $list = [];

        foreach (explode(",", $codes) as $code) {
            $stations[] = $this->stationManager->getStationByCode((int)$code);
        }

        foreach ($stations as $station) {
            if (null !== $station) {
                $regions = [];
                foreach ($station->getRegions() as $region) {
                    $regions[] = $region->getCode();
                }
                $list[] = [$station->getName(), $station->getCode(), implode(",", $regions)];
            }
        }


        usort($list, [$this, "compare"]);
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->setDelimiter(';');
        $csv->insertOne(['name', 'code', 'regions']);
        $csv->insertAll($list);

        $response = new Response($csv);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'stations.csv'
        );
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');

        return $response;
    }

    /**
     * @param $a
     * @param $b
     * @return bool
     */
    public function compare($a, $b): bool
    {
        return $a[1] > $b[1];
    }

    public function getStation(array $row, StationManager $stationManager): Station
    {
        $station = $stationManager->getStationByCode((int)$row['code']);
        if (!($station instanceof Station)) {
            $station = new Station();
            $station->setCode($row['code']);
        }
        $station->setName($row['name']);

        return $station;
    }

    public function getRegions(string $regionString): Collection
    {
        $regionString = str_replace(' ','', $regionString);

        if ($regionString === "") {
            return new ArrayCollection();
        }

        $regionCodes = array_unique(explode(",", $regionString));
        $regions = $this->regionManager->findRegionsByCodes($regionCodes);

        if (count($regionCodes) !== $regions->count()) {
            return new ArrayCollection();
        }

        return $regions;
    }

    public function exampleCsv(): Response
    {
        $list = [];
        $list[] = ['Arnhem', '1', 'E, S'];
        $list[] = ['Leeuwarden', '2', 'N, W'];

        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->setDelimiter(';');
        $csv->insertOne(['name', 'code', 'regions']);
        $csv->insertAll($list);

        $response = new Response($csv);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'example-stations.csv'
        );
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');

        return $response;
    }

    private function validateRow(array $row, int $rowNumber, ImportResult $result): bool
    {
        $isValid = true;

        if(!$row['name']) {
            $isValid = false;
            $result->addMessage('form.import.station.wrong-name-value', $rowNumber);
        }

        if((int)$row['code'] === 0) {
            $isValid = false;
            $result->addMessage('form.import.station.wrong-code-value', $rowNumber);
        }

        if ($this->getRegions($row['regions'])->count() === 0) {
            $result->addMessage('form.import.station.wrong-regions-value', $rowNumber);
            $isValid = false;
        }

        return $isValid;
    }

}
