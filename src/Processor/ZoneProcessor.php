<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Processor;

use App\Manager\RegionManager;
use App\Manager\ZoneManager;
use App\Processor\Import\Result\ImportResult;
use Arriva\Abt\Entity\Zone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use League\Csv\Reader;
use League\Csv\Writer;
use SplTempFileObject;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @package App\Processor
 */
class ZoneProcessor
{
    const IMPORT_COLUMN_NAMES = [
        'name',
        'code',
        'active',
        'regions'
    ];

    private $zoneManager;

    private $regionManager;

    public function __construct(
        ZoneManager $zoneManager,
        RegionManager $regionManager
    ) {
        $this->zoneManager = $zoneManager;
        $this->regionManager = $regionManager;
    }

    /**
     * @param UploadedFile $fileName
     * @throws \League\Csv\Exception
     * @return ImportResult
     */
    public function importCsv(UploadedFile $fileName): ImportResult
    {
        $result = new ImportResult();

        if (!($fileName->getClientOriginalExtension() === 'csv')) {
            $result->addMessage('form.import.wrong-file-extension');
            return $result;
        }
        $reader = Reader::createFromPath($fileName, 'r');
        $reader->setHeaderOffset(0);
        $reader->setDelimiter(';');

        if (!$this->validateColumnNames($reader->getHeader())) {
            $result->addMessage('form.import.wrong-column-names');

            return $result;
        }

        $validatedZones = $this->getValidatedZoneArray($reader, $result);

        if (!$result->hasMessages()) {
            foreach ($validatedZones as $zone) {
                $this->zoneManager->addZone($zone);
            }
        }

        return $result;
    }

    private function validateColumnNames(array $names): bool
    {
        return  count(array_diff(self::IMPORT_COLUMN_NAMES, $names)) === 0 &&
            count($names) === count(self::IMPORT_COLUMN_NAMES);
    }

    private function getValidatedZoneArray($reader, ImportResult $result): array
    {
        $validated = [];

        foreach ($reader as $rowNumber => $row) {
            if ($this->validateRow($row, $rowNumber, $result)) {
                $zone = $this->zoneManager->getZoneByCode($row['code']) ?: new Zone();
                $zone->setName($row['name']);
                $zone->setCode($row['code']);
                $zone->setActive($row['active']);
                $zone->setRegions($this->getRegions($row['regions']));

                $validated[] = $zone;
            }
        }

        return $validated;
    }

    public function exportCsv(string $codes): Response
    {
        $zones = [];
        foreach (explode(",", $codes) as $code) {
            $zones[] = $this->zoneManager->getZoneByCode((int)$code);
        }

        $list = [];
        foreach ($zones as $zone) {
            if (null !== $zone) {
                $regions = [];
                foreach ($zone->getRegions() as $region) {
                    $regions[] = (string)$region->getCode();
                }
                $list[] = [$zone->getName(), $zone->getCode(), $zone->getActive(), implode(",", $regions)];
            }
        }
        usort($list, [$this, "compare"]);
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->setDelimiter(';');
        $csv->insertOne(['name', 'code', 'active', 'regions']);
        $csv->insertAll($list);


        $response = new Response($csv);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'zones.csv'
        );
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');

        return $response;
    }

    public function compare($a, $b): bool
    {
        return $a[1] > $b[1];
    }

    public function getZone(array $row, ZoneManager $zoneManager): Zone
    {
        $zone = $zoneManager->getZoneByCode((int)$row['code']);
        if (!($zone instanceof Zone)) {
            $zone = new Zone();
            $zone->setCode($row['code']);
        } else {
            $zone->setRegions(new ArrayCollection());
        }
        $zone->setName($row['name']);
        $zone->setActive($row['active']);

        return $zone;
    }

    public function getRegions(string $regionString): Collection
    {
        $regionString = str_replace(' ','', $regionString);

        if ($regionString === "") {
            return new ArrayCollection();
        }

        $regionCodes = array_unique(explode(",", $regionString));
        $regions = $this->regionManager->findRegionsByCodes($regionCodes);

        if (count($regionCodes) !== $regions->count()) {
            return new ArrayCollection();
        }

        return $regions;
    }

    public function exampleCsv(): Response
    {
        $list = [];
        $list[] = ['Regio Gelderland', '1', '0', 'S, E'];
        $list[] = ['Regio Friesland', '2', '1', 'N, W'];

        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->setDelimiter(';');
        $csv->insertOne(['name', 'code', 'active', 'regions']);
        $csv->insertAll($list);

        $response = new Response($csv);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'example-zones.csv'
        );
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');

        return $response;
    }

    private function validateRow(array $row, int $rowNumber, ImportResult $result): bool
    {
        $isValid = true;

        if(!$row['name']) {
            $isValid = false;
            $result->addMessage('form.import.zone.wrong-name-value', $rowNumber);
        }

        if((int)$row['code'] === 0) {
            $isValid = false;
            $result->addMessage('form.import.zone.wrong-code-value', $rowNumber);
        }

        if(!in_array($row['active'], ["0","1"])) {
            $isValid = false;
            $result->addMessage('form.import.zone.wrong-active-value', $rowNumber);
        }

        if ($this->getRegions($row['regions'])->count() === 0) {
            $result->addMessage('form.import.zone.wrong-regions-value', $rowNumber);
            $isValid = false;
        }

        return $isValid;
    }
}
