<?php


namespace App\Repository;


use Arriva\Abt\Entity\SubscriptionRefund;
use Doctrine\Common\Collections\Criteria;

class SubscriptionRefundRepository implements SubscriptionRefundRepositoryInterface
{
    use DoctrineBackedRepositoryTrait;

    /**
     * @param Criteria $criteria
     * @return SubscriptionRefund[]|iterable
     */
    public function findBy(Criteria $criteria): iterable
    {
        return $this->findMatching(SubscriptionRefund::type(), $criteria);
    }
}