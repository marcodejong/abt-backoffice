<?php

namespace App\Repository;

use Arriva\Abt\Entity\PropositionElementInstance;
use Arriva\Abt\Value\PeInstanceId;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectRepository;
use SolidPhp\ValueObjects\Type\Type;

class PropositionElementInstanceRepository implements PropositionElementInstanceRepositoryInterface
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }
    public function save(PropositionElementInstance $instance): void
    {
        $manager = $this->getManager(Type::of($instance));
        if (!$manager->contains($instance)) {
            $manager->persist($instance);
        }
    }

    public function findPropositionElementInstances(array $propositionElementInstanceIds): array
    {
        return $this->getDoctrineRepository()->findBy(['id' => $propositionElementInstanceIds]);
    }

    private function getDoctrineRepository(): ObjectRepository
    {
        return $this->managerRegistry
            ->getManagerForClass(PropositionElementInstance::class)
            ->getRepository(PropositionElementInstance::class);
    }
}
