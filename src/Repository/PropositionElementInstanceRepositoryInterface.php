<?php

namespace App\Repository;

use Arriva\Abt\Entity\PropositionElementInstance;

interface PropositionElementInstanceRepositoryInterface
{
    public function save(PropositionElementInstance $instance): void;

    /**
     * @param int[] $propositionElementInstanceIds
     * @return PropositionElementInstance[]
     */
    public function findPropositionElementInstances(array $propositionElementInstanceIds): array;
}
