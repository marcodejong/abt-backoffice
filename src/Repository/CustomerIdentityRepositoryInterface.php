<?php

namespace App\Repository;

use Arriva\Reporting\Entity\CustomerIdentity;

interface CustomerIdentityRepositoryInterface
{
    public function findByIdmId(int $idmId): ?CustomerIdentity;
}
