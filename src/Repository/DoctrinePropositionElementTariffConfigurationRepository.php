<?php


namespace App\Repository;


use App\Model\Report\TariffConfiguration\RegularPeTariffConfiguration;
use App\Model\Report\TariffConfiguration\RoutePeTariffConfiguration;
use App\Model\Report\TariffConfiguration\StarPeTariffConfiguration;
use Arriva\Abt\Entity\Configuration\RegularItem;
use Arriva\Abt\Entity\Configuration\RouteItem;
use Arriva\Abt\Entity\Configuration\StarItem;
use Arriva\Abt\Entity\Price;
use Arriva\Abt\Entity\PricePeriod;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Utility\Iterator;
use Doctrine\Common\Persistence\ManagerRegistry;

class DoctrinePropositionElementTariffConfigurationRepository implements PropositionElementTariffConfigurationRepository
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @param PricePeriod $pricePeriod
     * @return RegularPeTariffConfiguration[]
     */
    public function findRegularPropositionElementTariffConfigurationsForPricePeriod(PricePeriod $pricePeriod): array
    {
        $result = $this->getEntityManager(Price::type())->createQuery("
            SELECT
                 pp              AS pricePeriod,
                 pr              AS price,
                 pi              AS pricedItem,
                 pe              AS propositionElement,
                 p               AS propositions,
                 pg              AS propositionGroup
            FROM Arriva\Abt\Entity\Price pr
            JOIN pr.pricePeriod pp
            JOIN pr.item pi WITH pi INSTANCE OF Arriva\Abt\Entity\Configuration\RegularItem
            JOIN pi.propositionElement pe
            JOIN pe.propositions p
            JOIN p.propositionGroup pg
            WHERE pp = :pricePeriod
        ")->setParameter('pricePeriod', $pricePeriod);

        return Iterator::create($result->getResult())->map(function(array $resultRow) {
            return $this->createRegularPropositionElementTariffConfiguration($resultRow['price']);
        })->getArray();
    }

    private function createRegularPropositionElementTariffConfiguration(Price $price): RegularPeTariffConfiguration
    {
        /** @var RegularItem $pricedItem */
        $pricedItem = $price->getItem();
        $propositionElement = $pricedItem->getPropositionElement();
        $propositions = $propositionElement->getPropositions()->getValues();

        $propositionGroups = Iterator::create($propositions)->map(function (Proposition $proposition) {
            return $proposition->getPropositionGroup();
        })->unique()->getArray();

        return new RegularPeTariffConfiguration(
            $propositionElement,
            $price->getPricePeriod(),
            $price->getMoneyAmount(),
            $propositions,
            $propositionGroups
        );
    }

    /**
     * @param PricePeriod $pricePeriod
     * @return StarPeTariffConfiguration[]
     */
    public function findStarPropositionElementTariffConfigurationsForPricePeriod(PricePeriod $pricePeriod): array
    {
        $result = $this->getEntityManager(Price::type())->createQuery("
            SELECT
                 pp              AS pricePeriod,
                 pr              AS price,
                 pi              AS pricedItem,
                 pe              AS propositionElement,
                 p               AS propositions,
                 pg              AS propositionGroup
            FROM Arriva\Abt\Entity\Price pr
            JOIN pr.pricePeriod pp
            JOIN pr.item pi WITH pi INSTANCE OF Arriva\Abt\Entity\Configuration\StarItem
            JOIN pi.propositionElement pe
            JOIN pe.propositions p
            JOIN p.propositionGroup pg
            WHERE pp = :pricePeriod
        ")->setParameter('pricePeriod', $pricePeriod);

        return Iterator::create($result->getResult())->map(function(array $resultRow) {
            return $this->createStarPropositionElementTariffConfiguration($resultRow['price']);
        })->getArray();
    }

    private function createStarPropositionElementTariffConfiguration(Price $price): StarPeTariffConfiguration
    {
        /** @var StarItem $pricedItem */
        $pricedItem = $price->getItem();
        $propositionElement = $pricedItem->getPropositionElement();
        $propositions = $propositionElement->getPropositions()->getValues();

        $propositionGroups = Iterator::create($propositions)->map(function (Proposition $proposition) {
            return $proposition->getPropositionGroup();
        })->unique()->getArray();

        return new StarPeTariffConfiguration(
            $propositionElement,
            $price->getPricePeriod(),
            $price->getMoneyAmount(),
            $propositions,
            $propositionGroups,
            $pricedItem->getStarValue()
        );
    }

    /**
     * @param PricePeriod $pricePeriod
     * @return RoutePeTariffConfiguration[]
     */
    public function findRoutePropositionElementTariffConfigurationsForPricePeriod(PricePeriod $pricePeriod): array
    {
        $result = $this->getEntityManager(Price::type())->createQuery("
            SELECT
                 pp              AS pricePeriod,
                 pr              AS price,
                 pi              AS pricedItem,
                 pe              AS propositionElement,
                 p               AS propositions,
                 pg              AS propositionGroup
            FROM Arriva\Abt\Entity\Price pr
            JOIN pr.pricePeriod pp
            JOIN pr.item pi WITH pi INSTANCE OF Arriva\Abt\Entity\Configuration\RouteItem
            JOIN pi.propositionElement pe
            JOIN pe.propositions p
            JOIN p.propositionGroup pg
            WHERE pp = :pricePeriod
        ")->setParameter('pricePeriod', $pricePeriod);

        return Iterator::create($result->getResult())->map(function(array $resultRow) {
            return $this->createRoutePropositionElementTariffConfiguration($resultRow['price']);
        })->getArray();
    }

    private function createRoutePropositionElementTariffConfiguration(Price $price): RoutePeTariffConfiguration
    {
        /** @var RouteItem $pricedItem */
        $pricedItem = $price->getItem();
        $propositionElement = $pricedItem->getPropositionElement();
        $propositions = $propositionElement->getPropositions()->getValues();

        $propositionGroups = Iterator::create($propositions)->map(function (Proposition $proposition) {
            return $proposition->getPropositionGroup();
        })->unique()->getArray();

        return new RoutePeTariffConfiguration(
            $propositionElement,
            $price->getPricePeriod(),
            $price->getMoneyAmount(),
            $propositions,
            $propositionGroups,
            $pricedItem->getFromStation(),
            $pricedItem->getToStation()
        );
    }
}