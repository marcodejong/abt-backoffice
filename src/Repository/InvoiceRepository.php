<?php

namespace App\Repository;

use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\InvoiceItem;
use Arriva\Abt\Entity\PropositionElementInstance;
use Arriva\Abt\Entity\Receivable\CreditReceivable;
use Arriva\Abt\Entity\Receivable\Receivable;
use Arriva\Abt\Entity\Receivable\SubscriptionTermReceivable;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Value\DateRange;
use Arriva\Abt\Value\InvoiceStatus;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\QueryException;
use SolidPhp\ValueObjects\Type\Type;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class InvoiceRepository implements InvoiceRepositoryInterface
{
    use DoctrineBackedRepositoryTrait;

    /** @var PropertyAccessorInterface */
    private $propertyAccessor;

    public function __construct(
        ManagerRegistry $managerRegistry,
        PropertyAccessorInterface $propertyAccessor
    ) {
        $this->managerRegistry = $managerRegistry;
        $this->propertyAccessor = $propertyAccessor;
    }
    public function save(PropositionElementInstance $instance): void
    {
        $manager = $this->getManager(Type::of($instance));
        if (!$manager->contains($instance)) {
            $manager->persist($instance);
        }
    }

    /**
     * @param int $invoiceId
     * @return Invoice|null&object
     */
    public function findInvoiceById(int $invoiceId): ?Invoice
    {
        return $this->getRepository(Type::of(Invoice::class))->findOneBy(['id' => $invoiceId]);
    }


    public function findDraftInvoicesByIdentifiers(array $invoiceIdentifiers): array
    {
        /** @var EntityRepository $repository */
        $repository = $this->getRepository(Type::of(Invoice::class));

        return $repository->createQueryBuilder('i')
            ->where('i.id IN(:invoiceIdentifiers)')
            ->andWhere('i.status = :status')
            ->setParameters([
                'invoiceIdentifiers' => $invoiceIdentifiers,
                'status' => InvoiceStatus::DRAFT()
            ])
            ->getQuery()
            ->setFetchMode(Invoice::class, 'i', ClassMetadata::FETCH_EXTRA_LAZY)
            ->getResult();
    }

    /**
     * @param Invoice[] $invoices
     * @param InvoiceStatus $invoiceStatus
     * @return int
     */
    public function bulkUpdateInvoiceStatus(array $invoices, InvoiceStatus $invoiceStatus): int
    {
        $query = $this->getEntityManager(Invoice::type())
            ->createQuery('UPDATE Arriva\Abt\Entity\Invoice i SET i.status = :status WHERE i IN(:invoices)')
            ->setParameters([
                'invoices' => $invoices,
                'status' => $invoiceStatus
            ]);

        return $query->execute();
    }

    public function getNumberOfInvoicesInDeliveringState(): int
    {
        return $this
            ->getEntityRepository(Type::of(Invoice::class))
            ->createQueryBuilder('i')
            ->select('COUNT(i.id)')
            ->where('i.status = :status')
            ->setParameters([
                'status' => InvoiceStatus::DELIVERING()
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findInvoice(int $invoiceId): array
    {
        $repository = $this->getEntityRepository(Invoice::type());

        return $repository->createQueryBuilder('i')
            ->select('i, ii')
            ->leftJoin('i.invoiceItems', 'ii')
            ->where('i.id = :id')
            ->setParameters([
                'id' => $invoiceId
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Invoice $invoice
     * @return array
     */
    private function getCollectionsForInvoice(Invoice $invoice): array
    {
        return Iterator::create($invoice->getInvoiceItems())->filter(
            static function (InvoiceItem $invoiceItem) {
                return $invoiceItem instanceof Invoice\Item\Collection;
            }
        )->getArray();
    }

    /**
     * @param Invoice $invoice
     * @param string $groupBy
     * @return SubscriptionTermReceivable[] - grouped array
     */
    public function getGroupedSubscriptionTermReceivables(Invoice $invoice, string $groupBy): array
    {
        return Iterator::create($this->getCollectionsForInvoice($invoice))->filter(
            static function (Invoice\Item\Collection $collection) {
                return $collection->getReceivable() instanceof SubscriptionTermReceivable;
            }
        )->map(
            static function (Invoice\Item\Collection $collection) {
                return $collection->getReceivable();
            }
        )->groupBy(
            function (SubscriptionTermReceivable $receivable) use($groupBy) {
                return $this->propertyAccessor->getValue(
                    $receivable,
                    $groupBy
                );
            }
        )->getArray();
    }

    public function getGroupedSubscriptionTermAndCreditReceivables(Invoice $invoice, string $groupBy): array
    {
        return Iterator::create($this->getCollectionsForInvoice($invoice))->filter(
            static function (Invoice\Item\Collection $collection) {
                return $collection->getReceivable() instanceof SubscriptionTermReceivable
                    || $collection->getReceivable() instanceof CreditReceivable;
            }
        )->map(
            static function (Invoice\Item\Collection $collection) {
                return $collection->getReceivable();
            }
        )->groupBy(
            function (Receivable $receivable) use($groupBy) {
                return $this->propertyAccessor->getValue(
                    $receivable,
                    $groupBy
                );
            }
        )->getArray();
    }

    /**
     * @param DateRange $period
     * @return Iterator&iterable<Invoice>
     * @throws QueryException
     */
    public function findInvoicesByInvoiceDate(DateRange $period): Iterator
    {
        $criteria = Criteria::create()
                            ->where(Criteria::expr()->gte('i.invoiceDate', ':startDate'))
            ->andWhere(Criteria::expr()->lte('i.invoiceDate', ':endDate'))
        ->orderBy(['i.invoiceNumber' => 'ASC']);

        $qb = $this->getEntityManager(Invoice::type())->createQueryBuilder()
            ->select('i')
            ->from(Invoice::class, 'i')
            ->addCriteria($criteria)
        ->setParameters([
            'startDate' => $period->getStartDate(),
            'endDate' => $period->getEndDate()
        ]);

        return Iterator::create($qb->getQuery()->iterate())->map('reset');
    }
}
