<?php


namespace App\Repository;


use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Value\Time\DateRangeInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;

class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @param Criteria $criteria
     * @return Subscription[]|iterable
     */
    public function findBy(Criteria $criteria): iterable
    {
        return $this->findMatching(Subscription::type(), $criteria);
    }

    public function findSubscriptionsByCreatedDate(DateRangeInterface $dateRange): array
    {
        return $this->getEntityManager(Subscription::type())->createQueryBuilder()
            ->select('s')
            ->from(Subscription::class, 's')
            ->where('s.createdAt >= :start')
            ->andWhere('s.createdAt <= :end')
            ->setParameter('start', $dateRange->getStartDate()->getStartOfDay())
            ->setParameter('end', $dateRange->getEndDate()->getEndOfDay())
            ->getQuery()->execute();
    }

    /**
     * @param Subscription $subscription
     * @return Subscription|null
     */
    public function findPreviousSubscription(Subscription $subscription): ?Subscription
    {
        return $this->getRepository(Subscription::type())->findOneBy(['renewedBy' => $subscription]);
    }
}