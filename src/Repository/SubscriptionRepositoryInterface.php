<?php


namespace App\Repository;


use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Value\Time\DateRangeInterface;
use Doctrine\Common\Collections\Criteria;

interface SubscriptionRepositoryInterface
{
    public function findBy(Criteria $criteria): iterable;

    public function findPreviousSubscription(Subscription $subscription): ?Subscription;

    public function findSubscriptionsByCreatedDate(DateRangeInterface $date): array;
}