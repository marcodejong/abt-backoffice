<?php


namespace App\Repository;


use App\Model\Report\TariffConfiguration\PropositionTariffConfiguration;

interface PropositionTariffConfigurationRepository
{
    /**
     * @return PropositionTariffConfiguration[]
     */
    public function findPropositionTariffConfigurations(): array;
}