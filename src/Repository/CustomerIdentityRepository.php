<?php


namespace App\Repository;


use Arriva\Reporting\Entity\CustomerIdentity;
use SolidPhp\ValueObjects\Type\Type;
use Doctrine\Common\Persistence\ManagerRegistry;

class CustomerIdentityRepository implements CustomerIdentityRepositoryInterface
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function findByIdmId(int $idmId): ?CustomerIdentity
    {
        return $this->getRepository(Type::of(CustomerIdentity::class))->findOneBy(['idmId' => $idmId]);
    }
}
