<?php


namespace App\Repository;


use App\Model\Report\TariffConfiguration\PropositionTariffConfiguration;
use Arriva\Abt\Entity\PricePeriod;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Utility\Iterator;
use Doctrine\Common\Persistence\ManagerRegistry;

class DoctrinePropositionTariffConfigurationRepository implements PropositionTariffConfigurationRepository
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return PropositionTariffConfiguration[]
     */
    public function findPropositionTariffConfigurations(): array
    {
        $result = $this->getEntityManager(Proposition::type())->createQuery("
            SELECT
                p                  AS proposition,
                pg                 AS propositionGroup,
                vp                 AS validityPeriod,
                sp                 AS salePeriod,
                pt                 AS propositionTerm,
                pe                 AS propositionElements
            FROM Arriva\Abt\Entity\Proposition p
            JOIN p.propositionGroup pg
            JOIN p.validityPeriod vp
            JOIN p.salePeriod sp
            JOIN p.propositionTerm pt
            JOIN p.propositionElements pe
        ");

        return Iterator::create($result->getResult())->map(function(array $resultRow) {
            return $this->createPropositionTariffConfiguration($resultRow['proposition']);
        })->getArray();
    }

    private function createPropositionTariffConfiguration(Proposition $proposition): PropositionTariffConfiguration
    {
        return new PropositionTariffConfiguration(
            $proposition,
            $proposition->getPropositionGroup(),
            $proposition->getValidityPeriod(),
            $proposition->getSalePeriod(),
            $proposition->getPropositionTerm(),
            $proposition->getPropositionElements()->getValues()
        );
    }
}