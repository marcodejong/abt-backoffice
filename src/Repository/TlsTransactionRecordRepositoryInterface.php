<?php

namespace App\Repository;

use Arriva\Abt\Entity\Transaction\TlsTransactionRecord;

interface TlsTransactionRecordRepositoryInterface
{
    public function findById(int $transactionId): ?TlsTransactionRecord;
}
