<?php

namespace App\Repository;

use Arriva\Abt\Entity\Proposition;

interface PropositionRepositoryInterface
{
    public function findPropositionBySlug(string $slug): ?Proposition;
}
