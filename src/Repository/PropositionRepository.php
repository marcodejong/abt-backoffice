<?php

namespace App\Repository;

use Arriva\Abt\Entity\Proposition;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectRepository;

class PropositionRepository implements PropositionRepositoryInterface
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function findPropositionBySlug(string $slug): ?Proposition
    {
        return $this->getRepository(Proposition::type())->findOneBy(['slug' => $slug]);
    }
}
