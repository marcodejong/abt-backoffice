<?php

namespace App\Repository;

use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Value\InvoiceStatus;

interface TransactionReceivableRepositoryInterface
{
    public function getTransactionReceivablesSummaryForInvoice(Invoice $invoice): array;
}
