<?php

namespace App\Repository;

use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\Receivable\SubscriptionTermReceivable;
use Arriva\Abt\Entity\Receivable\TransactionReceivable;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use SolidPhp\ValueObjects\Type\Type;

class TransactionReceivableRepository implements TransactionReceivableRepositoryInterface
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @param Invoice $invoice
     * @return SubscriptionTermReceivable[] - grouped array
     */
    public function getTransactionReceivablesSummaryForInvoice(Invoice $invoice): array
    {
        /** @var EntityRepository $repository */
        $repository = $this->getRepository(Type::of(TransactionReceivable::class));

        return $repository->createQueryBuilder('tr')
            ->select('c.engravedId, SUM(tr.amount.amount) AS amount, COUNT(tr.id) AS numberOfTransactions')
            ->leftJoin('tr.invoiceItem', 'ii')
            ->leftJoin('tr.card', 'c')
            ->where('ii.invoice = :invoice')
            ->setParameters([
                'invoice' => $invoice
            ])
            ->groupBy('tr.card')
            ->getQuery()
            ->getResult();
    }

}
