<?php


namespace App\Repository;


use App\Model\Report\InvoicedTransactions\InvoicedTransaction;
use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\Receivable\TransactionReceivable;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Value\InvoiceStatus;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;

class InvoicedTransactionRepository
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function findInvoicedTransactionReceivables(Criteria $criteria)
    {
        $qb = $this->getEntityManager(TransactionReceivable::type())->createQueryBuilder()
            ->select('tr')
            ->from(TransactionReceivable::class, 'tr')
            ->andWhere(sprintf("i.status NOT IN ('%s', '%s')", InvoiceStatus::DRAFT(), InvoiceStatus::DELIVERING()))
            ->join(Invoice\Item\Collection::class, 'ii', 'WITH', 'ii.receivable = tr')
            ->join(Invoice::class, 'i', 'WITH', 'ii.invoice = i')
            ->addCriteria($criteria)
            ->orderBy('i.invoiceNumber', 'DESC');

        return Iterator::create($qb->getQuery()->iterate())
            ->map(
                function ($transactionReceivable) {
                    return $this->createInvoicedTransactionFromTransactionReceivable(reset($transactionReceivable));
                }
            );
    }

    private function createInvoicedTransactionFromTransactionReceivable(TransactionReceivable $transactionReceivable
    ): InvoicedTransaction {
        return new InvoicedTransaction(
            $transactionReceivable->getInvoiceItem()->getInvoice(),
            $transactionReceivable->getTransaction()
        );
    }
}