<?php


namespace App\Repository;


use App\Model\Report\TariffConfiguration\RegularPeTariffConfiguration;
use App\Model\Report\TariffConfiguration\RoutePeTariffConfiguration;
use App\Model\Report\TariffConfiguration\StarPeTariffConfiguration;
use Arriva\Abt\Entity\PricePeriod;

interface PropositionElementTariffConfigurationRepository
{
    /**
     * @param PricePeriod $pricePeriod
     * @return RegularPeTariffConfiguration[]
     */
    public function findRegularPropositionElementTariffConfigurationsForPricePeriod(PricePeriod $pricePeriod): array;

    /**
     * @param PricePeriod $pricePeriod
     * @return StarPeTariffConfiguration[]
     */
    public function findStarPropositionElementTariffConfigurationsForPricePeriod(PricePeriod $pricePeriod): array;

    /**
     * @param PricePeriod $pricePeriod
     * @return RoutePeTariffConfiguration[]
     */
    public function findRoutePropositionElementTariffConfigurationsForPricePeriod(PricePeriod $pricePeriod): array;
}