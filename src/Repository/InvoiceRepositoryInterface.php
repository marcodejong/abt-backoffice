<?php

namespace App\Repository;

use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\Receivable\SubscriptionTermReceivable;
use Arriva\Abt\Entity\Receivable\CreditReceivable;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Value\DateRange;
use Arriva\Abt\Value\InvoiceStatus;

interface InvoiceRepositoryInterface
{
    /**
     * @param int $invoiceId
     * @return null|Invoice
     */
    public function findInvoiceById(int $invoiceId): ?Invoice;

    public function findDraftInvoicesByIdentifiers(array $invoiceIdentifiers): array;

    /**
     * @param DateRange $period
     * @return Iterator&iterable<Invoice>
     */
    public function findInvoicesByInvoiceDate(DateRange $period): Iterator;

    public function bulkUpdateInvoiceStatus(array $invoices, InvoiceStatus $invoiceStatus): int;

    public function getNumberOfInvoicesInDeliveringState(): int;

    public function getGroupedSubscriptionTermReceivables(Invoice $invoice, string $groupBy): array;

    /**
     * @param Invoice $invoice
     * @param string $groupBy
     * @return SubscriptionTermReceivable[][]|CreditReceivable[][] - grouped array
     */
    public function getGroupedSubscriptionTermAndCreditReceivables(Invoice $invoice, string $groupBy): array;
}
