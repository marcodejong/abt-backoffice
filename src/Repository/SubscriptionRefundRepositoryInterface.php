<?php


namespace App\Repository;


use Doctrine\Common\Collections\Criteria;

interface SubscriptionRefundRepositoryInterface
{
    public function findBy(Criteria $criteria): iterable;
}