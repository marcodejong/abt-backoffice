<?php

namespace App\Repository;

use Arriva\Abt\Entity\Transaction\TlsTransactionRecord;
use Doctrine\Common\Persistence\ManagerRegistry;
use SolidPhp\ValueObjects\Type\Type;

class TlsTransactionRecordRepository implements TlsTransactionRecordRepositoryInterface
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function findById(int $transactionId): ?TlsTransactionRecord
    {
        return $this->getRepository(Type::of(TlsTransactionRecord::class))->findOneBy(['id' => $transactionId]);
    }

}
