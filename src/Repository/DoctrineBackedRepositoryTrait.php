<?php

namespace App\Repository;

use Arriva\Abt\Utility\Iterator;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use RuntimeException;
use SolidPhp\ValueObjects\Type\Type;
use function get_class;

trait DoctrineBackedRepositoryTrait
{
    /** @var ManagerRegistry */
    protected $managerRegistry;

    protected function getManager(Type $type): ObjectManager
    {
        if ($objectManager = $this->managerRegistry->getManagerForClass($type->getFullyQualifiedName())) {
            return $objectManager;
        }

        throw new RuntimeException(sprintf('Unable to find Doctrine object manager for type "%s"', $type));
    }

    protected function getEntityManager(Type $type): EntityManagerInterface
    {
        $objectManager = $this->getManager($type);

        if ($objectManager instanceof EntityManagerInterface) {
            return $objectManager;
        }

        throw new RuntimeException(sprintf('Doctrine object manager for type "%s" is not an EntityManager, but an "%s"', $type, get_class($objectManager)));
    }

    /**
     * @param Type $type
     *
     * @return ObjectRepository&Selectable
     */
    protected function getRepository(Type $type): ObjectRepository
    {
        if ($repository = $this->getManager($type)->getRepository($type->getFullyQualifiedName())) {
            return $repository;
        }

        throw new RuntimeException(sprintf('Unable to find Doctrine repository for type "%s"', $type));
    }

    /**
     * @param Type $type
     *
     * @return ObjectRepository&EntityRepository&Selectable
     */
    protected function getEntityRepository(Type $type): EntityRepository
    {
        if ($repository = $this->getEntityManager($type)->getRepository($type->getFullyQualifiedName())) {
            return $repository;
        }

        throw new RuntimeException(sprintf('Unable to find Doctrine repository for type "%s"', $type));
    }

    /**
     * @param Type     $type
     * @param Criteria $criteria
     *
     * @return object[]|Iterator
     */
    protected function findMatching(Type $type, Criteria $criteria): Iterator
    {
        return Iterator::create($this->getRepository($type)->matching($criteria));
    }
}
