<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\DiscountSummary;
use App\Model\Report\Journal\Summary\MoneysPayableSummary;
use App\Model\Report\Journal\Summary\MoneysReceivableSummary;
use App\Model\Report\Journal\Summary\MoneysReceivedSummary;
use App\Model\Report\Journal\Summary\TransactionSummary;
use App\Repository\DoctrineBackedRepositoryTrait;
use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Entity\OrderItem;
use Arriva\Abt\Entity\Receivable\SubscriptionTermReceivable;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Entity\SubscriptionRefund;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\Financial\VatCode;
use Arriva\Abt\Value\TAccount;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

class DoctrinePaymentSummaryRepository implements
    MoneysReceivedSummaryRepository,
    MoneysReceivableSummaryRepository,
    MoneysPayableSummaryRepository,
    DiscountSummaryRepository
{
    use DoctrineBackedRepositoryTrait;

    /** @var Currency */
    private $currency;

    /** @var TAccount */
    private $paymentProviderTAccount;

    /** @var TAccount */
    private $debtorTAccount;

    /** @var TAccount */
    private $discountGroupTAccount;

    public function __construct(ManagerRegistry $managerRegistry, Currency $currency, TAccount $paymentProviderTAccount, TAccount $debtorTAccount, TAccount $discountGroupTAccount)
    {
        $this->managerRegistry = $managerRegistry;
        $this->currency = $currency;
        $this->paymentProviderTAccount = $paymentProviderTAccount;
        $this->debtorTAccount = $debtorTAccount;
        $this->discountGroupTAccount = $discountGroupTAccount;
    }

    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return TransactionSummary[]|iterable
     */
    public function getDiscountSummaries(FinancialSummaryCriteria $criteria): iterable
    {
        // discount amounts: totals of discountAmounts for orderItems linked to orders in the date range grouped by tAccount

        $startDateTime = $criteria->getPeriod()->getStartDate()->getStartOfDay();
        $endDateTime = $criteria->getPeriod()->getEndDate()->getEndOfDay();

        $qb = $this->getEntityManager(OrderItem::type())->createQueryBuilder()
               ->addSelect('orderItem.tAccount tAccount')
               ->addSelect('orderItem.costUnitCode costUnitCode')
               ->addSelect('orderItem.vatCode vatCode')
               ->addSelect('SUM(orderItem.discountAmount) totalDiscountAmount')
               ->addSelect(
                   'AVG(orderItem.vatAmountWithoutDiscount/orderItem.amountWithoutDiscount*100) averageVatPercentage'
               )
               ->addSelect('COUNT(orderItem) nrOfTransactions')
               ->from(OrderItem::class, 'orderItem')
               ->leftJoin(Subscription::class, 's', 'WITH', 's.orderItem = orderItem')
               ->join('orderItem.originalOrder', 'o')
               ->groupBy('orderItem.tAccount')
               ->addGroupBy('orderItem.vatCode')
               ->andWhere('s.createdAt >= :startDate')
               ->andWhere('s.createdAt <= :endDate')
               ->setParameter('startDate', $startDateTime->format('Y-m-d H:i:s'))
               ->setParameter('endDate', $endDateTime->format('Y-m-d H:i:s'));

        return Iterator::create($qb->getQuery()->iterate())
                       ->filter(
                           static function (array $resultRow) {
                               $result = reset($resultRow);
                               return (float)$result['totalDiscountAmount'] > 0;
                           }
                       )->map(
                function (array $resultRow) {
                    return $this->createDiscountSummaryFromResultRow(reset($resultRow));
                }
            );
    }

    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return TransactionSummary[]|iterable
     * @throws NonUniqueResultException
     */
    public function getMoneysPayableSummaries(FinancialSummaryCriteria $criteria): iterable
    {
        $startDateTime = $criteria->getPeriod()->getStartDate()->getStartOfDay();
        $endDateTime = $criteria->getPeriod()->getEndDate()->getEndOfDay();

        $resultRow = $this->getEntityManager(OrderItem::type())->createQueryBuilder()
            ->addSelect('SUM(orderItem.amountWithoutDiscount - orderItem.discountAmount) totalAmountWithDiscount')
            ->addSelect(
                'SUM((orderItem.amountWithoutDiscount - orderItem.discountAmount) 
                    * orderItem.vatAmountWithoutDiscount / orderItem.amountWithoutDiscount) totalVatAmountWithDiscount')
            ->addSelect('COUNT(orderItem) nrOfTransactions')
            ->from(OrderItem::class, 'orderItem')
            ->leftJoin(Subscription::class, 's', 'WITH', 's.orderItem = orderItem')
            ->leftJoin(SubscriptionRefund::class, 'sr', 'WITH', 'sr.subscription = s')
            ->andWhere('sr.createdAt >= :startDate')
            ->andWhere('sr.createdAt <= :endDate')
            ->setParameter('startDate', $startDateTime->format('Y-m-d H:i:s'))
            ->setParameter('endDate', $endDateTime->format('Y-m-d H:i:s'))
            ->getQuery()->getOneOrNullResult();

        $totalAmountWithDiscount = Money::amount($resultRow['totalAmountWithDiscount'] ?? 0, $this->currency);
        $totalVatAmountWithDiscount = Money::amount($resultRow['totalVatAmountWithDiscount'] ?? 0, $this->currency);

        return [
            new MoneysPayableSummary(
                'Debiteuren',
                -(int)$resultRow['nrOfTransactions'],
                new VatMoney($totalAmountWithDiscount, $totalVatAmountWithDiscount, null),
                $this->debtorTAccount
            )
        ];
    }

    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return TransactionSummary[]|iterable
     * @throws NonUniqueResultException
     */
    public function getMoneysReceivableSummaries(FinancialSummaryCriteria $criteria): iterable
    {
        // received amount: total of paid receivables linked to orders in the date range
        $startDateTime = $criteria->getPeriod()->getStartDate()->getStartOfDay();
        $endDateTime = $criteria->getPeriod()->getEndDate()->getEndOfDay();

        $resultRow = $this->getEntityManager(OrderItem::type())->createQueryBuilder()
                          ->addSelect('SUM(receivable.amount.amount) totalAmount')
                          ->addSelect('SUM(receivable.amount.vatAmount) totalVatAmount')
                          ->addSelect('COUNT(orderItem) nrOfTransactions')
                          ->from(SubscriptionTermReceivable::class, 'receivable')
                          ->join('receivable.subscription', 'subscription')
                          ->join('subscription.orderItem', 'orderItem')
                          ->join('orderItem.originalOrder', 'o')
                          ->where('receivable.payment IS NULL')
                          ->andWhere('subscription.createdAt >= :startDate')
                          ->andWhere('subscription.createdAt <= :endDate')
                          ->setParameter('startDate', $startDateTime->format('Y-m-d H:i:s'))
                          ->setParameter('endDate', $endDateTime->format('Y-m-d H:i:s'))
                          ->getQuery()
                          ->getOneOrNullResult();

        $totalAmount = $resultRow['totalAmount']
            ? Money::amount($resultRow['totalAmount'], $this->currency)
            : Money::zero($this->currency);

        $totalVatAmount = $resultRow['totalVatAmount']
            ? Money::amount($resultRow['totalVatAmount'], $this->currency)
            : Money::zero($this->currency);

        return [
            new MoneysReceivableSummary(
                'Debiteuren',
                (int)$resultRow['nrOfTransactions'],
                new VatMoney(
                    $totalAmount,
                    $totalVatAmount,
                    null
                ),
                $this->debtorTAccount
            ),
        ];
    }

    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return TransactionSummary[]|iterable
     * @throws NonUniqueResultException
     */
    public function getMoneysReceivedSummaries(FinancialSummaryCriteria $criteria): iterable
    {
        // received amounts: totals of paid receivables linked to orders in the date range

        $startDateTime = $criteria->getPeriod()->getStartDate()->getStartOfDay();
        $endDateTime = $criteria->getPeriod()->getEndDate()->getEndOfDay();

        $resultRow = $this->getEntityManager(OrderItem::type())->createQueryBuilder()
                          ->addSelect('SUM(receivable.amount.amount) totalAmount')
                          ->addSelect('SUM(receivable.amount.vatAmount) totalVatAmount')
                          ->addSelect('SUM(receivable.amount.vatPercentage) vatPercentage')
                          ->addSelect('COUNT(orderItem) nrOfTransactions')
                          ->from(SubscriptionTermReceivable::class, 'receivable')
                          ->join('receivable.subscription', 'subscription')
                          ->join('subscription.orderItem', 'orderItem')
                          ->where('receivable.payment IS NOT NULL')
                          ->andWhere('receivable.createdAt >= :startDate')
                          ->andWhere('receivable.createdAt <= :endDate')
                          ->setParameter('startDate', $startDateTime->format('Y-m-d H:i:s'))
                          ->setParameter('endDate', $endDateTime->format('Y-m-d H:i:s'))
                          ->getQuery()
                          ->getOneOrNullResult();

        $totalAmount = $resultRow['totalAmount']
            ? Money::amount($resultRow['totalAmount'], $this->currency)
            : Money::zero($this->currency);

        $totalVatAmount = $resultRow['totalVatAmount']
            ? Money::amount($resultRow['totalVatAmount'], $this->currency)
            : Money::zero($this->currency);

        return [
            new MoneysReceivedSummary(
                'Icepay',
                (int)$resultRow['nrOfTransactions'],
                new VatMoney(
                    $totalAmount,
                    $totalVatAmount,
                    null
                ),
                $this->paymentProviderTAccount
            ),
        ];
    }

    private function createDiscountSummaryFromResultRow(array $resultRow): TransactionSummary
    {
        $totalDiscountAmount = $resultRow['totalDiscountAmount']
            ? Money::amount($resultRow['totalDiscountAmount'], Currency::EUR())
            : Money::zero($this->currency);

        $vatMoney = VatMoney::fromAmountWithVat($totalDiscountAmount, $resultRow['averageVatPercentage']);

        return new DiscountSummary(
            'Korting',
            (int)$resultRow['nrOfTransactions'],
            $vatMoney,
            VatCode::of($resultRow['vatCode']),
            TAccount::of($resultRow['tAccount'] ?? '0'),
            $this->discountGroupTAccount
        );
    }
}
