<?php


namespace App\Repository\Financial;


use App\Model\Report\ManagementSalesChannel\PropositionRefund;
use App\Repository\DoctrineBackedRepositoryTrait;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Discount\DiscountCode;
use Arriva\Abt\Value\Time\DateRangeInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

class DoctrinePropositionRefundRepository implements PropositionRefundRepository
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function findPropositionRefundsForPeriod(DateRangeInterface $dateRange): array
    {
        $query = $this->getEntityManager(Subscription::type())->createQuery("
            SELECT
                s            AS subscription,
                cu           AS customer, 
                c            AS card,
                p            AS payments,
                o            AS order,
                oi           AS orderItem                
            FROM Arriva\Abt\Entity\Subscription s
            LEFT JOIN s.customer cu
            LEFT JOIN s.orderItem oi
            LEFT JOIN oi.card c
            LEFT JOIN oi.originalOrder o
            LEFT JOIN o.payments p
            LEFT JOIN Arriva\Abt\Entity\SubscriptionRefund sr WITH sr.subscription = s
            WHERE sr.createdAt >= :startOfDay AND sr.createdAt <= :endOfDay
        ")
            ->setParameter('startOfDay', $dateRange->getStartDate()->getStartOfDay())
            ->setParameter('endOfDay', $dateRange->getEndDate()->getEndOfDay());

        $result = $query->getResult();

        return Iterator::create($result)->map(function (array $resultRow) {
            return $this->buildPropositionRefundFromSubscription($resultRow['subscription']);
        })->getArray();
    }

    private function buildPropositionRefundFromSubscription(Subscription $subscription): PropositionRefund
    {
        $orderItem = $subscription->getOrderItem();
        $order = $orderItem->getOriginalOrder();

        return new PropositionRefund(
            $subscription,
            $orderItem->getCard(),
            $subscription->getCustomer(),
            $subscription->getPropositionInstance(),
            $orderItem,
            $order,
            $order->getPayments()->first()
        );
    }
}