<?php


namespace App\Repository\Financial;



use App\Model\Report\Journal\Summary\MoneysReceivableSummary;
use App\Repository\DoctrineBackedRepositoryTrait;
use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\Invoice\Item\Collection;
use Arriva\Abt\Entity\Receivable\TransactionReceivable;
use Arriva\Abt\Entity\Transaction\TlsTransactionOverride;
use Arriva\Abt\Entity\Transaction\TlsTransactionRecord;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\TAccount;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineInvoicedMoneysReceivableSummaryRepository implements MoneysReceivableSummaryRepository
{
    use DoctrineBackedRepositoryTrait;

    /** @var Currency */
    private $currency;

    public function __construct(ManagerRegistry $managerRegistry, Currency $currency)
    {
        $this->managerRegistry = $managerRegistry;
        $this->currency = $currency;
    }

    public function getMoneysReceivableSummaries(FinancialSummaryCriteria $criteria): iterable
    {
        $startDateTime = $criteria->getPeriod()->getStartDate()->getStartOfDay();
        $endDateTime = $criteria->getPeriod()->getEndDate()->getEndOfDay();

        $manager = $this->getManager(Invoice::type());
        if ($manager instanceof EntityManagerInterface) {
            $resultRow = $manager->createQueryBuilder()
                ->addSelect('COUNT(ttr) nrOfTransactions')
                ->addSelect('SUM(
                    CASE
                        WHEN tto.id IS NOT NULL THEN tto.override.priceAfterRating 
                        WHEN ttr.propValueAfterRating IS NOT NULL THEN ttr.propValueAfterRating
                        ELSE ttr.fceValue
                    END) totalAmountWithDiscount')
                ->from(Invoice::class, 'invoice')
                ->leftJoin(Collection::class, 'ii', 'WITH', 'ii.invoice = invoice')
                ->leftJoin(TransactionReceivable::class, 'tr', 'WITH', 'ii.receivable = tr')
                ->leftJoin(TlsTransactionRecord::class, 'ttr', 'WITH', 'tr.transaction = ttr')
                ->leftJoin(TlsTransactionOverride::class, 'tto', 'WITH', 'tto.transaction = ttr')
                ->andWhere('invoice.invoiceDate >= :startDate')
                ->andWhere('invoice.invoiceDate <= :endDate')
                ->setParameter('startDate', $startDateTime->format('Y-m-d H:i:s'))
                ->setParameter('endDate', $endDateTime->format('Y-m-d H:i:s'))
                ->getQuery()
                ->getOneOrNullResult();

            $totalAmountWithDiscount = $resultRow['totalAmountWithDiscount']
                ? Money::amount($resultRow['totalAmountWithDiscount'], Currency::EUR())
                : Money::zero($this->currency);

            return [
                new MoneysReceivableSummary(
                    'Debiteuren',
                    (int)$resultRow['nrOfTransactions'],
                    VatMoney::fromAmountWithVat($totalAmountWithDiscount, 9),
                    TAccount::of('115021')
                )
            ];
        }

        throw new \RuntimeException('EntityManager not available');
    }
}