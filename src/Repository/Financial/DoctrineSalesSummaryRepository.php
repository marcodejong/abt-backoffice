<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\SalesSummary;
use App\Model\Report\Journal\Summary\TransactionSummary;
use App\Repository\DoctrineBackedRepositoryTrait;
use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Entity\OrderItem;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Financial\CostCenter;
use Arriva\Abt\Value\Financial\VatCode;
use Arriva\Abt\Value\OrderStatus;
use Arriva\Abt\Value\TAccount;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class DoctrineSalesSummaryRepository implements SalesSummaryRepository
{
    use DoctrineBackedRepositoryTrait;

    /** @var Currency */
    private $currency;

    /** @var TAccount */
    private $defaultTAccount;

    public function __construct(ManagerRegistry $managerRegistry, Currency $currency, TAccount $defaultTAccount)
    {
        $this->managerRegistry = $managerRegistry;
        $this->currency = $currency;
        $this->defaultTAccount = $defaultTAccount;
    }

    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return TransactionSummary[]|iterable
     */
    public function findSalesSummariesPerCostUnit(FinancialSummaryCriteria $criteria): iterable
    {
        $startDateTime = $criteria->getPeriod()->getStartDate()->getStartOfDay();
        $endDateTime = $criteria->getPeriod()->getEndDate()->getEndOfDay();

        $manager = $this->getManager(OrderItem::type());
        if ($manager instanceof EntityManagerInterface) {
            $qb = $manager->createQueryBuilder()
                          ->addSelect('orderItem.tAccount tAccount')
                          ->addSelect('orderItem.costUnitCode costUnitCode')
                          ->addSelect('orderItem.vatCode vatCode')
                          ->addSelect('SUM(orderItem.amountWithoutDiscount) totalAmountWithoutDiscount')
                          ->addSelect('SUM(orderItem.vatAmountWithoutDiscount) totalVatAmountWithoutDiscount')
                          ->addSelect('orderItem.vatPercentage vatPercentage')
                          ->addSelect('COUNT(orderItem) nrOfTransactions')
                          ->addSelect('s.createdAt saleDate')
                          ->from(OrderItem::class, 'orderItem')
                          ->leftJoin(Subscription::class, 's', 'WITH', 's.orderItem = orderItem')
                          ->groupBy('orderItem.tAccount')
                          ->addGroupBy('orderItem.costUnitCode')
                          ->addGroupBy('orderItem.vatPercentage')
                          ->addGroupBy('orderItem.vatCode')
                          ->andWhere('s.createdAt >= :startDate')
                          ->andWhere('s.createdAt <= :endDate')
                          ->setParameter('startDate', $startDateTime->format('Y-m-d H:i:s'))
                          ->setParameter('endDate', $endDateTime->format('Y-m-d H:i:s'));

            return Iterator::create($qb->getQuery()->iterate())
                           ->map(
                               function ($resultRow) {
                                   return $this->createSalesSummaryFromResultRow(reset($resultRow));
                               }
                           );
        }

        throw new RuntimeException('EntityManager not available');
    }

    private function createSalesSummaryFromResultRow($resultRow): TransactionSummary
    {
        return new SalesSummary(
            '',
            (int)$resultRow['nrOfTransactions'],
            new VatMoney(
                Money::amount($resultRow['totalAmountWithoutDiscount'] ?? 0, $this->currency),
                Money::amount($resultRow['totalVatAmountWithoutDiscount'] ?? 0, $this->currency),
                $resultRow['vatPercentage']
            ),
            VatCode::of($resultRow['vatCode']),
            $resultRow['tAccount'] ? TAccount::of($resultRow['tAccount']) : $this->defaultTAccount,
            CostCenter::of('0'),
            $resultRow['costUnitCode'] ? CostUnitCode::of($resultRow['costUnitCode']) : null,
            $resultRow['saleDate']
        );
    }
}
