<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\TransactionSummary;

interface MoneysReceivedSummaryRepository
{
    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return TransactionSummary[]|iterable
     */
    public function getMoneysReceivedSummaries(FinancialSummaryCriteria $criteria): iterable;
}
