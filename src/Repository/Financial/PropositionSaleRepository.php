<?php


namespace App\Repository\Financial;


use App\Model\Report\ManagementSalesChannel\PropositionSale;
use Arriva\Abt\Value\Time\DateRangeInterface;

interface PropositionSaleRepository
{
    /**
     * @param DateRangeInterface $dateRange
     * @return PropositionSale[]
     */
    public function findPropositionSalesForPeriod(DateRangeInterface $dateRange): array;
}