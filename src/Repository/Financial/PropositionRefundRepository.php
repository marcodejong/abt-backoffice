<?php


namespace App\Repository\Financial;


use App\Model\Report\ManagementSalesChannel\PropositionRefund;
use Arriva\Abt\Value\Time\DateRangeInterface;

interface PropositionRefundRepository
{
    /**
     * @param DateRangeInterface $dateRange
     * @return PropositionRefund[]
     */
    public function findPropositionRefundsForPeriod(DateRangeInterface $dateRange): array;
}