<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\TransactionSummary;

interface DiscountSummaryRepository
{
    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return TransactionSummary[]|iterable
     */
    public function getDiscountSummaries(FinancialSummaryCriteria $criteria): iterable;
}
