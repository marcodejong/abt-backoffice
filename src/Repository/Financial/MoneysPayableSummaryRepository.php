<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\TransactionSummary;
use Doctrine\Common\Collections\Criteria;

interface MoneysPayableSummaryRepository
{
    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return TransactionSummary[]|iterable
     */
    public function getMoneysPayableSummaries(FinancialSummaryCriteria $criteria): iterable;
}
