<?php

namespace App\Repository\Financial;

use Arriva\Abt\Value\Time\DateRangeInterface;

class FinancialSummaryCriteria
{
    /** @var DateRangeInterface */
    private $period;

    public function __construct(DateRangeInterface $period)
    {
        $this->period = $period;
    }

    public function getPeriod(): DateRangeInterface
    {
        return $this->period;
    }
}
