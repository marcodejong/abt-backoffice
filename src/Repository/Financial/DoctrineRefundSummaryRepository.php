<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\RefundSummary;
use App\Model\Report\Journal\Summary\TransactionSummary;
use App\Repository\DoctrineBackedRepositoryTrait;
use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Entity\OrderItem;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Entity\SubscriptionRefund;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Financial\CostCenter;
use Arriva\Abt\Value\Financial\VatCode;
use Arriva\Abt\Value\TAccount;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class DoctrineRefundSummaryRepository implements RefundSummaryRepository
{
    use DoctrineBackedRepositoryTrait;

    /** @var Currency */
    private $currency;

    /** @var TAccount */
    private $defaultTAccount;

    public function __construct(ManagerRegistry $managerRegistry, Currency $currency, TAccount $defaultTAccount)
    {
        $this->managerRegistry = $managerRegistry;
        $this->currency = $currency;
        $this->defaultTAccount = $defaultTAccount;
    }

    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return RefundSummary[]|iterable
     */
    public function findRefundSummariesPerCostUnit(FinancialSummaryCriteria $criteria): iterable
    {
        $startDateTime = $criteria->getPeriod()->getStartDate()->getStartOfDay();
        $endDateTime = $criteria->getPeriod()->getEndDate()->getEndOfDay();

        $manager = $this->getManager(OrderItem::type());
        if ($manager instanceof EntityManagerInterface) {
            $qb = $manager->createQueryBuilder()
                ->addSelect('orderItem.tAccount tAccount')
                ->addSelect('orderItem.costUnitCode costUnitCode')
                ->addSelect('orderItem.vatCode vatCode')
                ->addSelect('SUM(orderItem.amountWithoutDiscount - orderItem.discountAmount) totalAmountWithDiscount')
                ->addSelect(
                    'SUM((orderItem.amountWithoutDiscount - orderItem.discountAmount) 
                    * orderItem.vatAmountWithoutDiscount / orderItem.amountWithoutDiscount) totalVatAmountWithDiscount')
                ->addSelect('orderItem.vatPercentage vatPercentage')
                ->addSelect('COUNT(orderItem) nrOfTransactions')
                ->addSelect('sr.createdAt refundDate')
                ->from(OrderItem::class, 'orderItem')
                ->leftJoin(Subscription::class, 's', 'WITH', 's.orderItem = orderItem')
                ->leftJoin(SubscriptionRefund::class, 'sr', 'WITH', 'sr.subscription = s')
                ->groupBy('orderItem.tAccount')
                ->addGroupBy('orderItem.costUnitCode')
                ->addGroupBy('orderItem.vatPercentage')
                ->addGroupBy('orderItem.vatCode')
                ->andWhere('sr.createdAt >= :startDate')
                ->andWhere('sr.createdAt <= :endDate')
                ->setParameter('startDate', $startDateTime->format('Y-m-d H:i:s'))
                ->setParameter('endDate', $endDateTime->format('Y-m-d H:i:s'));

            return Iterator::create($qb->getQuery()->iterate())
                ->map(
                    function ($resultRow) {
                        return $this->createRefundSummaryFromResultRow(reset($resultRow));
                    }
                );
        }

        throw new RuntimeException('EntityManager not available');
    }

    private function createRefundSummaryFromResultRow($resultRow): TransactionSummary
    {
        return new RefundSummary(
            '',
            -(int)$resultRow['nrOfTransactions'],
            new VatMoney(
                Money::amount($resultRow['totalAmountWithDiscount'] ?? 0, $this->currency),
                Money::amount($resultRow['totalVatAmountWithDiscount'] ?? 0, $this->currency),
                $resultRow['vatPercentage']
            ),
            VatCode::of($resultRow['vatCode']),
            $resultRow['tAccount'] ? TAccount::of($resultRow['tAccount']) : $this->defaultTAccount,
            CostCenter::of('0'),
            $resultRow['costUnitCode'] ? CostUnitCode::of($resultRow['costUnitCode']) : null,
            $resultRow['refundDate']
        );
    }
}
