<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\SalesSummary;

interface SalesSummaryRepository
{
    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return iterable<SalesSummary>
     */
    public function findSalesSummariesPerCostUnit(FinancialSummaryCriteria $criteria): iterable;
}
