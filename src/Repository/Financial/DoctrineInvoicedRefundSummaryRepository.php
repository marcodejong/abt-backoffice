<?php


namespace App\Repository\Financial;


use App\Model\Report\Journal\Summary\RefundSummary;
use App\Repository\DoctrineBackedRepositoryTrait;
use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\Invoice\Item\Collection;
use Arriva\Abt\Entity\PublicTransportOperator;
use Arriva\Abt\Entity\Receivable\TransactionReceivable;
use Arriva\Abt\Entity\Region;
use Arriva\Abt\Entity\Transaction\TlsTransactionOverride;
use Arriva\Abt\Entity\Transaction\TlsTransactionRecord;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Financial\CostCenter;
use Arriva\Abt\Value\Financial\VatCode;
use Arriva\Abt\Value\TAccount;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class DoctrineInvoicedRefundSummaryRepository implements RefundSummaryRepository
{
    use DoctrineBackedRepositoryTrait;

    /** @var Currency */
    private $currency;

    public function __construct(ManagerRegistry $managerRegistry, Currency $currency)
    {
        $this->managerRegistry = $managerRegistry;
        $this->currency = $currency;
    }

    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return iterable<RefundSummary>
     */
    public function findRefundSummariesPerCostUnit(FinancialSummaryCriteria $criteria): iterable
    {
        $startDateTime = $criteria->getPeriod()->getStartDate()->getStartOfDay();
        $endDateTime = $criteria->getPeriod()->getEndDate()->getEndOfDay();

        $manager = $this->getManager(Invoice::type());
        if ($manager instanceof EntityManagerInterface) {
            $qb = $manager->createQueryBuilder()
                ->addSelect('
                    CASE 
                        WHEN pto.tlsId = 8 THEN 860001 
                        ELSE 860002 
                    END AS tAccount'
                )
                ->addSelect('region.costUnitCode costUnitCode')
                ->addSelect('COUNT(ttr) nrOfTransactions')
                ->addSelect('SUM(
                        CASE
                            WHEN ttr.propValueAfterRating IS NOT NULL THEN ttr.propValueAfterRating  
                            ELSE ttr.fceValue
                        END) totalAmountWithDiscount')
                ->from(Invoice::class, 'invoice')
                ->leftJoin(Collection::class, 'ii', 'WITH', 'ii.invoice = invoice')
                ->leftJoin(TransactionReceivable::class, 'tr', 'WITH', 'ii.receivable = tr')
                ->leftJoin(TlsTransactionRecord::class, 'ttr', 'WITH', 'tr.transaction = ttr')
                ->leftJoin(TlsTransactionOverride::class, 'tto', 'WITH', 'tto.transaction = ttr')
                ->leftJoin(PublicTransportOperator::class, 'pto', 'WITH', 'ttr.serviceProvider = pto')
                ->leftJoin(Region::class, 'region', 'WITH', 'region.concessionId = ttr.concessionId')
                ->groupBy('tAccount')
                ->addGroupBy('costUnitCode')
                ->andWhere('tto.override.type = :overrideType')
                ->andWhere('invoice.invoiceDate >= :startDate')
                ->andWhere('invoice.invoiceDate <= :endDate')
                ->having('nrOfTransactions > 0')
                ->setParameter('overrideType', 'credit')
                ->setParameter('startDate', $startDateTime->format('Y-m-d H:i:s'))
                ->setParameter('endDate', $endDateTime->format('Y-m-d H:i:s'));

            return Iterator::create($qb->getQuery()->iterate())
                ->map(
                    function ($resultRow) {
                        return $this->createRefundSummaryFromResultRow(reset($resultRow));
                    }
                );
        }

        throw new RuntimeException('EntityManager not available');
    }

    private function createRefundSummaryFromResultRow($resultRow): RefundSummary
    {
        $totalAmountWithDiscount = $resultRow['totalAmountWithDiscount']
            ? Money::amount($resultRow['totalAmountWithDiscount'], Currency::EUR())
            : Money::zero($this->currency);

        return new RefundSummary(
            '',
            (int)$resultRow['nrOfTransactions'],
            VatMoney::fromAmountWithVat($totalAmountWithDiscount, 9),
            VatCode::of('VAT002'),
            TAccount::of($resultRow['tAccount']),
            CostCenter::of('1'),
            $resultRow['costUnitCode'] ? CostUnitCode::of($resultRow['costUnitCode']) : CostUnitCode::of('1040')
        );
    }
}