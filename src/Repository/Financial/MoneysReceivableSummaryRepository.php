<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\TransactionSummary;

interface MoneysReceivableSummaryRepository
{
    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return iterable<TransactionSummary>
     */
    public function getMoneysReceivableSummaries(FinancialSummaryCriteria $criteria): iterable;
}
