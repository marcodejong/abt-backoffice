<?php

namespace App\Repository\Financial;

use App\Model\Report\Journal\Summary\RefundSummary;
use Doctrine\Common\Collections\Criteria;

interface RefundSummaryRepository
{
    /**
     * @param FinancialSummaryCriteria $criteria
     *
     * @return RefundSummary[]|iterable
     */
    public function findRefundSummariesPerCostUnit(FinancialSummaryCriteria $criteria): iterable;
}
