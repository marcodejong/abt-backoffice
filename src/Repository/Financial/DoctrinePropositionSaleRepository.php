<?php


namespace App\Repository\Financial;


use App\Model\Report\ManagementSalesChannel\PropositionSale;
use App\Repository\DoctrineBackedRepositoryTrait;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Value\Time\DateRangeInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

class DoctrinePropositionSaleRepository implements PropositionSaleRepository
{
    use DoctrineBackedRepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @param DateRangeInterface $dateRange
     * @return PropositionSale[]
     */
    public function findPropositionSalesForPeriod(DateRangeInterface $dateRange): array
    {
        $query = $this->getEntityManager(Subscription::type())->createQuery("
            SELECT
                s            AS subscription,
                cu           AS customer, 
                c            AS card,
                p            AS payments,
                o            AS order,
                oi           AS orderItem                
            FROM Arriva\Abt\Entity\Subscription s
            LEFT JOIN s.customer cu
            LEFT JOIN s.orderItem oi
            LEFT JOIN oi.card c
            LEFT JOIN oi.originalOrder o
            LEFT JOIN o.payments p
            LEFT JOIN Arriva\Abt\Entity\SubscriptionRefund sr WITH sr.subscription = s
            WHERE s.createdAt >= :startOfDay AND s.createdAt <= :endOfDay AND sr IS NULL
        ")
            ->setParameter('startOfDay', $dateRange->getStartDate()->getStartOfDay())
            ->setParameter('endOfDay', $dateRange->getEndDate()->getEndOfDay());

        return Iterator::create($query->getResult())->map(function (array $resultRow) {
            return $this->buildPropositionSaleFromSubscription($resultRow['subscription']);
        })->getArray();
    }

    private function buildPropositionSaleFromSubscription(Subscription $subscription): PropositionSale
    {
        $orderItem = $subscription->getOrderItem();
        $order = $orderItem->getOriginalOrder();

        return new PropositionSale(
            $subscription,
            $orderItem->getCard(),
            $subscription->getCustomer(),
            $subscription->getPropositionInstance(),
            $orderItem,
            $order,
            $order->getPayments()->first()
        );
    }
}