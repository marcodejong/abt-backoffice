<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Datatable\Filter\IntegerFilter;
use App\Manager\PropositionTypeManager;
use Arriva\Abt\Entity\PropositionCategory;
use Arriva\Abt\Entity\PropositionGroup;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;

/**
 * @package App\Datatables
 */
class PropositionGroupDatatable extends AbstractDatatable
{

    /** @var PropositionTypeManager */
    private $propositionTypeManager;

    public function setPropositionTypeManager(PropositionTypeManager $propositionTypeManager): void
    {
        $this->propositionTypeManager = $propositionTypeManager;
    }

    public function getPropositionTypeManager(): PropositionTypeManager
    {
        return $this->propositionTypeManager;
    }

    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.proposition-group.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'propositionType.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'proposition-type'),
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] + $this->getPropositionTypes(),
                            'search_type' => 'eq'
                        ],
                    ],
                ]
            )
            ->add(
                'category',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'category'),
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] + [
                                    PropositionCategory::REGULAR()->getId() => "Regulier",
                                    PropositionCategory::STAR()->getId() => "Ster",
                                    PropositionCategory::ROUTE()->getId() => "Traject",
                                ],
                            'search_type' => 'like'
                        ],
                    ]
                ]
            )
            ->add(
                'vatRate.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'vat-rate'),
                ]
            )
            ->add(
                'order',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'order'),
                    'filter' => [
                        IntegerFilter::class,
                        [],
                    ],
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_propositiongroup_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_propositiongroup_edit',
                'routeParamName' => 'propositionGroupId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_propositiongroup_delete',
                'routeParamName' => 'propositionGroupId',
            ],
        ];
    }

    protected function formatLine(array $row): array
    {

        if ($row['category'] instanceof PropositionCategory) {
            if($row['category'] === PropositionCategory::REGULAR()) {
                $row['category'] = 'Regulier';
            } else if($row['category'] === PropositionCategory::STAR()) {
                $row['category'] = 'Ster';
            } else if($row['category'] === PropositionCategory::ROUTE()) {
                $row['category'] = 'Traject';
            }
        }

        return $row;
    }

    public function getEntity(): string
    {
        return PropositionGroup::class;
    }

    public function getPropositionTypes(): array
    {
        $propositionTypes = $this->getPropositionTypeManager()->getAllPropositionTypes();
        $result = [];
        foreach ($propositionTypes as $propositionType) {
            $name = $propositionType->getName();
            $result[$name] = $name;
        }

        return $result;
    }
}
