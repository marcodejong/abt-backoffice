<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\PricePeriod;
use Arriva\Abt\Value\Date;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;


/**
 * @package App\Datatables
 */
class PricePeriodDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.priceperiod.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'fromDate',
                DateTimeColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'datefrom'),
                    'date_format' => 'DD-MM-YYYY',
                ]
            )
            ->add(
                'toDate',
                DateTimeColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'dateto'),
                    'date_format' => 'DD-MM-YYYY',
                ]
            );
    }

    protected function formatLine(array $row): array
    {

        if ($row['toDate'] instanceof  Date) {
            $row['toDate'] = $row['toDate']->getDateTime();
        }
        if ($row['fromDate'] instanceof  Date) {
            $row['fromDate'] = $row['fromDate']->getDateTime();
        }

        return $row;
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_priceperiod_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_priceperiod_edit',
                'routeParamName' => 'pricePeriodId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_priceperiod_delete',
                'routeParamName' => 'pricePeriodId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return PricePeriod::class;
    }
}
