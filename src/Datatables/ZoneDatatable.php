<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Datatable\Filter\IntegerFilter;
use App\Manager\RegionManager;
use Arriva\Abt\Entity\Zone;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;

/**
 * @package App\Datatables
 */
class ZoneDatatable extends AbstractDatatable
{

    /** @var RegionManager */
    private $regionManager;

    public function setRegionManager(RegionManager $regionManager): void
    {
        $this->regionManager = $regionManager;
    }

    public function getRegionManager(): RegionManager
    {
        return $this->regionManager;
    }

    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.zone.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'code',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'code'),
                    'filter' => [
                        IntegerFilter::class,
                        [],
                    ],
                ]
            )
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'active',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'active'),
                ]
            )
            ->add(
                'regions.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'region'),
                    'data' => 'regions[, ].name',
                    'orderable' => false,
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] + $this->getRegionSelectOptions(),
                            'search_type' => 'like'
                        ],
                    ],
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_IMPORT => [
                'routeName' => 'app_administration_zone_import',
                'routeParamName' => null,
            ],
            self::ACTION_EXPORT => [
                'routeName' => 'app_administration_zone_export',
                'routeParamName' => 'codes',
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_zone_edit',
                'routeParamName' => 'zoneId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_zone_delete',
                'routeParamName' => 'zoneId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return Zone::class;
    }

    public function getRegionSelectOptions(): array
    {
        $regions = $this->getRegionManager()->getRegions();
        $result = [];
        foreach ($regions as $region) {
            $name = $region->getName();
            $result[$name] = $name;
        }

        return $result;
    }
}
