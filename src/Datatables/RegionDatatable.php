<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Manager\WebshopRegionManager;
use Arriva\Abt\Entity\Region;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;

/**
 * @package App\Datatables
 */
class RegionDatatable extends AbstractDatatable
{
    /** @var WebshopRegionManager */
    private $webshopRegionManager;

    public function setWebshopRegionManager(WebshopRegionManager $webshopRegionManager): void
    {
        $this->webshopRegionManager = $webshopRegionManager;
    }

    public function getWebshopRegionManager(): WebshopRegionManager
    {
        return $this->webshopRegionManager;
    }

    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.region.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'code',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'code'),
                ]
            )
            ->add(
                'costUnitName',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'cost-unit'),
                ]
            )
            ->add(
                'costUnitCode',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'cost-unit-code'),
                ]
            )
            ->add(
                'concessionId',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'concession-id')
                ]
            )
            ->add(
                'webshopRegion.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'webshop-region'),
                    'default_content' => '',
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] +
                                $this->getOptionsArrayFromEntities($this->getWebshopRegions(), 'name', 'name'),
                            'search_type' => 'eq'
                        ]
                    ]
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_region_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_region_edit',
                'routeParamName' => 'regionId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_region_delete',
                'routeParamName' => 'regionId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return Region::class;
    }

    private function getWebshopRegions(): array
    {
        return $this->getWebshopRegionManager()->getAllWebshopRegions();
    }
}
