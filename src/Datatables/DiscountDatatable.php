<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\Discount;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\Discount\DiscountType;
use Arriva\Abt\Value\Discount\DiscountCode;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;

/**
 * @package App\Datatables
 */
class DiscountDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.discount.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'code',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'code'),
                ]
            )
            ->add(
                'type',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'type'),
                ]
            )
            ->add(
                'startDate',
                DateTimeColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'start-date'),
                    'date_format' => 'YYYY-MM-DD',
                ]
            )
            ->add(
                'endDate',
                DateTimeColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'end-date'),
                    'date_format' => 'YYYY-MM-DD',
                ]
            )
            ->add(
                'percentage',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'percentage'),
                ]
            )
            ->add(
                'amount',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'amount'),
                ]
            );
    }

    protected function formatLine(array $row): array
    {

        if ($row['code'] instanceof DiscountCode) {
            $row['code'] = $row['code']->getCode();
        }
        if ($row['type'] instanceof DiscountType) {
            $row['type'] = $row['type']->getId();
        }
        if ($row['startDate'] instanceof  Date) {
            $row['startDate'] = $row['startDate']->getDateTime();
        }
        if ($row['endDate'] instanceof  Date) {
            $row['endDate'] = $row['endDate']->getDateTime();
        }

        $row['percentage'] = $row['percentage'] ? '%' : '€';

        return $row;
    }



    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_discount_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_discount_edit',
                'routeParamName' => 'discountId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_discount_delete',
                'routeParamName' => 'discountId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return Discount::class;
    }
}
