<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\EmailTemplate;
use Sg\DatatablesBundle\Datatable\Column\Column;


/**
 * @package App\Datatables
 */
class EmailTemplateDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.email-template.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'emailSender',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'email_sender'),
                ]
            )
            ->add(
                'subject',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'subject'),
                ]
            )
            ->add(
                'description',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'description'),
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_emailtemplate_edit',
                'routeParamName' => 'emailTemplateId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return EmailTemplate::class;
    }
}
