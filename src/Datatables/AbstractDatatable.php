<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Manager\CustomerManager;
use Sg\DatatablesBundle\Datatable\AbstractDatatable as BaseAbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @package App\Datatables
 */
abstract class AbstractDatatable extends BaseAbstractDatatable
{
    public const ACTION_CREATE = 'create';
    public const ACTION_COPY = 'copy';
    public const ACTION_READ = 'read';
    public const ACTION_UPDATE = 'update';
    public const ACTION_DELETE = 'delete';
    public const ACTION_IMPORT = 'import';
    public const ACTION_EXPORT = 'export';

    /** @var RequestStack */
    private $requestStack;

    /** @var CustomerManager */
    private $customerManager;

    public function setRequestStack(RequestStack $requestStack): void
    {
        $this->requestStack = $requestStack;
    }

    public function setCustomerManager(CustomerManager $customerManager): void
    {
        $this->customerManager = $customerManager;
    }

    /**
     * Returns the name of this datatable view.
     *
     * @return string
     * @throws \ReflectionException
     */
    public function getName(): string
    {
        $reflectionClass = new \ReflectionClass($this->getEntity());

        return \strtolower($reflectionClass->getShortName()).'_datatable';
    }

    /**
     * @param array $options
     * @throws \Exception
     * @return void
     */
    final public function buildDatatable(array $options = []): void
    {
        $this->getLanguage()->setLanguageByLocale(true);
        $this->getAjax()->set(array_key_exists('ajax', $options) ? $options['ajax'] : []);
        $this->features->set(
            $this->buildFeatures(
                [
                    'auto_width' => false,
                    'scroll_x' => true,
                    'scroll_y' => 'Math.abs(dataTableHeight)',
                    'info' => false,
                    'state_save' => true,
                    'length_change' => false,
                ]
            )
        );
        $this->options->set(
            $this->buildOptions(
                [
                    'classes' => 'datatable '.Style::FOUNDATION_STYLE,
                    'individual_filtering' => true,
                    'individual_filtering_position' => 'head',
                    'order_cells_top' => true,
                    'page_length' => 150,
                    'paging_type' => 'full_numbers',
                    'scroll_collapse' => true,
                    'order' => [
                        [0, 'asc'],
                    ],
                    'dom' => 'ltp',
                    'search_in_non_visible_columns' => true,

                    // Use session storage to save data table state instead of local storage
                    'state_duration' => -1,
                ]
            )
        );
        $this->callbacks->setStateSaveCallback(['template' => 'datatable/state_save_callback.js.twig']);
        $this->callbacks->setStateLoadCallback(['template' => 'datatable/state_load_callback.js.twig']);
        $this->callbacks->setDrawCallback(
            [
                'template' => 'datatable/draw_callback.js.twig',
                'vars' => [
                    'datatable_name' => 'sg-datatables-'.$this->getName(),
                ],
            ]
        );
        $this->extensions->setSelect(
            [
                'blurable' => false,
                'class_name' => 'row_selected',
                'info' => true,
                'items' => 'row',
                'selector' => 'td, th',
                'style' => 'single',
            ]
        );

        $this->buildColumns($options);
    }

    public function getLineFormatter(): callable
    {
        $globalAllowedActions = [];
        foreach ($this->getActions() as $actionName => $routeConfig) {
            if ($this->authorizationChecker->isGranted(null, $routeConfig['routeName'])) {
                $globalAllowedActions[$actionName] = $routeConfig;
            }
        }

        return function ($row) use ($globalAllowedActions) {
            $row['allowedActions'] = $this->getAllowedActionsForRow($row, array_keys($globalAllowedActions));

            return $this->formatLine($row);
        };
    }

    /**
     * Can be overridden to prevent actions based on the row data result should be the actions
     *
     * @param array $row
     * @param string[] $globalAllowedActions
     * @return array
     */
    protected function getAllowedActionsForRow(array $row, array $globalAllowedActions): array
    {
        return $globalAllowedActions;
    }

    /**
     * Can be overridden to add/modify/remove features
     *
     * @param array $features
     * @return array
     */
    protected function buildFeatures(array $features): array
    {
        return $features;
    }

    /**
     * Can be overridden to add/modify/remove options
     *
     * @param array $options
     * @return array
     */
    protected function buildOptions(array $options): array
    {
        return $options;
    }

    /**
     * Can be overridden to add/modify row data
     *
     * @param array $row
     * @return array
     */
    protected function formatLine(array $row): array
    {
        return $row;
    }

    /**
     * actionName => [routeName => 'routeName', routeParamName => 'routeParamName']
     *
     * @return array
     */
    public function getActions(): array
    {
        return [];
    }

    /**
     * Build columns or other table specific things
     *
     * @see buildDatatable
     * @param array $options
     */
    abstract protected function buildColumns(array $options = []): void;

    protected function getCurrencyFormatter(): \NumberFormatter
    {
        return new \NumberFormatter($this->getRequest()->getLocale(), \NumberFormatter::CURRENCY);
    }

    private function getRequest(): Request
    {
        if (!$this->requestStack) {
            throw new \LogicException('setRequestStack must be called first. No RequestStack injected');
        }

        return $this->requestStack->getMasterRequest();
    }

    public function getCustomerManager(): CustomerManager
    {
        if (!$this->customerManager) {
            throw new \LogicException('setCustomer must be called first. No CustomerManager injected');
        }

        return $this->customerManager;
    }
}
