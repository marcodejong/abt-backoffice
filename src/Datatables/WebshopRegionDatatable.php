<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Datatable\Filter\IntegerFilter;
use Arriva\Abt\Entity\WebshopRegion;
use Sg\DatatablesBundle\Datatable\Column\Column;

/**
 * @package App\Datatables
 */
class WebshopRegionDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.webshop-region.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'externalId',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'external-id'),
                    'filter' => [
                        IntegerFilter::class,
                        [],
                    ],
                ]
            )
            ->add(
                'externalName',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'external-name'),
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_webshopregion_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_webshopregion_edit',
                'routeParamName' => 'webshopRegionId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_webshopregion_delete',
                'routeParamName' => 'webshopRegionId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return WebshopRegion::class;
    }
}
