<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\CustomerGroup;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;

/**
 * @package App\Datatables
 */
class CustomerGroupDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.customergroup.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'code',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'code'),
                ]
            )
            ->add(
                'ageFrom',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'agefrom'),
                    'filter' => [
                        NumberFilter::class,
                        [
                            'search_type' => 'gte',
                            'type' => 'number',
                            'min' => '1',
                        ],
                    ],
                ]
            )
            ->add(
                'ageTo',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'ageto'),
                    'filter' => [
                        NumberFilter::class,
                        [
                            'search_type' => 'lte',
                            'type' => 'number',
                            'min' => '1',
                        ],
                    ],
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_customergroup_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_customergroup_edit',
                'routeParamName' => 'customerGroupId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_customergroup_delete',
                'routeParamName' => 'customerGroupId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return CustomerGroup::class;
    }
}
