<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Datatable\Column\EditableColumn;
use App\Datatable\Filter\DqlFilter;
use App\Datatable\Filter\IntegerFilter;
use App\Manager\PricePeriodManager;
use Arriva\Abt\Entity\Price;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;

/**
 * @package App\Datatables
 */
class PriceDatatable extends AbstractDatatable
{

    /** @var PricePeriodManager */
    private $pricePeriodManager;

    public function setPricePeriodManager(PricePeriodManager $pricePeriodManager): void
    {
        $this->pricePeriodManager = $pricePeriodManager;
    }

    public function getPricePeriodManager(): PricePeriodManager
    {
        return $this->pricePeriodManager;
    }

    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.price.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'description',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'proposition-element'),
                    'dql' => 'pe.description',
                    'filter' => [
                        DqlFilter::class,
                        [
                            'dql' => 'pe.description',
                            'field_type' => 'text'
                        ]
                    ]
                ]
            )
            ->add(
                'pricePeriod.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'price-period'),
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] + $this->getPricePeriodSelectOptions(),
                            'search_type' => 'eq'
                        ],
                    ],
                ]
            )
            ->add(
                'amount',
                EditableColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'amount'),
                    'filter' => [
                        IntegerFilter::class,
                        [],
                    ],
                    'editable' => array(TextEditable::class, array(
                        'url' => 'app_administration_price_editamount',
                        'placeholder' => 'Edit value',
                        'empty_text' => 'Empty Text',
                        'mode' => 'inline',
                        'highlight' => '#00929D'
                    ))
                ]
            );
    }

    public function getActions(): array
    {

        return [];
    }

    public function getEntity(): string
    {
        return Price::class;
    }

    public function getPricePeriodSelectOptions(): array
    {
        $pricePeriods = $this->getPricePeriodManager()->getAllPricePeriods();
        $result = [];
        foreach ($pricePeriods as $pricePeriod) {
            $name = $pricePeriod->getName();
            $result[$name] = $name;
        }

        return $result;
    }
}
