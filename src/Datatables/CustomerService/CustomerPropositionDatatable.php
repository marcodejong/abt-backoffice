<?php

namespace App\Datatables\CustomerService;

use App\Datatable\Column\StringableEntityValueColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;

class CustomerPropositionDatatable extends AbstractPropositionDatatable
{
    const LABEL_PREFIX = 'customer-service.datatable.arriva-card-proposition.';

    public function getName(): string
    {
        return 'arriva_customer_proposition_datatable';
    }

    public function getActions(): array
    {
        return [
            self::ACTION_UPDATE => [
                'routeName' => 'app_customerservice_customer_stopsubscription',
                'routeParamName' => 'subscriptionId',
            ],
        ];
    }

    protected function buildColumns(array $options = []): void
    {
        $this->getColumnBuilder()
            ->add(
                'propositionInstance.card.engravedId',
                StringableEntityValueColumn::class,
                [
                    'title' => $this->trans('card-engraved-id'),
                    'searchable' => false,
                ]
            );

        parent::buildColumns($options);
    }
}