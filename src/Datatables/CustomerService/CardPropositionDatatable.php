<?php

namespace App\Datatables\CustomerService;

use Arriva\Abt\Entity\Card;

class CardPropositionDatatable extends AbstractPropositionDatatable
{
    const LABEL_PREFIX = 'customer-service.datatable.arriva-card-proposition.';

    /**
     * @var Card
     */
    private $card;

    public function setCard(Card $card): self
    {
        $this->card = $card;

        return $this;
    }

    public function getName(): string
    {
        return 'arriva_card_proposition_datatable';
    }
}