<?php

namespace App\Datatables\CustomerService;

use App\Datatable\Column\MoneyColumn;
use App\Datatables\AbstractDatatable;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Money;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;

class AbstractPropositionDatatable extends AbstractDatatable
{
    const LABEL_PREFIX = 'customer-service.datatable.arriva-card-proposition.';
    const TRANSLATION_DOMAIN = 'customer-service';

    public function buildBaseQuery(QueryBuilder $queryBuilder): void
    {
        $queryBuilder
            ->addSelect('renewedBy.id AS renewedBySubscription')
            ->addSelect('
                CASE WHEN
                (
                  SELECT COUNT(s.id)
                  FROM Arriva\Abt\Entity\Subscription AS s
                  WHERE 
                  s.orderItem = subscription.orderItem AND s.effectiveDate < subscription.effectiveDate
                ) = 0 THEN 1 ELSE 0 END AS isFirstSubscription
            ')
            ->addSelect('
                CASE WHEN
                (
                  SELECT COUNT(s2.id)
                  FROM Arriva\Abt\Entity\Subscription AS s2
                  WHERE
                  s2.orderItem = subscription.orderItem AND s2.effectiveDate > subscription.effectiveDate
                ) = 0 THEN 1 ELSE 0 END AS isLastSubscription
            ')
            ->addSelect('
                CASE 
                    WHEN refund.id IS NULL AND subscription.expiryDate > current_date() THEN \'Actief\'
                    WHEN refund.id IS NULL AND subscription.expiryDate <= current_date() THEN \'Verlopen\'
                    WHEN refund.id IS NOT NULL AND refund.expiryDate > current_date() THEN \'Pending refund\'
                    WHEN refund.id IS NOT NULL AND refund.expiryDate <= current_date() THEN \'Refunded\'
                    ELSE \'Onbekend\'
                END AS status
            ')
            ->addSelect('renewedBy.effectiveDate AS renewedEffectiveDate')
            ->addSelect('renewedBy.expiryDate AS renewedExpiryDate')
            ->leftJoin('subscription.orderItem', 'orderItem')
            ->leftJoin('subscription.renewedBy', 'renewedBy')
            ->leftJoin('subscription.refund', 'refund')
            ->groupBy('subscription.id, subscription.renewedBy');
    }

    protected function buildColumns(array $options = []): void
    {
        $this->getColumnBuilder()
            ->add(
                'id',
                Column::class,
                [
                    'visible' => false,
                ]
            )
            ->add(
                'propositionInstance.proposition.name',
                Column::class,
                [
                    'title' => $this->trans('proposition-name'),
                    'searchable' => false,
                ]
            )
            ->add(
                'effectiveDate',
                DateTimeColumn::class,
                [
                    'title' => $this->trans('effective-date'),
                    'searchable' => false,
                    'date_format' => 'DD-MM-YYYY',
                ]
            )
            ->add(
                'expiryDate',
                DateTimeColumn::class,
                [
                    'title' => $this->trans('expiry-date'),
                    'searchable' => false,
                    'date_format' => 'DD-MM-YYYY',
                    'dql' => 'IFNULL(refund.expiryDate, subscription.expiryDate)'
                ]
            )
            ->add(
                'status',
                VirtualColumn::class,
                [
                    'default_content' => '',
                    'searchable' => false,
                    'title' => $this->trans('status'),
                ]
            )
            ->add(
                'monthlyPriceAmount',
                MoneyColumn::class,
                [
                    'title' => $this->trans('amount'),
                    'searchable' => false,
                    'formatter' => $this->getCurrencyFormatter(),
                    'dql' => '(
                        SELECT receivable.amount.amount
                        FROM Arriva\Abt\Entity\Receivable\SubscriptionTermReceivable receivable
                        WHERE receivable.id IN(
                            SELECT MAX(receivable2.id) AS id
                            FROM Arriva\Abt\Entity\Receivable\SubscriptionTermReceivable receivable2
                            WHERE receivable2.subscription = subscription
                        )
                        GROUP BY subscription.id
                    )',
                ]
            )
            ->add(
                'owner',
                VirtualColumn::class,
                [
                    'title' => $this->trans('owner'),
                    'searchable' => false,
                    'default_content' => 'Arriva',
                ]
            )
            ->add(
                'createdAt',
                DateTimeColumn::class,
                [
                    'title' => $this->trans('sale-date'),
                    'searchable' => false,
                    'date_format' => 'DD-MM-YYYY',
                ]
            )
            ->add(
                'PropositionElements',
                VirtualColumn::class,
                [
                    'title' => $this->trans('proposition-elements'),
                    'searchable' => false,
                    'dql' => "(
                        SELECT
                            GROUP_CONCAT(DISTINCT CONCAT_WS(' ', {pei}.peCode, {pei}.peDescription) SEPARATOR '<br />')
                        FROM Arriva\Abt\Entity\PropositionElementInstance {pei}, Arriva\Abt\Entity\PropositionInstance {pi}
                        WHERE {pei}.propositionInstance = propositionInstance and {pi}.orderItem = orderItem 
                        GROUP BY orderItem.id
                    )",
                ]
            );
    }

    protected function buildOptions(array $options): array
    {
        $options['page_length'] = 10;

        return parent::buildOptions($options);
    }

    protected function formatLine(array $row): array
    {
        $row['effectiveDate'] = $row['renewedEffectiveDate'] ?? $row['effectiveDate'];
        $row['monthlyPriceAmount'] = Money::amount((float)$row['monthlyPriceAmount'], Currency::EUR());

        if ((false === (bool)$row['isFirstSubscription'] && true === (bool)$row['isLastSubscription'])
            || (false === (bool)$row['isFirstSubscription'] && false === (bool)$row['isLastSubscription'])) {
            $row['owner'] = '';
            $row['createdAt'] = null;
        }

        return $row;
    }

    protected function trans($label, array $params = []): string
    {
        return $this->translator->trans(static::LABEL_PREFIX . $label, $params, static::TRANSLATION_DOMAIN);
    }

    public function getEntity()
    {
        return Subscription::class;
    }
}