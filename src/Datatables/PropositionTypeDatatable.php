<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Datatable\Filter\IntegerFilter;
use Arriva\Abt\Entity\PropositionType;
use Sg\DatatablesBundle\Datatable\Column\Column;


/**
 * @package App\Datatables
 */
class PropositionTypeDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.proposition-type.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'description',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'description'),
                ]
            )
            ->add(
                'order',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'order'),
                    'filter' => [
                        IntegerFilter::class,
                        [],
                    ],
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_propositiontype_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_propositiontype_edit',
                'routeParamName' => 'propositionTypeId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_propositiontype_delete',
                'routeParamName' => 'propositionTypeId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return PropositionType::class;
    }
}
