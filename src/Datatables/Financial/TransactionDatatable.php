<?php


namespace App\Datatables\Financial;

use App\Datatable\Column\EditableColumn;
use App\Datatable\Filter\DatePickerFilter;
use App\Datatable\Filter\DqlFilter;
use App\Datatable\Filter\EngravedIdFilter;
use App\Datatable\Filter\MultiColumnFilter;
use App\Datatables\AbstractDatatable;
use Arriva\Abt\Entity\Transaction\TlsTransactionRecord;
use Arriva\Abt\Utility\Money;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;

class TransactionDatatable extends AbstractDatatable
{
    /**
     * Returns the name of the entity.
     *
     * @return string
     */
    public function getEntity()
    {
        return TlsTransactionRecord::class;
    }

    /**
     * Build columns or other table specific things
     *
     * @param array $options
     * @throws \Exception
     * @see buildDatatable
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'financial.invoice.transaction.datatable.';
        $this->getColumnBuilder()

            ->add(
                'ckiMsgReportDate',
                DateTimeColumn::class,
                [
                    'title' => $this->trans($prefix.'check-in'),
                    'date_format' => 'DD-MM-YYYY H:m',
                    'filter' => [
                        DatePickerFilter::class,
                        [
                            'placeholder' => false,
                            'date_format' => 'Y-m-d\TH:i:s\Z'
                        ],
                    ],
                ]
            )
            ->add(
                'ckoMsgReportDate',
                DateTimeColumn::class,
                [
                    'title' => $this->trans($prefix.'check-out'),
                    'date_format' => 'DD-MM-YYYY H:m',
                    'filter' => [
                        DatePickerFilter::class,
                        [
                            'placeholder' => false,
                            'date_format' => 'Y-m-d\TH:i:s\Z'
                        ],
                    ],
                ]
            )
            ->add(
                'card.engravedId',
                Column::class,
                [
                    'title' => $this->trans($prefix.'engraved-id'),
                    'filter' => [
                        EngravedIdFilter::class,
                        [
                            'placeholder' => false,

                        ],
                    ],

                ]
            )
            ->add(
                'ckiStationName',
                Column::class,
                [
                    'title' => $this->trans($prefix.'from-station'),
                    'filter' => [
                        TextFilter::class,
                        [
                            'placeholder' => false,
                        ],
                    ],
                ]
            )
            ->add(
                'ckiStationName',
                Column::class,
                [
                    'title' => $this->trans($prefix.'to-station'),
                    'filter' => [
                        TextFilter::class,
                        [
                            'placeholder' => false,
                        ],
                    ],
                ]
            )
            ->add(
                'propositions',
                VirtualColumn::class,
                [
                    'title' => $this->trans($prefix.'propositions'),
                    'searchable' => true,
                    'search_column' => 'propositions',
                    'filter' => [
                        MultiColumnFilter::class,
                        [
                            'placeholder' => false,
                            'columns' => [
                                'tlstransactionrecord.propCategory1PeCommercialName',
                                'tlstransactionrecord.propCategory2PeCommercialName',
                                'tlstransactionrecord.propCategory3PeCommercialName'
                            ]
                        ]
                    ]
                ]
            )
            ->add(
                'serviceProvider.name',
                Column::class,
                [
                    'title' => $this->trans($prefix.'service-provider'),
                    'filter' => [
                        TextFilter::class,
                        [
                            'placeholder' => false
                        ]
                    ]
                ]
            )
            ->add(
                'propValueBeforeRating',
                EditableColumn::class,
                [
                    'title' => $this->trans($prefix.'value-before-rating'),
                    'class_name' => 'money',
                    'editable' => [
                        TextEditable::class,
                        [
                            'editable_if' => function($row) {
                                return !isset($row['override']['override.type'])
                                    || $row['override']['override.type'] === 'price'
                                    || $row['override']['override.type'] === 'none';

                            },
                            'mode' => 'inline',
                            'url' => 'app_financial_edit_transaction_value',
                            'empty_text' => ''
                        ]
                    ],
                    'filter' => [
                        DqlFilter::class,
                        [
                            'placeholder' => false,
                            'dql' =>
                            'CASE 
                                WHEN override IS NOT NULL THEN override.override.priceBeforeRating 
                                ELSE tlstransactionrecord.propValueBeforeRating 
                            END'
                        ]
                    ]
                ]
            )
            ->add(
                'propValueAfterRating',
                EditableColumn::class,
                [
                    'title' => $this->trans($prefix.'value-after-rating'),
                    'class_name' => 'money',
                    'editable' => [
                        TextEditable::class,
                        [
                            'editable_if' => function($row) {
                                return !isset($row['override']['override.type'])
                                    || $row['override']['override.type'] === 'price'
                                    || $row['override']['override.type'] === 'none';
                            },
                            'mode' => 'inline',
                            'url' => 'app_financial_edit_transaction_value',
                            'empty_text' => ''
                        ]
                    ],
                    'filter' => [
                        DqlFilter::class,
                        [
                            'placeholder' => false,
                            'dql' =>
                                'CASE 
                                WHEN override IS NOT NULL THEN override.override.priceAfterRating 
                                ELSE tlstransactionrecord.propValueAfterRating 
                            END'
                        ]
                    ]
                ]
            )
            ->add(
                null,
                ActionColumn::class,
                [
                    'visible' => true,
                    'actions' => [
                        [
                            'label' => $this->trans($prefix.'action.credit'),
                            'route' => 'app_financial_credit_transaction',
                            'route_parameters' => [
                                'tlsTransactionRecordId' => 'id',
                            ],
                            'confirm' => true,
                            'confirm_message' => $this->trans($prefix.'action.credit.confirm'),
                            'attributes' => [
                                'data-method' => 'POST',
                                'class' => 'inline-datatable-action'
                            ],
                            'render_if' => function($row) {
                                return !isset($row['override']['override.type']) || $row['override']['override.type'] !== 'credit';
                            }
                        ],
                        [
                            'label' => $this->trans($prefix.'action.remove'),
                            'route' => 'app_financial_remove_transaction',
                            'route_parameters' => [
                                'tlsTransactionRecordId' => 'id'
                            ],
                            'confirm' => true,
                            'confirm_message' => $this->trans($prefix.'action.remove.confirm'),
                            'attributes' => [
                                'data-method' => 'POST',
                                'class' => 'inline-datatable-action'
                            ],
                            'render_if' => function($row) {
                                return !isset($row['override']['override.type']) || $row['override']['override.type'] !== 'remove';
                            }
                        ],
                        [
                            'label' => $this->trans($prefix.'action.undo-remove'),
                            'route' => 'app_financial_undo_remove_transaction',
                            'route_parameters' => [
                                'tlsTransactionRecordId' => 'id',
                            ],
                            'confirm' => true,
                            'confirm_message' => $this->trans($prefix.'action.undo-remove.confirm'),
                            'attributes' => [
                                'data-method' => 'POST',
                                'class' => 'inline-datatable-action'
                            ],
                            'render_if' => function($row) {
                                return ($row['override']['override.type'] ?? 'none') === 'remove';
                            }
                        ],
                        [
                            'label' => $this->trans($prefix.'action.undo-credit'),
                            'route' => 'app_financial_undo_credit_transaction',
                            'route_parameters' => [
                                'tlsTransactionRecordId' => 'id',
                            ],
                            'confirm' => true,
                            'confirm_message' => $this->trans($prefix.'action.undo-credit.confirm'),
                            'attributes' => [
                                'data-method' => 'POST',
                                'class' => 'inline-datatable-action'
                            ],
                            'render_if' => function($row) {
                                return ($row['override']['override.type'] ?? 'none') === 'credit';
                            }
                        ],
                    ]
                ]
            )
            ->add('fceValue',
                Column::class,
                [
                    'visible' => false
                ])

        ;
        $this->callbacks->setInitComplete(['template' => 'financial/transaction/init_callback.js.twig']);
        $this->callbacks->setRowCallback(['template' => 'financial/transaction/row_callback.js.twig']);
    }

    protected function formatLine(array $row): array
    {
        $row['card']['engravedId'] = (string)$row['card']['engravedId'];
        $row['propositions'] =
            $row['propCategory1PeCommercialName'].'<br/>'.
            $row['propCategory2PeCommercialName'].'<br/>'.
            $row['propCategory3PeCommercialName'];

        if (isset($row['override']['override.priceBeforeRating'])
            && $row['override']['override.priceBeforeRating'] instanceof Money) {
            $row['propValueBeforeRating'] = $this->formatAmount($row['override']['override.priceBeforeRating']->getAmount());
            $row['propValueAfterRating'] = $this->formatAmount($row['override']['override.priceAfterRating']->getAmount());
        } else {
            $row['propValueBeforeRating'] = $this->formatAmount($row['propValueBeforeRating'] ? $row['propValueBeforeRating'] : 0);
            $row['propValueAfterRating'] = $this->formatAmount($row['propValueAfterRating'] ? $row['propValueAfterRating'] : $row['fceValue']);
        }

        if (!$row['serviceProvider']) {
            $row['serviceProvider'] = ['name' => ''];
        }

        return $row;
    }

    private function formatAmount(?float $amount)
    {
        if (null === $amount) {
            return null;
        }

        return number_format($amount, 2, ',', '.');
    }

    private function trans(string $message, array $params = [], string $domain = 'financial'): string
    {
        return $this->translator->trans($message, $params, $domain);
    }
}

