<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\PropositionElement;
use Sg\DatatablesBundle\Datatable\Column\Column;

/**
 * @package App\Datatables
 */
class PropositionElementDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.proposition-element.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'peCode',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'pe-code'),
                ]
            )
            ->add(
                'description',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'description'),
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_propositionelement_add',
                'routeParamName' => null,
            ],
            self::ACTION_COPY => [
                'routeName' => 'app_administration_propositionelement_copy',
                'routeParamName' => 'propositionElementId',
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_propositionelement_edit',
                'routeParamName' => 'propositionElementId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_propositionelement_delete',
                'routeParamName' => 'propositionElementId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return PropositionElement::class;
    }
}
