<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\Configuration\PropositionTerm\AbstractPropositionTerm;
use Arriva\Abt\Utility\Money;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;

/**
 * @package App\Datatables
 */
class PropositionTermDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.proposition-term.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'code',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'code'),
                ]
            )
            ->add(
                'fromAmount',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'from-amount'),
                    'filter' => [
                        NumberFilter::class,
                        [
                            'search_type' => 'gte',
                            'type' => 'number',
                            'min' => '1',
                        ],
                    ],
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_propositionterm_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_propositionterm_edit',
                'routeParamName' => 'abstractPropositionTermId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_propositionterm_delete',
                'routeParamName' => 'abstractPropositionTermId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return AbstractPropositionTerm::class;
    }

    public function getLineFormatter(): callable
    {
        return function ($row) {
            $row = call_user_func(parent::getLineFormatter(), $row);
            if ($row['fromAmount'] instanceof Money) {
                $row['fromAmount'] = number_format($row['fromAmount']->getAmount(), 2, ',', '.');
            }

            return $row;
        };
    }
}
