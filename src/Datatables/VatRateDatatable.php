<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Datatable\Filter\IntegerFilter;
use Arriva\Abt\Entity\VatRate;
use Sg\DatatablesBundle\Datatable\Column\Column;


/**
 * @package App\Datatables
 */
class VatRateDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.vat-rate.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'code',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'code'),
                ]
            )
            ->add(
                'percentage',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'percentage'),
                    'filter' => [IntegerFilter::class,[]]
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_vatrate_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_vatrate_edit',
                'routeParamName' => 'vatRateId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_vatrate_delete',
                'routeParamName' => 'vatRateId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return VatRate::class;
    }
}
