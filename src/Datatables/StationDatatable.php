<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Datatable\Filter\IntegerFilter;
use App\Manager\RegionManager;
use Arriva\Abt\Entity\Station;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;

/**
 * @package App\Datatables
 */
class StationDatatable extends AbstractDatatable
{

    /** @var RegionManager */
    private $regionManager;

    public function setRegionManager(RegionManager $regionManager): void
    {
        $this->regionManager = $regionManager;
    }

    public function getRegionManager(): RegionManager
    {
        return $this->regionManager;
    }

    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.station.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'code',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'code'),
                    'filter' => [
                        IntegerFilter::class,
                        [],
                    ],
                ]
            )
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'regions.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'region'),
                    'data' => 'regions[, ].name',
                    'orderable' => false,
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] + $this->getRegionSelectOptions(),
                            'search_type' => 'like'
                        ],
                    ],
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_IMPORT => [
                'routeName' => 'app_administration_station_import',
                'routeParamName' => null,
            ],
            self::ACTION_EXPORT => [
                'routeName' => 'app_administration_station_export',
                'routeParamName' => 'codes',
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_station_edit',
                'routeParamName' => 'stationId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_station_delete',
                'routeParamName' => 'stationId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return Station::class;
    }

    public function getRegionSelectOptions(): array
    {
        $regions = $this->getRegionManager()->getRegions();
        $result = [];
        foreach ($regions as $region) {
            $name = $region->getName();
            $result[$name] = $name;
        }

        return $result;
    }
}
