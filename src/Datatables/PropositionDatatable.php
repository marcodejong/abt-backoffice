<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use App\Datatable\Filter\IntegerFilter;
use App\Manager\PropositionGroupManager;
use App\Manager\PropositionTermManager;
use App\Manager\SalePeriodManager;
use App\Manager\ValidityPeriodManager;
use Arriva\Abt\Entity\Proposition;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;

/**
 * @package App\Datatables
 */
class PropositionDatatable extends AbstractDatatable
{

    /** @var PropositionGroupManager */
    private $propositionGroupManager;

    /** @var PropositionTermManager */
    private $propositionTermManager;

    /** @var ValidityPeriodManager */
    private $validityPeriodManager;

    /** @var SalePeriodManager */
    private $salePeriodManager;

    public function setPropositionGroupManager(PropositionGroupManager $propositionGroupManager): void
    {
        $this->propositionGroupManager = $propositionGroupManager;
    }

    public function getPropositionGroupManager(): PropositionGroupManager
    {
        return $this->propositionGroupManager;
    }

    public function setPropositionTermManager(PropositionTermManager $propositionTermManager): void
    {
        $this->propositionTermManager = $propositionTermManager;
    }

    public function getPropositionTermManager(): PropositionTermManager
    {
        return $this->propositionTermManager;
    }

    public function setValidityPeriodManager(ValidityPeriodManager $validityPeriodManager): void
    {
        $this->validityPeriodManager = $validityPeriodManager;
    }

    public function getValidityPeriodManager(): ValidityPeriodManager
    {
        return $this->validityPeriodManager;
    }

    public function setSalePeriodManager(SalePeriodManager $salePeriodManager): void
    {
        $this->salePeriodManager = $salePeriodManager;
    }

    public function getSalePeriodManager(): SalePeriodManager
    {
        return $this->salePeriodManager;
    }

    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.proposition.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'propositionGroup.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'proposition-group'),
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] +
                                $this->getOptionsArrayFromEntities($this->getPropositionGroups(), 'name', 'name'),
                            'search_type' => 'eq'
                        ]
                    ]
                ]
            )
            ->add(
                'propositionTerm.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'proposition-term'),
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] +
                                $this->getOptionsArrayFromEntities($this->getPropositionTerms(), 'name', 'name'),
                            'search_type' => 'eq'
                        ]
                    ]
                ]
            )
            ->add(
                'validityPeriod.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'validity-period'),
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] +
                                $this->getOptionsArrayFromEntities($this->getValidityPeriods(), 'name', 'name'),
                            'search_type' => 'eq'
                        ]
                    ]
                ]
            )
            ->add(
                'salePeriod.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'sale-period'),
                    'filter' => [
                        SelectFilter::class,
                        [
                            'select_options' => ['' => 'All'] +
                                $this->getOptionsArrayFromEntities($this->getSalePeriods(), 'name', 'name'),
                            'search_type' => 'eq'
                        ]
                    ]
                ]
            )
            ->add(
                'tAccount',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'t-account'),
                    'filter' => [
                        IntegerFilter::class,
                        [],
                    ],
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_proposition_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_proposition_edit',
                'routeParamName' => 'propositionId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_proposition_delete',
                'routeParamName' => 'propositionId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return Proposition::class;
    }

    private function getPropositionGroups(): array
    {
        return $this->getPropositionGroupManager()->getAllVisiblePropositionGroups();
    }

    private function getPropositionTerms(): array
    {
        return $this->getPropositionTermManager()->getAllPropositionTerms();
    }

    private function getValidityPeriods(): array
    {
        return $this->getValidityPeriodManager()->getAllValidityPeriods();
    }

    private function getSalePeriods(): array
    {
        return $this->getSalePeriodManager()->getAllSalePeriods();
    }
}
