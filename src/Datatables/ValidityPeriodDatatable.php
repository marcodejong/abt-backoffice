<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\ValidityPeriod;
use Arriva\Abt\Value\Date;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;


/**
 * @package App\Datatables
 */
class ValidityPeriodDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.validityperiod.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            )
            ->add(
                'fromDate',
                DateTimeColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'startdate'),
                    'date_format' => 'YYYY-MM-DD',
                ]
            )
            ->add(
                'toDate',
                DateTimeColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'enddate'),
                    'date_format' => 'YYYY-MM-DD',
                ]
            );
    }

    protected function formatLine(array $row): array
    {

        if ($row['toDate'] instanceof  Date) {
            $row['toDate'] = $row['toDate']->getDateTime();
        }
        if ($row['fromDate'] instanceof  Date) {
            $row['fromDate'] = $row['fromDate']->getDateTime();
        }

        return $row;
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_validityperiod_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_validityperiod_edit',
                'routeParamName' => 'validityPeriodId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_validityperiod_delete',
                'routeParamName' => 'validityPeriodId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return ValidityPeriod::class;
    }
}
