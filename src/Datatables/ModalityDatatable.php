<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\Modality;
use Sg\DatatablesBundle\Datatable\Column\Column;


/**
 * @package App\Datatables
 */
class ModalityDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.modality.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'name'),
                ]
            );
    }

    public function getActions(): array
    {

        return [
            self::ACTION_CREATE => [
                'routeName' => 'app_administration_modality_add',
                'routeParamName' => null,
            ],
            self::ACTION_UPDATE => [
                'routeName' => 'app_administration_modality_edit',
                'routeParamName' => 'modalityId',
            ],
            self::ACTION_DELETE => [
                'routeName' => 'app_administration_modality_delete',
                'routeParamName' => 'modalityId',
            ],
        ];
    }

    public function getEntity(): string
    {
        return Modality::class;
    }
}
