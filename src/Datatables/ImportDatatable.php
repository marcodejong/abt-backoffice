<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatables;

use Arriva\Abt\Entity\Import;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;

/**
 * @package App\Datatables
 */
class ImportDatatable extends AbstractDatatable
{
    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.price.import.search-results.datatable.';
        $this->getColumnBuilder()
            ->add(
                'filename',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'filename'),
                ]
            )
            ->add(
                'createdAt',
                DateTimeColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'created-at'),
                    'date_format' => 'YYYY-MM-DD',
                ]
            )
            ->add(
                'user',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'user'),
                ]
            );
    }

    public function getActions(): array
    {

        return [];
    }

    public function getEntity(): string
    {
        return Import::class;
    }
}
