<?php

namespace App\Datatables;

use App\Datatable\Column\MoneyColumn;
use Arriva\Abt\Entity\Order;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\PeCode;
use Arriva\Abt\Value\PropositionElementInstanceStatus;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use App\Datatable\Filter\EngravedIdFilter;
use Arriva\Abt\Entity\PropositionElementInstance;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;

/**
 * @package App\Datatables
 */
class PropositionElementTlsOrderDatatable extends AbstractDatatable
{
    /** @var Order $order */
    private $order;

    /**
     * @param array $options
     * @throws \Exception
     */
    protected function buildColumns(array $options = []): void
    {
        $prefix = 'administration.proposition-element.tls.order.datatable.';
        $this->getColumnBuilder()
            ->add(
                null,
                MultiselectColumn::class,
                [
                    'actions' => array(
                        array(
                            'route' => 'app_workflow_order_confirmpropositionelementstls',
                            'route_parameters' => array(
                                'orderId' => $this->order->getId(),
                            ),
                            'label' => $this->translator->trans('workflow.order.action.confirm-proposition-elements-tls'),
                            'confirm' => true,
                            'confirm_message' => $this->translator->trans('workflow.order.action.confirm-proposition-elements-tls.message'),
                            'attributes' => [
                                'data-method' => 'POST',
                                'style' => 'display:none',
                                'role' => 'button'
                            ]
                        ),
                    ),
                    'render_if' => function($row) {
                        return $row['status'] !== (string) PropositionElementInstanceStatus::CREATED();
                    },
                ]
            )
            ->add(
                'propositionInstance.proposition.name',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'proposition-order-item.name'),
                ]
            )
            ->add(
                'effectiveDate',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'effective-date'),
                ]
            )
            ->add(
                'engravedId',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'engraved-id'),
                    'filter' => [
                        EngravedIdFilter::class,
                        [
                            'placeholder' => true,
                        ],
                    ],
                ]
            )
            ->add(
                'peCode',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'pe-code'),
                ]
            )
            ->add(
                'instanceAmount',
                MoneyColumn::class,
                [
                    'title' => $this->translator->trans($prefix.'instance-amount'),
                    'searchable' => false,
                    'formatter' => $this->getCurrencyFormatter(),
                ]
            )
            ->add(
                'status',
                Column::class,
                [
                    'title' => $this->translator->trans($prefix.'status'),
                ]
            );

        $this->extensions->setSelect([
            'blurable' => false,
            'class_name' => '',
            'info' => true,
            'items' => 'row',
            'selector' => 'tr:not(.no-select)',
            'style' => 'multi',
        ]);

        $this->callbacks->setRowCallback([
            'template' => 'customer_service/order/row_callback.js.twig'
        ]);
    }

    public function getActions(): array
    {
        return [];
    }

    public function formatLine(array $row): array
    {
        if ($row['peCode'] instanceof PeCode) {
            $row['peCode'] = $row['peCode']->getPeCodeString();
        }

        if (!$row['propositionInstance']['proposition']) {
            $row['propositionInstance']['proposition']['name'] = '';
        }

        if (array_key_exists('engravedId', $row)) {
            $row['engravedId'] = implode(
                '.',
                str_split($row['engravedId'], 4)
            );
        }
        if ($row['effectiveDate'] instanceof Date) {
            $row['effectiveDate'] = $row['effectiveDate']->getAsDateTime()->format('d-m-Y');
        }
        if ($row['status'] instanceof  PropositionElementInstanceStatus) {
            $row['status'] = $row['status']->getId();
        }

        return parent::formatLine($row);
    }

    protected function buildOptions(array $options): array
    {
        $options['individual_filtering'] = false;
        return parent::buildOptions($options);
    }

    protected function buildFeatures(array $features): array
    {
        $features['ordering'] = false;

        return parent::buildFeatures($features); // TODO: Change the autogenerated stub
    }


    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getEntity(): string
    {
        return PropositionElementInstance::class;
    }
}
