<?php

namespace App\Test\Fixture\Loader;

use Nelmio\Alice\FixtureBuilderInterface;
use Nelmio\Alice\FixtureSet;
use Nelmio\Alice\GeneratorInterface;
use Nelmio\Alice\Loader\NativeLoader;
use Nelmio\Alice\Parser\Chainable\YamlParser;
use Nelmio\Alice\Parser\IncludeProcessor\IncludeDataMerger;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Parser as SymfonyYamlParser;

/**
 * Class AliceFixtureLoader
 *
 * This class uses Alice to load fixtures from a folder of (yaml) fixture definition files.
 * It generates the fixture files once, on-demand, and on subsequent calls to loadObjects will
 * use the already-loaded fixture set to re-generate the object set, to maximize performance.
 */
class AliceFixtureLoader implements ObjectLoaderInterface
{
    /** @var string */
    private $fixturesPath;

    /** @var array */
    private $loadedFixtureSet;

    /** @var FixtureBuilderInterface */
    private $fixtureBuilder;

    /** @var GeneratorInterface */
    private $objectGenerator;

    public function __construct(NativeLoader $aliceLoader, string $fixturesPath)
    {
        $this->fixtureBuilder = $aliceLoader->getFixtureBuilder();
        $this->objectGenerator = $aliceLoader->getGenerator();
        $this->fixturesPath = $fixturesPath;
    }

    /**
     * @throws \Nelmio\Alice\Throwable\LoadingThrowable
     */
    public function loadObjects(): array
    {
        return $this->objectGenerator->generate($this->getLoadedFixtureSet())->getObjects();
    }

    private function createParser()
    {
        return new YamlParser(new SymfonyYamlParser());
    }

    private function getLoadedFixtureSet(): FixtureSet
    {
        if (!$this->loadedFixtureSet) {
            $this->loadedFixtureSet = $this->loadFixtureSet();
        }

        return $this->loadedFixtureSet;
    }

    private function loadFixtureSet(): FixtureSet
    {
        $fileParser = $this->createParser();

        $files = $this->getFixtureFiles($this->fixturesPath);
        $dataMerger = new IncludeDataMerger();

        $data = array_reduce(
            array_unique($files),
            function (array $data, string $file) use ($fileParser, $dataMerger): array {
                return $dataMerger->mergeInclude($data, $fileParser->parse($file));
            },
            []
        );

        return $this->fixtureBuilder->build($data, [], []);
    }

    /**
     * @param string $path
     * @return string[]
     */
    private function getFixtureFiles(string $path): array
    {
        $finder = new Finder();
        $files = [];
        foreach ($finder->files()->in($this->fixturesPath)->name('/(\.yml|\.yaml)$/') as $file) {
            /* @var SplFileInfo $file */
            $files[] = $file->getRealPath();
        }

        return $files;
    }
}
