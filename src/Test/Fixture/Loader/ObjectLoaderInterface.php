<?php

namespace App\Test\Fixture\Loader;

interface ObjectLoaderInterface
{
    public function loadObjects(): array;
}
