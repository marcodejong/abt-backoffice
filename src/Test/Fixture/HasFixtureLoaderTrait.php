<?php

namespace App\Test\Fixture;

use App\Test\Fixture\Loader\AliceFixtureLoader;
use App\Test\Fixture\Loader\ObjectLoaderInterface;
use Faker\Generator;
use Nelmio\Alice\Loader\NativeLoader;

trait HasFixtureLoaderTrait
{
    /** @var ObjectLoaderInterface */
    private static $fixtureLoader;

    private static function createFixtureLoader(): ObjectLoaderInterface
    {
        $fakerGenerator = new Generator();

        return new AliceFixtureLoader(
            new NativeLoader($fakerGenerator),
            __DIR__ . '/../../../tests/fixtures'
        );
    }

    private static function getFixtureLoader(): ObjectLoaderInterface
    {
        if (!self::$fixtureLoader) {
            self::$fixtureLoader = self::createFixtureLoader();
        }

        return self::$fixtureLoader;
    }
}
