<?php

namespace App\Test\Fixture;

use App\Test\Fixture\Loader\ObjectLoaderInterface;
use App\Test\ObjectEntityManagerFactory;
use Arriva\Assist\ORM\EntityManager\EntityManagerInterface;

class FixtureEntityManagerFactory
{
    /** @var ObjectEntityManagerFactory */
    private $objectEntityManagerFactory;

    public function __construct(ObjectEntityManagerFactory $objectEntityManagerFactory)
    {
        $this->objectEntityManagerFactory = $objectEntityManagerFactory;
    }

    public function createEntityManagerFromFixtures(ObjectLoaderInterface $fixtureLoader): EntityManagerInterface
    {
        return $this->objectEntityManagerFactory->createEntityManagerFromObjects($fixtureLoader->loadObjects());
    }
}
