<?php

namespace App\Test\Faker\Provider;

use Arriva\Abt\Value\ChannelOrderId;
use Arriva\Abt\Value\ContractTariffId;
use Arriva\Abt\Value\NalOrderId;
use Faker\Provider\Base;

/**
 * Faker provider for NAL value objects.
 *
 * Example usage in your Alice YAML fixture:
 *
 *      my_object:
 *          nalOrderId: '<createNalOrderId(1234)>'
 *          contractTariffId: '<createContractTariffId(123)>'
 *          channelOrderId: '<createChannelOrderId("A-1234567890-1234")>'
 */
class NalTypesProvider extends Base
{
    public function createNalOrderId(string $value): NalOrderId
    {
        return NalOrderId::of($value);
    }

    public function createContractTariffId(string $value): ContractTariffId
    {
        return ContractTariffId::of($value);
    }

    public function createChannelOrderId(string $value): ChannelOrderId
    {
        return ChannelOrderId::of($value);
    }
}
