<?php

namespace App\Test\Faker\Provider;

use Arriva\Abt\Value\IbanNumber;
use Faker\Provider\Base;

/**
 * Faker provider for IbanNumber value objects.
 */
final class IbanNumberProvider extends Base
{
    public function ibanNumber(string $ibanNumberString): IbanNumber
    {
        return IbanNumber::fromString($ibanNumberString);
    }
}
