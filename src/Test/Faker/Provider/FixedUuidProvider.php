<?php

namespace App\Test\Faker\Provider;

use Faker\Provider\Base;

final class FixedUuidProvider extends Base
{
    public function fixedUuid(int $number): string
    {
        return sprintf('d34db33f-1337-f00d-4b1d-0000000000%\'02d', $number);
    }
}
