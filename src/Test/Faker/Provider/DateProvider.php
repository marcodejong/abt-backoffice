<?php

namespace App\Test\Faker\Provider;

use Arriva\Abt\Value\Date;
use DateTimeImmutable;
use Faker\Provider\Base;

/**
 * Faker provider for date value objects.
 *
 * Example usage in your Alice YAML fixture:
 *
 *      my_object:
 *          date: '<createDate("2018-01-01")>'
 *          dateTime: '<createDateTime("2018-01-01\T12:34:56+04:15")>'
 */
final class DateProvider extends Base
{
    public function createDate(string $dateString): Date
    {
        return Date::fromAtomString($dateString);
    }

    public function createDateTime(string $dateTimeString): DateTimeImmutable
    {
        return DateTimeImmutable::createFromFormat(DateTimeImmutable::ATOM, $dateTimeString);
    }
}
