<?php

namespace App\Test\Faker\Provider;

use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Money;
use Faker\Provider\Base;

/**
 * Faker provider for money value objects.
 *
 * Example usage in your Alice YAML fixture:
 *
 *      my_object:
 *          amount: '<moneyEuro(1.20)>'
 */
final class MoneyProvider extends Base
{
    public function moneyEuro(float $amount): Money
    {
        return $this->money($amount, Currency::EUR());
    }

    public function money(float $amount, Currency $currency): Money
    {
        return Money::amount($amount, $currency);
    }
}
