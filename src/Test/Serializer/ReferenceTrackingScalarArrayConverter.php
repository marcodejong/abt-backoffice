<?php

namespace App\Test\Serializer;

use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\DateRange;
use Arriva\Assist\Value\CustomerNumber;
use function array_map;
use Arriva\Abt\Utility\Money;
use DateTimeInterface;
use Exception;
use function get_class;
use function implode;
use function is_iterable;
use function is_numeric;
use function is_object;
use function is_scalar;
use ReflectionObject;
use RuntimeException;
use Serializable;
use SolidPhp\ValueObjects\Type\Type;
use SolidPhp\ValueObjects\Value\SingleValueObjectInterface;
use SplObjectStorage;
use function sprintf;

class ReferenceTrackingScalarArrayConverter implements ScalarArrayConverterInterface
{
    private $knownTypeConverters = [];

    public function setTypeConverter(string $type, callable $converter): void
    {
        $this->knownTypeConverters[$type] = $converter;

        $this->sortKnownTypeConverters();
    }

    public function addDefaultTypeConverters(): void
    {
        $this->setTypeConverter(
            Reference::class,
            static function (Reference $reference) {
                $pathSegments = array_map(
                    static function ($pathSegment) {
                        return sprintf(is_numeric($pathSegment) ? '[%d]' : '->%s', $pathSegment);
                    },
                    $reference->getPath()
                );

                return sprintf('REF: root%s', implode('', $pathSegments));
            }
        );

        $this->setTypeConverter(
            Serializable::class,
            static function (Serializable $serializable) {
                return $serializable->serialize();
            }
        );

        $this->setTypeConverter(
            DateTimeInterface::class,
            static function (DateTimeInterface $dateTime) {
                return $dateTime->format(DateTimeInterface::ATOM);
            }
        );

        $this->setTypeConverter(
            Date::class,
            static function (Date $date) {
                return (string)$date;
            }
        );

        $this->setTypeConverter(
            DateRange::class,
            static function (DateRange $range) {
                return (string)$range;
            }
        );

        $this->setTypeConverter(
            CustomerNumber::class,
            static function (CustomerNumber $customerNumber) {
                return sprintf('%d', $customerNumber->getNumber());
            }
        );

        $this->setTypeConverter(
            Money::class,
            static function (Money $money) {
                return sprintf('%s %s', $money->getCurrency()->getId(), $money->getAmount());
            }
        );

        $this->setTypeConverter(
            SingleValueObjectInterface::class,
            static function (SingleValueObjectInterface $singleValueObject) {
                return sprintf('%s(%s)', Type::of($singleValueObject)->getShortName(), var_export((string)$singleValueObject, true));
            }
        );
    }

    /**
     * @param mixed $subject
     *
     * @return array|int|bool|string|null
     * @throws Exception
     */
    public function convertToScalarArray($subject)
    {
        return $this->doConvertToScalarArray($subject, new SplObjectStorage(), []);
    }

    /**
     * @param mixed             $subject
     * @param SplObjectStorage $knownReferences
     * @param array             $currentPath
     *
     * @return array|int|bool|string|null
     * @throws Exception
     */
    private function doConvertToScalarArray($subject, SplObjectStorage $knownReferences, array $currentPath)
    {
        if ($subject === null || is_scalar($subject)) {
            return $subject;
        }

        if (is_iterable($subject)) {
            return $this->convertIterableToScalarArray($subject, $knownReferences, $currentPath);
        }

        if (is_object($subject)) {
            return $this->convertObjectToScalarArray($subject, $knownReferences, $currentPath);
        }

        throw new Exception(sprintf('Unsupported serialization subject: %s', var_export($subject, true)));
    }

    private function convertObjectToScalarArray(object $object, SplObjectStorage $knownReferences, array $currentPath)
    {
        $convertedObject = $this->convertKnownType($object);

        if (!is_object($convertedObject)) {
            return $this->doConvertToScalarArray($convertedObject, $knownReferences, $currentPath);
        }

        $object = $convertedObject;

        if ($knownReferences->contains($object)) {
            return $this->doConvertToScalarArray(
                $knownReferences->offsetGet($object),
                $knownReferences,
                $currentPath
            );
        }

        $reference = new Reference($currentPath);
        $knownReferences->offsetSet($object, $reference);

        $returnValue = [
            '__CLASS__' => get_class($object),
            '__REF__' => $this->doConvertToScalarArray($reference, $knownReferences, $currentPath),
        ];
        $reflectionObject = new ReflectionObject($object);

        $parentProperties = [];
        if ($reflectionClass = $reflectionObject->getParentClass()) {
            do {
                $parentProperties[] = $reflectionClass->getProperties();
            } while ($reflectionClass = $reflectionClass->getParentClass());
        }

        $properties = array_merge($reflectionObject->getProperties(), ...$parentProperties);

        foreach ($properties as $property) {
            /** @var \ReflectionProperty $property */
            if (!$property->isStatic()) {
                $property->setAccessible(true);
                $name = $property->getName();
                $value = $property->getValue($object);
                $returnValue[$name] = $returnValue[$name] ?? $this->doConvertToScalarArray(
                    $value,
                    $knownReferences,
                    array_merge($currentPath, [$name])
                );
            }
        }

        return $returnValue;
    }

    private function convertIterableToScalarArray(
        iterable $subject,
        SplObjectStorage $knownReferences,
        array $currentPath
    ): array {
        $returnValue = [];
        foreach ($subject as $key => $value) {
            $returnValue[$this->convertToArrayKey($key)] = $this->doConvertToScalarArray(
                $value,
                $knownReferences,
                array_merge($currentPath, [$key])
            );
        }

        return $returnValue;
    }

    private function convertToArrayKey($source)
    {
        if (is_string($source) || is_numeric($source)) {
            return $source;
        }

        if (is_object($source) && method_exists($source, '__toString')) {
            return (string)$source;
        }

        throw new RuntimeException('Value cannot be converted to array key');
    }

    private function convertKnownType(object $object)
    {
        foreach ($this->knownTypeConverters as $knownType => $converter) {
            if (is_a($object, $knownType)) {
                return $converter($object);
            }
        }

        return $object;
    }

    private function sortKnownTypeConverters(): void
    {
        uksort(
            $this->knownTypeConverters,
            function (string $typeA, string $typeB) {
                if (is_subclass_of($typeA, $typeB)) {
                    return -1;
                }
                if (is_subclass_of($typeB, $typeA)) {
                    return 1;
                }

                return 0;
            }
        );
    }
}
