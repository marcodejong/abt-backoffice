<?php

namespace App\Test\Serializer;

/**
 * Interface ScalarArrayConverterInterface
 *
 * This interface specifies that the implementing class can be used to convert
 * an arbitrary subject to a scalar array (an array containing only scalar values and/or other scalar arrays).
 * Scalar arrays are guaranteed not to contain circular references, and are thus suitable for serialization.
 */
interface ScalarArrayConverterInterface
{
    /**
     * @param mixed $subject
     *
     * @return array|int|bool|string|null
     * @throws \Exception
     */
    public function convertToScalarArray($subject);
}
