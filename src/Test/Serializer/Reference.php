<?php

namespace App\Test\Serializer;

use RuntimeException;
use Serializable;

/**
 * Class Reference
 *
 * This class represents a reference to a node within an object/array tree.
 */
class Reference implements Serializable
{
    /** @var array */
    private $path;

    public function __construct(array $path)
    {
        $this->path = $path;
    }

    public function getPath(): array
    {
        return $this->path;
    }

    public function serialize()
    {
        return sprintf('@%s', implode('|', $this->path));
    }

    public function unserialize($serialized)
    {
        if (1 !== preg_match('/^@((\w|((?<!\|)\|))*)$/', $serialized, $matches)) {
            throw new RuntimeException(sprintf('Invalid serialized Reference: %s', $serialized));
        }

        $this->path = explode('|', $matches[1]);
    }
}
