<?php

namespace App\Test\Serializer\Traits;

use App\Test\Serializer\ReferenceTrackingScalarArrayConverter;
use App\Test\Serializer\ScalarArrayConverterInterface;

/**
 * Trait which provides an helper function to serialize an object to JSON
 * by using reflection and convert all private or protected properties to public.
 *
 * Useful when using JSON snapshots for unit tests.
 */
trait JsonSerializerTrait
{
    /** @var ScalarArrayConverterInterface */
    private static $_converter;

    /**
     * @param mixed $object
     *              The subject to serialize
     * @param int   $maxSerializeLevels
     *              This param is deprecated; its value is no longer used
     *
     * @return string
     * @throws \Exception
     */
    protected function serializeToJson($object, int $maxSerializeLevels = 10): string
    {
        if (!self::$_converter) {
            self::$_converter = new ReferenceTrackingScalarArrayConverter();
            self::$_converter->addDefaultTypeConverters();
        }

        return json_encode(self::$_converter->convertToScalarArray($object));
    }
}
