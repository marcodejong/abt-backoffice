<?php

namespace App\Test;

use App\Service\DateTime\DateTimeServiceInterface;
use Arriva\Assist\ORM\EntityConstraint\TypeEntityConstraint;
use Arriva\Assist\ORM\EntityManager\EntityManagerInterface;
use Arriva\Assist\ORM\EntityManager\EntitySynchronizerInterface;

class DummyEntitySynchronizer implements EntitySynchronizerInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DateTimeServiceInterface */
    private $dateTimeService;

    public function __construct(EntityManagerInterface $entityManager, DateTimeServiceInterface $dateTimeService)
    {
        $this->entityManager = $entityManager;
        $this->dateTimeService = $dateTimeService;
    }

    public function flush(): void
    {
        $this->updateEntityIds();
        $this->updateEntityTimestamps();
    }

    public function refresh(&$object): void
    {
    }

    /**
     * Fake the Doctrine/DB logic of setting IDs to flushed entities
     */
    private function updateEntityIds(): void
    {
        $idsPerType = [];
        $newEntities = [];
        foreach ($this->entityManager->getEntities() as $entity) {
            $type = \get_class($entity);
            if (!isset($idsPerType[$type])) {
                $idsPerType[$type] = [];
            }
            if (method_exists($entity, 'getId')) {
                if ($id = $entity->getId()) {
                    $idsPerType[$type][] = $id;
                } elseif (method_exists($entity, 'setId') || property_exists($entity, 'id')) {
                    $newEntities[] = $entity;
                }
            }
        }

        foreach ($newEntities as $entity) {
            $type = \get_class($entity);
            $newId = !empty($idsPerType[$type]) ? max($idsPerType[$type]) + 1 : 1;
            $this->setEntityId($entity, $newId);
            $idsPerType[$type][] = $newId;
        }
    }

    /**
     * Fake the Doctrine timestampable extension
     */
    private function updateEntityTimestamps(): void
    {
        foreach ($this->entityManager->getEntities() as $entity) {
            if (\is_callable([$entity, 'setCreatedAt'])) {
                $entity->setCreatedAt($this->dateTimeService->now());
            }
            if (\is_callable([$entity, 'setUpdatedAt'])) {
                $entity->setUpdatedAt($this->dateTimeService->now());
            }
        }
    }

    private function setEntityId($entity, $id)
    {
        $reflectionObject = new \ReflectionObject($entity);
        if ($reflectionObject->hasMethod('setId') && $reflectionObject->getMethod('setId')->isPublic()) {
            $entity->setId($id);
        } elseif ($reflectionObject->hasProperty('id')
            && $reflectionProperty = $reflectionObject->getProperty('id')
        ) {
            $reflectionProperty->setAccessible(true);
            $reflectionProperty->setValue($entity, $id);
        }
    }
}
