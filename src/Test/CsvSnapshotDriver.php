<?php

namespace App\Test;

use League\Csv\Reader;
use League\Csv\Statement;
use PHPUnit\Framework\Assert;
use Spatie\Snapshots\Driver;

/**
 * Class CsvSnapshotDriver
 *
 * @package App\Test
 */
final class CsvSnapshotDriver implements Driver
{
    private $delimiter;
    private $enclosure;
    private $escape;

    /**
     * @param $delimiter
     * @param $enclosure
     * @param $escape
     */
    public function __construct($delimiter = ';', $enclosure = '"', $escape = "\\")
    {
        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
        $this->escape = $escape;
    }

    public function serialize($data): string
    {
        return $data;
    }

    public function extension(): string
    {
        return 'csv';
    }

    public function match($expected, $actual): void
    {
        $expectedReader = $this->createCsvReader($expected);
        $actualReader = $this->createCsvReader($actual);

        $expectedRecords = (new Statement())->process($expectedReader)->jsonSerialize();
        $actualRecords = (new Statement())->process($actualReader)->jsonSerialize();

        Assert::assertEquals($expectedRecords, $actualRecords);
    }

    private function createCsvReader(string $contents): Reader
    {
        return Reader::createFromString($contents)
            ->setDelimiter($this->delimiter)
            ->setEnclosure($this->enclosure)
            ->setEscape($this->escape);
    }
}
