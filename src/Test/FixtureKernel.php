<?php

namespace App\Test;

use App\Kernel;
use App\Test\Fixture\HasFixtureLoaderTrait;
use Arriva\Assist\ORM\EntityManager\ObjectStorageInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FixtureKernel
 *
 * A test-specific subclass of the regular application kernel that injects a set of
 * test fixtures into the application object storage that can be reset at will.
 */
class FixtureKernel extends Kernel
{
    use HasFixtureLoaderTrait;

    /** @var array */
    private $loadedObjects = [];

    public function resetFixtures(): void
    {
        $this->loadedObjects = self::getFixtureLoader()->loadObjects();
    }

    public function boot()
    {
        parent::boot();
        $this->injectFixtures();
    }

    public function terminate(Request $request, Response $response)
    {
        $this->extractNewObjects();

        return parent::terminate($request, $response);
    }

    private function injectFixtures(): void
    {
        try {
            /** @var ObjectStorageInterface $appObjectStorage */
            $appObjectStorage = $this->getContainer()->get('objectStorage.app');
            $appObjectStorage->clear();
            foreach ($this->loadedObjects as $object) {
                $appObjectStorage->add($object);
            }
        } finally {
        }
    }

    private function extractNewObjects(): void
    {
        try {
            /** @var ObjectStorageInterface $appObjectStorage */
            $appObjectStorage = $this->getContainer()->get('objectStorage.app');
            foreach ($appObjectStorage->getAll() as $object) {
                if (!\in_array($object, $this->loadedObjects, true)) {
                    $this->loadedObjects[] = $object;
                }
            }
        } finally {
        }
    }
}
