<?php

namespace App\Test\Repository\Traits;

use App\Util\IterableFunctions;
use Arriva\Assist\ORM\EntityConstraint\CallableEntityConstraint;
use Arriva\Assist\ORM\EntityConstraint\TypeEntityConstraint;
use Arriva\Assist\ORM\EntityManager\EntityManagerInterface;
use Traversable;

/**
 * In memory persistence will keep objects stored in memory.
 * Be aware that if you execute multiple requests in your functional test,
 * a new container is built for every request so all objects will be rebuilt from the fixture files.
 */
trait EntityManagerRepositoryTrait /* implements HasEntityManagerInterface */
{
    /** @var object[]|null */
    private $cachedObjects;

    abstract public function getEntityManager(): EntityManagerInterface;

    public function getEntitiesByType(string $typeName): array
    {
        return IterableFunctions::iterable_to_array(
            $this->getEntityManager()->getEntities(new TypeEntityConstraint([$typeName]))
        );
    }

    /**
     * @param callable $test
     * @return object|null
     */
    public function getEntityByTest(callable $test)
    {
        $matchingEntities = $this->getEntityManager()->getEntities(new CallableEntityConstraint($test));
        foreach ($matchingEntities as $matchingEntity) {
            return $matchingEntity;
        }

        return null;
    }

    public function getEntitiesByTest(callable $test): array
    {
        return IterableFunctions::iterable_to_array(
            $this->getEntityManager()->getEntities(new CallableEntityConstraint($test))
        );
    }

    /**
     * @param string $type
     * @param string $id
     * @return null|object
     */
    public function getEntityByTypeAndId(string $type, string $id)
    {
        return $this->getEntityByTest(
            function ($entity) use ($type, $id) {
                return is_a($entity, $type) &&
                    method_exists($entity, 'getId') &&
                    (string)$entity->getId($id) === $id;
            }
        );
    }
}
