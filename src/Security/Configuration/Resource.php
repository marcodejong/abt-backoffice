<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Security\Configuration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation;

/**
 * @package App\Security\Configuration
 * @Annotation
 */
class Resource extends ConfigurationAnnotation
{
    /** @var string */
    private $value;

    public function getAliasName(): string
    {
        return 'resource';
    }

    public function allowArray(): bool
    {
        return false;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
