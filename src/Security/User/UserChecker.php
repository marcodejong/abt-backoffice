<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Security\User;

use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    /** @var UserManager */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param UserInterface $user
     * @throws \LogicException
     * @throws AccountStatusException
     * @return void
     */
    public function checkPreAuth(UserInterface $user): void
    {
        // nothing to do
    }

    public function checkPostAuth(UserInterface $user): void
    {
        $this->assertUser($user);

        /** @var $user User */
        if ($user->isDisabled()) {
            $exception = new DisabledException('User account is disabled.');
            $exception->setUser($user);
            throw $exception;
        }

        if ($this->userManager->isBlockedForTooManyInvalidLoginAttempts($user)) {
            $exception = new LockedException('User account is locked.');
            $exception->setUser($user);
            throw $exception;
        }

        if ($this->userManager->isPasswordExpired($user)) {
            $exception = new CredentialsExpiredException('User credentials have expired.');
            $exception->setUser($user);
            throw $exception;
        }
    }

    /**
     * @param UserInterface $user
     * @throws \LogicException when $user is not an instance of \App\Entity\User
     * @return void
     */
    private function assertUser(UserInterface $user): void
    {
        if (!$user instanceof User) {
            throw new \LogicException('Accept only instance of: ' . User::class);
        }
    }
}
