<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Security\Voter;

use App\Entity\User;
use App\Manager\AclManager;
use Arriva\Abt\Entity\Menu;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @package App\Security\Voter
 */
class MenuItemVoter extends Voter
{
    private const VIEW = 'view';

    /** @var AclManager */
    private $aclManager;

    public function __construct(AclManager $aclManager)
    {
        $this->aclManager = $aclManager;
    }

    protected function supports($attribute, $subject): bool
    {
        return self::VIEW === $attribute && $subject instanceof Menu;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (self::VIEW === $attribute && $subject instanceof Menu && $user instanceof User) {
            $privilege = $subject->getPrivilege();
            $resource = $subject->getResource();
            if ($this->aclManager->hasPrivilegeForAclResource($user->getAclRole(), $privilege, $resource)) {
                return true;
            }
        }

        return false;
    }
}
