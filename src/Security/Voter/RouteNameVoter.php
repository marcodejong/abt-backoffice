<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Security\Voter;

use App\Annotation\ResourcePrivilegesReader;
use App\Entity\User;
use App\Manager\AclManager;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @package App\Security\Voter
 */
class RouteNameVoter extends Voter
{
    /** @var RouterInterface */
    private $router;

    /** @var ResourcePrivilegesReader */
    private $reader;

    /** @var AclManager */
    private $aclManager;

    public function __construct(
        RouterInterface $router,
        ResourcePrivilegesReader $reader,
        AclManager $aclManager
    ) {
        $this->router = $router;
        $this->reader = $reader;
        $this->aclManager = $aclManager;
    }

    protected function supports($attribute, $subject): bool
    {
        return \is_string($subject) && null !== $this->router->getRouteCollection()->get($subject);
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @throws \ReflectionException when the controller class does not exist.
     * @throws \ReflectionException when the controller class exists but action method does not exist.
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        $route = $this->router->getRouteCollection()->get($subject);
        if ($route && $user instanceof User) {
            $controllerActionString = \str_replace('::', ':', $route->getDefault('_controller'));
            [$className, $methodName] = \explode(':', $controllerActionString);
            [$resource, $privilege] = $this->reader->getResourceAndPrivilege($className, $methodName);
            if ($this->aclManager->hasPrivilegeForAclResource($user->getAclRole(), $privilege, $resource)) {
                return true;
            }
        }

        return false;
    }
}
