<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Security\Voter;

use App\Annotation\ResourcePrivilegesReader;
use App\Controller\HomepageController;
use App\Entity\User;
use App\Manager\AclManager;
use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

/**
 * @package App\Security\Voter
 */
class AclRoleResourcePrivilegesVoter implements VoterInterface
{
    /** @var ResourcePrivilegesReader */
    private $reader;

    /** @var AclManager */
    private $aclManager;

    public function __construct(ResourcePrivilegesReader $reader, AclManager $aclManager)
    {
        $this->reader = $reader;
        $this->aclManager = $aclManager;
    }

    /**
     * {@inheritdoc}
     *
     * @param TokenInterface $token
     * @param mixed $subject
     * @param array $attributes
     * @throws \ReflectionException when the controller class does not exist.
     * @throws \ReflectionException when the controller class exists but action method does not exist.
     * @return int
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        if ($subject instanceof Request && $subject->attributes->has('_controller')) {
            $controllerActionString = \str_replace('::', ':', $subject->attributes->get('_controller'));
            [$className, $methodName] = \explode(':', $controllerActionString);
            if ((RedirectController::class === $className && 'urlRedirectAction' === $methodName) ||
                (HomepageController::class === $className && 'indexAction' === $methodName)
            ) {
                return self::ACCESS_GRANTED;
            }

            [$resource, $privilege] = $this->reader->getResourceAndPrivilege($className, $methodName);
            if (!empty($resource)) {
                $user = $token->getUser();
                if ($user instanceof User &&
                    $this->aclManager->hasPrivilegeForAclResource($user->getAclRole(), $privilege, $resource)
                ) {
                    return self::ACCESS_GRANTED;
                }
            }
        }

        return self::ACCESS_DENIED;
    }
}
