<?php

namespace App\DependencyInjection\CompilerPass;

use App\Model\Export\ChainExporter;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class RegisterExportersCompilerPass
 *
 * This compiler pass registers all Exporter classes with the ChainExporter service
 */
class RegisterExportersCompilerPass implements CompilerPassInterface
{
    /** @var string */
    private $tagName;

    public function __construct(string $tagName)
    {
        $this->tagName = $tagName;
    }

    public function process(ContainerBuilder $container): void
    {
        $chainExporterDefinition = $container->findDefinition(ChainExporter::class);

        foreach ($container->findTaggedServiceIds($this->tagName) as $id => $attrs) {
            $exporterDefinition = $container->findDefinition($id);
            if ($exporterDefinition !== $chainExporterDefinition) {
                $chainExporterDefinition->addMethodCall('registerExporter', [new Reference($id)]);
            }
        }
    }
}
