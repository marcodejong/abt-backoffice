<?php

namespace App\Util;

use Arriva\Abt\Value\File;
use League\Flysystem\FilesystemInterface;

class FlysystemFile extends File
{
    /** @var FilesystemInterface */
    private $filesystem;

    /** @var string */
    private $path;

    protected function __construct(FilesystemInterface $filesystem, string $path)
    {
        $this->filesystem = $filesystem;
        $this->path = $path;
    }

    public static function of(FilesystemInterface $filesystem, string $path): self
    {
        return static::getInstance($filesystem, $path);
    }

    public function getFilesystem(): FilesystemInterface
    {
        return $this->filesystem;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getContentType(): string
    {
        return $this->filesystem->getMimetype($this->getPath());
    }

    public function write($contents, bool $overwrite = true): bool
    {
        if ($this->exists()) {
            if ($overwrite) {
                $this->delete();
            } else {
                return false;
            }
        }

        switch (true) {
            case is_string($contents):
                return $this->writeString($contents);
            case is_iterable($contents):
                return $this->writeIterable($contents);
            case is_resource($contents):
                return $this->writeResource($contents);
            default:
                throw new \InvalidArgumentException('$contents must be a string, an iterable of strings or a resource');
        }
    }

    public function exists(): bool
    {
        return $this->filesystem->has($this->path);
    }

    public function delete(): bool
    {
        if ($this->exists()) {
            return $this->filesystem->delete($this->path);
        }

        return true;
    }

    private function writeString(string $contents): bool
    {
        return $this->filesystem->put($this->path, $contents);
    }

    private function writeIterable(iterable $chunks): bool
    {
        $tmpFile = tmpfile();
        foreach ($chunks as $chunk) {
            fwrite($tmpFile, $chunk);
        }
        rewind($tmpFile);

        $result = $this->filesystem->writeStream($this->path, $tmpFile);

        if (is_resource($tmpFile)) {
            fclose($tmpFile);
        }

        return $result;
    }

    private function writeResource($resource): bool
    {
        return $this->filesystem->writeStream($this->path, $resource);
    }

    public function __toString(): string
    {
        return sprintf('FlysystemFile(path=%s)', $this->path);
    }

    public function readByChunk(int $chunkSize = 1024): iterable
    {
        if (!$this->exists()) {
            return;
        }

        if ($stream = $this->filesystem->readStream($this->path)) {
            rewind($stream);
            while (false !== ($chunk = fread($stream, $chunkSize))) {
                yield $chunk;
            }

            if (is_resource($stream)) {
                fclose($stream);
            }

            return;
        }

        $contents = $this->read();
        for($i = 0, $len = strlen($contents); $i < $len; $i+=$chunkSize) {
            yield substr($contents, $i, $chunkSize);
        }
    }

    public function read(): string
    {
        if ($this->exists()) {
            return $this->filesystem->read($this->path) ?: '';
        }

        return '';
    }

    public function readByLine(): iterable
    {
        if (!$this->exists()) {
            return;
        }

        if ($stream = $this->filesystem->readStream($this->path)) {
            while (false !== ($line = fgets($stream))) {
                foreach (explode("\r", rtrim($line, "\r\n")) as $subLine) {
                    yield $subLine;
                }
            }

            if (is_resource($stream)) {
                fclose($stream);
            }

            return;
        }

        $contents = $this->read();
        for ($i = 0, $len = strlen($contents); $i < $len;) {
            $firstNLPos = strpos($contents, "\n", $i);
            $firstNLPos = false === $firstNLPos ? $len : $firstNLPos;
            $firstCRPos = strpos($contents, "\r", $i);
            $firstCRPos = false === $firstCRPos ? $len : $firstNLPos;

            $eol = min($firstNLPos, $firstCRPos);
            $startOfNextLine = max($firstNLPos, $firstCRPos) === $eol + 1 ? $eol + 2 : $eol + 1;

            yield trim(substr($contents, $i, $eol), "\r\n");

            $i = $startOfNextLine;
        }
    }
}
