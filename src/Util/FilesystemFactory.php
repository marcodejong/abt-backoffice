<?php

namespace App\Util;

use League\Flysystem\Adapter\Ftp;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Adapter\NullAdapter;
use League\Flysystem\AdapterInterface;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;

class FilesystemFactory
{
    public function createFilesystemFromUrl(string $url): FilesystemInterface
    {
        $adapter = $this->createAdapterFromUrl($url);

        return new Filesystem($adapter, []);
    }

    private function createAdapterFromUrl(string $url): AdapterInterface
    {
        if (strtolower(trim($url)) === 'null') {
            return $this->createNullAdapter();
        }

        $components = parse_url($url);
        $scheme = strtolower($components['scheme'] ?? '');

        switch ($scheme) {
            case 'null':
            case 'none':
                return $this->createNullAdapter();
            case 'file':
            case 'local':
                return $this->createLocalAdapter($components);
            case 'ftp':
            case 'ftps':
                return $this->createFtpAdapter($components);
            default:
                throw new \InvalidArgumentException(sprintf('Scheme "%s" not supported. Supported schemes: null|none,file|local,ftp,ftps', $scheme));
        }
    }

    private function createNullAdapter(): NullAdapter
    {
        return new NullAdapter();
    }

    private function createFtpAdapter(array $components): Ftp
    {
        static $allowedParams = [
            'timeout',
            'permPrivate',
            'permPublic',
            'passive',
            'transferMode',
            'systemType',
            'ignorePassiveAddress',
            'recurseManually',
            'utf8',
        ];

        $isFtps = strtolower($components['scheme']) === 'ftps';
        $queryParams = $this->getQueryStringParams($components['query'] ?? '', $allowedParams);

        $config = [
                'host'     => urldecode($components['host']),
                'port'     => (int)urldecode($components['port'] ?? ($isFtps ? '990' : '22')),
                'username' => urldecode($components['user']),
                'password' => urldecode($components['pass']),
                'root'     => urldecode($components['path']),
                'ssl'      => $isFtps,
            ] + $queryParams;

        return new Ftp($config);
    }

    private function createLocalAdapter(array $components): Local
    {
        $allowedParams = [
            'publicFilePermissions',
            'privateFilePermissions',
            'publicDirPermissions',
            'privateDirPermissions'
        ];

        $params = $this->getQueryStringParams($components['query'] ?? '', $allowedParams);

        if (!isset($components['path'])) {
            throw new \InvalidArgumentException('local filesystem must specify a path');
        }

        $root = urldecode($components['path']);
        $writeFlags = LOCK_EX;
        $linkHandling = Local::DISALLOW_LINKS;
        $permissions = [
            'file' => [
                'public'  => $params['publicFilePermissions'] ?? 0644,
                'private' => $params['privateFilePermissions'] ?? 0600,
            ],
            'dir'  => [
                'public'  => $params['publicDirPermissions'] ?? 0755,
                'private' => $params['privateDirPermissions'] ?? 0700,
            ],
        ];

        return new Local($root, $writeFlags, $linkHandling, $permissions);
    }

    private function getQueryStringParams(string $queryString, ?array $allowedParams = null): array
    {
        $queryParams = [];
        if ($queryString) {
            parse_str($queryString, $queryParams);
        }

        if (in_array('ignorePassiveAddress', array_keys($queryParams))) {
            $queryParams['ignorePassiveAddress'] = (bool) $queryParams['ignorePassiveAddress'];
        }

        if (is_array($allowedParams)) {
            return array_intersect_key($queryParams, array_flip($allowedParams));
        }

        return $queryParams;
    }
}
