<?php

namespace App\Util;

use Arriva\Abt\Utility\Iterator;

/**
 * Class IterableFunctions
 *
 * @deprecated use Arriva\Abt\Utility\Iterator
 */
class IterableFunctions
{
    /**
     * @param iterable $iterable
     *
     * @return array
     *
     * @deprecated use Arriva\Abt\Utility\Iterator::getArray()
     * @see        Iterator::getArray()
     */
    static function iterable_to_array(iterable $iterable): array
    {
        if (\is_array($iterable)) {
            return $iterable;
        }

        $ret = [];
        foreach ($iterable as $item) {
            $ret[] = $item;
        }

        return $ret;
    }

    /**
     * @param iterable $it
     *
     * @return \Traversable
     *
     * @deprecated use Arriva\Abt\Utility\Iterator
     * @see Iterator
     */
    static function iterable_to_traversable(iterable $it): \Traversable
    {
        yield from $it;
    }
}
