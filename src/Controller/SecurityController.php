<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 * @Route("/")
 */
class SecurityController extends Controller
{
   /**
     * @Route("/authentication", name="login", methods={"GET"})
     * @Route("/authentication/index", methods={"GET"})
     * @Route("/authentication/login", methods={"GET"})
     */
    public function loginAction()
    {
        return $this->redirect($this->getParameter('app.assist.login_url'));
    }

    /**
     * @Route("/logout", name="logout", methods={"GET"})
     * @Route("/authentication/logout", name="logout", methods={"GET"})
     */
    public function logoutAction(): void
    {
        // Nothing to do will be handled by a security listener
    }
}
