<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package App\Controller
 */
class HealthCheckController extends Controller
{
    /**
     * @Route("/_healthcheck", name="app_healthcheck", methods={"HEAD"})
     * @return Response
     */
    public function indexAction()
    {
        return new Response();
    }
}
