<?php


namespace App\Controller\Financial;


use App\Entity\User;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 * @Resource("abt-financial:index")
 */
class IndexController extends Controller
{
    /**
     * @Route("/index", name="app_financial_index", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser() instanceof User) {
            return [];
        }

        return $this->redirectToRoute('login', ['_locale' => $request->getLocale()]);
    }
}