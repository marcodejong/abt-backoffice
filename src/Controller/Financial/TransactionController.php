<?php

namespace App\Controller\Financial;

use App\Datatables\Financial\TransactionDatatable;
use App\Repository\TlsTransactionRecordRepositoryInterface;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use App\Service\AbtApi\ClientInterface;
use App\Service\AbtApi\Entity\Request\SetTransactionOverrideRequest;
use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\Transaction\TlsTransactionRecord;
use Arriva\Abt\Value\Transaction\Override\PriceOverride;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TransactionController
 * @package App\Controller\Financial
 * @Route("/transaction", options={"expose" = true})
 * @Resource("abt-financial:invoice")
 */
class TransactionController extends Controller
{
    /** @var TransactionDatatable */
    private $transactionDatatable;

    /** @var ClientInterface */
    private $abtApiClient;

    public function __construct(
        TransactionDatatable $transactionDatatable,
        ClientInterface $abtApiClient
    ) {
        $this->transactionDatatable = $transactionDatatable;
        $this->abtApiClient = $abtApiClient;
    }

    /**
     * @Route("/{invoiceId}", methods={"GET"}, name="app_financial_invoice_transaction_index")
     * @Template()
     * @Privilege("read")
     *
     * @param Invoice $invoice
     * @return array
     * @throws \Exception
     */
    public function index(Invoice $invoice)
    {
        return [
            'datatable' => $this->getTransactionDataTable($invoice),
            'invoice' => $invoice
        ];
    }

    /**
     * @param Invoice $invoice
     * @return TransactionDatatable
     * @throws \Exception
     */
    private function getTransactionDataTable(Invoice $invoice): TransactionDatatable
    {
        $this->transactionDatatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_financial_transactiondatatable',
                        ['invoiceId' => $invoice->getId()]
                    ),
                ],
            ]
        );

        return $this->transactionDatatable;
    }

    /**
     * @Route("/transactions/{tlsTransactionRecordId}/remove", name="app_financial_remove_transaction", defaults={"type"="remove"})
     * @Route("/transactions/{tlsTransactionRecordId}/credit", name="app_financial_credit_transaction", defaults={"type"="credit"})
     * @Route("/transactions/{tlsTransactionRecordId}/undo-credit", name="app_financial_undo_credit_transaction", defaults={"type"="none"})
     * @Route("/transactions/{tlsTransactionRecordId}/undo-remove", name="app_financial_undo_remove_transaction", defaults={"type"="none"})
     * @param TlsTransactionRecord $tlsTransactionRecord
     * @param string $type
     * @return RedirectResponse
     */
    public function processTransaction(TlsTransactionRecord $tlsTransactionRecord, string $type): RedirectResponse
    {
        $request = new SetTransactionOverrideRequest(
            $tlsTransactionRecord->getId(),
            $type
        );
        $invoice = $tlsTransactionRecord->getReceivable()->getInvoiceItem()->getInvoice();
        $this->abtApiClient->setTransactionOverride($request);

        return new RedirectResponse(
            $this->generateUrl('app_financial_invoice_transaction_index', ['invoiceId' => $invoice->getId()])
        );
    }



    /**
     * @Route(
     *     "/transactions/edit-value",
     *     methods={"POST"},
     *     name="app_financial_edit_transaction_value"
     * )
     * @param Request $request
     * @param TlsTransactionRecordRepositoryInterface $repository
     * @return JsonResponse|Response
     */
    public function editTransactionValue(Request $request, TlsTransactionRecordRepositoryInterface $repository)
    {
        if (!$transactionRecord = $repository->findById($request->get('pk'))) {
            return new Response('financial.transaction.edit.transaction_not_found', 404);
        }

        $newValue = $request->get('value');
        $currentValueBeforeRating = $this->getCurrentPriceForTransaction($transactionRecord, 'valueBeforeRating');
        $currentValueAfterRating = $this->getCurrentPriceForTransaction($transactionRecord, 'valueAfterRating');

        if ($request->get('name') === 'propValueBeforeRating') {
            if (!$newValue || (float)$newValue === $currentValueBeforeRating) {
                return new Response($currentValueBeforeRating, 200);
            }

            $input = new SetTransactionOverrideRequest(
                $transactionRecord->getId(),
                SetTransactionOverrideRequest::TYPE__PRICE,
                $this->formatAmount($newValue),
                $this->formatAmount($currentValueAfterRating)
            );
        } else {
            if (!$newValue || (float)$newValue === $currentValueAfterRating) {
                return new Response($currentValueAfterRating, 200);
            }

            $input = new SetTransactionOverrideRequest(
                $transactionRecord->getId(),
                SetTransactionOverrideRequest::TYPE__PRICE,
                $this->formatAmount($currentValueBeforeRating),
                $this->formatAmount($newValue)
            );
        }

        $response = $this->abtApiClient->setTransactionOverride($input);
        if ($response->getMessage() !== null) {
            return new Response('financial.transaction.edit.transaction_edit_failed', 400);
        }

        return new Response($newValue, 200);
    }

    private function formatAmount(?string $amount): ?string
    {
        if (null === $amount) {
            return null;
        }

        return str_replace(',', '.', $amount);
    }

    /**
     * @Route("/transactions/datatable/{invoiceId}", methods={"GET"}, condition="request.isXmlHttpRequest()",
     *     name="app_financial_transactiondatatable")
     * @Privilege("read")
     *
     * @param Invoice $invoice
     * @return JsonResponse
     * @throws \Exception
     */
    public function transactionDatatableAction(Invoice $invoice): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getTransactionDataTable($invoice));

        $responseService->getDatatableQueryBuilder()->getQb()
            ->select('tlstransactionrecord, override')
            ->leftJoin('tlstransactionrecord.receivable', 'receivable')
            ->leftJoin('tlstransactionrecord.override', 'override')
            ->leftJoin('receivable.invoiceItem', 'invoiceItem')
            ->where('invoiceItem.invoice = :invoice')->setParameter('invoice', $invoice);

        return $responseService->getResponse();
    }

    private function getCurrentPriceForTransaction(TlsTransactionRecord $transaction, string $priceType): ?float
    {
        if ($transaction->getOverride() &&
            ($price = $transaction->getOverride()->getOverride())) {
            if ($price instanceof PriceOverride && $priceType == 'valueBeforeRating') {
                return $price->getPriceBeforeRating()->getAmount();
            } elseif ($price instanceof PriceOverride && $priceType == 'valueAfterRating') {
                return $price->getPriceAfterRating()->getAmount();
            }
        }

        if ($priceType == 'valueBeforeRating') {
            return $transaction->getPropValueBeforeRating() ?? 0;
        } elseif ($priceType == 'valueAfterRating') {
            return $transaction->getPropValueAfterRating() ?? ($transaction->getFceValue() ?? 0);
        }

        return $transaction->getPrice()->getAmount() ?? null;
    }
}