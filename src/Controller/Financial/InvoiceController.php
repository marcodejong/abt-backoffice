<?php

namespace App\Controller\Financial;

use App\Datatables\Financial\InvoicingDatatable;
use App\Queue\Handler\InvoiceQueueHandler;
use App\Queue\Handler\Options\CreateInvoiceHandlerOptions;
use App\Queue\Handler\Result\InvoiceQueueHandlerResult;
use App\Repository\InvoiceRepositoryInterface;
use App\Repository\TransactionReceivableRepositoryInterface;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Invoice;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CardController
 * @package App\Controller\CustomerService
 * @Route("/invoice", options={"expose" = true})
 * @Resource("abt-financial:invoice")
 */
Class InvoiceController extends Controller
{
    /** @var InvoicingDatatable */
    private $invoicingDatatable;

    /** @var TranslatorInterface */
    private $translator;

    public function __construct(
        InvoicingDatatable $invoicingDatatable,
        TranslatorInterface $translator
    ) {
        $this->translator = $translator;
        $this->invoicingDatatable = $invoicingDatatable;
    }

    /**
     * @Route("/", methods={"GET"},
     *      name="app_financial_invoice_invoicing_index",
     *      requirements={"reset-selection": "true|false"},
     *      defaults={"reset-selection": false}
     * )
     * @Privilege("read")
     * @Template
     * @param InvoiceRepositoryInterface $repository
     * @return array
     */
    public function index(InvoiceRepositoryInterface $repository, Request $request): array
    {
        return [
            'datatable' => $this->getInvoicingDataTable($request->query->getBoolean('reset-selection')),
            'numberOfInvoicesInDeliveringState' => $repository->getNumberOfInvoicesInDeliveringState()
        ];
    }

    /**
     * @Route("/draft-details/{invoiceId}", methods={"GET"},
     *      name="app_financial_invoice_invoicing_draft_details"
     * )
     * @Privilege("read")
     * @Template
     * @param Invoice $invoice
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param TransactionReceivableRepositoryInterface $receivableRepository
     * @return array
     */
    public function draftDetails(
        Invoice $invoice,
        InvoiceRepositoryInterface $invoiceRepository,
        TransactionReceivableRepositoryInterface $receivableRepository
    ): array {
        $transactionReceivables = $receivableRepository->getTransactionReceivablesSummaryForInvoice(
            $invoice
        );
        $subscriptionTermAndCreditReceivables = $invoiceRepository->getGroupedSubscriptionTermAndCreditReceivables(
            $invoice, 'subscription.propositionInstance.card.engravedId'
        );

        return [
            'invoice' => $invoice,
            'transactionReceivables' => $transactionReceivables,
            'subscriptionTermAndCreditReceivables' => $subscriptionTermAndCreditReceivables,
        ];
    }

    /**
     * @Route("/get-number-of-delivering-invoices", methods={"GET"},
     *     name="app_financial_invoice_invoicing_stats"
     * )
     * @Privilege("read")
     * @param InvoiceRepositoryInterface $repository
     * @return JsonResponse
     */
    public function getNumberOfInvoicesInDeliveringState(InvoiceRepositoryInterface $repository): JsonResponse
    {
        return new JsonResponse([
            'numberOfInvoicesInDeliveringState' => $repository->getNumberOfInvoicesInDeliveringState()
        ]);
    }

    /**
     * @Route("/invoicing/edit-period", methods={"POST"},
     *     name="app_financial_invoice_invoicing_edit_period"
     * )
     * @Privilege("update")
     *
     * @param Request $request
     * @param InvoiceRepositoryInterface $repository
     * @return Response
     */
    public function inlineEditPeriod(Request $request, InvoiceRepositoryInterface $repository): Response
    {
        if (($invoice = $repository->findInvoiceById($request->get('pk'))) instanceof Invoice) {
            $invoice->setPeriod($request->get('value'));
            $this->getDoctrine()->getManager()->flush();
        } else {
            return new JsonResponse($this->translator->trans('financial.invoice.invoicing.invoice_not_found'), 404);
        }

        return new Response($request->get('value'), 200);
    }


    /**
     * @Route("/finalize-invoice", methods={"POST"},
     *     name="app_financial_invoice_finalize_invoices"
     * )
     * @Privilege("update")
     *
     * @param Request $request
     * @param InvoiceQueueHandler $invoiceHandler
     * @return RedirectResponse
     */
    public function finalizeInvoice(Request $request,
        InvoiceQueueHandler $invoiceHandler
    ): RedirectResponse {
        $selectedInvoiceIds = array_map(
            static function (string $reportingInvoiceId) {
                return (int)str_replace('ABT_', '', $reportingInvoiceId);
            },
            $request->get('data')
        );

        $options = new CreateInvoiceHandlerOptions($selectedInvoiceIds, $this->getUser());

        /** @var InvoiceQueueHandlerResult $result */
        $result = $invoiceHandler->handle($options);

        if (count($result->getFailedInvoices()) > 0) {
            $this->addFlash('warning', $this->translator->trans('app_financial_invoice_invoicing_failed'));
        }

        return $this->redirectToRoute('app_financial_invoice_invoicing_index', ['reset-selection' => true]);
    }


    /**
     * @Route("/invoicing/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()",
     *     name="app_financial_invoice_invoicingdatatable"
     * )
     * @Privilege("read")
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function invoicingDatatable(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getInvoicingDataTable());
        $queryBuilder = $responseService->getDatatableQueryBuilder()->getQb();
        $queryBuilder->orderBy('draftinvoice.createdAt', 'DESC');

        return $responseService->getResponse();
    }

    private function getInvoicingDataTable(bool $resetSelection = false): InvoicingDatatable
    {
        $this->invoicingDatatable
            ->buildDatatable([
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_financial_invoice_invoicingdatatable'
                    ),
                ],
                'resetSelection'=> $resetSelection
            ]);

        return $this->invoicingDatatable;
    }

}
