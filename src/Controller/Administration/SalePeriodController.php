<?php

namespace App\Controller\Administration;

use App\Datatables\SalePeriodDatatable;
use App\Form\DataObject\SalePeriodDataObject;
use App\Form\SalePeriodType;
use App\Manager\SalePeriodManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\SalePeriod;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SalePeriodController
 * @package App\Controller\Administration
 * @Route("/sale-period", options={"expose" = true})
 * @Resource("abt-administration:sale-period")
 */
Class SalePeriodController extends Controller
{
    /** @var SalePeriodDatatable */
    private $datatable;

    /** @var SalePeriodManager */
    private $salePeriodManager;

    public function __construct(
        SalePeriodDatatable $datatable,
        SalePeriodManager $salePeriodManager
    ) {
        $this->datatable = $datatable;
        $this->salePeriodManager = $salePeriodManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{salePeriodId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|SalePeriod $salePeriod
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(SalePeriod $salePeriod = null): RedirectResponse
    {
        if ($salePeriod) {
            try {
                $this->salePeriodManager->removeSalePeriod($salePeriod);
                $this->addFlash('success', 'administration.saleperiod.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.saleperiod.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.saleperiod.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_saleperiod_index');
    }

    /**
     * @Route("/edit/{salePeriodId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|SalePeriod $salePeriod
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, SalePeriod $salePeriod = null)
    {
        if (!$salePeriod) {
            $this->addFlash('warning', 'administration.saleperiod.search-results.nodata');

            return $this->redirectToRoute('app_administration_saleperiod_index');
        }

        $salePeriodDataObject = SalePeriodDataObject::fromSalePeriod($salePeriod);
        $salePeriodDataObject->markAsModifiedObject();

        $salePeriodForm = $this->createForm(SalePeriodType::class, $salePeriodDataObject);
        $salePeriodForm->handleRequest($request);
        if ($salePeriodForm->isSubmitted() && $salePeriodForm->isValid()) {
            $salePeriodDataObject->toSalePeriod($salePeriod);
            $this->salePeriodManager->addSalePeriod($salePeriod);
            $this->addFlash('success', 'administration.saleperiod.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_saleperiod_index'
            );
        }

        return [
            'salePeriodForm' => $salePeriodForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $salePeriodDataObject = new SalePeriodDataObject();
        $salePeriodDataObject->markAsModifiedObject();

        $salePeriodForm = $this->createForm(SalePeriodType::class, $salePeriodDataObject);
        $salePeriodForm->handleRequest($request);
        if ($salePeriodForm->isSubmitted() && $salePeriodForm->isValid()) {
            $salePeriod = new SalePeriod();
            $salePeriodDataObject->toSalePeriod($salePeriod);
            $this->salePeriodManager->addSalePeriod($salePeriod);
            $this->addFlash('success', 'administration.saleperiod.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_saleperiod_index'
            );
        }

        return [
            'salePeriodForm' => $salePeriodForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return SalePeriodDatatable
     * @throws \Exception
     */
    private function getDataTable(): SalePeriodDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_saleperiod_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}