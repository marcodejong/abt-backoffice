<?php

namespace App\Controller\Administration;

use App\Datatables\RegionDatatable;
use App\Form\DataObject\RegionDataObject;
use App\Form\RegionType;
use App\Manager\RegionManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Region;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RegionController
 * @package App\Controller\Administration
 * @Route("/region", options={"expose" = true})
 * @Resource("abt-administration:region")
 */
Class RegionController extends Controller
{
    /** @var RegionDatatable */
    private $datatable;

    /** @var RegionManager */
    private $regionManager;

    public function __construct(
        RegionDatatable $datatable,
        RegionManager $regionManager
    ) {
        $this->datatable = $datatable;
        $this->regionManager = $regionManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{regionId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|Region $region
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(Region $region = null): RedirectResponse
    {
        if ($region) {
            try {
                $this->regionManager->removeRegion($region);
                $this->addFlash('success', 'administration.region.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.region.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.region.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_region_index');
    }

    /**
     * @Route("/edit/{regionId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|Region $region
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Region $region = null)
    {
        if (!$region) {
            $this->addFlash('warning', 'administration.region.search-results.nodata');

            return $this->redirectToRoute('app_administration_region_index');
        }

        $regionDataObject = RegionDataObject::fromRegion($region);
        $regionDataObject->markAsModifiedObject();

        $regionForm = $this->createForm(RegionType::class, $regionDataObject);
        $regionForm->handleRequest($request);
        if ($regionForm->isSubmitted() && $regionForm->isValid()) {
            $regionDataObject->toRegion($region);
            $this->regionManager->addRegion($region);
            $this->addFlash('success', 'administration.region.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_region_index'
            );
        }

        return [
            'regionForm' => $regionForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $regionDataObject = new RegionDataObject();
        $regionDataObject->markAsModifiedObject();

        $regionForm = $this->createForm(RegionType::class, $regionDataObject);
        $regionForm->handleRequest($request);
        if ($regionForm->isSubmitted() && $regionForm->isValid()) {
            $region = new Region();
            $regionDataObject->toRegion($region);
            $this->regionManager->addRegion($region);
            $this->addFlash('success', 'administration.region.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_region_index'
            );
        }

        return [
            'regionForm' => $regionForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return RegionDatatable
     * @throws \Exception
     */
    private function getDataTable(): RegionDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_region_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}