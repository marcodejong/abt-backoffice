<?php

namespace App\Controller\Administration;

use App\Datatables\ImportDatatable;
use App\Datatables\PriceDatatable;
use App\Form\DataObject\FileDataObject;
use App\Form\DataObject\PriceDataObject;
use App\Form\FileType;
use App\Manager\ImportManager;
use App\Manager\PriceManager;
use App\Processor\PriceProcessor;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Configuration\PropositionElementPricedItem;
use Arriva\Abt\Entity\Configuration\RegularItem;
use Arriva\Abt\Entity\Import;
use Arriva\Abt\Entity\PropositionElement;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PriceController
 * @package App\Controller\Administration
 * @Route("/price", options={"expose" = true})
 * @Resource("abt-administration:price")
 */
Class PriceController extends Controller
{
    /** @var PriceDatatable */
    private $datatable;

    /** @var ImportDatatable */
    private $importDatatable;

    /** @var PriceManager */
    private $priceManager;

    /** @var PriceProcessor */
    private $priceProcessor;

    /** @var ImportManager */
    private $importManager;

    public function __construct(
        PriceDatatable $datatable,
        ImportDatatable $importDatatable,
        PriceManager $priceManager,
        PriceProcessor $priceProcessor,
        ImportManager $importManager
    ) {
        $this->datatable = $datatable;
        $this->priceManager = $priceManager;
        $this->importDatatable = $importDatatable;
        $this->priceProcessor = $priceProcessor;
        $this->importManager = $importManager;
    }

    /**
     * @Route("/", methods={"GET","POST"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array|redirectResponse
     */
    public function indexAction(Request $request)
    {
        $importDataObject = new FileDataObject();
        $importDataObject->markAsModifiedObject();

        $importForm = $this->createForm(FileType::class, $importDataObject);
        $importForm->handleRequest($request);
        $errors = [];
        if ($importForm->isSubmitted() && $importForm->isValid()) {
            /** @var UploadedFile $file */
            $file = $importDataObject->getFile();
            if ($file !== null) {
                $importResult = $this->priceProcessor->importCsv($file);
                if (!$importResult->hasMessages()) {

                    $import = new Import();
                    $import->setFilename($file->getClientOriginalName());
                    $import->setUser($this->getUser()->getUsername());
                    $this->importManager->addImport($import);

                    $this->addFlash('success', 'administration.price.import.add.succeeded');

                    return $this->redirectToRoute(
                        'app_administration_price_index'
                    );
                } else {
                    $this->addFlash('error', 'administration.price.import.import.failed');
                    $errors = $importResult->getMessages();
                }
            } else {
                $this->addFlash('error', 'administration.price.import.import.failed');
            }
        }

        return [
            'importForm' => $importForm->createView(),
            "datatable" => $this->getDataTable(),
            "importDatatable" => $this->getImportDataTable(),
            'errors' => $errors
        ];
    }

    /**
     * @Route("/example", methods={"GET"})
     * @Privilege("read")
     *
     * @return Response
     */
    public function exampleAction(): Response
    {

        return $this->priceProcessor->exampleCsv();
    }

    /**
     * @Route("/edit/amount", methods={"POST"})
     * @Privilege("update")
     *
     * @
     * @param Request $request
     * @return Response
     */
    public function editAmountAction(Request $request): Response
    {
        $price = $this->priceManager->getPriceById($request->get('pk'));
        if ($price) {
            $amount = \str_replace(',', '.', $request->get('value'));

            if (is_numeric($amount) && $amount >= 0) {
                $priceDataObject = PriceDataObject::fromPrice($price);
                $priceDataObject->markAsModifiedObject();
                $priceDataObject->amount = $amount;
                $priceDataObject->toPrice($price);
                $this->priceManager->addPrice($price);

                return new Response('', 200);
            }
        }

        return new JsonResponse('Vul een juiste waarde in.', 400);
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());

        /** @var QueryBuilder $qb */
        $qb = $responseService->getDatatableQueryBuilder()->getQb();
        $qb->select('price');
        $qb->leftJoin(PropositionElementPricedItem::class, 'i', 'WITH', 'price.item = i');
        $qb->leftJoin(PropositionElement::class, 'pe', 'WITH', 'i.propositionElement = pe');
        $qb->where('i INSTANCE OF ' . RegularItem::class);
        return $responseService->getResponse();
    }


    /**
     * @Route("/import/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function importDatatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getImportDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return PriceDatatable
     * @throws \Exception
     */
    private function getDataTable(): PriceDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_price_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }

    /**
     * @return ImportDatatable
     * @throws \Exception
     */
    private function getImportDataTable(): ImportDatatable
    {
        $this->importDatatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_price_importdatatable'
                    ),
                ],
            ]
        );

        return $this->importDatatable;
    }
}