<?php

namespace App\Controller\Administration;

use App\Datatables\DiscountDatatable;
use App\Form\DataObject\DiscountDataObject;
use App\Form\DiscountType;
use App\Manager\DiscountManager;
use App\Manager\PropositionManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Discount;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DiscountController
 * @package App\Controller\Administration
 * @Route("/discount", options={"expose" = true})
 * @Resource("abt-administration:discount")
 */
Class DiscountController extends Controller
{
    /** @var DiscountDatatable */
    private $datatable;

    /** @var DiscountManager */
    private $discountManager;

    /** @var PropositionManager */
    private $propositionManager;

    public function __construct(
        DiscountDatatable $datatable,
        DiscountManager $discountManager,
        PropositionManager $propositionManager
    ) {
        $this->datatable = $datatable;
        $this->discountManager = $discountManager;
        $this->propositionManager = $propositionManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{$discountId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|Discount $discount
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(Discount $discount = null): RedirectResponse
    {
        if ($discount) {
            try {
                $this->discountManager->removeDiscount($discount);
                $this->addFlash('success', 'administration.discount.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.discount.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.discount.delete.not-found');
        }

        return $this->redirectToRoute('app_administration_discount_index');
    }

    /**
     * @Route("/edit/{discountId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|Discount $discount
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Discount $discount = null)
    {
        if (!$discount) {
            $this->addFlash('warning', 'administration.discount.edit.not-found');

            return $this->redirectToRoute('app_administration_discount_index');
        }

        $discountDataObject = DiscountDataObject::fromDiscount($discount);
        $discountDataObject->markAsModifiedObject();

        $discountForm = $this->createForm(DiscountType::class, $discountDataObject);
        $discountForm->handleRequest($request);

        $propositions = $discountDataObject->propositions;

        if ($discountForm->isSubmitted() && $discountForm->isValid()) {
            $discountDataObject->toDiscount($discount);
            $this->discountManager->addDiscount($discount);
            $this->addFlash('success', 'administration.discount.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_discount_index'
            );
        }

        return [
            'discountForm' => $discountForm->createView(),
            'allPropositions' => $this->propositionManager->getAllPropositions(),
            'selectedPropositions' => $propositions
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $discountDataObject = new DiscountDataObject();
        $discountDataObject->markAsModifiedObject();

        $discountForm = $this->createForm(DiscountType::class, $discountDataObject);
        $discountForm->handleRequest($request);

        $propositions = $discountDataObject->propositions;

        if ($discountForm->isSubmitted() && $discountForm->isValid()) {
            $discount = new Discount();
            $discountDataObject->toDiscount($discount);
            $this->discountManager->addDiscount($discount);
            $this->addFlash('success', 'administration.discount.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_discount_index'
            );
        }

        return [
            'discountForm' => $discountForm->createView(),
            'allPropositions' => $this->propositionManager->getAllPropositions(),
            'selectedPropositions' => $propositions
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return DiscountDatatable
     * @throws \Exception
     */
    private function getDataTable(): DiscountDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_discount_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}