<?php


namespace App\Controller\Administration;

use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use App\Service\ProductApi\ProductApiRestClient;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MemcacheController
 * @package App\Controller\Administration
 * @Route("/product-model", options={"expose" = true})
 * @Resource("abt-administration:product-model")
 */
class ProductModelController
{
    /** @var ProductApiRestClient */
    private $productApiRestClient;

    public function __construct(ProductApiRestClient $productApiRestClient)
    {
        $this->productApiRestClient = $productApiRestClient;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @return array
     */
    public function indexAction(): array
    {
        return [];
    }

    /**
     * @Route("/build-model", methods={"GET"})
     */
    public function buildModel(): JsonResponse
    {
        $statusCode = $this->productApiRestClient->buildModel();

        return new JsonResponse(['response_code' => $statusCode]);
    }

    /**
     * @Route("/build-model-status", methods={"GET"})
     */
    public function buildModelStatus(): JsonResponse
    {
        $statusCode = $this->productApiRestClient->getBuildModelStatus();

        return new JsonResponse(['response_code' => $statusCode]);
    }
}