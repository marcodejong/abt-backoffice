<?php


namespace App\Controller\Administration;


use App\Form\DataObject\ExportDataObject;
use App\Form\Export\DebtorReceivablesType;
use App\Form\Export\ExportType;
use App\Form\Export\InvoicedTransactionsType;
use App\Form\Export\JournalTransactionsType;
use App\Form\Export\JournalType;
use App\Form\Export\MonthlyJournalType;
use App\Form\Export\TariffConfigurationType;
use App\Form\Value\ExportFormType;
use App\Model\Export\Exporter;
use App\Model\Export\Target\CsvFile;
use App\Model\Report\DebtorReceivables\DebtorReceivablesReportInput;
use App\Model\Report\DebtorReceivables\DebtorReceivablesReportModel;
use App\Model\Report\InvoicedTransactions\InvoicedTransactionsReportInput;
use App\Model\Report\InvoicedTransactions\InvoicedTransactionsReportModel;
use App\Model\Report\Journal\JournalReportInput;
use App\Model\Report\Journal\JournalReportModel;
use App\Model\Report\JournalTransactions\JournalTransactionsInput;
use App\Model\Report\JournalTransactions\JournalTransactionsModel;
use App\Model\Report\TariffConfiguration\TariffConfigurationReportModel;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\DateRange;
use Arriva\Abt\Value\LocalFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExportController
 * @package App\Controller\Administration
 * @Route("/export", options={"expose" = true})
 * @Resource("abt-administration:export")
 */
class ExportController extends Controller
{
    /** @var TariffConfigurationReportModel */
    private $tariffConfigurationReportModel;

    /** @var JournalReportModel */
    private $journalReportModel;

    /** @var DebtorReceivablesReportModel */
    private $debtorReceivablesReportModel;

    /** @var InvoicedTransactionsReportModel */
    private $invoicedTransactionsReportModel;

    /** @var JournalTransactionsModel */
    private $journalTransactionsReportModel;

    /** @var Exporter */
    private $exporter;

    /** @var string */
    private $fileBasePath;

    public function __construct(
        TariffConfigurationReportModel $tariffConfigurationReportModel,
        JournalReportModel $journalReportModel,
        DebtorReceivablesReportModel $debtorReceivablesReportModel,
        InvoicedTransactionsReportModel $invoicedTransactionsReportModel,
        JournalTransactionsModel $journalTransactionsReportModel,
        Exporter $exporter,
        string $fileBasePath
    ) {
        $this->tariffConfigurationReportModel = $tariffConfigurationReportModel;
        $this->journalReportModel = $journalReportModel;
        $this->debtorReceivablesReportModel = $debtorReceivablesReportModel;
        $this->invoicedTransactionsReportModel = $invoicedTransactionsReportModel;
        $this->journalTransactionsReportModel = $journalTransactionsReportModel;
        $this->exporter = $exporter;
        $this->fileBasePath = $fileBasePath;
    }

    /**
     * @Route("/", methods={"GET", "POST"})
     * @Privilege("read")
     * @Template
     * @param Request $request
     * @return array|BinaryFileResponse
     */
    public function indexAction(Request $request)
    {
        $exportForm = $this->getSelectedForm($request->get('type'));
        $exportForm->handleRequest($request);

        if ($exportForm->isSubmitted() && $exportForm->isValid()) {
            $filePath = $this->generateExportFile($exportForm);

            $response = new BinaryFileResponse($filePath);
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
            $response->deleteFileAfterSend();

            return $response;
        }

        return [
            'exportForm' => $exportForm->createView()
        ];
    }

    private function getSelectedForm(?string $formType = null): ?FormInterface
    {
        $exportDataObject = new ExportDataObject();

        switch ($formType) {
            case ExportFormType::DEBTOR_RECEIVABLES()->getId():
                $exportDataObject->setExportType(ExportFormType::DEBTOR_RECEIVABLES);
                return $this->createForm(DebtorReceivablesType::class, $exportDataObject);
            case ExportFormType::INVOICED_TRANSACTIONS()->getId():
                $exportDataObject->setExportType(ExportFormType::INVOICED_TRANSACTIONS);
                return $this->createForm(InvoicedTransactionsType::class, $exportDataObject);
            case ExportFormType::JOURNAL()->getId():
                $exportDataObject->setExportType(ExportFormType::JOURNAL);
                return $this->createForm(JournalType::class, $exportDataObject);
            case ExportFormType::JOURNAL_TRANSACTIONS()->getId():
                $exportDataObject->setExportType(ExportFormType::JOURNAL_TRANSACTIONS);
                return $this->createForm(JournalTransactionsType::class, $exportDataObject);
            case ExportFormType::MONTHLY_JOURNAL()->getId():
                $exportDataObject->setExportType(ExportFormType::MONTHLY_JOURNAL);
                return $this->createForm(MonthlyJournalType::class, $exportDataObject);
            case ExportFormType::TARIFF_CONFIGURATION()->getId():
                $exportDataObject->setExportType(ExportFormType::TARIFF_CONFIGURATION);
                return $this->createForm(TariffConfigurationType::class, $exportDataObject);
            default:
                return $this->createForm(ExportType::class, $exportDataObject);
        }
    }

    private function generateExportFile(FormInterface $form): string
    {
        $formType = $form->getConfig()->getType()->getInnerType();

        /** @var ExportDataObject $data */
        $data = $form->getViewData();
        $date = $data->getDate() ? Date::fromDateTime($data->getDate()) : null;

        switch (true) {
            case $formType instanceof DebtorReceivablesType:
                $filename = sprintf('%s_debiteurenvorderingen_abt_1.csv', $date->format('Ymd'));
                $report =  $this->debtorReceivablesReportModel->generateDebtorReceivablesReport(
                    new DebtorReceivablesReportInput(DateRange::of($date, $date))
                );
                break;
            case $formType instanceof InvoicedTransactionsType:
                $filename = sprintf('%s_gefactureerde_ritten_abt_1.csv', $date->format('Ymd'));
                $report =  $this->invoicedTransactionsReportModel->generateInvoicedTransactionsReport(
                    new InvoicedTransactionsReportInput(DateRange::of($date, $date))
                );
                break;
            case $formType instanceof JournalType:
                $filename = sprintf('%s_journal_abt.csv', $date->format('Ymd'));
                $report =  $this->journalReportModel->generateJournalReport(
                    new JournalReportInput(DateRange::of($date, $date))
                );
                break;
            case $formType instanceof JournalTransactionsType:
                $filename = sprintf('%s_journal_abttransactions_1.csv', $date->format('Ymd'));
                $report =  $this->journalTransactionsReportModel->generateJournalTransactionsReport(
                    new JournalTransactionsInput(DateRange::of($date, $date))
                );
                break;
            case $formType instanceof MonthlyJournalType:
                $filename = sprintf('%s_journal_abt.csv', $date->format('md'));
                $report =  $this->journalReportModel->generateJournalReport(
                    new JournalReportInput(DateRange::of($date->getStartOfMonth(), $date->getEndOfMonth()))
                );
                break;
            case $formType instanceof TariffConfigurationType:
                $filename = 'tarieven_abt.csv';
                $report = $this->tariffConfigurationReportModel->generateTariffConfigurationReportModel(
                    $data->getConfigurationType(),
                    $data->getPricePeriod()
                );
                break;
            default:
                throw new \DomainException(sprintf('FormType %s not allowed', get_class($formType)));
        }

        $path = $this->fileBasePath.$filename;
        $target = new CsvFile(LocalFile::of($path));

        if ($this->exporter->supportsExport($report, $target)) {
            $this->exporter->export($report, $target);
            return $path;
        }

        throw new \DomainException(sprintf('no exporter found for %s', get_class($report)));
    }
}