<?php

namespace App\Controller\Administration;

use App\Datatables\PropositionElementDatatable;
use App\Form\DataObject\PropositionElementDataObject;
use App\Form\PropositionElementType;
use App\Manager\PropositionElementManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\PropositionElement;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropositionElementController
 * @package App\Controller\Administration
 * @Route("/proposition-element", options={"expose" = true})
 * @Resource("abt-administration:proposition-element")
 */
Class PropositionElementController extends Controller
{
    /** @var PropositionElementDatatable */
    private $datatable;

    /** @var PropositionElementManager */
    private $propositionElementManager;

    public function __construct(
        PropositionElementDatatable $datatable,
        PropositionElementManager $propositionElementManager
    ) {
        $this->datatable = $datatable;
        $this->propositionElementManager = $propositionElementManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{$propositionElementId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|PropositionElement $propositionElement
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(PropositionElement $propositionElement = null): RedirectResponse
    {
        if ($propositionElement) {
            try {
                $this->propositionElementManager->removePropositionElement($propositionElement);
                $this->addFlash('success', 'administration.proposition-element.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.proposition-element.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.proposition-element.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_propositionelement_index');
    }

    /**
     * @Route("/edit/{propositionElementId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|PropositionElement $propositionElement
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, PropositionElement $propositionElement = null)
    {
        if (!$propositionElement) {
            $this->addFlash('warning', 'administration.proposition-element.edit.cannot_find');

            return $this->redirectToRoute('app_administration_propositionelement_index');
        }

        $propositionElementDataObject = PropositionElementDataObject::fromPropositionElement($propositionElement);
        $propositionElementDataObject->markAsModifiedObject();

        $propositionElementForm = $this->createForm(PropositionElementType::class, $propositionElementDataObject);
        $propositionElementForm->handleRequest($request);
        if ($propositionElementForm->isSubmitted() && $propositionElementForm->isValid()) {
            $propositionElementDataObject->toPropositionElement($propositionElement);
            $this->propositionElementManager->addPropositionElement($propositionElement);
            $this->addFlash('success', 'administration.proposition-element.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositionelement_index'
            );
        }


        return [
            'propositionElementForm' => $propositionElementForm->createView(),
        ];
    }

    /**
     * @Route("/copy/{propositionElementId}", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @param null|PropositionElement $propositionElement
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function copyAction(Request $request, PropositionElement $propositionElement = null)
    {
        if (!$propositionElement) {
            $this->addFlash('warning', 'administration.proposition-element.copy.cannot_find');

            return $this->redirectToRoute('app_administration_propositionelement_index');
        }

        $propositionElementDataObject = PropositionElementDataObject::fromPropositionElement($propositionElement);
        $propositionElementDataObject->markAsModifiedObject();

        $propositionElementForm = $this->createForm(PropositionElementType::class, $propositionElementDataObject);
        $propositionElementForm->handleRequest($request);
        if ($propositionElementForm->isSubmitted() && $propositionElementForm->isValid()) {
            $propositionElement = new PropositionElement();
            $propositionElementDataObject->toPropositionElement($propositionElement);
            $this->propositionElementManager->addPropositionElement($propositionElement);
            $this->addFlash('success', 'administration.proposition-element.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositionelement_index'
            );
        }


        return [
            'propositionElementForm' => $propositionElementForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $propositionElementDataObject = new PropositionElementDataObject();
        $propositionElementDataObject->markAsModifiedObject();
        $propositionElementForm = $this->createForm(PropositionElementType::class, $propositionElementDataObject);
        $propositionElementForm->handleRequest($request);
        if ($propositionElementForm->isSubmitted() && $propositionElementForm->isValid()) {
            $propositionElement = new PropositionElement();
            $propositionElementDataObject->toPropositionElement($propositionElement);
            $this->propositionElementManager->addPropositionElement($propositionElement);
            $this->addFlash('success', 'administration.proposition-element.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositionelement_index'
            );
        }

        return [
            'propositionElementForm' => $propositionElementForm->createView()
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return PropositionElementDatatable
     * @throws \Exception
     */
    private function getDataTable(): PropositionElementDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_propositionelement_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}