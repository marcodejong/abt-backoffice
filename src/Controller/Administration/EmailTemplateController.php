<?php

namespace App\Controller\Administration;

use App\Datatables\EmailTemplateDatatable;
use App\Form\DataObject\EmailTemplateDataObject;
use App\Form\EmailTemplateType;
use App\Manager\EmailTemplateManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\EmailTemplate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EmailTemplateController
 * @package App\Controller\Administration
 * @Route("/email-template", options={"expose" = true})
 * @Resource("abt-administration:email-template")
 */
Class EmailTemplateController extends Controller
{
    /** @var EmailTemplateDatatable */
    private $datatable;

    /** @var EmailTemplateManager */
    private $emailTemplateManager;

    public function __construct(
        EmailTemplateDatatable $datatable,
        EmailTemplateManager $emailTemplateManager
    ) {
        $this->datatable = $datatable;
        $this->emailTemplateManager = $emailTemplateManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/edit/{emailTemplateId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|EmailTemplate $emailTemplate
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, EmailTemplate $emailTemplate = null)
    {
        if (!$emailTemplate) {
            $this->addFlash('warning', 'administration.email-template.search-results.nodata');

            return $this->redirectToRoute('app_administration_emailtemplate_index');
        }

        $emailTemplateDataObject = EmailTemplateDataObject::fromEmailTemplate($emailTemplate);
        $emailTemplateDataObject->markAsModifiedObject();

        $emailTemplateForm = $this->createForm(EmailTemplateType::class, $emailTemplateDataObject);
        $emailTemplateForm->handleRequest($request);
        if ($emailTemplateForm->isSubmitted() && $emailTemplateForm->isValid()) {
            $emailTemplateDataObject->toEmailTemplate($emailTemplate);
            $this->emailTemplateManager->addEmailTemplate($emailTemplate);
            $this->addFlash('success', 'administration.email-template.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_emailtemplate_index'
            );
        }

        return [
            'emailTemplateForm' => $emailTemplateForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return EmailTemplateDatatable
     * @throws \Exception
     */
    private function getDataTable(): EmailTemplateDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_emailtemplate_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}