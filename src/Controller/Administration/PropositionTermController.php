<?php

namespace App\Controller\Administration;

use App\Datatables\PropositionTermDatatable;
use App\Form\DataObject\PropositionTermDataObject;
use App\Form\PropositionTermType;
use App\Manager\PropositionTermManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Configuration\PropositionTerm\AbstractPropositionTerm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropositionTermController
 * @package App\Controller\Administration
 * @Route("/proposition-term", options={"expose" = true})
 * @Resource("abt-administration:proposition-term")
 */
Class PropositionTermController extends Controller
{
    /** @var PropositionTermDatatable */
    private $datatable;

    /** @var PropositionTermManager */
    private $propositionManager;

    public function __construct(
        PropositionTermDatatable $datatable,
        PropositionTermManager $propositionManager
    ) {
        $this->datatable = $datatable;
        $this->propositionManager = $propositionManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{$propositionTermId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|AbstractPropositionTerm $propositionTerm
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(AbstractPropositionTerm $propositionTerm = null): RedirectResponse
    {
        if ($propositionTerm) {
            try {
                $this->propositionManager->removePropositionTerm($propositionTerm);
                $this->addFlash('success', 'administration.proposition-term.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.proposition-term.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.proposition-term.delete.cannot-find-term');
        }

        return $this->redirectToRoute('app_administration_propositionterm_index');
    }

    /**
     * @Route("/edit/{abstractPropositionTermId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|AbstractPropositionTerm $propositionTerm
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, AbstractPropositionTerm $propositionTerm = null)
    {
        if (!$propositionTerm) {
            $this->addFlash('warning', 'administration.proposition-term.edit.cannot-find-term');

            return $this->redirectToRoute('app_administration_propositionterm_index');
        }

        $propositionTermDataObject = PropositionTermDataObject::fromPropositionTerm($propositionTerm);
        $propositionTermDataObject->markAsModifiedObject();

        $propositionTermForm = $this->createForm(PropositionTermType::class, $propositionTermDataObject);
        $propositionTermForm->handleRequest($request);
        if ($propositionTermForm->isSubmitted() && $propositionTermForm->isValid()) {
            $propositionTermDataObject->toPropositionTerm($propositionTerm);
            $this->propositionManager->addPropositionTerm($propositionTerm);
            $this->addFlash('success', 'administration.proposition-term.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositionterm_index'
            );
        }

        return [
            'propositionTermForm' => $propositionTermForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $propositionTermDataObject = new PropositionTermDataObject();
        $propositionTermDataObject->markAsModifiedObject();

        $propositionTermForm = $this->createForm(PropositionTermType::class, $propositionTermDataObject);
        $propositionTermForm->handleRequest($request);
        if ($propositionTermForm->isSubmitted() && $propositionTermForm->isValid()) {
            $propositionTerm = $propositionTermDataObject->toNewPropositionTerm();
            $this->propositionManager->addPropositionTerm($propositionTerm);
            $this->addFlash('success', 'administration.proposition-term.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositionterm_index'
            );
        }

        return [
            'propositionTermForm' => $propositionTermForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return PropositionTermDatatable
     * @throws \Exception
     */
    private function getDataTable(): PropositionTermDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_propositionterm_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}