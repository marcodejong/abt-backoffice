<?php

namespace App\Controller\Administration;

use App\Datatables\StationDatatable;
use App\Form\DataObject\FileDataObject;
use App\Form\DataObject\StationDataObject;
use App\Form\FileType;
use App\Form\StationType;
use App\Manager\RegionManager;
use App\Manager\StationManager;
use App\Processor\StationProcessor;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Station;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StationController
 * @package App\Controller\Administration
 * @Route("/station", options={"expose" = true})
 * @Resource("abt-administration:station")
 */
Class StationController extends Controller
{
    /** @var StationDatatable */
    private $datatable;

    /** @var StationManager */
    private $stationManager;

    /** @var RegionManager */
    private $regionManager;

    /** @var StationProcessor */
    private $stationProcessor;

    public function __construct(
        StationDatatable $datatable,
        StationManager $stationManager,
        RegionManager $regionManager,
        StationProcessor $stationProcessor
    ) {
        $this->datatable = $datatable;
        $this->stationManager = $stationManager;
        $this->regionManager = $regionManager;
        $this->stationProcessor = $stationProcessor;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/example", methods={"GET"})
     * @Privilege("read")
     *
     * @return Response
     */
    public function exampleAction(): Response
    {

        return $this->stationProcessor->exampleCsv();
    }

    /**
     * @Route("/delete/{stationId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|Station $station
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(Station $station = null): RedirectResponse
    {
        if ($station) {
            try {
                $this->stationManager->removeStation($station);
                $this->addFlash('success', 'administration.station.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.station.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.station.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_station_index');
    }

    /**
     * @Route("/edit/{stationId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|Station $station
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Station $station = null)
    {
        if (!$station) {
            $this->addFlash('warning', 'administration.station.search-results.nodata');

            return $this->redirectToRoute('app_administration_station_index');
        }

        $stationDataObject = StationDataObject::fromStation($station);
        $stationDataObject->markAsModifiedObject();

        $stationForm = $this->createForm(StationType::class, $stationDataObject);
        $stationForm->handleRequest($request);

        $regions = $stationDataObject->regions;

        if ($stationForm->isSubmitted() && $stationForm->isValid()) {
            $stationDataObject->toStation($station);
            $this->stationManager->addStation($station);
            $this->addFlash('success', 'administration.station.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_station_index'
            );
        }

        return [
            'allRegions' => $this->regionManager->getRegions(),
            'selectedRegions' => $regions,
            'stationForm' => $stationForm->createView(),
        ];
    }

    /**
     * @Route("/import", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @throws \League\Csv\Exception
     * @return array|RedirectResponse
     */
    public function importAction(Request $request)
    {
        $importDataObject = new FileDataObject();
        $importDataObject->markAsModifiedObject();

        $importForm = $this->createForm(FileType::class, $importDataObject);
        $importForm->handleRequest($request);

        $errors = [];
        if ($importForm->isSubmitted() && $importForm->isValid()) {

            $fileName = $importForm->get('file')->getData();
            $importResult = $this->stationProcessor->importCsv($fileName);
            if (!$importResult->hasMessages()) {
                $this->addFlash('success', 'administration.station.add.succeeded');

                return $this->redirectToRoute(
                    'app_administration_station_index'
                );
            } else {
                $this->addFlash('error', 'administration.station.import.failed');
                $errors = $importResult->getMessages();
            }

        }

        return [
            'importForm' => $importForm->createView(),
            'errors' => $errors
        ];
    }


    /**
     * @Route("/export/{codes}", methods={"GET","POST"})
     * @Privilege("read")
     *
     * @param string $codes
     * @return Response
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     */
    public function exportAction(string $codes): Response
    {

        return $this->stationProcessor->exportCsv($codes);
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return StationDatatable
     * @throws \Exception
     */
    private function getDataTable(): StationDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_station_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}