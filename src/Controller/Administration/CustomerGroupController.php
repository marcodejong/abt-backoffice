<?php

namespace App\Controller\Administration;

use App\Datatables\CustomerGroupDatatable;
use App\Form\DataObject\CustomerGroupDataObject;
use App\Form\CustomerGroupType;
use App\Manager\CustomerGroupManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\CustomerGroup;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CustomerGroupController
 * @package App\Controller\Administration
 * @Route("/customer-group", options={"expose" = true})
 * @Resource("abt-administration:customer-group")
 */
Class CustomerGroupController extends Controller
{
    /** @var CustomerGroupDatatable */
    private $datatable;

    /** @var CustomerGroupManager */
    private $customerGroupManager;

    public function __construct(
        CustomerGroupDatatable $datatable,
        CustomerGroupManager $customerGroupManager
    ) {
        $this->datatable = $datatable;
        $this->customerGroupManager = $customerGroupManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{customerGroupId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|CustomerGroup $customerGroup
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(CustomerGroup $customerGroup = null): RedirectResponse
    {
        if ($customerGroup) {
            try {
                $this->customerGroupManager->removeCustomerGroup($customerGroup);
                $this->addFlash('success', 'administration.customergroup.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.customergroup.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.customergroup.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_customergroup_index');
    }

    /**
     * @Route("/edit/{customerGroupId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|CustomerGroup $customerGroup
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, CustomerGroup $customerGroup = null)
    {
        if (!$customerGroup) {
            $this->addFlash('warning', 'administration.customergroup.edit.cannot_find_customergroup');

            return $this->redirectToRoute('app_administration_customergroup_index');
        }

        $customerGroupDataObject = CustomerGroupDataObject::fromCustomerGroup($customerGroup);
        $customerGroupDataObject->markAsModifiedObject();

        $customerGroupForm = $this->createForm(CustomerGroupType::class, $customerGroupDataObject);
        $customerGroupForm->handleRequest($request);
        if ($customerGroupForm->isSubmitted() && $customerGroupForm->isValid()) {
            $customerGroupDataObject->toCustomergroup($customerGroup);
            $this->customerGroupManager->addCustomerGroup($customerGroup);
            $this->addFlash('success', 'administration.customergroup.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_customergroup_index'
            );
        }

        return [
            'customerGroupForm' => $customerGroupForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $customerGroupDataObject = new CustomerGroupDataObject();
        $customerGroupDataObject->markAsModifiedObject();

        $customerGroupForm = $this->createForm(CustomerGroupType::class, $customerGroupDataObject);
        $customerGroupForm->handleRequest($request);
        if ($customerGroupForm->isSubmitted() && $customerGroupForm->isValid()) {
            $customerGroup = new CustomerGroup();
            $customerGroupDataObject->toCustomerGroup($customerGroup);
            $this->customerGroupManager->addCustomerGroup($customerGroup);
            $this->addFlash('success', 'administration.customergroup.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_customergroup_index'
            );
        }

        return [
            'customerGroupForm' => $customerGroupForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return CustomerGroupDatatable
     * @throws \Exception
     */
    private function getDataTable(): CustomerGroupDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_customergroup_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}