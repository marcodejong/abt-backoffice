<?php

namespace App\Controller\Administration;

use App\Datatables\VatRateDatatable;
use App\Form\DataObject\VatRateDataObject;
use App\Form\VatRateType;
use App\Manager\VatRateManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\VatRate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VatRateController
 * @package App\Controller\Administration
 * @Route("/vat-rate", options={"expose" = true})
 * @Resource("abt-administration:vat-rate")
 */
Class VatRateController extends Controller
{
    /** @var VatRateDatatable */
    private $datatable;

    /** @var VatRateManager */
    private $vatRateManager;

    public function __construct(
        VatRateDatatable $datatable,
        VatRateManager $vatRateManager
    ) {
        $this->datatable = $datatable;
        $this->vatRateManager = $vatRateManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{vatRateId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|VatRate $vatRate
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(VatRate $vatRate = null): RedirectResponse
    {
        if ($vatRate) {
            try {
                $this->vatRateManager->removeVatRate($vatRate);
                $this->addFlash('success', 'administration.vat-rate.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.vat-rate.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.vat-rate.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_vatrate_index');
    }

    /**
     * @Route("/edit/{vatRateId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|VatRate $vatRate
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, VatRate $vatRate = null)
    {
        if (!$vatRate) {
            $this->addFlash('warning', 'administration.vat-rate.search-results.nodata');

            return $this->redirectToRoute('app_administration_vatrate_index');
        }

        $vatRateDataObject = VatRateDataObject::fromVatRate($vatRate);
        $vatRateDataObject->markAsModifiedObject();

        $vatRateForm = $this->createForm(VatRateType::class, $vatRateDataObject);
        $vatRateForm->handleRequest($request);
        if ($vatRateForm->isSubmitted() && $vatRateForm->isValid()) {
            $vatRateDataObject->toVatRate($vatRate);
            $this->vatRateManager->addVatRate($vatRate);
            $this->addFlash('success', 'administration.vat-rate.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_vatrate_index'
            );
        }

        return [
            'vatRateForm' => $vatRateForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $vatRateDataObject = new VatRateDataObject();
        $vatRateDataObject->markAsModifiedObject();

        $vatRateForm = $this->createForm(VatRateType::class, $vatRateDataObject);
        $vatRateForm->handleRequest($request);
        if ($vatRateForm->isSubmitted() && $vatRateForm->isValid()) {
            $vatRate = new VatRate();
            $vatRateDataObject->toVatRate($vatRate);
            $this->vatRateManager->addVatRate($vatRate);
            $this->addFlash('success', 'administration.vat-rate.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_vatrate_index'
            );
        }

        return [
            'vatRateForm' => $vatRateForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return VatRateDatatable
     * @throws \Exception
     */
    private function getDataTable(): VatRateDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_vatrate_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}