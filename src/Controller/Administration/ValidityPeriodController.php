<?php

namespace App\Controller\Administration;

use App\Datatables\ValidityPeriodDatatable;
use App\Form\DataObject\ValidityPeriodDataObject;
use App\Form\ValidityPeriodType;
use App\Manager\ValidityPeriodManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\ValidityPeriod;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ValidityPeriodController
 * @package App\Controller\Administration
 * @Route("/validity-period", options={"expose" = true})
 * @Resource("abt-administration:validity-period")
 */
Class ValidityPeriodController extends Controller
{
    /** @var ValidityPeriodDatatable */
    private $datatable;

    /** @var ValidityPeriodManager */
    private $validityPeriodManager;

    public function __construct(
        ValidityPeriodDatatable $datatable,
        ValidityPeriodManager $validityPeriodManager
    ) {
        $this->datatable = $datatable;
        $this->validityPeriodManager = $validityPeriodManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{validityPeriodId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|ValidityPeriod $validityPeriod
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(ValidityPeriod $validityPeriod = null): RedirectResponse
    {
        if ($validityPeriod) {
            try {
                $this->validityPeriodManager->removeValidityPeriod($validityPeriod);
                $this->addFlash('success', 'administration.validityperiod.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.validityperiod.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.validityperiod.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_validityperiod_index');
    }

    /**
     * @Route("/edit/{validityPeriodId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|ValidityPeriod $validityPeriod
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, ValidityPeriod $validityPeriod = null)
    {
        if (!$validityPeriod) {
            $this->addFlash('warning', 'administration.validityperiod.search-results.nodata');

            return $this->redirectToRoute('app_administration_validityperiod_index');
        }

        $validityPeriodDataObject = ValidityPeriodDataObject::fromValidityPeriod($validityPeriod);
        $validityPeriodDataObject->markAsModifiedObject();

        $validityPeriodForm = $this->createForm(ValidityPeriodType::class, $validityPeriodDataObject);
        $validityPeriodForm->handleRequest($request);
        if ($validityPeriodForm->isSubmitted() && $validityPeriodForm->isValid()) {
            $validityPeriodDataObject->toValidityPeriod($validityPeriod);
            $this->validityPeriodManager->addValidityPeriod($validityPeriod);
            $this->addFlash('success', 'administration.validityperiod.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_validityperiod_index'
            );
        }

        return [
            'validityPeriodForm' => $validityPeriodForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $validityPeriodDataObject = new ValidityPeriodDataObject();
        $validityPeriodDataObject->markAsModifiedObject();

        $validityPeriodForm = $this->createForm(ValidityPeriodType::class, $validityPeriodDataObject);
        $validityPeriodForm->handleRequest($request);
        if ($validityPeriodForm->isSubmitted() && $validityPeriodForm->isValid()) {
            $validityPeriod = new ValidityPeriod();
            $validityPeriodDataObject->toValidityPeriod($validityPeriod);
            $this->validityPeriodManager->addValidityPeriod($validityPeriod);
            $this->addFlash('success', 'administration.validityperiod.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_validityperiod_index'
            );
        }

        return [
            'validityPeriodForm' => $validityPeriodForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return ValidityPeriodDatatable
     * @throws \Exception
     */
    private function getDataTable(): ValidityPeriodDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_validityperiod_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}