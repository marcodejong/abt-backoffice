<?php

namespace App\Controller\Administration;

use App\Datatables\PropositionGroupDatatable;
use App\Form\DataObject\PropositionGroupDataObject;
use App\Form\PropositionGroupType;
use App\Manager\ModalityManager;
use App\Manager\PropositionGroupManager;
use App\Manager\RegionManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\PropositionGroup;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropositionGroupController
 * @package App\Controller\Administration
 * @Route("/proposition-group", options={"expose" = true})
 * @Resource("abt-administration:proposition-group")
 */
Class PropositionGroupController extends Controller
{
    /** @var PropositionGroupDatatable */
    private $datatable;

    /** @var PropositionGroupManager */
    private $propositionManager;

    /** @var RegionManager */
    private $regionManager;

    /** @var ModalityManager */
    private $modalityManager;

    public function __construct(
        PropositionGroupDatatable $datatable,
        PropositionGroupManager $propositionManager,
        RegionManager $regionManager,
        ModalityManager $modalityManager
    ) {
        $this->datatable = $datatable;
        $this->propositionManager = $propositionManager;
        $this->regionManager = $regionManager;
        $this->modalityManager = $modalityManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{$propositionGroupId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|PropositionGroup $propositionGroup
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(PropositionGroup $propositionGroup = null): RedirectResponse
    {
        if ($propositionGroup) {
            try {
                $this->propositionManager->removePropositionGroup($propositionGroup);
                $this->addFlash('success', 'administration.proposition-group.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.proposition-group.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.proposition-group.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_propositiongroup_index');
    }

    /**
     * @Route("/edit/{propositionGroupId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|PropositionGroup $propositionGroup
     * @throws \ConfigurationException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, PropositionGroup $propositionGroup = null)
    {
        if (!$propositionGroup) {
            $this->addFlash('warning', 'administration.proposition-group.edit.cannot_find_term');

            return $this->redirectToRoute('app_administration_propositiongroup_index');
        }

        $propositionGroupDataObject = PropositionGroupDataObject::fromPropositionGroup($propositionGroup);
        $propositionGroupDataObject->markAsModifiedObject();

        $propositionGroupForm = $this->createForm(PropositionGroupType::class, $propositionGroupDataObject);
        $propositionGroupForm->handleRequest($request);

        $regions = $propositionGroupDataObject->regions;
        $modalities = $propositionGroupDataObject->modalities;

        if ($propositionGroupForm->isSubmitted() && $propositionGroupForm->isValid()) {
            $propositionGroupDataObject->toPropositionGroup($propositionGroup);
            $this->propositionManager->addPropositionGroup($propositionGroup);
            $this->addFlash('success', 'administration.proposition-group.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositiongroup_index'
            );
        }


        return [
            'propositionGroupForm' => $propositionGroupForm->createView(),
            'allRegions' => $this->regionManager->getRegions(),
            'selectedRegions' => $regions,
            'allModalities' => $this->modalityManager->getAllModalities(),
            'selectedModalities' => $modalities,
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \ConfigurationException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $propositionGroupDataObject = new PropositionGroupDataObject();
        $propositionGroupDataObject->markAsModifiedObject();

        $propositionGroupForm = $this->createForm(PropositionGroupType::class, $propositionGroupDataObject);
        $propositionGroupForm->handleRequest($request);

        $regions = $propositionGroupDataObject->regions;
        $modalities = $propositionGroupDataObject->modalities;

        if ($propositionGroupForm->isSubmitted() && $propositionGroupForm->isValid()) {
            $propositionGroup = new PropositionGroup();
            $propositionGroupDataObject->toPropositionGroup($propositionGroup);
            $this->propositionManager->addPropositionGroup($propositionGroup);
            $this->addFlash('success', 'administration.proposition-group.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositiongroup_index'
            );
        }

        return [
            'propositionGroupForm' => $propositionGroupForm->createView(),
            'allRegions' => $this->regionManager->getRegions(),
            'selectedRegions' => $regions,
            'allModalities' => $this->modalityManager->getAllModalities(),
            'selectedModalities' => $modalities,
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder()->getQb()->andWhere('propositiongroup.hidden = 0');

        return $responseService->getResponse();
    }


    /**
     * @return PropositionGroupDatatable
     * @throws \Exception
     */
    private function getDataTable(): PropositionGroupDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_propositiongroup_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}
