<?php

namespace App\Controller\Administration;

use App\Datatables\PricePeriodDatatable;
use App\Form\DataObject\PricePeriodDataObject;
use App\Form\PricePeriodType;
use App\Manager\PricePeriodManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\PricePeriod;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PricePeriodController
 * @package App\Controller\Administration
 * @Route("/price-period", options={"expose" = true})
 * @Resource("abt-administration:price-period")
 */
Class PricePeriodController extends Controller
{
    /** @var PricePeriodDatatable */
    private $datatable;

    /** @var PricePeriodManager */
    private $pricePeriodManager;

    public function __construct(
        PricePeriodDatatable $datatable,
        PricePeriodManager $pricePeriodManager
    ) {
        $this->datatable = $datatable;
        $this->pricePeriodManager = $pricePeriodManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{pricePeriodId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|PricePeriod $pricePeriod
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(PricePeriod $pricePeriod = null): RedirectResponse
    {
        if ($pricePeriod) {
            try {
                $this->pricePeriodManager->removePricePeriod($pricePeriod);
                $this->addFlash('success', 'administration.priceperiod.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.priceperiod.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.priceperiod.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_priceperiod_index');
    }

    /**
     * @Route("/edit/{pricePeriodId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|PricePeriod $pricePeriod
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, PricePeriod $pricePeriod = null)
    {
        if (!$pricePeriod) {
            $this->addFlash('warning', 'administration.priceperiod.search-results.nodata');

            return $this->redirectToRoute('app_administration_priceperiod_index');
        }

        $pricePeriodDataObject = PricePeriodDataObject::fromPricePeriod($pricePeriod);
        $pricePeriodDataObject->markAsModifiedObject();

        $pricePeriodForm = $this->createForm(PricePeriodType::class, $pricePeriodDataObject);
        $pricePeriodForm->handleRequest($request);
        if ($pricePeriodForm->isSubmitted() && $pricePeriodForm->isValid()) {
            $pricePeriodDataObject->toPricePeriod($pricePeriod);
            $this->pricePeriodManager->addPricePeriod($pricePeriod);
            $this->addFlash('success', 'administration.priceperiod.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_priceperiod_index'
            );
        }

        return [
            'pricePeriodForm' => $pricePeriodForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $pricePeriodDataObject = new PricePeriodDataObject();
        $pricePeriodDataObject->markAsModifiedObject();

        $pricePeriodForm = $this->createForm(PricePeriodType::class, $pricePeriodDataObject);
        $pricePeriodForm->handleRequest($request);
        if ($pricePeriodForm->isSubmitted() && $pricePeriodForm->isValid()) {
            $pricePeriod = new PricePeriod();
            $pricePeriodDataObject->toPricePeriod($pricePeriod);
            $this->pricePeriodManager->addPricePeriod($pricePeriod);
            $this->addFlash('success', 'administration.priceperiod.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_priceperiod_index'
            );
        }

        return [
            'pricePeriodForm' => $pricePeriodForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return PricePeriodDatatable
     * @throws \Exception
     */
    private function getDataTable(): PricePeriodDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_priceperiod_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}