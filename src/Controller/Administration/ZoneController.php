<?php

namespace App\Controller\Administration;

use App\Datatables\ZoneDatatable;
use App\Form\DataObject\FileDataObject;
use App\Form\DataObject\ZoneDataObject;
use App\Form\FileType;
use App\Form\ZoneType;
use App\Manager\RegionManager;
use App\Manager\ZoneManager;
use App\Processor\ZoneProcessor;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Zone;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ZoneController
 * @package App\Controller\Administration
 * @Route("/zone", options={"expose" = true})
 * @Resource("abt-administration:zone")
 */
Class ZoneController extends Controller
{
    /** @var ZoneDatatable */
    private $datatable;

    /** @var ZoneManager */
    private $zoneManager;

    /** @var RegionManager */
    private $regionManager;

    /** @var ZoneProcessor */
    private $zoneProcessor;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        ZoneDatatable $datatable,
        ZoneManager $zoneManager,
        RegionManager $regionManager,
        LoggerInterface $logger,
        ZoneProcessor $zoneProcessor
    ) {
        $this->datatable = $datatable;
        $this->zoneManager = $zoneManager;
        $this->regionManager = $regionManager;
        $this->logger = $logger;
        $this->zoneProcessor = $zoneProcessor;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/example", methods={"GET"})
     * @Privilege("read")
     *
     * @return Response
     */
    public function exampleAction(): Response
    {

        return $this->zoneProcessor->exampleCsv();
    }

    /**
     * @Route("/delete/{zoneId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|Zone $zone
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(Zone $zone = null): RedirectResponse
    {
        if ($zone) {
            try {
                $this->zoneManager->removeZone($zone);
                $this->addFlash('success', 'administration.zone.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.zone.delete.failed');
                $this->logger->error($exception);
            }
        } else {
            $this->addFlash('warning', 'administration.zone.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_zone_index');
    }

    /**
     * @Route("/edit/{zoneId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|Zone $zone
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Zone $zone = null)
    {
        if (!$zone) {
            $this->addFlash('warning', 'administration.zone.search-results.nodata');

            return $this->redirectToRoute('app_administration_zone_index');
        }

        $zoneDataObject = ZoneDataObject::fromZone($zone);
        $zoneDataObject->markAsModifiedObject();


        $zoneForm = $this->createForm(ZoneType::class, $zoneDataObject);
        $zoneForm->handleRequest($request);

        $regions = $zoneDataObject->regions;

        if ($zoneForm->isSubmitted() && $zoneForm->isValid()) {
            $zoneDataObject->toZone($zone);
            $this->zoneManager->addZone($zone);
            $this->addFlash('success', 'administration.zone.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_zone_index'
            );
        }

        return [
            'allRegions' => $this->regionManager->getRegions(),
            'selectedRegions' => $regions,
            'zoneForm' => $zoneForm->createView(),
        ];
    }

    /**
     * @Route("/import", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function importAction(Request $request)
    {
        $importDataObject = new FileDataObject();
        $importDataObject->markAsModifiedObject();

        $importForm = $this->createForm(FileType::class, $importDataObject);
        $importForm->handleRequest($request);

        $errors = [];
        if ($importForm->isSubmitted() && $importForm->isValid()) {

            $zoneDataObject = new ZoneDataObject();
            $zoneDataObject->markAsModifiedObject();
            $fileName = $importForm->get('file')->getData();
            $importResult = $this->zoneProcessor->importCsv($fileName);
            if (!$importResult->hasMessages()) {
                $this->addFlash('success', 'administration.zone.add.succeeded');

                return $this->redirectToRoute(
                    'app_administration_zone_index'
                );
            } else {
                $this->addFlash('error', 'administration.zone.import.failed');
                $errors = $importResult->getMessages();
            }
        }

        return [
            'importForm' => $importForm->createView(),
            'errors' => $errors
        ];
    }


    /**
     * @Route("/export/{codes}", methods={"GET","POST"})
     * @Privilege("read")
     *
     * @param string $codes
     * @throws \LogicException
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     */
    public function exportAction(string $codes): Response
    {
        return $this->zoneProcessor->exportCsv($codes);
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @param $csvFile
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return ZoneDatatable
     * @throws \Exception
     */
    private function getDataTable(): ZoneDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_zone_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }


}