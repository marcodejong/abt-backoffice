<?php

namespace App\Controller\Administration;

use App\Datatables\PropositionDatatable;
use App\Form\DataObject\PropositionDataObject;
use App\Form\PropositionType;
use App\Manager\PropositionElementManager;
use App\Manager\PropositionManager;
use App\Manager\CustomerGroupManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Utility\Iterator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropositionController
 * @package App\Controller\Administration
 * @Route("/proposition", options={"expose" = true})
 * @Resource("abt-administration:proposition")
 */
Class PropositionController extends Controller
{
    /** @var PropositionDatatable */
    private $datatable;

    /** @var PropositionManager */
    private $propositionManager;

    /** @var CustomerGroupManager */
    private $customerGroupManager;

    /** @var PropositionElementManager */
    private $propositionElementManager;

    /** @var Proposition */
    private $baseProposition;

    public function __construct(
        PropositionDatatable $datatable,
        PropositionManager $propositionManager,
        CustomerGroupManager $customerGroupManager,
        PropositionElementManager $propositionElementManager,
        Proposition $baseProposition
    ) {
        $this->datatable = $datatable;
        $this->propositionManager = $propositionManager;
        $this->customerGroupManager = $customerGroupManager;
        $this->propositionElementManager = $propositionElementManager;
        $this->baseProposition = $baseProposition;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{$propositionId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|Proposition $proposition
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(Proposition $proposition = null): RedirectResponse
    {
        if ($proposition) {
            try {
                $this->propositionManager->removeProposition($proposition);
                $this->addFlash('success', 'administration.proposition.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.proposition.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.proposition.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_proposition_index');
    }

    /**
     * @Route("/edit/{propositionId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|Proposition $proposition
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Proposition $proposition = null)
    {
        if (!$proposition) {
            $this->addFlash('warning', 'administration.proposition.edit.cannot_find');

            return $this->redirectToRoute('app_administration_proposition_index');
        }

        $propositionDataObject = PropositionDataObject::fromProposition($proposition);
        $propositionDataObject->markAsModifiedObject();

        $propositionForm = $this->createForm(PropositionType::class, $propositionDataObject);
        $propositionForm->handleRequest($request);

        $customerGroups = $propositionDataObject->customerGroups;
        $propositionElements = $propositionDataObject->propositionElements;

        if ($propositionForm->isSubmitted() && $propositionForm->isValid()) {
            $propositionDataObject->toProposition($proposition);
            $this->propositionManager->addProposition($proposition);
            $this->addFlash('success', 'administration.proposition.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_proposition_index'
            );
        }


        return [
            'propositionForm' => $propositionForm->createView(),
            'allCustomerGroups' => $this->customerGroupManager->getAllCustomerGroups(),
            'selectedCustomerGroups' => $customerGroups,
            'allPropositionElements' => $this->getAllAvailablePropositionElements(),
            'selectedPropositionElements' => $propositionElements
            ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $propositionDataObject = new PropositionDataObject();
        $propositionDataObject->markAsModifiedObject();

        $propositionForm = $this->createForm(PropositionType::class, $propositionDataObject);
        $propositionForm->handleRequest($request);

        $customerGroups = $propositionDataObject->customerGroups;
        $propositionElements = $propositionDataObject->propositionElements;

        if ($propositionForm->isSubmitted() && $propositionForm->isValid()) {
            $proposition = $propositionDataObject->toNewProposition();
            $this->propositionManager->addProposition($proposition);
            $this->addFlash('success', 'administration.proposition.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_proposition_index'
            );
        }

        return [
            'propositionForm' => $propositionForm->createView(),
            'allCustomerGroups' => $this->customerGroupManager->getAllCustomerGroups(),
            'selectedCustomerGroups' => $customerGroups,
            'allPropositionElements' => $this->getAllAvailablePropositionElements(),
            'selectedPropositionElements' => $propositionElements

        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder()->getQb()->andWhere('propositionGroup.hidden = 0');

        return $responseService->getResponse();
    }


    /**
     * @return PropositionDatatable
     * @throws \Exception
     */
    private function getDataTable(): PropositionDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_proposition_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }

    private function getAllAvailablePropositionElements(): array
    {
        $baseCompanionPropositionElements = $this->baseProposition->getPropositionElements();
        return Iterator::create($this->propositionElementManager->getAllPropositionElements())
            ->filter(function (PropositionElement $element) use ($baseCompanionPropositionElements) {
                return !$baseCompanionPropositionElements->contains($element);
            })->getArray(false);
    }
}
