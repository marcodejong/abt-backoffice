<?php

namespace App\Controller\Administration;

use App\Datatables\ModalityDatatable;
use App\Form\DataObject\ModalityDataObject;
use App\Form\ModalityType;
use App\Manager\ModalityManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\Modality;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ModalityController
 * @package App\Controller\Administration
 * @Route("/modality", options={"expose" = true})
 * @Resource("abt-administration:modality")
 */
Class ModalityController extends Controller
{
    /** @var ModalityDatatable */
    private $datatable;

    /** @var ModalityManager */
    private $modalityManager;

    public function __construct(
        ModalityDatatable $datatable,
        ModalityManager $modalityManager
    ) {
        $this->datatable = $datatable;
        $this->modalityManager = $modalityManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{modalityId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|Modality $modality
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(Modality $modality = null): RedirectResponse
    {
        if ($modality) {
            try {
                $this->modalityManager->removeModality($modality);
                $this->addFlash('success', 'administration.modality.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.modality.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.modality.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_modality_index');
    }

    /**
     * @Route("/edit/{modalityId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|Modality $modality
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Modality $modality = null)
    {
        if (!$modality) {
            $this->addFlash('warning', 'administration.modality.search-results.nodata');

            return $this->redirectToRoute('app_administration_modality_index');
        }

        $modalityDataObject = ModalityDataObject::fromModality($modality);
        $modalityDataObject->markAsModifiedObject();

        $modalityForm = $this->createForm(ModalityType::class, $modalityDataObject);
        $modalityForm->handleRequest($request);
        if ($modalityForm->isSubmitted() && $modalityForm->isValid()) {
            $modalityDataObject->toModality($modality);
            $this->modalityManager->addModality($modality);
            $this->addFlash('success', 'administration.modality.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_modality_index'
            );
        }

        return [
            'modalityForm' => $modalityForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $modalityDataObject = new ModalityDataObject();
        $modalityDataObject->markAsModifiedObject();

        $modalityForm = $this->createForm(ModalityType::class, $modalityDataObject);
        $modalityForm->handleRequest($request);
        if ($modalityForm->isSubmitted() && $modalityForm->isValid()) {
            $modality = new Modality();
            $modalityDataObject->toModality($modality);
            $this->modalityManager->addModality($modality);
            $this->addFlash('success', 'administration.modality.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_modality_index'
            );
        }

        return [
            'modalityForm' => $modalityForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return ModalityDatatable
     * @throws \Exception
     */
    private function getDataTable(): ModalityDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_modality_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}