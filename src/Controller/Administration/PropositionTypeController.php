<?php

namespace App\Controller\Administration;

use App\Datatables\PropositionTypeDatatable;
use App\Form\DataObject\PropositionTypeDataObject;
use App\Form\PropositionTypeType;
use App\Manager\PropositionTypeManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\PropositionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropositionTypeController
 * @package App\Controller\Administration
 * @Route("/proposition-type", options={"expose" = true})
 * @Resource("abt-administration:proposition-type")
 */
Class PropositionTypeController extends Controller
{
    /** @var PropositionTypeDatatable */
    private $datatable;

    /** @var PropositionTypeManager */
    private $propositionTypeManager;

    public function __construct(
        PropositionTypeDatatable $datatable,
        PropositionTypeManager $propositionTypeManager
    ) {
        $this->datatable = $datatable;
        $this->propositionTypeManager = $propositionTypeManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{propositionTypeId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|PropositionType $propositionType
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(PropositionType $propositionType = null): RedirectResponse
    {
        if ($propositionType) {
            try {
                $this->propositionTypeManager->removePropositionType($propositionType);
                $this->addFlash('success', 'administration.proposition-type.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.proposition-type.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.proposition-type.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_propositiontype_index');
    }

    /**
     * @Route("/edit/{propositionTypeId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|PropositionType $propositionType
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, PropositionType $propositionType = null)
    {
        if (!$propositionType) {
            $this->addFlash('warning', 'administration.proposition-type.search-results.nodata');

            return $this->redirectToRoute('app_administration_propositiontype_index');
        }

        $propositionTypeDataObject = PropositionTypeDataObject::fromPropositionType($propositionType);
        $propositionTypeDataObject->markAsModifiedObject();

        $propositionTypeForm = $this->createForm(PropositionTypeType::class, $propositionTypeDataObject);
        $propositionTypeForm->handleRequest($request);
        if ($propositionTypeForm->isSubmitted() && $propositionTypeForm->isValid()) {
            $propositionTypeDataObject->toPropositionType($propositionType);
            $this->propositionTypeManager->addPropositionType($propositionType);
            $this->addFlash('success', 'administration.proposition-type.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositiontype_index'
            );
        }

        return [
            'propositionTypeForm' => $propositionTypeForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $propositionTypeDataObject = new PropositionTypeDataObject();
        $propositionTypeDataObject->markAsModifiedObject();

        $propositionTypeForm = $this->createForm(PropositionTypeType::class, $propositionTypeDataObject);
        $propositionTypeForm->handleRequest($request);
        if ($propositionTypeForm->isSubmitted() && $propositionTypeForm->isValid()) {
            $propositionType = new PropositionType();
            $propositionTypeDataObject->toPropositionType($propositionType);
            $this->propositionTypeManager->addPropositionType($propositionType);
            $this->addFlash('success', 'administration.proposition-type.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_propositiontype_index'
            );
        }

        return [
            'propositionTypeForm' => $propositionTypeForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return PropositionTypeDatatable
     * @throws \Exception
     */
    private function getDataTable(): PropositionTypeDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_propositiontype_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}