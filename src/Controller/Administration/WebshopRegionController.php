<?php

namespace App\Controller\Administration;

use App\Datatables\WebshopRegionDatatable;
use App\Form\DataObject\WebshopRegionDataObject;
use App\Form\WebshopRegionType;
use App\Manager\WebshopRegionManager;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Arriva\Abt\Entity\WebshopRegion;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WebshopRegionController
 * @package App\Controller\Administration
 * @Route("/webshop-region", options={"expose" = true})
 * @Resource("abt-administration:webshop-region")
 */
Class WebshopRegionController extends Controller
{
    /** @var WebshopRegionDatatable */
    private $datatable;

    /** @var WebshopRegionManager */
    private $webshopRegionManager;

    public function __construct(
        WebshopRegionDatatable $datatable,
        WebshopRegionManager $webshopRegionManager
    ) {
        $this->datatable = $datatable;
        $this->webshopRegionManager = $webshopRegionManager;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Privilege("read")
     * @Template
     * @throws \Exception
     * @return array
     */
    public function indexAction(): array
    {
        return ["datatable" => $this->getDataTable()];
    }

    /**
     * @Route("/delete/{webshopRegionId}", methods={"GET"})
     * @Privilege("delete")
     *
     * @param null|WebshopRegion $webshopRegion
     * @throws \LogicException
     * @return RedirectResponse
     */
    public function deleteAction(WebshopRegion $webshopRegion = null): RedirectResponse
    {
        if ($webshopRegion) {
            try {
                $this->webshopRegionManager->removeWebshopRegion($webshopRegion);
                $this->addFlash('success', 'administration.webshop-region.delete.succeeded');
            } catch (\Exception $exception) {
                $this->addFlash('error', 'administration.webshop-region.delete.failed');
            }
        } else {
            $this->addFlash('warning', 'administration.webshop-region.delete.cannot_find');
        }

        return $this->redirectToRoute('app_administration_webshopregion_index');
    }

    /**
     * @Route("/edit/{webshopRegionId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param Request $request
     * @param null|WebshopRegion $webshopRegion
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, WebshopRegion $webshopRegion = null)
    {
        if (!$webshopRegion) {
            $this->addFlash('warning', 'administration.webshop-region.search-results.nodata');

            return $this->redirectToRoute('app_administration_webshopregion_index');
        }

        $webshopRegionDataObject = WebshopRegionDataObject::fromWebshopRegion($webshopRegion);
        $webshopRegionDataObject->markAsModifiedObject();

        $webshopRegionForm = $this->createForm(WebshopRegionType::class, $webshopRegionDataObject);
        $webshopRegionForm->handleRequest($request);
        if ($webshopRegionForm->isSubmitted() && $webshopRegionForm->isValid()) {
            $webshopRegionDataObject->toWebshopRegion($webshopRegion);
            $this->webshopRegionManager->addWebshopRegion($webshopRegion);
            $this->addFlash('success', 'administration.webshop-region.edit.succeeded');

            return $this->redirectToRoute(
                'app_administration_webshopregion_index'
            );
        }

        return [
            'webshopRegionForm' => $webshopRegionForm->createView(),
        ];
    }

    /**
     * @Route("/add", methods={"GET","POST"})
     * @Template
     * @Privilege("create")
     *
     * @param Request $request
     * @throws \LogicException
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $webshopRegionDataObject = new WebshopRegionDataObject();
        $webshopRegionDataObject->markAsModifiedObject();

        $webshopRegionForm = $this->createForm(WebshopRegionType::class, $webshopRegionDataObject);
        $webshopRegionForm->handleRequest($request);
        if ($webshopRegionForm->isSubmitted() && $webshopRegionForm->isValid()) {
            $webshopRegion = new WebshopRegion();
            $webshopRegionDataObject->toWebshopRegion($webshopRegion);
            $this->webshopRegionManager->addWebshopRegion($webshopRegion);
            $this->addFlash('success', 'administration.webshop-region.add.succeeded');

            return $this->redirectToRoute(
                'app_administration_webshopregion_index'
            );
        }

        return [
            'webshopRegionForm' => $webshopRegionForm->createView(),
        ];
    }

    /**
     * @Route("/datatable", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function datatableAction(): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getDataTable());
        $responseService->getDatatableQueryBuilder();

        return $responseService->getResponse();
    }


    /**
     * @return WebshopRegionDatatable
     * @throws \Exception
     */
    private function getDataTable(): WebshopRegionDatatable
    {
        $this->datatable->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_administration_webshopregion_datatable'
                    ),
                ],
            ]
        );

        return $this->datatable;
    }
}