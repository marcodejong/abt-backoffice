<?php

namespace App\Controller\CustomerService;

use App\Datatables\NalTlsOrderDatatable;
use App\Datatables\PropositionElementTlsOrderDatatable;
use App\Form\ResendConfirmationEmailType;
use App\Repository\PropositionElementInstanceRepositoryInterface;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use App\Service\AbtApi\ClientInterface;
use App\Service\AbtApi\Entity\Request\CreateNalCardProductInstancesRequest;
use App\Service\AbtApi\Entity\Request\CreatePropositionElementInstancesRequest;
use App\Service\AbtApi\Entity\Request\DeliverOrderRequest;
use App\Service\AbtApi\Entity\Request\NalCardProductInstance;
use App\Service\AbtApi\Entity\Request\Order as RequestOrder;
use App\Service\AbtApi\Entity\Request\PropositionElementInstance;
use App\Service\AbtApi\Entity\Request\SendOrderConfirmationEmailRequest;
use App\Service\AbtApi\Entity\Response\CreateNalCardProductInstancesResponse;
use App\Service\AbtApi\Entity\Response\CreatePropositionElementInstancesResponse;
use App\Service\AbtApi\Entity\Response\NalCardProductInstance as NalCardProductInstanceResponse;
use App\Service\AbtApi\Entity\Response\PropositionElementInstance as PropositionElementInstanceResponse;
use Arriva\Abt\Entity\Order;
use Arriva\Abt\Value\OrderStatus;
use Arriva\Abt\Value\PropositionElementInstanceStatus;
use Arriva\Idm\Bundle\RestClient;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class OrderController
 * @package App\Controller\CustomerService
 * @Route("/order", options={"expose" = true})
 * @Resource("abt-customer-service:order")
 */
Class OrderController extends Controller
{
    /** @var PropositionElementTlsOrderDatatable */
    private $propositionElementTlsOrderDatatable;

    /** @var NalTlsOrderDatatable */
    private $nalTlsOrderDatatable;

    /** @var ClientInterface */
    private $abtApiClient;

    /** @var PropositionElementInstanceRepositoryInterface */
    private $elementInstanceRepository;

    /** @var TranslatorInterface */
    private $translator;

    public function __construct(
        PropositionElementTlsOrderDatatable $propositionElementTlsOrderDatatable,
        PropositionElementInstanceRepositoryInterface $elementInstanceRepository,
        NalTlsOrderDatatable $nalTlsOrderDatatable,
        ClientInterface $abtApiClient,
        TranslatorInterface $translator
    ) {
        $this->propositionElementTlsOrderDatatable = $propositionElementTlsOrderDatatable;
        $this->nalTlsOrderDatatable = $nalTlsOrderDatatable;
        $this->abtApiClient = $abtApiClient;
        $this->elementInstanceRepository = $elementInstanceRepository;
        $this->translator = $translator;
    }

    /**
     * @Route("/", methods={"GET"}, name="app_administration_order_index")
     * @Privilege("update")
     *
     * @return array|RedirectResponse
     */
    public function indexAction(): RedirectResponse
    {
        return $this->redirect(sprintf('%s/workflow/orders', $this->getParameter('app.assist_base_url')));
    }

    /**
     * @Route("/{orderId}", methods={"GET","POST"})
     * @Template
     * @Privilege("update")
     *
     * @param null|Order $order
     * @param RestClient $idmClient
     * @param Request $request
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function showAction(RestClient $idmClient, Request $request, Order $order = null)
    {
        if (!$order) {
            $this->addFlash('warning', 'administration.order.edit.cannot_find_order');

            return $this->indexAction();
        }

        $form = $this->createForm(ResendConfirmationEmailType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $requestOrder = RequestOrder::createAcceptedOrder($order->getPublicId());
                $this->abtApiClient->sendOrderConfirmationEmail(
                    new SendOrderConfirmationEmailRequest($requestOrder, $form->getData()->email)
                );
                $this->addFlash('success', 'administration.order.resend-confirmation-email.success');
            } catch (\Exception $exception) {
                $this->addFlash('notice', 'administration.order.resend-confirmation-email.failed');
            }
        }

        return [
            'order' => $order,
            'payment' => $order->getLastPayment(),
            'customer' => $idmClient->getIdentityById($order->getCustomer()->getIdmId()),
            'propositionElementTlsOrderDatatable' => $this->getPropositionElementDataTable($order),
            'resendConfirmationForm' => $form->createView(),
            'nalTlsOrderDatatable' => $this->getNalTlsOrderDatatable($order),
        ];
    }


    /**
     * @Route("/change-status/{orderId}", methods={"GET"}, name="app_customerservice_order_change_status")
     * @Template
     * @Privilege("update")
     *
     * @param null|Order $order
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function changeStatusAction(Order $order = null)
    {
        if (!$order) {
            $this->addFlash('warning', 'administration.order.edit.cannot_find_order');

            return $this->redirectToRoute('app_administration_order_index');
        }

        try {
            $order->setStatus(OrderStatus::IBAN_VERIFIED());
            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush();

            $this->abtApiClient->deliverOrder(new DeliverOrderRequest($order->getPublicId()));

            $this->addFlash('success', 'administration.order.status.changed');
        } catch (\Exception $exception) {
            $this->addFlash('warning', 'administration.order.status.failed');
        }

        return new RedirectResponse($this->generateUrl('app_customerservice_order_show', ['orderId' => $order->getId()]));
    }

    /**
     * @Route("/proposition-element-tls-order/datatable/{orderId}", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @param Order|null $order
     * @return JsonResponse
     * @throws \Exception
     */
    public function PropositionElementTlsOrderDatatableAction(Order $order): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getPropositionElementDataTable($order));
        /** @var QueryBuilder $qb */
        $qb = $responseService->getDatatableQueryBuilder()->getQb();
        $qb->leftJoin('propositionelementinstance.propositionInstance', 'pi');
        $qb->leftJoin('pi.orderItem', 'orderItem');
        $qb->leftJoin('pi.proposition', 'proposition');
        $qb->andWhere('orderItem.order = :order')
            ->orderBy('propositionelementinstance.peDescription', 'ASC')
            ->orderBy('propositionelementinstance.effectiveDate', 'ASC')
            ->orderBy('propositionelementinstance.engravedId', 'ASC')
            ->setParameter('order', $order)
        ;

        return $responseService->getResponse();
    }


    private function getPropositionElementDataTable(Order $order): PropositionElementTlsOrderDatatable
    {
        $this->propositionElementTlsOrderDatatable->setOrder($order)->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_customerservice_order_propositionelementtlsorderdatatable',
                        ['orderId' => $order->getId()]
                    ),
                ],
            ]
        );

        return $this->propositionElementTlsOrderDatatable;
    }

    /**
     * @Route("/nal-tls-order/datatable/{orderId}", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @param Order|null $order
     * @return JsonResponse
     * @throws \Exception
     */
    public function nalTlsOrderDatatableAction(Order $order): JsonResponse
    {
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($this->getNalTlsOrderDatatable($order));
        /** @var QueryBuilder $qb */
        $qb = $responseService->getDatatableQueryBuilder()->getQb();
        $qb->leftJoin('nalcardproductinstance.propositionInstance', 'propositionInstance');
        $qb->leftJoin('propositionInstance.orderItem', 'orderItem');
        $qb->andWhere('orderItem.order = :order')
            ->orderBy('nalcardproductinstance.engravedId', 'ASC')
            ->orderBy('nalcardproductinstance.contractTariffId', 'ASC')
            ->setParameter('order', $order);

        return $responseService->getResponse();
    }


    private function getNalTlsOrderDatatable(Order $order): NalTlsOrderDatatable
    {
        $this->nalTlsOrderDatatable->setOrder($order)->buildDatatable(
            [
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_customerservice_order_naltlsorderdatatable',
                        ['orderId' => $order->getId()]
                    ),
                ],
            ]
        );

        return $this->nalTlsOrderDatatable;
    }

    /**
     * @Route("/confirm/proposition-element/{orderId}", methods={"POST"},
     *     name="app_workflow_order_confirmpropositionelementstls"
     * )
     * @Template
     * @Privilege("read")
     * @param Order|null $order
     * @param Request $request
     * @param LoggerInterface $logger
     * @return RedirectResponse
     */
    public function confirmPropositionElementsTls(
        Order $order = null, Request $request, LoggerInterface $logger
    ): RedirectResponse {

        $elementInstances = array_map(function($instance) {
            return new PropositionElementInstance($instance);
        }, $request->get('data'));

        $response = $this->abtApiClient->createPropositionElementInstances(
            new CreatePropositionElementInstancesRequest($order->getPublicId(), $elementInstances)
        );

        if ($response->isValid()) {
            try {
                $this->processPropositionElementsResponse($response);
            } catch (\Exception $exception) {
                $this->addFlash('error', $this->translator->trans('customer-service.order.instance-confirmation.failed',
                    ['message' => $exception->getMessage()])
                );
            }
        } else {
            $this->addFlash('notice', 'customer-service.order.instance-confirmation.invalid-response');
            $logger->notice(
                sprintf('Could not create proposition element instances: %s', print_r($response->getErrors(), true)));
        }

        return $this->redirectToRoute('app_customerservice_order_show', ['orderId' => $order->getId()]);
    }

    private function processPropositionElementsResponse(
        CreatePropositionElementInstancesResponse $response
    ): void {
        $elementInstances = $this->elementInstanceRepository->findPropositionElementInstances(
            array_map(function(PropositionElementInstanceResponse $instance) {
                return $instance->getId();
            }, $response->getSucceededPropositionElementInstances())
        );

        foreach ($elementInstances as $elementInstance) {
            $elementInstance->setStatus(PropositionElementInstanceStatus::CREATED());
            $this->elementInstanceRepository->save($elementInstance);
        }

        $this->getDoctrine()->getManager()->flush();

        if ($response->propositionElementInstancesPartiallySucceeded()) {
            $this->addFlash('notice', $this->translator->trans('customer-service.order.instance-creation.partially-failed', [
                '%failedInstances%' => $this->formatPropositionElementFeedbackMessage(
                    $response->getFailedPropositionElementInstances()
                )]));
        } elseif (!empty($response->getFailedPropositionElementInstances())) {
            $this->addFlash('notice', $this->translator->trans('customer-service.order.instance-creation.failed', [
                '%failedInstances%' => $this->formatPropositionElementFeedbackMessage(
                    $response->getFailedPropositionElementInstances()
                )]));
        } else {
            $this->addFlash('success', 'customer-service.order.instance-creation.succeeded');
        }
    }

    /**
     * @param PropositionElementInstanceResponse[] $propositionElementInstances
     * @return string
     */
    private function formatPropositionElementFeedbackMessage(array $propositionElementInstances): string
    {
        return implode("\n", array_map(function(PropositionElementInstanceResponse $propositionElementInstance) {
            return sprintf('PeCode: %s, EngravedId: %s',
                $propositionElementInstance->getPeCode(),
                $propositionElementInstance->getEngravedId()
            );
        }, $propositionElementInstances));
    }

    /**
     * @Route("/confirm/nal-tls/{orderId}", methods={"POST"},
     *  name="app_workflow_order_confirm_nal_tls"
     * )
     * @Template
     * @Privilege("read")
     * @param Order|null $order
     * @param Request $request
     * @param LoggerInterface $logger
     * @return RedirectResponse
     */
    public function confirmNalTls(Order $order = null, Request $request, LoggerInterface $logger): RedirectResponse
    {
        $elementInstances = array_map(function($instance) {
            return new NalCardProductInstance($instance);
        }, $request->get('data'));

        $response = $this->abtApiClient->createNalCardProductInstances(
            new CreateNalCardProductInstancesRequest($order->getPublicId(), $elementInstances)
        );

        if ($response->isValid()) {
            try {
                $this->processNalCardProductInstancesResponse($response);
            } catch (\Exception $exception) {
                $this->addFlash('error', $this->translator->trans('customer-service.order.instance-confirmation.failed',
                    ['message' => $exception->getMessage()])
                );
            }
        } else {
            $this->addFlash('notice', 'customer-service.order.nal-instance-confirmation.invalid-response');
            $logger->notice(
                sprintf('Could not create nal card product instances: %s', print_r($response->getErrors(), true)));
        }

        return $this->redirectToRoute('app_customerservice_order_show', ['orderId' => $order->getId()]);
    }


    private function processNalCardProductInstancesResponse(
        CreateNalCardProductInstancesResponse $response
    ): void {
        if ($response->nalCardProductInstancesPartiallySucceeded()) {
            $this->addFlash('notice', $this->translator->trans('customer-service.order.nal-instance-creation.partially-failed', [
                '%failedInstances%' => $this->formatNalCardProductFeedbackMessage(
                    $response->getFailedNalCardProductInstances()
                )]));
        } elseif (!empty($response->getFailedNalCardProductInstances())) {
            $this->addFlash('notice', $this->translator->trans('customer-service.order.nal-instance-creation.failed', [
                '%failedInstances%' => $this->formatNalCardProductFeedbackMessage(
                    $response->getFailedNalCardProductInstances()
                )]));
        } else {
            $this->addFlash('success', 'customer-service.order.nal-instance-creation.succeeded');
        }
    }

    /**
     * @param NalCardProductInstanceResponse[] $nalCardProductInstances
     * @return string
     */
    private function formatNalCardProductFeedbackMessage(array $nalCardProductInstances): string
    {
        return implode("\n", array_map(function(NalCardProductInstanceResponse $nalCardProductInstance) {
            return sprintf('ContractTariff: %s, EngravedId: %s',
                $nalCardProductInstance->getContractTariff(),
                $nalCardProductInstance->getEngravedId()
            );
        }, $nalCardProductInstances));
    }
}
