<?php

namespace App\Controller\CustomerService;

use App\Datatables\CustomerService\CustomerPropositionDatatable;
use App\Form\DataObject\StopPropositionSubscriptionDataObject;
use App\Form\StopPropositionSubscriptionType;
use App\Service\AbtApi\ClientInterface as AbtApiClientInterface;
use App\Service\AbtApi\Entity\Request\CancelAndCreditSubscriptionRequest;
use Arriva\Abt\Entity\Customer;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\ORM\Repository\CustomerRepositoryInterface;
use Sg\DatatablesBundle\Response\DatatableResponse;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CustomerController
 * @package App\Controller\CustomerService
 * @Route("/customer", options={"expose" = true})
 * @Resource("abt-customer-service:card")
 */
class CustomerController extends Controller
{
    /**
     * @var CustomerPropositionDatatable
     */
    private $customerPropositionDatatable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var AbtApiClientInterface
     */
    private $abtApiClient;

    public function __construct(
        CustomerPropositionDatatable $customerPropositionDatatable,
        CustomerRepositoryInterface $customerRepository,
        AbtApiClientInterface $abtApiClient
    ) {
        $this->customerPropositionDatatable = $customerPropositionDatatable;
        $this->customerRepository = $customerRepository;
        $this->abtApiClient = $abtApiClient;
    }

    /**
     * @Route(
     *     "/{idmId}/propositions",
     *     name="app_customerservice_customer_customerpropositions",
     *     methods={"GET"},
     *     requirements={"idmId": "\d+"}
     * )
     * @Privilege("read")
     * @Template
     * @param int $idmId
     * @return array
     * @throws \Exception
     */
    public function customerPropositionsAction(int $idmId): array
    {
        $customer = $this->customerRepository->findOneByIdmId($idmId);
        return [
            'datatable' => $customer ? $this->getCustomerPropositionsDatatable($customer) : null,
        ];
    }

    /**
     * @Route(
     *     "/{customerId}/propositions/data",
     *     methods={"GET"},
     *     name="app_customerservice_customer_propositionsdatatable",
     *     condition="request.isXmlHttpRequest()"
     * )
     * @Privilege("read")
     *
     * @param Customer $customer
     * @return JsonResponse
     * @throws \Exception
     */
    public function propositionsDatatableAction(Customer $customer): JsonResponse
    {
        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $customerPropositionsDatatable = $this->getCustomerPropositionsDatatable($customer);
        $responseService->setDatatable($customerPropositionsDatatable);
        $queryBuilder = $responseService->getDatatableQueryBuilder()->getQb();
        $customerPropositionsDatatable->buildBaseQuery($queryBuilder);
        $queryBuilder
            ->andWhere('subscription.customer = :customer')
            ->setParameter('customer', $customer);

        return $responseService->getResponse();
    }

    /**
     * @Route(
     *     "/subscription/{subscriptionId}/stop",
     *     name="app_customerservice_customer_stopsubscription",
     *     methods={"GET","POST"},
     *     requirements={"subscriptionId": "\d+"}
     * )
     * @Privilege("read")
     * @Template
     * @param Request $request
     * @param Subscription $subscription
     * @return array|RedirectResponse
     */
    public function stopPropositionSubscriptionAction(
        Request $request,
        Subscription $subscription
    ) {
        $stopPropositionSubscriptionData = new StopPropositionSubscriptionDataObject($subscription);
        $stopPropositionSubscriptionForm = $this->createForm(
            StopPropositionSubscriptionType::class,
            $stopPropositionSubscriptionData
        );
        $stopPropositionSubscriptionForm->handleRequest($request);
        if ($stopPropositionSubscriptionForm->isSubmitted() && $stopPropositionSubscriptionForm->isValid()) {
            $response = $this->abtApiClient->cancelAndCreditSubscription(
                new CancelAndCreditSubscriptionRequest(
                    $subscription->getPublicId(),
                    $stopPropositionSubscriptionData->endDate,
                    $stopPropositionSubscriptionData->creditAmount
                )
            );
            if ($response->success) {
                $this->addFlash('success', 'The proposition has been stopped.');
            } else {
                $this->addFlash('error', $response->message);
            }

            return $this->redirectToRoute(
                'app_customerservice_customer_customerpropositions',
                ['idmId' => $subscription->getCustomer()->getIdmId()]
            );
        }

        return [
            'customer' => $subscription->getCustomer(),
            'subscription' => $subscription,
            'stopPropositionSubscriptionForm' => $stopPropositionSubscriptionForm->createView(),
        ];
    }

    /**
     * @param Customer $customer
     * @return CustomerPropositionDatatable
     * @throws \Exception
     */
    private function getCustomerPropositionsDatatable(Customer $customer): CustomerPropositionDatatable
    {
        $this->customerPropositionDatatable
            ->buildDatatable([
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_customerservice_customer_propositionsdatatable',
                        [
                            'customerId' => $customer->getId(),
                        ]
                    ),
                ],
            ]);

        return $this->customerPropositionDatatable;
    }
}
