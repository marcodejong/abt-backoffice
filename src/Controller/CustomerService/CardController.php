<?php

namespace App\Controller\CustomerService;

use App\Datatables\CustomerService\CardPropositionDatatable;
use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use App\Service\Tls\Client\Proposition\PropositionInformationServiceInterface;
use Arriva\Abt\Entity\Card;
use SDOA\Model\Proposition\Entity\Information\InstanceDataType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CardController
 * @package App\Controller\CustomerService
 * @Route("/card", options={"expose" = true})
 * @Resource("abt-customer-service:card")
 */
Class CardController extends Controller
{
    /**
     * @var CardPropositionDatatable
     */
    private $arrivaCardPropositionDatatable;

    /**
     * @var PropositionInformationServiceInterface
     */
    private $propositionInformationService;

    /**
     * @var int
     */
    private $arrivaOperatorId;


    public function __construct(
        CardPropositionDatatable $arrivaCardPropositionDatatable,
        PropositionInformationServiceInterface $propositionInformationService,
        int $arrivaOperatorId
    ) {
        $this->arrivaCardPropositionDatatable = $arrivaCardPropositionDatatable;
        $this->propositionInformationService = $propositionInformationService;
        $this->arrivaOperatorId = $arrivaOperatorId;
    }

    /**
     * @Route("/{cardId}/propositions", methods={"GET"}, requirements={"cardId": "\d+"})
     * @Privilege("read")
     * @Template
     * @param Card $card
     * @return array
     * @throws \Exception
     */
    public function cardPropositionsAction(Card $card): array
    {
        return [
            'card' => $card,
            'arrivaPropositionDatatable' => $this->getDataTableWithArrivaPropositions($card),
            'otherOperatorPropositions' => $this->getOtherOperatorPropositionsForCard($card),
        ];
    }

    /**
     * @Route("/{cardId}/propositions/arriva", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @Privilege("read")
     *
     * @param Card $card
     * @return JsonResponse
     * @throws \Exception
     */
    public function propositionsDatatableAction(Card $card): JsonResponse
    {
        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $cardPropositionsDatatable = $this->getDataTableWithArrivaPropositions($card);
        $responseService->setDatatable($cardPropositionsDatatable);
        $queryBuilder = $responseService->getDatatableQueryBuilder()->getQb();
        $cardPropositionsDatatable->buildBaseQuery($queryBuilder);
        $queryBuilder
            ->andWhere('propositionInstance.card = :card')
            ->setParameter('card', $card);

        return $responseService->getResponse();
    }

    /**
     * @param Card $card
     * @return CardPropositionDatatable
     * @throws \Exception
     */
    private function getDataTableWithArrivaPropositions(Card $card): CardPropositionDatatable
    {
        $this->arrivaCardPropositionDatatable
            ->setCard($card)
            ->buildDatatable([
                'ajax' => [
                    'url' => $this->generateUrl(
                        'app_customerservice_card_propositionsdatatable',
                        [
                            'cardId' => $card->getId(),
                        ]
                    ),
                ],
            ]);

        return $this->arrivaCardPropositionDatatable;
    }

    /**
     * @param Card $card
     * @return array
     */
    private function getOtherOperatorPropositionsForCard(Card $card): array
    {
        return array_filter(
            $this->propositionInformationService->getInstances($card->getEngravedId()),
            function(InstanceDataType $instance) {
                return $instance->getRetailerID() != $this->arrivaOperatorId;
            }
        );
    }
}