<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Controller;

use App\Entity\User;
use App\Security\Configuration\Privilege;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 */
class HomepageController extends Controller
{
    /**
     * @Route("/", name="homepage", methods={"GET"})
     * @Privilege("read")
     * @param Request $request
     * @return RedirectResponse
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser() instanceof User) {
            return $this->redirectToRoute('app_administration_index', ['_locale' => $request->getLocale()]);
        }

        return $this->redirectToRoute('login', ['_locale' => $request->getLocale()]);
    }
}
