<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Session;

use App\ZendLegacyConstants as ZLC;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

/**
 * This class is a replacement for the default symfony flash bag and is needed to synchronize the flash messages with the
 * legacy assist zend backoffice. Flash messages are only stored the zf 1 way so it is easy to remove this class when it
 * becomes obsolete. In the mean time no changes according reading/writing flash messages is needed in the zend legacy
 * application
 *
 * @package App\Session
 */
class FlashBag implements FlashBagInterface
{
    public function add($type, $message): void
    {
        $this->ensureSessionDataExists();
        $messageObject = $this->createMessageObject($type, $message);
        $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY][] = $messageObject;
    }

    public function set($type, $message): void
    {
        $this->ensureSessionDataExists();
        $numberOfMessages = \count($_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY]);
        for ($index = 0; $index < $numberOfMessages; $index++) {
            $messageObject = $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY][$index];
            if ($type === $messageObject->severity) {
                unset($_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY][$index]);
            }
        }

        $messageObject = $this->createMessageObject($type, $message);
        $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY] = (array)$messageObject;
    }

    public function peek($type, array $default = []): array
    {
        $this->ensureSessionDataExists();
        if (!$this->has($type)) {
            return $default;
        }

        return $this->peekAll()[$type];
    }

    public function peekAll(): array
    {
        $this->ensureSessionDataExists();
        $result = [];
        foreach ($_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY] as $messageObject) {
            if (!\array_key_exists($messageObject->severity, $result)) {
                $result[$messageObject->severity] = [];
            }

            $result[$messageObject->severity][] = $messageObject->message;
        }

        return $result;
    }

    public function get($type, array $default = []): array
    {
        $this->ensureSessionDataExists();
        if (!$this->has($type)) {
            return $default;
        }

        $result = [];
        $numberOfMessages = \count($_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY]);
        for ($index = 0; $index < $numberOfMessages; $index++) {
            $messageObject = $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY][$index];
            if ($type === $messageObject->severity) {
                $result[] = $messageObject->message;
                unset($_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY][$index]);
            }
        }

        return $result;
    }

    public function all(): array
    {
        $this->ensureSessionDataExists();
        $messages = $this->peekAll();
        $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY] = [];

        return $messages;
    }

    public function setAll(array $messages): void
    {
        $this->ensureSessionDataExists();
        $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY] = [];
        foreach ($messages as $type => $messagesForType) {
            $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY][] = \array_map(
                function (array $messageArray): \stdClass {
                    return $this->createMessageObject($messageArray['type'], $messageArray['message']);
                },
                $messagesForType
            );
        }
    }

    public function has($type): bool
    {
        $this->ensureSessionDataExists();

        return \array_key_exists($type, $this->peekAll());
    }

    public function keys(): array
    {
        $this->ensureSessionDataExists();

        return \array_keys($this->peekAll());
    }

    public function getName(): string
    {
        return 'flashes';
    }

    public function initialize(array &$array): void
    {
        $this->ensureSessionDataExists();
        foreach ($array as $type => $messages) {
            $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY][] = \array_map(
                function (array $messageArray): \stdClass {
                    return $this->createMessageObject($messageArray['type'], $messageArray['message']);
                },
                $messages
            );
        }
    }

    public function getStorageKey(): string
    {
        return '_symfony_flashes';
    }

    public function clear(): array
    {
        $this->ensureSessionDataExists();
        $result = $this->peekAll();
        $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY] = [];

        return $result;
    }

    private function ensureSessionDataExists(): void
    {
        if (!\array_key_exists(ZLC::MESSAGES_ROOT_KEY, $_SESSION)) {
            $_SESSION[ZLC::MESSAGES_ROOT_KEY] = [];
        }

        if (!\array_key_exists(ZLC::MESSAGES_CATEGORY_KEY, $_SESSION[ZLC::MESSAGES_ROOT_KEY])) {
            $_SESSION[ZLC::MESSAGES_ROOT_KEY][ZLC::MESSAGES_CATEGORY_KEY] = [];
        }
    }

    private function createMessageObject(string $severity, string $message): \stdClass
    {
        $messageObject = new \stdClass();
        $messageObject->severity = $severity;
        $messageObject->message = $message;

        return $messageObject;
    }
}
