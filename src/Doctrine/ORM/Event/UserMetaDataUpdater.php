<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Doctrine\ORM\Event;

use Arriva\Assist\Entity\User;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

/**
 * @package App\Doctrine\ORM\Event
 */
class UserMetaDataUpdater
{
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $classMetadata = $eventArgs->getClassMetadata();
        if (User::class === $classMetadata->name) {
            $classMetadata->isMappedSuperclass = true;
        }
    }
}
