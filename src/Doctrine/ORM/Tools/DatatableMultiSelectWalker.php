<?php

namespace App\Doctrine\ORM\Tools;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\AST\ConditionalTerm;
use Doctrine\ORM\Query\AST\InExpression;
use Doctrine\ORM\Query\SqlWalker;

class DatatableMultiSelectWalker extends SqlWalker
{
    public const PARAMETER_KEY = 'DatatableMultiSelectWalker';

    public function walkWhereClause($whereClause)
    {
        $result = parent::walkWhereClause($whereClause);

        if (null !== $whereClause &&
            $inExpression = $this->getMultiSelectInExpression($whereClause->conditionalExpression)
        ) {
            $dqlAlias = $inExpression->expression->simpleArithmeticExpression->identificationVariable;
            $field = $inExpression->expression->simpleArithmeticExpression->field;
            $queryComponent = $this->getQueryComponent($dqlAlias);
            /** @var ClassMetadata $metadata */
            $metadata = $queryComponent['metadata'];
            $column = $this->getSQLTableAlias($metadata->getTableName(), $dqlAlias);

            return str_replace(
                "{$column}.{$field} IN (?) AND (",
                "{$column}.{$field} IN (?) OR (",
                $result
            );
        }

        return $result;
    }

    private function getMultiSelectInExpression($conditionalExpression): ?InExpression
    {
        if (!$conditionalExpression instanceof ConditionalTerm) {
            return null;
        }

        foreach ($conditionalExpression->conditionalFactors as $conditionalFactor) {
            if (!$conditionalFactor->simpleConditionalExpression instanceof InExpression) {
                continue;
            }

            $multiSelectExpressions = array_filter(
                $conditionalFactor->simpleConditionalExpression->literals,
                function ($literal) {
                    return $literal->name === self::PARAMETER_KEY;
                }
            );

            if ($multiSelectExpressions) {
                return $conditionalFactor->simpleConditionalExpression;
            }
        }

        return null;
    }
}
