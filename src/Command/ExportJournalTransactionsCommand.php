<?php


namespace App\Command;


use App\Model\Export\Exporter;
use App\Model\Export\Result\FailureExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\Target\CsvFile;
use App\Model\Report\JournalTransactions\JournalTransactionsInput;
use App\Model\Report\JournalTransactions\JournalTransactionsModel;
use App\Util\FlysystemFile;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\DateRange;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportJournalTransactionsCommand extends Command
{
    const NAME = 'arriva:export:journal-abt-transactions';

    /** @var JournalTransactionsModel */
    private $journalTransactionsModel;

    /** @var FilesystemInterface */
    private $filesystem;

    /** @var Exporter */
    private $exporter;

    public function __construct(JournalTransactionsModel $journalTransactionsModel, Exporter $exporter, FilesystemInterface $filesystem) {
        parent::__construct(self::NAME);

        $this->journalTransactionsModel = $journalTransactionsModel;
        $this->exporter = $exporter;
        $this->filesystem = $filesystem;
    }

    protected function configure()
    {
        $this->setDescription('Generate and export a journal abt transactions report');
        $this->addArgument('date', InputArgument::OPTIONAL, 'the date over which to generate the report',
            'yesterday');
        $this->addArgument('filename', InputArgument::OPTIONAL, 'the output filename', null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = Date::of($input->getArgument('date'));

        $reportInput = new JournalTransactionsInput(DateRange::of($date, $date));

        $filename = $input->getArgument('filename') ?: sprintf('%s_journal_abttransactions_1.csv', $date->format('Ymd'));

        $target = new CsvFile(FlysystemFile::of($this->filesystem, $filename));

        $report = $this->journalTransactionsModel->generateJournalTransactionsReport($reportInput);

        if ($this->exporter->supportsExport($report, $target)) {
            $result = $this->exporter->export($report, $target);
            if ($result instanceof SuccessExportResult) {
                $output->writeln(sprintf('Report file <info>%s</info> created with journal transactions report for <info>%s</info>', $filename, $date->format('Y-m-d')));
                return 0;
            }

            if ($result instanceof FailureExportResult) {
                $output->writeln(sprintf('Journal transactions report for <info>%s</info> could not be generated: %s', $date->format('Y-m-d'), $result->getReason()));
                return 1;
            }
        } else {
            $output->writeln(sprintf('<error>Export target %s not supported</error>', $target));
            return 1;
        }
    }
}