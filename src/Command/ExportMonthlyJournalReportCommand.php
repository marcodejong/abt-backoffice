<?php

namespace App\Command;

use App\Model\Export\Exporter;
use App\Model\Export\Result\FailureExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\Target\CsvFile;
use App\Model\Export\Target\FileEmail;
use App\Model\Export\Target\PdfFile;
use App\Model\Report\Journal\JournalReportInput;
use App\Model\Report\Journal\JournalReportModel;
use App\Service\Mailer\Data\Body;
use App\Service\Mailer\Data\Identity;
use App\Service\Mailer\Data\Subject;
use App\Util\FlysystemFile;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\DateRange;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportMonthlyJournalReportCommand extends Command
{
    const NAME = 'arriva:export:monthly-journal-report';

    /** @var JournalReportModel */
    private $journalReportModel;

    /** @var Exporter */
    private $exporter;

    /** @var Identity */
    private $sender;

    /** @var Identity */
    private $defaultReceiver;

    /** @var Subject */
    private $subject;

    /** @var Body */
    private $body;

    /** @var FilesystemInterface */
    private $fileSystem;

    public function __construct(
        JournalReportModel $journalReportModel,
        Exporter $exporter,
        FilesystemInterface $filesystem,
        Identity $sender,
        Identity $defaultReceiver,
        Subject $subject,
        Body $body
    ) {
        $this->journalReportModel = $journalReportModel;
        $this->exporter = $exporter;
        $this->fileSystem = $filesystem;

        $this->sender = $sender;
        $this->defaultReceiver = $defaultReceiver;
        $this->subject = $subject;
        $this->body = $body;

        parent::__construct(self::NAME);
    }

    protected function configure()
    {
        $this->setDescription('Generate and export a journal report');
        $this->addArgument(
            'date',
            InputArgument::OPTIONAL,
            'the date over which to generate the report',
            'yesterday'
        );
        $this->addOption('filename', 'f',InputOption::VALUE_OPTIONAL, 'the output filename', '{{year}}{{month}}_journal_abt');
        $this->addOption('to','t', InputOption::VALUE_OPTIONAL, 'the e-mail address to mail the report to', $this->defaultReceiver->toString());
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $date = Date::of($input->getArgument('date'));

        $reportInput = new JournalReportInput(DateRange::of($date->getStartOfMonth(), $date->getEndOfMonth()));

        $receiver = Identity::ofString($input->getOption('to'));

        $filenameBase = str_replace(
            ['{{year}}', '{{month}}'],
            [$date->format('Y'), $date->format('m')],
            $input->getOption('filename')
        );

        $csvFilename = sprintf('%s.csv', $filenameBase);
        $pdfFilename = sprintf('%s.pdf', $filenameBase);

        $target = new FileEmail(
            $this->sender,
            $receiver,
            $this->subject,
            $this->body,
            [
                new CsvFile(FlysystemFile::of($this->fileSystem, $csvFilename)),
                new PdfFile(FlysystemFile::of($this->fileSystem, $pdfFilename)),
            ]
        );

        $report = $this->journalReportModel->generateJournalReport($reportInput);

        if ($this->exporter->supportsExport($report, $target)) {
            $result = $this->exporter->export($report, $target);
            if ($result instanceof SuccessExportResult) {
                $output->writeln(
                    sprintf(
                        'Monthly journal report files for <info>%s</info> sent by mail to <info>%s</info>',
                        $date->format('Y-m'),
                        $receiver
                    )
                );

                return 0;
            }

            if ($result instanceof FailureExportResult) {
                $output->writeln(
                    sprintf(
                        'Monthly journal report files for <info>%s</info> could not be sent: <info>%s</info>',
                        $date->format('Y-m'),
                        $result->getReason()
                    )
                );

                return 1;
            }
        }

        $output->writeln(sprintf('<error>Export target %s not supported</error>', $target));

        return 1;
    }
}
