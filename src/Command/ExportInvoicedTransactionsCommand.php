<?php


namespace App\Command;


use App\Model\Export\Exporter;
use App\Model\Export\Result\FailureExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\Target\CsvFile;
use App\Model\Report\InvoicedTransactions\InvoicedTransactionsReportInput;
use App\Model\Report\InvoicedTransactions\InvoicedTransactionsReportModel;
use App\Util\FlysystemFile;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\DateRange;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportInvoicedTransactionsCommand extends Command
{
    const NAME = 'arriva:export:invoiced-transactions';

    /** @var InvoicedTransactionsReportModel */
    private $invoicedTransactionsReportModel;

    /** @var FilesystemInterface */
    private $filesystem;

    /** @var Exporter */
    private $exporter;

    public function __construct(
        InvoicedTransactionsReportModel $invoicedTransactionsReportModel,
        Exporter $exporter,
        FilesystemInterface $filesystem
    ) {
        parent::__construct(self::NAME);

        $this->invoicedTransactionsReportModel = $invoicedTransactionsReportModel;
        $this->exporter = $exporter;
        $this->filesystem = $filesystem;
    }

    protected function configure()
    {
        $this->setDescription('export invoiced transactions report');
        $this->addArgument('date', InputArgument::OPTIONAL, 'the date over which to generate the report',
            Date::of('yesterday')->format('Y-m-d'));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = Date::of($input->getArgument('date'));

        $reportInput = new InvoicedTransactionsReportInput(DateRange::of($date, $date));

        $filename = sprintf('%s_gefactureerde_ritten_abt_1.csv', $date->format('Ymd'));

        $target = new CsvFile(FlysystemFile::of($this->filesystem, $filename));

        $report = $this->invoicedTransactionsReportModel->generateInvoicedTransactionsReport($reportInput);

        if ($this->exporter->supportsExport($report, $target)) {
            $result = $this->exporter->export($report, $target);
            if ($result instanceof SuccessExportResult) {
                $output->writeln(
                    sprintf(
                        'Report file <info>%s</info> created with invoiced transactions for <info>%s</info>',
                        $filename,
                        $date->format('Y-m-d')
                    )
                );
                return 0;
            }

            if ($result instanceof FailureExportResult) {
                $output->writeln(
                    sprintf(
                        'Invoiced transactions for <info>%s</info> could not be generated: %s',
                        $date->format('Y-m-d'),
                        $result->getReason()
                    )
                );
                return 1;
            }
        } else {
            $output->writeln(sprintf('<error>Export target %s not supported</error>', $target));
            return 1;
        }
    }

}