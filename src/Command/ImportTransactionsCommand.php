<?php


namespace App\Command;


use App\Import\AbtTransaction\TravelInformationFileImportManager;
use App\Import\AbtTransaction\TravelSettlementFileImportManager;
use App\Import\ImportResult;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportTransactionsCommand extends Command
{
    const NAME = 'arriva:import:transactions';

    /** @var TravelInformationFileImportManager */
    private $travelInformationImportManager;

    /** @var TravelSettlementFileImportManager */
    private $travelSettlementImportManager;

    public function __construct(
        TravelInformationFileImportManager $travelInformationImportManagerManager,
        TravelSettlementFileImportManager $travelSettlementImportManagerManager
    ) {
        $this->travelInformationImportManager = $travelInformationImportManagerManager;
        $this->travelSettlementImportManager = $travelSettlementImportManagerManager;

        parent::__construct(self::NAME);
    }

    protected function configure()
    {
        $this->setDescription('Import travel information file from the Sqills Import FTP');
        $this->addArgument('date', InputArgument::OPTIONAL, 'datum van import',
            (new \DateTimeImmutable('now'))->format('d-m-Y'));
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int 0: no failures, 1: TIF failures, 2: TSF failures, 3: TIF and TSF failures
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = (new \DateTimeImmutable($input->getArgument('date')));

        $output->writeln('<info>Importing TIF file(s)</info>');
        $this->travelInformationImportManager->process($date);
        $tifResult = $this->travelInformationImportManager->getImportResult();
        $this->outputImportResultReport($output, $tifResult, 'TIF');
        foreach ($tifResult->getSuccessfulRecordDeletions() as $result) {
            $output->writeln(
                sprintf(
                    '<info>TIF records deleted that were imported on %s:</info> %s',
                    $date->sub(new \DateInterval('P1D'))->format('d-m-Y'),
                    $result->getMessage()
                )
            );
        }
        foreach ($tifResult->getFailedRecordDeletions() as $result) {
            $output->writeln(
                sprintf(
                    '<error>Failure deleting TIF records that were imported on %s:</error> %s',
                    $date->sub(new \DateInterval('P1D'))->format('d-m-Y'),
                    $result->getError()
                )
            );
        }
        $output->writeln('<info>Import TIF done</info>');

        $output->writeln('');

        $output->writeln('<info>Importing TSF file(s)</info>');
        $this->travelSettlementImportManager->process($date);
        $tsfResult = $this->travelSettlementImportManager->getImportResult();
        $this->outputImportResultReport($output, $tsfResult, 'TSF');
        $output->writeln('<info>Import TSF done</info>');

        return ($tifResult->hasFailures() ? 1 : 0) | ($tsfResult->hasFailures() ? 2 : 0);
    }

    private function outputImportResultReport(OutputInterface $output, ImportResult $importResult, string $type)
    {
        $successfulFileImports = $importResult->getSuccessfulFileImports();
        $output->writeln(
            sprintf('<info>Successful %s imports:</info> %d', $type, count($successfulFileImports))
        );
        foreach ($successfulFileImports as $result) {
            $output->writeln($result->getFile()->getFilename().": ".$result->getMessage());
        }
        if ($numberOfFailedFileImports = count($importResult->getFailedFileImports())) {
            $output->writeln(
                sprintf('<error>Failed %s imports:</error> %d', $type, $numberOfFailedFileImports)
            );
            foreach ($importResult->getFailedFileImports() as $result) {
                $output->writeln($result->getFile()->getFilename().": ".$result->getError());
            }
        }
    }
}