<?php


namespace App\Command;


use App\Model\Export\Exporter;
use App\Model\Export\Result\FailureExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\Target\CsvFile;
use App\Model\Report\DebtorReceivables\DebtorReceivablesReportInput;
use App\Model\Report\DebtorReceivables\DebtorReceivablesReportModel;
use App\Util\FlysystemFile;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\DateRange;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportDebtorReceivablesReportCommand extends Command
{
    const NAME = 'arriva:export:debtor-receivables-report';

    /** @var DebtorReceivablesReportModel */
    private $debtorReceivablesReportModel;

    /** @var Exporter */
    private $exporter;

    /** @var FilesystemInterface */
    private $filesystem;

    public function __construct(
        DebtorReceivablesReportModel $debtorReceivablesReportModel,
        Exporter $exporter,
        FilesystemInterface $filesystem
    ) {
        parent::__construct(self::NAME);

        $this->debtorReceivablesReportModel = $debtorReceivablesReportModel;
        $this->exporter = $exporter;
        $this->filesystem = $filesystem;
    }

    protected function configure()
    {
        $this->setDescription('Generate and export a debtor receivables report');
        $this->addArgument('date', InputArgument::OPTIONAL, 'the date over which to generate the report',
            'yesterday'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = Date::of($input->getArgument('date'));

        $reportInput = new DebtorReceivablesReportInput(DateRange::of($date, $date));

        $filename = sprintf('%s_debiteurenvorderingen_abt_1.csv', $date->format('Ymd'));
        $target = new CsvFile(FlysystemFile::of($this->filesystem, $filename));

        $report = $this->debtorReceivablesReportModel->generateDebtorReceivablesReport($reportInput);

        if ($this->exporter->supportsExport($report, $target)) {
            $result = $this->exporter->export($report, $target);
            if ($result instanceof SuccessExportResult) {
                $output->writeln(
                    sprintf(
                        'Report file <info>%s</info> created with debtor receivables for <info>%s</info>',
                        $filename,
                        $date->format('Y-m-d')
                    )
                );
                return 0;
            }

            if ($result instanceof FailureExportResult) {
                $output->writeln(
                    sprintf('Debtor receivables report for <info>%s</info> could not be generated: %s',
                        $date->format('Y-m-d'),
                        $result->getReason()
                    )
                );
                return 1;
            }
        } else {
            $output->writeln(sprintf('<error>Export target %s not supported</error>', $target));
            return 1;
        }
    }
}
