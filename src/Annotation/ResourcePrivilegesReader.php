<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Annotation;

use App\Security\Configuration\Privilege;
use App\Security\Configuration\Resource;
use Doctrine\Common\Annotations\Reader;

/**
 * @package App\Annotation
 */
class ResourcePrivilegesReader
{
    /** @var Reader */
    private $reader;

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @param string $className
     * @param string $methodName
     * @throws \ReflectionException when the class $className does not exist.
     * @throws \ReflectionException when the class exists but method $methodName does not exist.
     * @return null[]|string[]
     */
    public function getResourceAndPrivilege(string $className, string $methodName): array
    {
        $reflectionClass = new \ReflectionClass($className);
        $reflectionMethod = $reflectionClass->getMethod($methodName);
        $resource = $this->reader->getMethodAnnotation($reflectionMethod, Resource::class);
        if (!$resource instanceof Resource) {
            $resource = $this->reader->getClassAnnotation($reflectionClass, Resource::class);
        }
        $privilege = $this->reader->getMethodAnnotation($reflectionMethod, Privilege::class);

        if ($resource instanceof Resource) {
            if (!$privilege instanceof Privilege) {
                $privilege = new Privilege(['value' => Privilege::READ]);
            }

            return [$resource->getValue(), $privilege->getValue()];
        }

        return [null, null];
    }
}
