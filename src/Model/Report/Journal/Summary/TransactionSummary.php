<?php

namespace App\Model\Report\Journal\Summary;

use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Financial\CostCenter;
use Arriva\Abt\Value\TAccount;

class TransactionSummary
{
    /** @var string */
    private $label;

    /** @var int */
    private $nrOfTransactions;

    /** @var VatMoney */
    private $amount;

    /** @var TAccount */
    private $tAccount;

    /** @var CostCenter|null */
    private $costCenter;

    /** @var CostUnitCode|null */
    private $costUnitCode;

    /**
     * @param string            $label
     * @param int               $nrOfTransactions
     * @param VatMoney          $amount
     * @param TAccount          $tAccount
     * @param CostCenter|null   $costCenter
     * @param CostUnitCode|null $costUnitCode
     */
    public function __construct(
        string $label,
        int $nrOfTransactions,
        VatMoney $amount,
        TAccount $tAccount,
        ?CostCenter $costCenter = null,
        ?CostUnitCode $costUnitCode = null
    ) {
        $this->label = $label;
        $this->nrOfTransactions = $nrOfTransactions;
        $this->amount = $amount;
        $this->tAccount = $tAccount;
        $this->costCenter = $costCenter;
        $this->costUnitCode = $costUnitCode;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getNrOfTransactions(): int
    {
        return $this->nrOfTransactions;
    }

    public function getAmount(): VatMoney
    {
        return $this->amount;
    }

    public function getTAccount(): TAccount
    {
        return $this->tAccount;
    }

    public function getCostCenter(): ?CostCenter
    {
        return $this->costCenter;
    }

    public function getCostUnitCode(): ?CostUnitCode
    {
        return $this->costUnitCode;
    }
}
