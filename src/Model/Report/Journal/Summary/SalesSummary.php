<?php

namespace App\Model\Report\Journal\Summary;

use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Financial\CostCenter;
use Arriva\Abt\Value\Financial\VatCode;
use Arriva\Abt\Value\TAccount;
use DateTimeImmutable;

class SalesSummary extends TransactionSummary
{
    /** @var VatCode */
    private $vatCode;

    /** @var DateTimeImmutable|null */
    private $saleDate;

    public function __construct(
        string $label,
        int $nrOfTransactions,
        VatMoney $amount,
        VatCode $vatCode,
        TAccount $tAccount,
        ?CostCenter $costCenter = null,
        ?CostUnitCode $costUnitCode = null,
        ?DateTimeImmutable $saleDate = null
    ) {
        parent::__construct($label, $nrOfTransactions, $amount, $tAccount, $costCenter, $costUnitCode);

        $this->vatCode = $vatCode;
        $this->saleDate = $saleDate;
    }

    public function getVatCode(): VatCode
    {
        return $this->vatCode;
    }

    public function getSaleDate(): ?DateTimeImmutable
    {
        return $this->saleDate;
    }
}
