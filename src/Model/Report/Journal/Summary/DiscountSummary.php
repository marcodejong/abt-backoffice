<?php

namespace App\Model\Report\Journal\Summary;

use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Financial\CostCenter;
use Arriva\Abt\Value\Financial\VatCode;
use Arriva\Abt\Value\TAccount;

class DiscountSummary extends TransactionSummary
{
    /** @var VatCode */
    protected $vatCode;

    /** @var TAccount */
    protected $groupTAccount;

    public function __construct(
        string $label,
        int $nrOfTransactions,
        VatMoney $amount,
        VatCode $vatCode,
        TAccount $tAccount,
        TAccount $tGroupAccount,
        ?CostCenter $costCenter = null,
        ?CostUnitCode $costUnitCode = null
    ) {
        parent::__construct($label, $nrOfTransactions, $amount, $tAccount, $costCenter, $costUnitCode);

        $this->vatCode = $vatCode;
        $this->groupTAccount = $tGroupAccount;
    }

    public function getGroupTAccount(): TAccount
    {
        return $this->groupTAccount;
    }

    public function getVatCode(): VatCode
    {
        return $this->vatCode;
    }
}
