<?php

namespace App\Model\Report\Journal\Summary;

use Arriva\Abt\Entity\Embeddable\VatMoney;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Financial\CostCenter;
use Arriva\Abt\Value\Financial\VatCode;
use Arriva\Abt\Value\TAccount;
use DateTimeImmutable;

class RefundSummary extends TransactionSummary
{
    /** @var VatCode */
    private $vatCode;

    /** @var DateTimeImmutable|null */
    private $refundDate;

    public function __construct(
        string $label,
        int $nrOfTransactions,
        VatMoney $amount,
        VatCode $vatCode,
        TAccount $tAccount,
        ?CostCenter $costCenter = null,
        ?CostUnitCode $costUnitCode = null,
        ?DateTimeImmutable $refundDate = null
    ) {
        parent::__construct($label, $nrOfTransactions, $amount, $tAccount, $costCenter, $costUnitCode);

        $this->vatCode = $vatCode;
        $this->refundDate = $refundDate;
    }

    public function getVatCode(): VatCode
    {
        return $this->vatCode;
    }

    public function getRefundDate(): ?DateTimeImmutable
    {
        return $this->refundDate;
    }
}
