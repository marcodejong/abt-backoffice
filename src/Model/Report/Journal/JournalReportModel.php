<?php

namespace App\Model\Report\Journal;

use App\Model\Report\Journal\Summary\RefundSummary;
use App\Model\Report\Journal\Summary\TransactionSummary;
use App\Repository\Financial\DiscountSummaryRepository;
use App\Repository\Financial\FinancialSummaryCriteria;
use App\Repository\Financial\MoneysPayableSummaryRepository;
use App\Repository\Financial\MoneysReceivableSummaryRepository;
use App\Repository\Financial\MoneysReceivedSummaryRepository;
use App\Repository\Financial\RefundSummaryRepository;
use App\Repository\Financial\SalesSummaryRepository;
use Arriva\Abt\Utility\Iterator;

class JournalReportModel
{
    /** @var SalesSummaryRepository */
    private $salesSummaryRepository;

    /** @var RefundSummaryRepository */
    private $refundSummaryRepository;

    /** @var MoneysReceivedSummaryRepository */
    private $moneysReceivedSummaryRepository;

    /** @var MoneysPayableSummaryRepository */
    private $moneysPayableSummaryRepository;

    /** @var MoneysReceivableSummaryRepository */
    private $moneysReceivableSummaryRepository;

    /** @var DiscountSummaryRepository */
    private $discountSummaryRepository;

    /**
     * @param SalesSummaryRepository            $salesSummaryRepository
     * @param RefundSummaryRepository           $refundSummaryRepository
     * @param MoneysReceivedSummaryRepository   $moneysReceivedSummaryRepository
     * @param MoneysPayableSummaryRepository    $moneysPayableSummaryRepository
     * @param MoneysReceivableSummaryRepository $moneysReceivableSummaryRepository
     * @param DiscountSummaryRepository         $discountSummaryRepository
     */
    public function __construct(
        SalesSummaryRepository $salesSummaryRepository,
        RefundSummaryRepository $refundSummaryRepository,
        MoneysReceivedSummaryRepository $moneysReceivedSummaryRepository,
        MoneysPayableSummaryRepository $moneysPayableSummaryRepository,
        MoneysReceivableSummaryRepository $moneysReceivableSummaryRepository,
        DiscountSummaryRepository $discountSummaryRepository
    ) {
        $this->salesSummaryRepository = $salesSummaryRepository;
        $this->refundSummaryRepository = $refundSummaryRepository;
        $this->moneysReceivedSummaryRepository = $moneysReceivedSummaryRepository;
        $this->moneysPayableSummaryRepository = $moneysPayableSummaryRepository;
        $this->moneysReceivableSummaryRepository = $moneysReceivableSummaryRepository;
        $this->discountSummaryRepository = $discountSummaryRepository;
    }

    public function generateJournalReport(JournalReportInput $input): JournalReport
    {
        $report = new JournalReport($input->getPeriod());

        foreach ($this->generateSalesSummaries($input) as $summary) {
            $report->addTransactionSummary($summary);
        }

        foreach ($this->generateRefundSummaries($input) as $summary) {
            $report->addTransactionSummary($summary);
        }

        foreach ($this->generatePaymentSummaries($input) as $summary) {
            $report->addTransactionSummary($summary);
        }

        return $report;
    }

    /**
     * @param JournalReportInput $input
     *
     * @return TransactionSummary[]|iterable
     */
    private function generateSalesSummaries(JournalReportInput $input): iterable
    {
        return $this->salesSummaryRepository->findSalesSummariesPerCostUnit(
            new FinancialSummaryCriteria($input->getPeriod())
        );
    }

    /**
     * @param JournalReportInput $input
     *
     * @return RefundSummary[]|iterable
     */
    private function generateRefundSummaries(JournalReportInput $input): iterable
    {
        return $this->refundSummaryRepository->findRefundSummariesPerCostUnit(
            new FinancialSummaryCriteria($input->getPeriod())
        );
    }

    /**
     * @param JournalReportInput $input
     *
     * @return TransactionSummary[]|iterable
     */
    private function generatePaymentSummaries(JournalReportInput $input): iterable
    {
        $criteria = new FinancialSummaryCriteria($input->getPeriod());

        return Iterator::create([])
                       ->concat(
                           $this->moneysReceivedSummaryRepository->getMoneysReceivedSummaries($criteria),
                           $this->moneysPayableSummaryRepository->getMoneysPayableSummaries($criteria),
                           $this->moneysReceivableSummaryRepository->getMoneysReceivableSummaries($criteria),
                           $this->discountSummaryRepository->getDiscountSummaries($criteria)
                       );
    }
}
