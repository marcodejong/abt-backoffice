<?php


namespace App\Model\Report\Journal\Exporter;


use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\SimpleExporter;
use App\Model\Export\SingleTypeExporterTrait;
use App\Model\Export\Target\PdfFile;
use App\Model\Report\Journal\JournalReport;
use App\Service\FileGenerator\Entity\File;
use App\Service\FileGenerator\Entity\FileTemplate;
use App\Service\FileGenerator\Entity\OutputType;
use App\Service\FileGenerator\Entity\Request\CreateRequest;
use App\Service\FileGenerator\Entity\Response\CreateStreamResponse;
use App\Service\FileGenerator\Factory\JournalReportPdfDataFactory;
use App\Service\FileGenerator\FileGeneratorClient;
use SolidPhp\ValueObjects\Type\Type;

class JournalReportPdfExporter extends SimpleExporter
{
    use SingleTypeExporterTrait;

    /** @var FileGeneratorClient */
    private $client;

    /** @var JournalReportPdfDataFactory */
    private $pdfDataFactory;

    public function __construct(FileGeneratorClient $client, JournalReportPdfDataFactory $pdfDataFactory)
    {
        $this->setSupport(
            Type::of(JournalReport::class),
            Type::of(PdfFile::class)
        );

        $this->client = $client;
        $this->pdfDataFactory = $pdfDataFactory;
    }

    /**
     * @param JournalReport $subject
     * @param PdfFile $target
     * @return ExportResult
     */
    protected function doExport($subject, $target): ExportResult
    {
        $data = $this->pdfDataFactory->create($subject);

        $response = $this->client->createFile(new CreateRequest(
            OutputType::STREAM(),
            [File::createPdfType(FileTemplate::ABT_MONTHLY_JOURNAL_REPORT())],
            $data
        ));

        if ($response instanceof CreateStreamResponse) {
            $target->getFile()->write($response->getStream()->detach());
        }

        return new SuccessExportResult();
    }

}
