<?php

namespace App\Model\Report\Journal\Exporter;

use App\Exception\UnsupportedSubtypeException;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\SimpleExporter;
use App\Model\Export\Result\ExportResult;
use App\Model\Export\SingleTypeExporterTrait;
use App\Model\Export\Target\CsvFile;
use App\Model\Report\Journal\JournalReport;
use App\Model\Report\Journal\Summary\DiscountSummary;
use App\Model\Report\Journal\Summary\MoneysPayableSummary;
use App\Model\Report\Journal\Summary\MoneysReceivableSummary;
use App\Model\Report\Journal\Summary\MoneysReceivedSummary;
use App\Model\Report\Journal\Summary\RefundSummary;
use App\Model\Report\Journal\Summary\SalesSummary;
use App\Model\Report\Journal\Summary\TransactionSummary;
use Arriva\Abt\Utility\Money;
use League\Csv\Writer;
use League\Flysystem\FilesystemInterface;
use SolidPhp\ValueObjects\Type\Type;

class JournalReportCsvExporter extends SimpleExporter
{
    use SingleTypeExporterTrait;

    /** @var FilesystemInterface */
    private $fileSystem;

    public function __construct(FilesystemInterface $fileSystem)
    {
        $this->setSupport(
            Type::of(JournalReport::class),
            Type::of(CsvFile::class)
        );

        $this->fileSystem = $fileSystem;
    }

    /**
     * @param JournalReport $subject
     * @param CsvFile $target
     *
     * @return ExportResult
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     */
    protected function doExport($subject, $target): ExportResult
    {
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');

        $lineNr = 0;
        $csv->insertOne(['lineNr' => $lineNr++] + $this->createHeaderRecord($subject));

        foreach ($subject->getTransactionSummaries() as $transactionSummary) {
            $csv->insertOne(
                ['lineNr' => $lineNr++] + $this->createTransactionSummaryRecord($subject, $transactionSummary)
            );
        }

        $target->getFile()->write($csv->getContent(), true);

        return new SuccessExportResult();
    }

    private function createHeaderRecord(JournalReport $report): array
    {
        return [
            'department'           => '',
            'year'                 => $report->getPeriod()->getStartDate()->getYearObject()->getNumber(),
            'month'                => $report->getPeriod()->getStartDate()->getMonthObject()->getNumber(),
            'journal'              => '',
            'date'                 => '',
            'description'          => 'ABT',
            'account'              => '',
            'amount_including_vat' => '',
            'vat_code'             => '',
            'vat_amount'           => '',
            'cost_center'          => '',
            'cost_unit_code'       => '',
            'nr_of_transactions'   => '',
        ];
    }

    private function createTransactionSummaryRecord(JournalReport $report, TransactionSummary $summary): array
    {
        switch (true) {
            case $summary instanceof SalesSummary:
                return $this->createSalesSummaryRecord($report, $summary);
            case $summary instanceof RefundSummary:
                return $this->createRefundSummaryRecord($report, $summary);
            case $summary instanceof MoneysReceivedSummary:
                return $this->createMoneysReceivedSummaryRecord($report, $summary);
            case $summary instanceof MoneysPayableSummary:
                return $this->createMoneysPayableSummaryRecord($report, $summary);
            case $summary instanceof MoneysReceivableSummary:
                return $this->createMoneysReceivableSummaryRecord($report, $summary);
            case $summary instanceof DiscountSummary:
                return $this->createDiscountSummaryRecord($report, $summary);
            default:
                throw new UnsupportedSubtypeException(Type::of(TransactionSummary::class), $summary);
        }
    }

    private function createSalesSummaryRecord(JournalReport $report, SalesSummary $summary): array
    {

        return [
            'department'           => 'ABT',
            'year'                 => $report->getPeriod()->getStartDate()->getYearObject()->getNumber(),
            'month'                => $report->getPeriod()->getStartDate()->getMonthObject()->getNumber(),
            'journal'              => '',
            'date'                 => $summary->getSaleDate() ? $summary->getSaleDate()->format('d-m-Y') : null,
            'description'          => 'ABT',
            'account'              => $summary->getTAccount()->getValue(),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()->multiply(-1)),
            'vat_code'             => $summary->getVatCode()->getValue(),
            'vat_amount'           => $this->formatMoney($summary->getAmount()->getVatAmount()),
            'cost_center'          => $summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '',
            'cost_unit_code'       => $summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '',
            'nr_of_transactions'   => $summary->getNrOfTransactions(),
        ];
    }

    private function createRefundSummaryRecord(JournalReport $report, RefundSummary $summary): array
    {
        return [
            'department'           => 'ABT',
            'year'                 => $report->getPeriod()->getStartDate()->getYearObject()->getNumber(),
            'month'                => $report->getPeriod()->getStartDate()->getMonthObject()->getNumber(),
            'journal'              => '',
            'date'                 => $summary->getRefundDate() ? $summary->getRefundDate()->format('d-m-Y') : null,
            'description'          => 'ABT',
            'account'              => $summary->getTAccount()->getValue(),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()),
            'vat_code'             => $summary->getVatCode()->getValue(),
            'vat_amount'           => $this->formatMoney($summary->getAmount()->getVatAmount()->multiply(-1)),
            'cost_center'          => $summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '',
            'cost_unit_code'       => $summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '',
            'nr_of_transactions'   => $summary->getNrOfTransactions(),
        ];
    }

    private function createMoneysReceivedSummaryRecord(JournalReport $report, MoneysReceivedSummary $summary): array
    {
        $date = $report->getPeriod()->getStartDate();
        $isDailyReport = 0 === $report->getPeriod()->getStartDate()->compare($report->getPeriod()->getEndDate());

        return [
            'department'           => 'ABT',
            'year'                 => $date->getYearObject()->getNumber(),
            'month'                => $date->getMonthObject()->getNumber(),
            'journal'              => '',
            'date'                 => $isDailyReport ? $date->format('d-m-Y') : null,
            'description'          => sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')),
            'account'              => $summary->getTAccount()->getValue(),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()),
            'vat_code'             => '',
            'vat_amount'           => $this->formatMoney($summary->getAmount()->getVatAmount()),
            'cost_center'          => '',
            'cost_unit_code'       => '',
            'nr_of_transactions'   => $summary->getNrOfTransactions(),
        ];
    }

    private function createMoneysPayableSummaryRecord(JournalReport $report, MoneysPayableSummary $summary): array
    {
        $date = $report->getPeriod()->getStartDate();
        $isDailyReport = 0 === $report->getPeriod()->getStartDate()->compare($report->getPeriod()->getEndDate());

        return [
            'department'           => 'ABT',
            'year'                 => $date->getYearObject()->getNumber(),
            'month'                => $date->getMonthObject()->getNumber(),
            'journal'              => '',
            'date'                 => $isDailyReport ? $date->format('d-m-Y') : null,
            'description'          => sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')),
            'account'              => $summary->getTAccount()->getValue(),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()->multiply(-1)),
            'vat_code'             => '',
            'vat_amount'           => $this->formatMoney($summary->getAmount()->getVatAmount()->multiply(-1)),
            'cost_center'          => $summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '',
            'cost_unit_code'       => $summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '',
            'nr_of_transactions'   => $summary->getNrOfTransactions(),
        ];
    }

    private function createMoneysReceivableSummaryRecord(JournalReport $report, MoneysReceivableSummary $summary): array
    {
        $date = $report->getPeriod()->getStartDate();
        $isDailyReport = 0 === $report->getPeriod()->getStartDate()->compare($report->getPeriod()->getEndDate());

        return [
            'department'           => 'ABT',
            'year'                 => $date->getYearObject()->getNumber(),
            'month'                => $date->getMonthObject()->getNumber(),
            'journal'              => '',
            'date'                 => $isDailyReport ? $date->format('d-m-Y') : null,
            'description'          => sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')),
            'account'              => $summary->getTAccount()->getValue(),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()),
            'vat_code'             => '',
            'vat_amount'           => $this->formatMoney($summary->getAmount()->getVatAmount()),
            'cost_center'          => $summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '',
            'cost_unit_code'       => $summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '',
            'nr_of_transactions'   => $summary->getNrOfTransactions(),
        ];
    }

    private function createDiscountSummaryRecord(JournalReport $report, DiscountSummary $summary): array
    {
        $date = $report->getPeriod()->getStartDate();
        $isDailyReport = 0 === $report->getPeriod()->getStartDate()->compare($report->getPeriod()->getEndDate());

        return [
            'department'           => 'ABT',
            'year'                 => $date->getYearObject()->getNumber(),
            'month'                => $date->getMonthObject()->getNumber(),
            'journal'              => '',
            'date'                 => $isDailyReport ? $date->format('d-m-Y') : null,
            'description'          => sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')),
            'account'              => sprintf(
                '%s %s',
                $summary->getGroupTAccount()->getValue(),
                $summary->getTAccount()->getValue()
            ),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()),
            'vat_code'             => $summary->getVatCode()->getValue(),
            'vat_amount'           => $this->formatMoney($summary->getAmount()->getVatAmount()),
            'cost_center'          => $summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '',
            'cost_unit_code'       => $summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '',
            'nr_of_transactions'   => $summary->getNrOfTransactions(),
        ];
    }

    private function formatMoney(Money $money): string
    {
        return number_format(
            (float)$money->getAmount(),
            2,
            ',',
            ''
        );
    }
}
