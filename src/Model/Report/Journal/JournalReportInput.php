<?php

namespace App\Model\Report\Journal;

use Arriva\Abt\Value\DateRange;

class JournalReportInput
{
    /** @var DateRange */
    private $period;

    public function __construct(DateRange $period)
    {
        $this->period = $period;
    }

    public function getPeriod(): DateRange
    {
        return $this->period;
    }
}
