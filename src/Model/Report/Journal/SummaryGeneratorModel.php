<?php

namespace App\Model\Report\Journal;

use Arriva\Abt\Value\Time\DateRangeInterface;

class SummaryGeneratorModel
{
    public function getSalesSummariesPerCostUnit(DateRangeInterface $dateRange): iterable
    {
        // TODO use an iterated query to construct the sales summaries
        return [];
    }

    public function getRefundSummariesPerCostUnit(DateRangeInterface $dateRange): iterable
    {
        // TODO use an iterated query to construct the refund summaries
        return [];
    }

    public function getPaymentSummaries(DateRangeInterface $dateRange): iterable
    {
        // TODO use queries to construct the four payment summaries (received amounts, credited amounts, receivable amounts and discount amounts)
        return [];
    }
}
