<?php

namespace App\Model\Report\Journal;

use App\Model\Report\Journal\Summary\TransactionSummary;
use App\Model\Report\Report;
use Arriva\Abt\Value\Time\DateRangeInterface;

class JournalReport implements Report
{
    /** @var DateRangeInterface */
    private $period;

    /** @var TransactionSummary[] */
    private $transactionSummaries = [];

    /**
     * @param DateRangeInterface $period
     */
    public function __construct(DateRangeInterface $period)
    {
        $this->period = $period;
    }

    public function getPeriod(): DateRangeInterface
    {
        return $this->period;
    }

    /**
     * @return TransactionSummary[]|iterable
     */
    public function getTransactionSummaries(): iterable
    {
        return $this->transactionSummaries;
    }

    public function addTransactionSummary(TransactionSummary $summary): void
    {
        $this->transactionSummaries[] = $summary;
    }
}
