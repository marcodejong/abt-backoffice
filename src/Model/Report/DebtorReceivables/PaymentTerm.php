<?php


namespace App\Model\Report\DebtorReceivables;


use Arriva\Abt\Entity\Customer;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\OrderNumber\OrderNumber;

class PaymentTerm
{
    /** @var Date */
    private $date;

    /** @var Customer */
    private $customer;

    /** @var Money */
    private $receivableAmount;

    /** @var Money */
    private $payableAmount;

    /** @var Date */
    private $expiryDate;

    /** @var OrderNumber */
    private $orderNumber;

    /** @var int */
    private $termNumber;

    /** @var int */
    private $totalTerms;

    /** @var int */
    private $entryNumber;

    public function __construct(
        Date $date,
        Customer $customer,
        Money $receivableAmount,
        Money $payableAmount,
        Date $expiryDate,
        OrderNumber $orderNumber,
        int $termNumber,
        int $totalTerms,
        int $entryNumber
    ) {
        $this->date = $date;
        $this->customer = $customer;
        $this->receivableAmount = $receivableAmount;
        $this->payableAmount = $payableAmount;
        $this->expiryDate = $expiryDate;
        $this->orderNumber = $orderNumber;
        $this->termNumber = $termNumber;
        $this->totalTerms = $totalTerms;
        $this->entryNumber = $entryNumber;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function getReceivableAmount(): Money
    {
        return $this->receivableAmount;
    }

    public function getPayableAmount(): Money
    {
        return $this->payableAmount;
    }

    public function getExpiryDate(): Date
    {
        return $this->expiryDate;
    }

    public function getOrderNumber(): OrderNumber
    {
        return $this->orderNumber;
    }

    public function getTermNumber(): int
    {
        return $this->termNumber;
    }

    public function getTotalTerms(): int
    {
        return $this->totalTerms;
    }

    public function getEntryNumber(): int
    {
        return $this->entryNumber;
    }
}