<?php

namespace App\Model\Report\DebtorReceivables;

use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\SimpleExporter;
use App\Model\Export\SingleTypeExporterTrait;
use App\Model\Export\Target\CsvFile;
use Arriva\Abt\Utility\Money;
use League\Csv\Exception;
use League\Csv\Writer;
use SolidPhp\ValueObjects\Type\Type;
use Traversable;

class DebtorReceivablesCsvExporter extends SimpleExporter
{
    use SingleTypeExporterTrait;

    public function __construct()
    {
        $this->setSupport(
            Type::of(DebtorReceivablesReport::class),
            Type::of(CsvFile::class)
        );
    }

    /**
     * @param DebtorReceivablesReport $subject
     * @param CsvFile                 $target
     *
     * @return ExportResult
     * @throws Exception
     */
    protected function doExport($subject, $target): ExportResult
    {
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');

        foreach ($subject->getCollectionSummaries() as $collectionSummary) {
            $csv->insertAll($this->createCollectionRecords($collectionSummary));
        }

        $target->getFile()->write($csv->getContent(), true);

        return new SuccessExportResult();
    }

    /**
     * @param CollectionSummary $summary
     * @return array[]|Traversable
     */
    private function createCollectionRecords(CollectionSummary $summary): array
    {
        $invoicedOnDMY = $summary->getInvoicedOn()->format('dmY');
        $invoicedOnN = $summary->getInvoicedOn()->format('n');
        $expiresOnDMY = $summary->getExpiresOn()->format('dmY');

        $generalLedgerNumber = $summary->getGeneralLedger()->getValue();
        $costUnitCode = $summary->getCostUnitCode()->getValue();
        $debtorNumber = $summary->getDebtorNumber()->getNumber();
        $debtorLedgerNumber = $summary->getDebtorLedger()->getValue();
        $paymentMethodLabel = $summary->getPaymentMethod()->getValue();
        $journalNumber = $summary->getJournal()->getValue();

        $description = $summary->getDescription();

        $amount = $summary->getAmount();

        return [
            [
                'line_nr'        => 0,
                'empty'          => '',
                'journal'        => $journalNumber,
                'date'           => $invoicedOnDMY,
                'period'         => $invoicedOnN,
                'grbrek'         => $generalLedgerNumber,
                'cost_unit'      => '',
                'debtor_number'  => (string)$debtorNumber,
                'description'    => $description,
                'amount'         => 0,
                'empty_1'        => '',
                'empty_2'        => '',
                'expiry_date'    => $expiresOnDMY,
                'payment_method' => $paymentMethodLabel,
                'V'              => '',
                'number'         => '',
            ],
            [
                'line_nr'        => 1,
                'empty'          => '',
                'journal'        => '',
                'date'           => $invoicedOnDMY,
                'period'         => $invoicedOnN,
                'grbrek'         => $debtorLedgerNumber,
                'cost_unit'      => $costUnitCode,
                'debtor_number'  => $debtorNumber,
                'description'    => $description,
                'amount'         => $this->formatMoney($amount),
                'empty_1'        => '',
                'empty_2'        => '',
                'expiry_date'    => $expiresOnDMY,
                'payment_method' => '',
                'V'              => 1,
                'number'         => '',
            ],
            [
                'line_nr'        => 2,
                'empty'          => '',
                'journal'        => '',
                'date'           => $invoicedOnDMY,
                'period'         => $invoicedOnN,
                'grbrek'         => $generalLedgerNumber,
                'cost_unit'      => $costUnitCode,
                'debtor_number'  => $debtorNumber,
                'description'    => $description,
                'amount'         => $this->formatMoney($amount->times(-1)),
                'empty_1'        => '',
                'empty_2'        => '',
                'expiry_date'    => $expiresOnDMY,
                'payment_method' => '',
                'V'              => 1,
                'number'         => '',
            ],
        ];
    }

    private function formatMoney(Money $money): string
    {
        return number_format($money->getAmount(), 2, '.', '');
    }
}
