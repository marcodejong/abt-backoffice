<?php

namespace App\Model\Report\DebtorReceivables;

use App\Repository\InvoiceRepositoryInterface;
use App\Service\CustomerIdService\CustomerIdentityService;
use Arriva\Abt\Entity\Customer;
use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\InvoiceItem;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Duration\Duration;
use Arriva\Abt\Value\Financial\Journal;
use Arriva\Abt\Value\PaymentMethod;
use Arriva\Abt\Value\TAccount;
use Arriva\Assist\Value\CustomerNumber as AssistCustomerNumber;
use Closure;
use RuntimeException;

class DebtorReceivablesReportModel
{
    /** @var Journal */
    private $generalJournal;

    /** @var TAccount */
    private $generalLedger;

    /** @var TAccount */
    private $debtorLedger;

    /** @var CostUnitCode */
    private $costUnitCode;

    /** @var Duration */
    private $expiryDuration;

    /** @var InvoiceRepositoryInterface */
    private $invoiceRepository;

    /** @var CustomerIdentityService */
    private $customerIdentityService;

    /**
     * DebtorReceivablesReportModel constructor.
     *
     * @param Journal                    $generalJournal
     * @param TAccount                   $generalLedger
     * @param TAccount                   $debtorLedger
     * @param CostUnitCode               $costUnitCode
     * @param Duration                   $expiryDuration
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param CustomerIdentityService    $customerIdentityService
     */
    public function __construct(
        Journal $generalJournal,
        TAccount $generalLedger,
        TAccount $debtorLedger,
        CostUnitCode $costUnitCode,
        Duration $expiryDuration,
        InvoiceRepositoryInterface $invoiceRepository,
        CustomerIdentityService $customerIdentityService
    ) {
        $this->generalJournal = $generalJournal;
        $this->generalLedger = $generalLedger;
        $this->debtorLedger = $debtorLedger;
        $this->costUnitCode = $costUnitCode;
        $this->expiryDuration = $expiryDuration;
        $this->invoiceRepository = $invoiceRepository;
        $this->customerIdentityService = $customerIdentityService;
    }

    public function generateDebtorReceivablesReport(DebtorReceivablesReportInput $input): DebtorReceivablesReport
    {
        return new DebtorReceivablesReport(
            $this->invoiceRepository->findInvoicesByInvoiceDate($input->getPeriod())
                                    ->flatMap(Closure::fromCallable([$this, 'getCollectionSummaries']))
        );
    }

    /**
     * @param Invoice $invoice
     * @return Iterator&iterable<CollectionSummary>
     */
    private function getCollectionSummaries(Invoice $invoice): Iterator
    {
        return $invoice->getInvoiceItemsIterator()
                       ->filter(
                           static function (InvoiceItem $item) {
                               return $item instanceof Invoice\Item\Collection;
                           }
                       )
                       ->map(Closure::fromCallable([$this, 'getCollectionSummary']));
    }

    private function getCollectionSummary(Invoice\Item\Collection $collection): CollectionSummary
    {
        $invoice = $collection->getInvoice();
        $invoicedOn = $collection->getInvoicedOn();

        if (!$invoice || !$invoicedOn) {
            throw new RuntimeException('Cannot summarize an un-invoiced collection');
        }

        return new CollectionSummary(
            $invoicedOn,
            $this->expiryDuration->computeEndDateFrom($invoicedOn),
            $this->generalJournal,
            $this->generalLedger,
            $this->debtorLedger,
            $this->getDebtorNumber($invoice->getCustomer()),
            sprintf('F%s', $invoice->getInvoiceNumber()),
            PaymentMethod::of('Partial_payment'),
            $this->costUnitCode,
            $collection->getAmount()->getVatMoney()->getAmountWithVat()
        );
    }

    private function getDebtorNumber(Customer $customer): ?AssistCustomerNumber
    {
        if ($assistId = $this->customerIdentityService->getAssistIdByIdmId($customer->getIdmId())) {
            return AssistCustomerNumber::fromCustomerId($assistId);
        }

        return null;
    }
}
