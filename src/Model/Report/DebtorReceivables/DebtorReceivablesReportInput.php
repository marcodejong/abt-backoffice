<?php


namespace App\Model\Report\DebtorReceivables;


use Arriva\Abt\Value\DateRange;

class DebtorReceivablesReportInput
{
    /** @var DateRange */
    private $period;

    public function __construct(DateRange $period)
    {
        $this->period = $period;
    }

    public function getPeriod(): DateRange
    {
        return $this->period;
    }
}