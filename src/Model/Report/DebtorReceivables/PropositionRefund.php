<?php


namespace App\Model\Report\DebtorReceivables;


use Arriva\Abt\Entity\Customer;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\OrderNumber\OrderNumber;

class PropositionRefund
{
    /** @var Date */
    private $date;

    /** @var Customer */
    private $customer;

    /** @var Money */
    private $amount;

    /** @var Date */
    private $expiryDate;

    /** @var OrderNumber */
    private $orderNumber;

    /** @var int */
    private $entryNumber;

    public function __construct(
        Date $date,
        Customer $customer,
        Money $amount,
        Date $expiryDate,
        OrderNumber $orderNumber,
        int $entryNumber
    ) {
        $this->date = $date;
        $this->customer = $customer;
        $this->amount = $amount;
        $this->expiryDate = $expiryDate;
        $this->orderNumber = $orderNumber;
        $this->entryNumber = $entryNumber;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getExpiryDate(): Date
    {
        return $this->expiryDate;
    }

    public function getOrderNumber(): OrderNumber
    {
        return $this->orderNumber;
    }

    public function getEntryNumber(): int
    {
        return $this->entryNumber;
    }
}