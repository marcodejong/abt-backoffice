<?php

namespace App\Model\Report\DebtorReceivables;

use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\CostUnitCode;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\Financial\Journal;
use Arriva\Abt\Value\PaymentMethod;
use Arriva\Abt\Value\TAccount;
use Arriva\Assist\Value\CustomerNumber as AssistCustomerNumber;

final class CollectionSummary
{
    /** @var Date */
    private $invoicedOn;

    /** @var Date */
    private $expiresOn;

    /** @var Journal */
    private $journal;

    /** @var TAccount */
    private $generalLedger;

    /** @var TAccount */
    private $debtorLedger;

    /** @var AssistCustomerNumber */
    private $debtorNumber;

    /** @var string */
    private $description;

    /** @var PaymentMethod */
    private $paymentMethod;

    /** @var CostUnitCode */
    private $costUnitCode;

    /** @var Money */
    private $amount;

    /**
     * @param Date                 $invoicedOn
     * @param Date                 $expiresOn
     * @param Journal              $journal
     * @param TAccount             $generalLedger
     * @param TAccount             $debtorLedger
     * @param AssistCustomerNumber $debtorNumber
     * @param string               $description
     * @param PaymentMethod        $paymentMethod
     * @param CostUnitCode         $costUnit
     * @param Money                $amount
     */
    public function __construct(
        Date $invoicedOn,
        Date $expiresOn,
        Journal $journal,
        TAccount $generalLedger,
        TAccount $debtorLedger,
        AssistCustomerNumber $debtorNumber,
        string $description,
        PaymentMethod $paymentMethod,
        CostUnitCode $costUnit,
        Money $amount
    ) {
        $this->invoicedOn = $invoicedOn;
        $this->expiresOn = $expiresOn;
        $this->journal = $journal;
        $this->generalLedger = $generalLedger;
        $this->debtorLedger = $debtorLedger;
        $this->debtorNumber = $debtorNumber;
        $this->description = $description;
        $this->paymentMethod = $paymentMethod;
        $this->costUnitCode = $costUnit;
        $this->amount = $amount;
    }

    public function getInvoicedOn(): Date
    {
        return $this->invoicedOn;
    }

    public function getExpiresOn(): Date
    {
        return $this->expiresOn;
    }

    public function getJournal(): Journal
    {
        return $this->journal;
    }

    public function getGeneralLedger(): TAccount
    {
        return $this->generalLedger;
    }

    public function getDebtorLedger(): TAccount
    {
        return $this->debtorLedger;
    }

    public function getDebtorNumber(): AssistCustomerNumber
    {
        return $this->debtorNumber;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function getCostUnitCode(): CostUnitCode
    {
        return $this->costUnitCode;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }
}
