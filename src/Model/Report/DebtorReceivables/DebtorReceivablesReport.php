<?php

namespace App\Model\Report\DebtorReceivables;

use App\Model\Report\Report;
use Arriva\Abt\Utility\Iterator;

class DebtorReceivablesReport implements Report
{
    /** @var iterable<CollectionSummary> */
    private $collectionSummaries;

    public function __construct(iterable $collectionSummaries)
    {

        $this->collectionSummaries  = $collectionSummaries;
    }

    /**
     * @return iterable<CollectionSummary>
     */
    public function getCollectionSummaries(): iterable
    {
        return Iterator::create($this->collectionSummaries);
    }
}
