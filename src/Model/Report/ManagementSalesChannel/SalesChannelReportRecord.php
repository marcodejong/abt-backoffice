<?php


namespace App\Model\Report\ManagementSalesChannel;


use Arriva\Abt\Entity\Card;
use Arriva\Abt\Entity\Customer;
use Arriva\Abt\Entity\Order;
use Arriva\Abt\Entity\Payment;
use Arriva\Abt\Entity\PropositionInstance;
use Arriva\Abt\Entity\PropositionOrderItem;
use Arriva\Abt\Entity\Subscription;

interface SalesChannelReportRecord
{
    public function getSubscription(): Subscription;

    public function getCard(): Card;

    public function getCustomer(): Customer;

    public function getPropositionInstance(): PropositionInstance;

    public function getOrderItem(): PropositionOrderItem;

    public function getOrder(): Order;

    public function getPayment(): Payment;
}