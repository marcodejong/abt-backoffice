<?php


namespace App\Model\Report\ManagementSalesChannel;


use App\Repository\Financial\PropositionRefundRepository;
use App\Repository\Financial\PropositionSaleRepository;

class ManagementSalesChannelReportModel
{
    /** @var PropositionSaleRepository */
    private $propositionSaleRepository;

    /** @var PropositionRefundRepository */
    private $propositionRefundRepository;

    public function __construct(
        PropositionSaleRepository $propositionSaleRepository,
        PropositionRefundRepository $propositionRefundRepository
    ) {
        $this->propositionSaleRepository = $propositionSaleRepository;
        $this->propositionRefundRepository = $propositionRefundRepository;
    }

    public function generateManagementSalesChannelReport(
        ManagementSalesChannelReportInput $reportInput
    ): ManagementSalesChannelReport {
        $report = new ManagementSalesChannelReport();

        foreach($this->generateSales($reportInput) as $sale) {
            $report->addSalesChannelReportRecord($sale);
        }

        foreach($this->generateRefunds($reportInput) as $refund) {
            $report->addSalesChannelReportRecord($refund);
        }

        return $report;
    }

    /**
     * @param ManagementSalesChannelReportInput $reportInput
     * @return PropositionSale[]
     */
    private function generateSales(ManagementSalesChannelReportInput $reportInput): array
    {
        return $this->propositionSaleRepository->findPropositionSalesForPeriod($reportInput->getPeriod());
    }

    /**
     * @param ManagementSalesChannelReportInput $reportInput
     * @return PropositionRefund[]
     */
    private function generateRefunds(ManagementSalesChannelReportInput $reportInput): array
    {
        return $this->propositionRefundRepository->findPropositionRefundsForPeriod($reportInput->getPeriod());
    }
}