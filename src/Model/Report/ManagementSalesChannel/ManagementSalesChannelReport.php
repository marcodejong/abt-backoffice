<?php


namespace App\Model\Report\ManagementSalesChannel;


class ManagementSalesChannelReport
{
    /** @var SalesChannelReportRecord[] */
    private $salesChannelReportRecords;

    /**
     * ManagementSalesChannelReport constructor.
     */
    public function __construct()
    {
        $this->salesChannelReportRecords = [];
    }

    public function addSalesChannelReportRecord(SalesChannelReportRecord $salesChannelReportRecord): void
    {
        $this->salesChannelReportRecords[] = $salesChannelReportRecord;
    }

    /**
     * @return SalesChannelReportRecord[]
     */
    public function getSalesChannelReportRecords(): array
    {
        return $this->salesChannelReportRecords;
    }
}