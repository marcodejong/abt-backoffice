<?php


namespace App\Model\Report\ManagementSalesChannel;


use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\SimpleExporter;
use App\Model\Export\SingleTypeExporterTrait;
use App\Model\Export\Target\CsvFile;
use App\Service\CustomerIdService\CustomerIdentityService;
use Arriva\Abt\Entity\Configuration\RouteConfiguration;
use Arriva\Abt\Entity\Configuration\StarConfiguration;
use Arriva\Abt\Entity\PropositionElementInstance;
use Arriva\Abt\Entity\PropositionInstance;
use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Iterator;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\Duration\Duration;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SolidPhp\ValueObjects\Type\Type;

class ManagementSalesChannelReportCsvExporter extends SimpleExporter
{
    use SingleTypeExporterTrait;

    /** @var CustomerIdentityService */
    private $customerIdentityService;

    /** @var Duration */
    private $termDuration;

    public function __construct(
        CustomerIdentityService $customerIdentityService,
        Duration $termDuration
    ) {
        $this->setSupport(
            Type::of(ManagementSalesChannelReport::class),
            Type::of(CsvFile::class)
        );

        $this->customerIdentityService = $customerIdentityService;
        $this->termDuration = $termDuration;
    }


    /**
     * @param ManagementSalesChannelReport $subject
     * @param CsvFile $target
     *
     * @return ExportResult
     * @throws CannotInsertRecord
     * @throws Exception
     */
    protected function doExport($subject, $target): ExportResult
    {
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');

        $csv->insertOne($this->createHeaderRecord());

        foreach ($subject->getSalesChannelReportRecords() as $propositionSale) {
            $csv->insertOne($this->createSalesChannelCsvRecord($propositionSale));
        }

        $target->getFile()->write($csv->getContent(), true);

        return new SuccessExportResult();
    }

    private function createSalesChannelCsvRecord(SalesChannelReportRecord $record)
    {
        $customerIdentity = $this->customerIdentityService->getCustomerIdentityByIdmId(
            $record->getCustomer()->getIdmId()
        );
        $propositionInstance = $record->getPropositionInstance();
        $subscription = $record->getSubscription();

        $peCodeString = $this->createPeCodeString($propositionInstance);
        $instanceAmount = $this->calculateInstanceAmount($propositionInstance);
        $subscriptionTerms = $this->calculateSubscriptionTerms($subscription);

        $piConfiguration = $propositionInstance->getConfiguration();
        switch (true) {
            case $piConfiguration instanceof StarConfiguration:
                $starValue = $piConfiguration->getStarValue()->getValue();
                $zone = $piConfiguration->getZone()->getName();
                $fromStation = null;
                $toStation = null;
                break;
            case $piConfiguration instanceof RouteConfiguration:
                $starValue = null;
                $zone = null;
                $fromStation = $piConfiguration->getFromStation()->getCode();
                $toStation = $piConfiguration->getToStation()->getCode();
                break;
            default:
                $starValue = null;
                $zone = null;
                $fromStation = null;
                $toStation = null;
                break;
        }

        $order = $record->getOrder();
        $orderItem = $record->getOrderItem();

        $discountAmount = $order->getDiscountAmount() ? $order->getDiscountAmount()->getAmount() : null;
        $discountCode = $order->getDiscountCode() ? $order->getDiscountCode()->getCode() : null;

        $isRenewal = $subscription->getHasRenewed();

        return [
            'engravedId' => $record->getCard()->getEngravedId()->getEngravedIdString(),
            'customerNumber' => $customerIdentity->getAssistCustomerId(),
            'surname' => $customerIdentity->getSurname(),
            'paymentId' => $record->getPayment()->getId(),
            'orderId' => $order->getOrderNumber()->toString(),
            'createdAt' => $orderItem->getCreatedAt()->format('d-m-Y'),
            'peCode' => $peCodeString,
            'propositionName' => $record->getPropositionInstance()->getProposition()->getName(),
            'effectiveDate' => $orderItem->getEffectiveDate()->format('d-m-Y'),
            'instanceAmount' => $instanceAmount->getAmount(),
            'amountPaid' => $isRenewal ? null : $record->getPayment()->getAmountPaid()->getAmount(),
            'discountAmount' => $isRenewal ? null : $discountAmount,
            'discountCode' => $isRenewal ? null : $discountCode,
            'subscriptionTerms' => $subscriptionTerms,
            'costUnitCode' => $orderItem->getCostUnitCode() ? $orderItem->getCostUnitCode()->getValue() : null,
            'zone' => $zone,
            'starValue' => $starValue,
            'fromCode' => $fromStation,
            'toCode' => $toStation,
            'type' => $record instanceof PropositionRefund ? 'Propositie annulering' : 'Propositie verkoop',
        ];
    }

    private function createPeCodeString(PropositionInstance $propositionInstance): string
    {
        return substr(Iterator::create($propositionInstance->getPropositionElementInstances())
            ->reduce(function (string $peCodeString, PropositionElementInstance $instance) {
                return $peCodeString.$instance->getPeCode()->getPeCodeString().', ';
            }, ''), 0, -2);
    }

    private function calculateInstanceAmount(PropositionInstance $getPropositionInstance): Money
    {
        return Iterator::create($getPropositionInstance->getPropositionElementInstances())
            ->reduce(function (Money $totalAmount, PropositionElementInstance $instance) {
                return $totalAmount->add($instance->getInstanceAmount());
            }, Money::zero(Currency::EUR()));
    }

    private function calculateSubscriptionTerms(Subscription $subscription)
    {
        return $subscription->isPermanent()
            ? 'Doorlopend'
            : count($subscription->getValidityDateRange()->computeTerms($this->termDuration));
    }

    private function createHeaderRecord(): array
    {
        return [
            'OV_chipkaartnummer',
            'Klantnummer',
            'Achternaam',
            'Betaalreferentie_Icepay',
            'Bestelnummer',
            'Aangemaakt op',
            'PE_Code',
            'Propositie naam',
            'Ingangsdatum',
            'Bedrag',
            'Direct betaald bedrag',
            'Kortingsbedrag',
            'Kortingscode',
            'Betaaltermijnen',
            'KDR',
            'Zone',
            'Sterwaarde',
            'Van',
            'Naar',
            'Type',
        ];
    }
}