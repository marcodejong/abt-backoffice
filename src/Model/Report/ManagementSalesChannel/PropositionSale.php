<?php


namespace App\Model\Report\ManagementSalesChannel;


use Arriva\Abt\Entity\Card;
use Arriva\Abt\Entity\Customer;
use Arriva\Abt\Entity\Order;
use Arriva\Abt\Entity\Payment;
use Arriva\Abt\Entity\PropositionInstance;
use Arriva\Abt\Entity\PropositionOrderItem;
use Arriva\Abt\Entity\Subscription;

class PropositionSale implements SalesChannelReportRecord
{
    /** @var Subscription */
    private $subscription;

    /** @var Card */
    private $card;

    /** @var Customer */
    private $customer;

    /** @var PropositionInstance */
    private $propositionInstance;

    /** @var PropositionOrderItem */
    private $orderItem;

    /** @var Order */
    private $order;

    /** @var Payment */
    private $payment;

    public function __construct(
        Subscription $subscription,
        Card $card,
        Customer $customer,
        PropositionInstance $propositionInstance,
        PropositionOrderItem $orderItem,
        Order $order,
        Payment $payment
    ) {
        $this->subscription = $subscription;
        $this->card = $card;
        $this->customer = $customer;
        $this->propositionInstance = $propositionInstance;
        $this->orderItem = $orderItem;
        $this->order = $order;
        $this->payment = $payment;
    }

    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }

    public function getCard(): Card
    {
        return $this->card;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function getPropositionInstance(): PropositionInstance
    {
        return $this->propositionInstance;
    }

    public function getOrderItem(): PropositionOrderItem
    {
        return $this->orderItem;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }
}