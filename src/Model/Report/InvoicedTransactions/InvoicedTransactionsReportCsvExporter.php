<?php


namespace App\Model\Report\InvoicedTransactions;


use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\SimpleExporter;
use App\Model\Export\SingleTypeExporterTrait;
use App\Model\Export\Target\CsvFile;
use App\Service\CustomerIdService\CustomerIdentityService;
use App\Service\EncrypterMediaSerialIdService;
use Arriva\Abt\Entity\Invoice;
use League\Csv\Exception;
use League\Csv\Writer;
use SolidPhp\ValueObjects\Type\Type;

class InvoicedTransactionsReportCsvExporter extends SimpleExporter
{
    use SingleTypeExporterTrait;

    /** @var EncrypterMediaSerialIdService */
    private $encrypter;

    /** @var CustomerIdentityService */
    private $customerIdentityService;


    public function __construct(
        EncrypterMediaSerialIdService $encrypter,
        CustomerIdentityService $customerIdentityService
    ) {
        $this->setSupport(
            Type::of(InvoicedTransactionsReport::class),
            Type::of(CsvFile::class)
        );

        $this->encrypter = $encrypter;
        $this->customerIdentityService = $customerIdentityService;
    }

    /**
     * @param InvoicedTransactionsReport $subject
     * @param CsvFile $target
     *
     * @return ExportResult
     * @throws Exception
     */
    protected function doExport($subject, $target): ExportResult
    {
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');

        $csv->insertOne($this->createHeaderRecord());
        foreach ($subject->getInvoicedTransactions() as $invoicedTransaction) {
            $csv->insertOne($this->createInvoicedTransactionRecord($invoicedTransaction));
        }

        $target->getFile()->write($csv->getContent(), true);

        return new SuccessExportResult();
    }

    private function createHeaderRecord()
    {
        return [
            'Factuurnummer',
            'Factuurdatum',
            'Debiteurnummer',
            'MediaSerialNumberId',
            'CKIMsgReportDate',
            'CKOMsgReportDate',
            'ServiceProviderId',
            'PropElementCode',
            'FCETravelId',
            'CKIStationId',
            'CKOStationId',
            'CKOEntryStation',
            'ProductPassengerClass',
            'ConcessionId',
            'PropDirectorId',
            'PropRetailerId',
            'CKIApplicationTransactionSequenceNumber',
            'CKOApplicationTransactionSequenceNumber',
            'PropValueBeforeRating',
            'PropValueAfterRating',
            'CCHS_CKO_COException',
            'CCHS_CKO_CIException',
            'CCHS_CKO_COGoodForSettle',
            'CCHS_CKO_CIGoodForSettle',
            'CCHS_CKI_COException',
            'CCHS_CKI_CIException',
            'CCHS_CKI_COGoodForSettle',
            'CCHS_CKI_CIGoodForSettle',
            'CCHS_FCEException',
            'CCHS_FCEGoodForSettle',
        ];
    }

    private function createInvoicedTransactionRecord(InvoicedTransaction $invoicedTransaction)
    {
        $invoice = $invoicedTransaction->getInvoice();
        $transaction = $invoicedTransaction->getTransaction();

        return [
            'invoice_number' => $invoice->getInvoiceNumber() ? $invoice->getInvoiceNumber()->toString() : '',
            'invoice_date' => $invoice->getInvoiceDate() ? $invoice->getInvoiceDate()->format('dmY') : '',
            'debtor_number' => $this->getAssistCustomerId($invoice),
            'media_serial_number_id' => $this->encrypter->decryptMediaSerialId(
                $transaction->getEncryptedMediaSerialNumberId()
            ),
            'cki_msg_report_date' => $transaction->getCkiMsgReportDate(),
            'cko_msg_report_date' => $transaction->getCkoMsgReportDate(),
            'service_provider_id' => $transaction->getServiceProvider()
                ? $transaction->getServiceProvider()->getName()
                : '',
            'prop_element_code' => $transaction->getPropElementCode(),
            'fce_travel_id' => $transaction->getFceTravelId(),
            'cki_station_id' => $transaction->getCkiStationId(),
            'cko_station_id' => $transaction->getCkoStationId(),
            'cko_entry_station' => $transaction->getCkoEntryStation(),
            'product_passenger_class' => $transaction->getPassengerClass()
                ? $transaction->getPassengerClass()->getId()
                : '',
            'consession_id' => $transaction->getConcessionId(),
            'prop_director_id' => $transaction->getPropDirectorId(),
            'prop_retailer_id' => $transaction->getPropRetailerId(),
            'cki_application_transaction_sequence_number' => $transaction->getCkiApplicationTransactionSequenceNumber(),
            'cko_application_transaction_sequence_number' => $transaction->getCkoApplicationTransactionSequenceNumber(),
            'prop_value_before_rating' => $transaction->getPropValueBeforeRating(),
            'prop_value_after_rating' => $transaction->getPropValueAfterRating(),
            'cchs_cko_co_exception' => $transaction->getCchsCkoCoException(),
            'cchs_cko_ci_exception' => $transaction->getCchsCkoCiException(),
            'cchs_cko_co_good_for_settle' => $transaction->getCchsCkoCoGoodForSettle(),
            'cchs_cko_ci_good_for_settle' => $transaction->getCchsCkiCiGoodForSettle(),
            'cchs_cki_co_exception' => $transaction->getCchsCkiCoException(),
            'cchs_cki_ci_exception' => $transaction->getCchsCkiCiException(),
            'cchs_cki_co_good_for_settle' => $transaction->getCchsCkiCoGoodForSettle(),
            'cchs_cki_ci_good_for_settle' => $transaction->getCchsCkiCiGoodForSettle(),
            'cchs_fce_exception' => $transaction->getCchsFceException(),
            'cchs_fce_good_for_settle' => $transaction->getCchsFceGoodForSettle(),
        ];
    }

    private function getAssistCustomerId(Invoice $invoice): ?int
    {
        if (!$invoice->getCustomer()) {
            return null;
        }

        $idmId = $invoice->getCustomer()->getIdmId();

        return $this->customerIdentityService->getAssistIdByIdmId($idmId);
    }
}