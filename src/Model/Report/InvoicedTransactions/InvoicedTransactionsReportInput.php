<?php


namespace App\Model\Report\InvoicedTransactions;


use Arriva\Abt\Value\DateRange;

class InvoicedTransactionsReportInput
{
    /** @var DateRange */
    private $period;

    public function __construct(DateRange $period)
    {
        $this->period = $period;
    }

    public function getPeriod(): DateRange
    {
        return $this->period;
    }
}