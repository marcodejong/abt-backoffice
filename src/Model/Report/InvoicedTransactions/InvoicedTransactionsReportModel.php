<?php


namespace App\Model\Report\InvoicedTransactions;


use App\Repository\InvoicedTransactionRepository;
use Doctrine\Common\Collections\Criteria;

class InvoicedTransactionsReportModel
{
    /** @var InvoicedTransactionRepository */
    private $receivableRepository;

    public function __construct(InvoicedTransactionRepository $receivableRepository)
    {
        $this->receivableRepository = $receivableRepository;
    }

    public function generateInvoicedTransactionsReport(
        InvoicedTransactionsReportInput $reportInput
    ): InvoicedTransactionsReport {
        $report = new InvoicedTransactionsReport();

        foreach ($this->generateInvoicedTransactions($reportInput) as $invoicedTransaction) {
            $report->addInvoicedTransaction($invoicedTransaction);
        }

        return $report;
    }

    /**
     * @param InvoicedTransactionsReportInput $input
     * @return InvoicedTransaction[]|iterable
     */
    private function generateInvoicedTransactions(InvoicedTransactionsReportInput $input): iterable
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->gte('i.invoiceDate', $input->getPeriod()->getStartDate()->getStartOfDay()))
            ->andWhere(Criteria::expr()->lte('i.invoiceDate', $input->getPeriod()->getEndDate()->getEndOfDay()));

        return $this->receivableRepository->findInvoicedTransactionReceivables($criteria);
    }
}