<?php


namespace App\Model\Report\InvoicedTransactions;


use Arriva\Abt\Entity\Invoice;
use Arriva\Abt\Entity\Transaction\TlsTransactionRecord;

class InvoicedTransaction
{
    /** @var Invoice */
    private $invoice;

    /** @var TlsTransactionRecord */
    private $transaction;

    public function __construct(Invoice $invoice, TlsTransactionRecord $transaction)
    {
        $this->invoice = $invoice;
        $this->transaction = $transaction;
    }

    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    public function getTransaction(): TlsTransactionRecord
    {
        return $this->transaction;
    }
}