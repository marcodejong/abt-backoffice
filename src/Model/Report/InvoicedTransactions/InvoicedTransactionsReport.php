<?php


namespace App\Model\Report\InvoicedTransactions;


use App\Model\Report\Report;

class InvoicedTransactionsReport implements Report
{
    /** @var InvoicedTransaction[] */
    private $invoicedTransactions;

    public function __construct()
    {
        $this->invoicedTransactions = [];
    }

    /**
     * @return InvoicedTransaction[]
     */
    public function getInvoicedTransactions(): array
    {
        return $this->invoicedTransactions;
    }

    public function addInvoicedTransaction(InvoicedTransaction $invoicedTransaction): void
    {
        $this->invoicedTransactions[] = $invoicedTransaction;
    }
}