<?php


namespace App\Model\Report\TariffConfiguration;


use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\SimpleExporter;
use App\Model\Export\SingleTypeExporterTrait;
use App\Model\Export\Target\CsvFile;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\Utility\Iterator;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SolidPhp\ValueObjects\Type\Type;

class PeTariffConfigurationReportCsvExporter extends SimpleExporter
{
    use SingleTypeExporterTrait;

    public function __construct()
    {
        $this->setSupport(
            Type::of(PeTariffConfigurationReport::class),
            Type::of(CsvFile::class)
        );
    }

    /**
     * @param PeTariffConfigurationReport $subject
     * @param CsvFile $target
     *
     * @return ExportResult
     * @throws CannotInsertRecord
     * @throws Exception
     */
    protected function doExport($subject, $target): ExportResult
    {
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');

        foreach ($subject->getPeTariffConfigurations() as $peTariffConfiguration) {
            $csv->insertOne(
                $this->createPeTariffConfigurationRecord($peTariffConfiguration)
            );
        }

        $target->getFile()->write($csv->getContent(), true);

        return new SuccessExportResult();
    }

    private function createPeTariffConfigurationRecord(PeTariffConfiguration $peTariffConfiguration): array
    {
        switch (true) {
            case $peTariffConfiguration instanceof StarPeTariffConfiguration:
                return $this->createStarPeTariffConfigurationRecord($peTariffConfiguration);
            case $peTariffConfiguration instanceof RoutePeTariffConfiguration:
                return $this->createRoutePeTariffConfigurationRecord($peTariffConfiguration);
            case $peTariffConfiguration instanceof RegularPeTariffConfiguration:
                return $this->createRegularPeTariffConfigurationRecord($peTariffConfiguration);
            default:
                throw new \DomainException(sprintf(
                    'Invalid tariffConfiguration %s', get_class($peTariffConfiguration)
                ));
        }
    }

    private function createStarPeTariffConfigurationRecord(StarPeTariffConfiguration $peTariffConfiguration): array
    {
        $propositionElement = $peTariffConfiguration->getPropositionElement();

        return [
            'propositionElement' => $propositionElement->getName(),
            'peCode' => $propositionElement->getCode() ? $propositionElement->getCode()->getPeCodeString() : null,
            'starValue' => $peTariffConfiguration->getStarValue()->getValue(),
            'pricePeriod' => $peTariffConfiguration->getPricePeriod()->getName(),
            'amount' => $peTariffConfiguration->getAmount()->getAmount(),
            'propositions' => $this->createPropositionNameString($peTariffConfiguration->getPropositions()),
            'propositionGroups' => $this->createPropositionGroupNameString(
                $peTariffConfiguration->getPropositionGroups()
            )
        ];
    }

    private function createRoutePeTariffConfigurationRecord(RoutePeTariffConfiguration $peTariffConfiguration): array
    {
        $propositionElement = $peTariffConfiguration->getPropositionElement();
        $fromStation = $peTariffConfiguration->getFromStation();
        $toStation = $peTariffConfiguration->getToStation();

        return [
            'propositionElement' => $propositionElement->getName(),
            'peCode' => $propositionElement->getCode() ? $propositionElement->getCode()->getPeCodeString() : null,
            'fromStationCode' => $fromStation->getCode(),
            'fromStationName' => $fromStation->getName(),
            'toStationCode' => $toStation->getCode(),
            'toStationName' => $toStation->getName(),
            'pricePeriod' => $peTariffConfiguration->getPricePeriod()->getName(),
            'amount' => $peTariffConfiguration->getAmount()->getAmount(),
            'propositions' => $this->createPropositionNameString($peTariffConfiguration->getPropositions()),
            'propositionGroups' => $this->createPropositionGroupNameString(
                $peTariffConfiguration->getPropositionGroups()
            )
        ];
    }

    private function createRegularPeTariffConfigurationRecord(
        RegularPeTariffConfiguration $peTariffConfiguration
    ): array {
        $propositionElement = $peTariffConfiguration->getPropositionElement();

        return [
            'propositionElement' => $propositionElement->getName(),
            'peCode' => $propositionElement->getCode() ? $propositionElement->getCode()->getPeCodeString() : null,
            'pricePeriod' => $peTariffConfiguration->getPricePeriod()->getName(),
            'amount' => $peTariffConfiguration->getAmount()->getAmount(),
            'propositions' => $this->createPropositionNameString($peTariffConfiguration->getPropositions()),
            'propositionGroups' => $this->createPropositionGroupNameString(
                $peTariffConfiguration->getPropositionGroups()
            )
        ];
    }

    /**
     * @param Proposition[] $propositions
     * @return string
     */
    private function createPropositionNameString(array $propositions): string
    {
        return substr(Iterator::create($propositions)->reduce(
            static function (string $result, Proposition $proposition) {
                return $result . $proposition->getName() . ', ';
            }, ''), 0, -2);
    }

    /**
     * @param array $propositionGroups
     * @return string
     */
    private function createPropositionGroupNameString(array $propositionGroups): string
    {
        return substr(Iterator::create($propositionGroups)->reduce(
            static function (string $result, PropositionGroup $propositionGroup) {
                return $result . $propositionGroup->getName() . ', ';
            }, ''), 0, -2);
    }
}