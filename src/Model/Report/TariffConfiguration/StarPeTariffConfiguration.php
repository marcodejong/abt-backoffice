<?php


namespace App\Model\Report\TariffConfiguration;


use Arriva\Abt\Entity\PricePeriod;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Entity\StarValue;
use Arriva\Abt\Utility\Money;

class StarPeTariffConfiguration extends PeTariffConfiguration
{
    /** @var StarValue */
    private $starValue;

    public function __construct(
        PropositionElement $propositionElement,
        PricePeriod $pricePeriod,
        Money $amount,
        array $propositions,
        array $propositionGroups,
        StarValue $starValue
    ) {
        parent::__construct(
            $propositionElement,
            $pricePeriod,
            $amount,
            $propositions,
            $propositionGroups
        );

        $this->starValue = $starValue;
    }

    public function getStarValue(): StarValue
    {
        return $this->starValue;
    }
}