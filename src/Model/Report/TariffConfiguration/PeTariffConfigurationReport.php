<?php


namespace App\Model\Report\TariffConfiguration;


use App\Model\Report\Report;

class PeTariffConfigurationReport implements Report
{
    /** @var PeTariffConfiguration[] */
    private $peTariffConfigurations;

    public function __construct()
    {
        $this->peTariffConfigurations = [];
    }

    /**
     * @return PeTariffConfiguration[]
     */
    public function getPeTariffConfigurations(): array
    {
        return $this->peTariffConfigurations;
    }

    public function addPeTariffConfiguration(PeTariffConfiguration $configuration): void
    {
        $this->peTariffConfigurations[] = $configuration;
    }
}