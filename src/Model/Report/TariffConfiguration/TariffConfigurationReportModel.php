<?php


namespace App\Model\Report\TariffConfiguration;


use App\Model\Report\Report;
use App\Repository\PropositionElementTariffConfigurationRepository;
use App\Repository\PropositionTariffConfigurationRepository;
use Arriva\Abt\Entity\PricePeriod;

class TariffConfigurationReportModel
{
    /** @var PropositionTariffConfigurationRepository */
    private $propositionTariffConfigurationRepository;

    /** @var PropositionElementTariffConfigurationRepository */
    private $propositionElementTariffConfigurationRepository;

    public function __construct(
        PropositionTariffConfigurationRepository $propositionTariffConfigurationRepository,
        PropositionElementTariffConfigurationRepository $propositionElementTariffConfigurationRepository
    ) {
        $this->propositionTariffConfigurationRepository = $propositionTariffConfigurationRepository;
        $this->propositionElementTariffConfigurationRepository = $propositionElementTariffConfigurationRepository;
    }

    public function generateTariffConfigurationReportModel(
        TariffConfigurationType $configurationType,
        PricePeriod $pricePeriod = null
    ): Report {
        switch ($configurationType) {
            case TariffConfigurationType::PROPOSITION_EXPORT():
                return $this->generatePropositionTariffConfigurationReport();
            case TariffConfigurationType::REGULAR_TARIFFS():
                return $this->generateRegularPropositionElementTariffConfigurationReport($pricePeriod);
            case TariffConfigurationType::STAR_TARIFFS():
                return $this->generateStarPropositionElementTariffConfigurationReport($pricePeriod);
            case TariffConfigurationType::ROUTE_TARIFFS():
                return $this->generateRoutePropositionElementTariffConfigurationReport($pricePeriod);
            default:
                throw new \DomainException(sprintf('invalid TariffConfigurationType: %s', $configurationType->getId()));
        }
    }

    private function generatePropositionTariffConfigurationReport(): PropositionTariffConfigurationReport {
        $report = new PropositionTariffConfigurationReport();
        $propositionsTariffConfigurations = $this->propositionTariffConfigurationRepository
            ->findPropositionTariffConfigurations();

        foreach ($propositionsTariffConfigurations as $proposition) {
            $report->addPropositionTariffConfiguration($proposition);
        }

        return $report;
    }

    private function generateRegularPropositionElementTariffConfigurationReport(
        PricePeriod $pricePeriod
    ): PeTariffConfigurationReport {
        $report = new PeTariffConfigurationReport();
        $propositionElementTariffConfigurations = $this->propositionElementTariffConfigurationRepository
            ->findRegularPropositionElementTariffConfigurationsForPricePeriod($pricePeriod);

        foreach ($propositionElementTariffConfigurations as $propositionElementTariffConfiguration) {
            $report->addPeTariffConfiguration($propositionElementTariffConfiguration);
        }

        return $report;
    }

    private function generateStarPropositionElementTariffConfigurationReport(
        PricePeriod $pricePeriod
    ): PeTariffConfigurationReport {
        $report = new PeTariffConfigurationReport();
        $propositionElementTariffConfigurations = $this->propositionElementTariffConfigurationRepository
            ->findStarPropositionElementTariffConfigurationsForPricePeriod($pricePeriod);

        foreach ($propositionElementTariffConfigurations as $propositionElementTariffConfiguration) {
            $report->addPeTariffConfiguration($propositionElementTariffConfiguration);
        }

        return $report;
    }

    private function generateRoutePropositionElementTariffConfigurationReport(
        PricePeriod $pricePeriod
    ): PeTariffConfigurationReport {
        $report = new PeTariffConfigurationReport();
        $propositionElementTariffConfigurations = $this->propositionElementTariffConfigurationRepository
            ->findRoutePropositionElementTariffConfigurationsForPricePeriod($pricePeriod);

        foreach ($propositionElementTariffConfigurations as $propositionElementTariffConfiguration) {
            $report->addPeTariffConfiguration($propositionElementTariffConfiguration);
        }

        return $report;
    }
}