<?php


namespace App\Model\Report\TariffConfiguration;


use Arriva\Abt\Entity\Configuration\PropositionTerm\AbstractPropositionTerm;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\Entity\SalePeriod;
use Arriva\Abt\Entity\ValidityPeriod;
use Arriva\Abt\Value\TAccount;

class PropositionTariffConfiguration
{
    /** @var Proposition */
    private $proposition;

    /** @var PropositionGroup */
    private $propositionGroup;

    /** @var ValidityPeriod */
    private $validityPeriod;

    /** @var SalePeriod */
    private $salePeriod;

    /** @var AbstractPropositionTerm */
    private $propositionTerm;

    /** @var PropositionElement[] */
    private $propositionElements;

    public function __construct(
        Proposition $proposition,
        PropositionGroup $propositionGroup,
        ValidityPeriod $validityPeriod,
        SalePeriod $salePeriod,
        AbstractPropositionTerm $propositionTerm,
        array $propositionElements
    ) {
        $this->proposition = $proposition;
        $this->propositionGroup = $propositionGroup;
        $this->validityPeriod = $validityPeriod;
        $this->salePeriod = $salePeriod;
        $this->propositionTerm = $propositionTerm;
        $this->propositionElements = $propositionElements;
    }

    public function getProposition(): Proposition
    {
        return $this->proposition;
    }

    public function getPropositionGroup(): PropositionGroup
    {
        return $this->propositionGroup;
    }

    public function getValidityPeriod(): ValidityPeriod
    {
        return $this->validityPeriod;
    }

    public function getSalePeriod(): SalePeriod
    {
        return $this->salePeriod;
    }

    public function getPropositionTerm(): AbstractPropositionTerm
    {
        return $this->propositionTerm;
    }

    /**
     * @return PropositionElement[]
     */
    public function getPropositionElements(): array
    {
        return $this->propositionElements;
    }
}