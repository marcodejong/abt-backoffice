<?php


namespace App\Model\Report\TariffConfiguration;


use Arriva\Abt\Utility\Enum;

class TariffConfigurationType extends Enum
{
    const PROPOSITION_EXPORT = 'propositions';
    const REGULAR_TARIFFS = 'regular-tariffs';
    const ROUTE_TARIFFS = 'route-tariffs';
    const STAR_TARIFFS = 'star-tariffs';

    public static function PROPOSITION_EXPORT(): self
    {
        return self::define(self::PROPOSITION_EXPORT);
    }

    public static function REGULAR_TARIFFS(): self
    {
        return self::define(self::REGULAR_TARIFFS);
    }

    public static function ROUTE_TARIFFS(): self
    {
        return self::define(self::ROUTE_TARIFFS);
    }

    public static function STAR_TARIFFS(): self
    {
        return self::define(self::STAR_TARIFFS);
    }
}