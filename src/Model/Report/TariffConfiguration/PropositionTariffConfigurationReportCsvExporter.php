<?php


namespace App\Model\Report\TariffConfiguration;


use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\SimpleExporter;
use App\Model\Export\SingleTypeExporterTrait;
use App\Model\Export\Target\CsvFile;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Utility\Iterator;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SolidPhp\ValueObjects\Type\Type;

class PropositionTariffConfigurationReportCsvExporter extends SimpleExporter
{
    use SingleTypeExporterTrait;

    public function __construct()
    {
        $this->setSupport(
            Type::of(PropositionTariffConfigurationReport::class),
            Type::of(CsvFile::class)
        );
    }

    /**
     * @param PropositionTariffConfigurationReport $subject
     * @param CsvFile $target
     *
     * @return ExportResult
     * @throws CannotInsertRecord
     * @throws Exception
     */
    protected function doExport($subject, $target): ExportResult
    {
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');

        foreach ($subject->getPropositionTariffConfigurations() as $propositionTariffConfiguration) {
            $csv->insertOne(
                $this->createPropositionTariffConfigurationRecord($propositionTariffConfiguration)
            );
        }

        $target->getFile()->write($csv->getContent(), true);

        return new SuccessExportResult();
    }

    private function createPropositionTariffConfigurationRecord(
        PropositionTariffConfiguration $propositionTariffConfiguration
    ): array {
        $proposition = $propositionTariffConfiguration->getProposition();

        return [
            'name' => $proposition->getName(),
            'propositionGroup' => $propositionTariffConfiguration->getPropositionGroup()->getName(),
            'validityPeriod' => $propositionTariffConfiguration->getValidityPeriod()->getName(),
            'salePeriod' => $propositionTariffConfiguration->getSalePeriod()->getName(),
            'propositionTerm' => $propositionTariffConfiguration->getPropositionTerm()->getName(),
            'tAccount' => $proposition->getValueTAccount() ? $proposition->getValueTAccount()->getValue() : null,
            'refund' => $this->createRefundValue($proposition),
            'propositionElements' => $this->createPropositionElementsString(
                $propositionTariffConfiguration->getPropositionElements()
            )
        ];
    }

    private function createRefundValue(Proposition $proposition): ?string
    {
        if ($proposition->getRefundPeriod() && $proposition->getRefundPeriodValue()) {
            return sprintf('%s %s', $proposition->getRefundPeriodValue(), $proposition->getRefundPeriod());
        }

        return null;
    }

    /**
     * @param PropositionElement[] $propositionElements
     * @return string|null
     */
    private function createPropositionElementsString(array $propositionElements): ?string
    {
        if (empty($propositionElements)) {
            return null;
        }

        return substr(Iterator::create($propositionElements)->reduce(
            static function (string $result, PropositionElement $propositionElement) {
                return $propositionElement->getCode()
                    ? $result . $propositionElement->getCode() . ', '
                    : $result;
            }, ''), 0, -2);
    }

}