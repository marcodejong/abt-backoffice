<?php


namespace App\Model\Report\TariffConfiguration;


use Arriva\Abt\Entity\PricePeriod;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\Entity\Station;
use Arriva\Abt\Utility\Money;

class RoutePeTariffConfiguration extends PeTariffConfiguration
{
    /** @var Station */
    private $fromStation;

    /** @var Station */
    private $toStation;

    /**
     * RoutePeTariffConfiguration constructor.
     * @param PropositionElement $propositionElement
     * @param PricePeriod $pricePeriod
     * @param Money $amount
     * @param Proposition[] $propositions
     * @param PropositionGroup[] $propositionGroups
     * @param Station $fromStation
     * @param Station $toStation
     */
    public function __construct(
        PropositionElement $propositionElement,
        PricePeriod $pricePeriod,
        Money $amount,
        array $propositions,
        array $propositionGroups,
        Station $fromStation,
        Station $toStation
    ) {
        parent::__construct(
            $propositionElement,
            $pricePeriod,
            $amount,
            $propositions,
            $propositionGroups
        );

        $this->fromStation = $fromStation;
        $this->toStation = $toStation;
    }

    public function getFromStation(): Station
    {
        return $this->fromStation;
    }

    public function getToStation(): Station
    {
        return $this->toStation;
    }
}