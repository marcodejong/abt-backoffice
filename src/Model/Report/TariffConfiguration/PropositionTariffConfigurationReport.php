<?php


namespace App\Model\Report\TariffConfiguration;


use App\Model\Report\Report;

class PropositionTariffConfigurationReport implements Report
{
    /** @var PropositionTariffConfiguration[] */
    private $propositionTariffConfigurations;

    public function __construct()
    {
        $this->propositionTariffConfigurations = [];
    }

    /**
     * @return PropositionTariffConfiguration[]
     */
    public function getPropositionTariffConfigurations(): array
    {
        return $this->propositionTariffConfigurations;
    }

    public function addPropositionTariffConfiguration(PropositionTariffConfiguration $configuration): void
    {
        $this->propositionTariffConfigurations[] = $configuration;
    }
}