<?php


namespace App\Model\Report\TariffConfiguration;


use Arriva\Abt\Entity\PricePeriod;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\Utility\Money;

abstract class PeTariffConfiguration
{
    /** @var PropositionElement */
    private $propositionElement;

    /** @var PricePeriod */
    private $pricePeriod;

    /** @var Money */
    private $amount;

    /** @var Proposition[] */
    private $propositions;

    /** @var PropositionGroup[] */
    private $propositionGroups;

    /**
     * RegularPeTariffConfiguration constructor.
     * @param PropositionElement $propositionElement
     * @param PricePeriod $pricePeriod
     * @param Money $amount
     * @param Proposition[] $propositions
     * @param PropositionGroup[] $propositionGroups
     */
    public function __construct(
        PropositionElement $propositionElement,
        PricePeriod $pricePeriod,
        Money $amount,
        array $propositions,
        array $propositionGroups
    ) {
        $this->propositionElement = $propositionElement;
        $this->pricePeriod = $pricePeriod;
        $this->amount = $amount;
        $this->propositions = $propositions;
        $this->propositionGroups = $propositionGroups;
    }

    public function getPropositionElement(): PropositionElement
    {
        return $this->propositionElement;
    }

    public function getPricePeriod(): PricePeriod
    {
        return $this->pricePeriod;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return Proposition[]
     */
    public function getPropositions(): array
    {
        return $this->propositions;
    }

    /**
     * @return PropositionGroup[]
     */
    public function getPropositionGroups(): array
    {
        return $this->propositionGroups;
    }
}