<?php


namespace App\Model\Report\JournalTransactions;


use Arriva\Abt\Value\DateRange;

class JournalTransactionsInput
{
    /** @var DateRange */
    private $period;

    public function __construct(DateRange $period)
    {
        $this->period = $period;
    }

    public function getPeriod(): DateRange
    {
        return $this->period;
    }
}