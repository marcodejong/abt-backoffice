<?php


namespace App\Model\Report\JournalTransactions;


use App\Model\Report\Journal\Summary\MoneysReceivableSummary;
use App\Model\Report\Journal\Summary\RefundSummary;
use App\Model\Report\Journal\Summary\SalesSummary;
use App\Repository\Financial\DoctrineInvoicedMoneysReceivableSummaryRepository;
use App\Repository\Financial\DoctrineInvoicedRefundSummaryRepository;
use App\Repository\Financial\DoctrineInvoicedSalesSummaryRepository;
use App\Repository\Financial\FinancialSummaryCriteria;

class JournalTransactionsModel
{
    /** @var DoctrineInvoicedSalesSummaryRepository */
    private $salesSummaryRepository;

    /** @var DoctrineInvoicedRefundSummaryRepository */
    private $refundSummaryRepository;

    /** @var DoctrineInvoicedMoneysReceivableSummaryRepository */
    private $moneysReceivableSummaryRepository;

    public function __construct(
        DoctrineInvoicedSalesSummaryRepository $salesSummaryRepository,
        DoctrineInvoicedRefundSummaryRepository $refundSummaryRepository,
        DoctrineInvoicedMoneysReceivableSummaryRepository $moneysReceivableSummaryRepository
    ) {
        $this->salesSummaryRepository = $salesSummaryRepository;
        $this->refundSummaryRepository = $refundSummaryRepository;
        $this->moneysReceivableSummaryRepository = $moneysReceivableSummaryRepository;
    }

    public function generateJournalTransactionsReport(JournalTransactionsInput $reportInput): JournalTransactionsReport
    {
        $report = new JournalTransactionsReport($reportInput->getPeriod());

        foreach ($this->generateSalesSummaries($reportInput) as $salesSummary) {
            $report->addTransactionSummary($salesSummary);
        }

        foreach ($this->generateRefundSummaries($reportInput) as $refundSummary) {
            $report->addTransactionSummary($refundSummary);
        }

        foreach($this->generatePaymentSummaries($reportInput) as $paymentSummary) {
            $report->addTransactionSummary($paymentSummary);
        }

        return $report;
    }

    /**
     * @param JournalTransactionsInput $input
     * @return iterable<SalesSummary>
     */
    private function generateSalesSummaries(JournalTransactionsInput $input): iterable
    {
        return $this->salesSummaryRepository->findSalesSummariesPerCostUnit(
            new FinancialSummaryCriteria($input->getPeriod())
        );
    }

    /**
     * @param JournalTransactionsInput $input
     * @return iterable<RefundSummary>
     */
    private function generateRefundSummaries(JournalTransactionsInput $input): iterable
    {
        return $this->refundSummaryRepository->findRefundSummariesPerCostUnit(
            new FinancialSummaryCriteria($input->getPeriod())
        );
    }

    /**
     * @param JournalTransactionsInput $input
     * @return iterable<MoneysReceivableSummary>
     */
    private function generatePaymentSummaries(JournalTransactionsInput $input): iterable
    {
        return $this->moneysReceivableSummaryRepository->getMoneysReceivableSummaries(
            new FinancialSummaryCriteria($input->getPeriod())
        );
    }
}