<?php


namespace App\Model\Report\JournalTransactions;


use App\Model\Report\Journal\Summary\TransactionSummary;
use App\Model\Report\Report;
use Arriva\Abt\Value\Time\DateRangeInterface;

class JournalTransactionsReport implements Report
{
    /** @var DateRangeInterface */
    private $period;

    /** @var TransactionSummary[] */
    private $transactionSummaries;

    public function __construct(DateRangeInterface $period)
    {
        $this->period = $period;
        $this->transactionSummaries = [];
    }

    public function addTransactionSummary(TransactionSummary $transactionSummary) {
        $this->transactionSummaries[] = $transactionSummary;
    }

    /**
     * @return TransactionSummary[]
     */
    public function getTransactionSummaries(): array
    {
        return $this->transactionSummaries;
    }

    public function getPeriod(): DateRangeInterface
    {
        return $this->period;
    }
}