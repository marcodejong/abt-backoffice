<?php


namespace App\Model\Report\JournalTransactions;


use App\Exception\UnsupportedSubtypeException;
use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\SimpleExporter;
use App\Model\Export\SingleTypeExporterTrait;
use App\Model\Export\Target\CsvFile;
use App\Model\Report\Journal\Summary\MoneysReceivableSummary;
use App\Model\Report\Journal\Summary\RefundSummary;
use App\Model\Report\Journal\Summary\SalesSummary;
use App\Model\Report\Journal\Summary\TransactionSummary;
use Arriva\Abt\Utility\Money;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SolidPhp\ValueObjects\Type\Type;

class JournalTransactionsCsvExporter extends SimpleExporter
{
    use SingleTypeExporterTrait;

    public function __construct()
    {
        $this->setSupport(
            Type::of(JournalTransactionsReport::class),
            Type::of(CsvFile::class)
        );
    }

    /**
     * @param JournalTransactionsReport $subject
     * @param CsvFile $target
     *
     * @return ExportResult
     * @throws CannotInsertRecord
     * @throws Exception
     */
    protected function doExport($subject, $target): ExportResult
    {
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');

        $lineNr = 0;
        $csv->insertOne(['lineNr' => $lineNr++] + $this->createHeaderRecord($subject));

        foreach ($subject->getTransactionSummaries() as $transactionSummary) {
            $csv->insertOne(
                ['lineNr' => $lineNr++] + $this->createTransactionSummaryRecord($subject, $transactionSummary)
            );
        }

        $target->getFile()->write($csv->getContent(), true);

        return new SuccessExportResult();
    }

    private function createHeaderRecord(JournalTransactionsReport $report): array
    {
        return [
            'department' => '',
            'year' => $report->getPeriod()->getStartDate()->getYearObject()->getNumber(),
            'month' => $report->getPeriod()->getStartDate()->getMonthObject()->getNumber(),
            'journal' => '',
            'date' => '',
            'description' => 'ABT ritten',
            'account' => '',
            'amount_including_vat' => '',
            'vat_code' => '',
            'vat_amount' => '',
            'cost_center' => '',
            'cost_unit_code' => '',
            'nr_of_transactions' => '',
        ];
    }

    private function createTransactionSummaryRecord(
        JournalTransactionsReport $report,
        TransactionSummary $summary
    ): array {
        switch (true) {
            case $summary instanceof SalesSummary:
                return $this->createSalesSummaryRecord($report, $summary);
            case $summary instanceof RefundSummary:
                return $this->createRefundSummaryRecord($report, $summary);
            case $summary instanceof MoneysReceivableSummary:
                return $this->createMoneysReceivableSummaryRecord($report, $summary);
            default:
                throw new UnsupportedSubtypeException(Type::of(TransactionSummary::class), $summary);
        }
    }

    private function createSalesSummaryRecord(JournalTransactionsReport $report, SalesSummary $summary): array
    {
        $date = $report->getPeriod()->getStartDate();

        return [
            'department' => 'AB',
            'year' => $date->getYearObject()->getNumber(),
            'month' => $date->getMonthObject()->getNumber(),
            'journal' => '',
            'date' => $date->format('d-m-Y'),
            'description' => '550 551 ABT ritten',
            'account' => $summary->getTAccount()->getValue(),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()->multiply(-1)),
            'vat_code' => $summary->getVatCode()->getValue(),
            'vat_amount' => $this->formatMoney($summary->getAmount()->getVatAmount()),
            'cost_center' => $summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '',
            'cost_unit_code' => $summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '',
            'nr_of_transactions' => $summary->getNrOfTransactions(),
        ];
    }

    private function createRefundSummaryRecord(JournalTransactionsReport $report, RefundSummary $summary): array
    {
        $date = $report->getPeriod()->getStartDate();

        return [
            'department' => 'AB',
            'year' => $date->getYearObject()->getNumber(),
            'month' => $date->getMonthObject()->getNumber(),
            'journal' => '',
            'date' => $date->format('d-m-Y'),
            'description' => '550 551 ABT ritten',
            'account' => $summary->getTAccount()->getValue(),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()),
            'vat_code' => $summary->getVatCode()->getValue(),
            'vat_amount' => $this->formatMoney($summary->getAmount()->getVatAmount()->multiply(-1)),
            'cost_center' => $summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '',
            'cost_unit_code' => $summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '',
            'nr_of_transactions' => $summary->getNrOfTransactions(),
        ];
    }

    private function createMoneysReceivableSummaryRecord(
        JournalTransactionsReport $report,
        MoneysReceivableSummary $summary
    ): array {
        $date = $report->getPeriod()->getStartDate();

        return [
            'department' => 'AB',
            'year' => $date->getYearObject()->getNumber(),
            'month' => $date->getMonthObject()->getNumber(),
            'journal' => '',
            'date' => $date->format('d-m-Y'),
            'description' => sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')),
            'account' => $summary->getTAccount()->getValue(),
            'amount_including_vat' => $this->formatMoney($summary->getAmount()->getAmountWithVat()),
            'vat_code' => '',
            'vat_amount' => $this->formatMoney($summary->getAmount()->getVatAmount()),
            'cost_center' => '',
            'cost_unit_code' => '',
            'nr_of_transactions' => $summary->getNrOfTransactions(),
        ];
    }

    private function formatMoney(Money $money): string
    {
        return number_format(
            (float)$money->getAmount(),
            2,
            ',',
            ''
        );
    }
}