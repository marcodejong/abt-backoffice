<?php

namespace App\Model\Export;

use App\Model\Export\Result\ExportResult;
use SolidPhp\ValueObjects\Type\Type;

/**
 * Interface Exporter
 *
 * This interface specifies that the implementing class can export
 * some class of subjects to some class of targets.
 *
 * A subject can be of any php type; an object, array, resource, scalar, whatever.
 * A target must be defined as an ExportTarget.
 *
 * What a subject and a target mean is defined by the exporters that can handle them;
 * a target might be a string, or a file put on an external FTP server, or a call
 * to a webhook.
 */
interface Exporter
{
    /**
     * @return Type[]|iterable
     */
    public function getSupportedSubjectTypes(): array;

    public function supportsSubject($subject): bool;

    /**
     * @param Type $subjectType
     *
     * @return Type[]|array|iterable
     */
    public function getSupportedTargetTypes(Type $subjectType): array;

    public function supportsExport($subject, ExportTarget $target): bool;

    public function export($subject, ExportTarget $target): ExportResult;
}
