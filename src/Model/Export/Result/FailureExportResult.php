<?php

namespace App\Model\Export\Result;

class FailureExportResult implements ExportResult
{
    /** @var string */
    private $reason;

    /**
     * @param string $reason
     */
    public function __construct(string $reason)
    {
        $this->reason = $reason;
    }

    public function getReason(): string
    {
        return $this->reason;
    }
}
