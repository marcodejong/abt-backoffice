<?php

namespace App\Model\Export;

use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\FailureExportResult;
use App\Model\Export\Result\SuccessExportResult;
use App\Model\Export\Target\FileEmail;
use App\Service\Mailer\Data\Attachment;
use App\Service\Mailer\Data\Builder\MailBuilder;
use App\Service\Mailer\Data\Mail;
use App\Service\Mailer\MailerService;
use App\Service\Mailer\Result\SendMail\DeliveryFailedResult;
use App\Service\Mailer\Result\SendMail\OkResult;
use SolidPhp\ValueObjects\Type\Type;

class FileEmailExporter implements Exporter
{
    /** @var Exporter */
    private $fileTargetExporter;

    /** @var MailerService */
    private $mailer;

    /**
     * @param Exporter      $fileTargetExporter
     * @param MailerService $mailer
     */
    public function __construct(Exporter $fileTargetExporter, MailerService $mailer)
    {
        $this->fileTargetExporter = $fileTargetExporter;
        $this->mailer = $mailer;
    }

    /**
     * @return Type[]|iterable
     */
    public function getSupportedSubjectTypes(): array
    {
        return $this->fileTargetExporter->getSupportedSubjectTypes();
    }

    public function supportsSubject($subject): bool
    {
        return $this->fileTargetExporter->supportsSubject($subject);
    }

    /**
     * @param Type $subjectType
     *
     * @return Type[]|array|iterable
     */
    public function getSupportedTargetTypes(Type $subjectType): array
    {
        return [Type::of(FileEmail::class)];
    }

    public function supportsExport($subject, ExportTarget $target): bool
    {
        if (! $target instanceof FileEmail) {
            return false;
        }

        foreach ($target->getFileTargets() as $fileTarget) {
            if (!$this->fileTargetExporter->supportsExport($subject, $fileTarget)) {
                return false;
            }
        }

        return true;
    }

    public function export($subject, ExportTarget $target): ExportResult
    {
        if(!$this->supportsExport($subject, $target)) {
            return new FailureExportResult('Export not supported');
        }

        /** @var FileEmail $target */
        /** @var FileExport[] $fileExports */
        $fileExports = array_map(
            function (FileExportTarget $target) use ($subject) {
                return new FileExport($target,$this->fileTargetExporter->export($subject, $target));
            },
            $target->getFileTargets()
        );

        foreach ($fileExports as $fileExport) {
            if ($fileExport->result instanceof FailureExportResult) {
                return new FailureExportResult(sprintf('File %s could not be exported: %s', $fileExport->target, $fileExport->result->getReason()));
            }
        }

        $mail = $this->buildMail($target);

        $sendMailResult = $this->mailer->sendMail($mail);

        foreach ($fileExports as $fileExport) {
            $fileExport->target->getFile()->delete();
        }

        if ($sendMailResult instanceof OkResult) {
            return new SuccessExportResult();
        }

        if ($sendMailResult instanceof DeliveryFailedResult) {
            return new FailureExportResult('Mail delivery failed');
        }

        return new FailureExportResult(sprintf('Unknown mail send result type: %s', get_class($sendMailResult)));
    }

    private function buildMail(FileEmail $target): Mail
    {
        $mailBuilder = new MailBuilder();
        $mailBuilder->setSender($target->getSender());
        $mailBuilder->setReceiver($target->getReceiver());
        $mailBuilder->setSubject($target->getSubject());
        $mailBuilder->setBody($target->getBody());

        foreach ($target->getFileTargets() as $fileTarget) {
            $file = $fileTarget->getFile();
            $mailBuilder->addAttachment(new Attachment($file->getPath(), $file->getContentType()));
        }

        return $mailBuilder->getMail();
    }
}

/** @internal */
class FileExport {
    /** @var FileExportTarget */
    public $target;

    /** @var ExportResult */
    public $result;

    public function __construct(FileExportTarget $target, ExportResult $result)
    {
        $this->target = $target;
        $this->result = $result;
    }
}
