<?php

namespace App\Model\Export;

use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\FailureExportResult;
use Arriva\Abt\Utility\Iterator;
use SolidPhp\ValueObjects\Type\Type;

class ChainExporter implements Exporter
{
    /** @var Exporter[]|array */
    private $exporters = [];

    public function registerExporter(Exporter $exporter): void
    {
        if (!in_array($exporter, $this->exporters, true)) {
            $this->exporters[] = $exporter;
        }
    }

    /**
     * @return Type[]|iterable
     */
    public function getSupportedSubjectTypes(): array
    {
        return Iterator::create($this->exporters)
                       ->map(
                           static function (Exporter $exporter) {
                               return $exporter->getSupportedSubjectTypes();
                           }
                       )
                       ->flatten()
                       ->unique()
                       ->getArray(false);
    }

    /**
     * @param Type $subjectType
     *
     * @return Type[]|array|iterable
     */
    public function getSupportedTargetTypes(Type $subjectType): array
    {
        return Iterator::create($this->exporters)
                       ->map(
                           static function (Exporter $exporter) use ($subjectType) {
                               return $exporter->getSupportedTargetTypes($subjectType);
                           }
                       )
                       ->flatten()
                       ->unique()
                       ->getArray(false);
    }

    public function export($subject, ExportTarget $target): ExportResult
    {
        if ($firstMatchingExporter = $this->findFirstSupportingExporter($subject, $target)) {
            return $firstMatchingExporter->export($subject, $target);
        }

        return new FailureExportResult(
            sprintf(
                'No exporter found that can handle a subject of type %s with target %s',
                Type::of($subject),
                $target
            )
        );
    }

    /**
     * @param              $subject
     * @param ExportTarget $target
     *
     * @return Exporter|null
     */
    private function findFirstSupportingExporter($subject, ExportTarget $target): ?Exporter
    {
        return Iterator::create($this->exporters)
                       ->first(
                           static function (Exporter $exporter) use ($subject, $target) {
                               return $exporter->supportsExport($subject, $target);
                           }
                       );
    }

    public function supportsSubject($subject): bool
    {
        return Iterator::create($this->exporters)
                       ->any(
                           static function (Exporter $exporter) use ($subject) {
                               return $exporter->supportsSubject($subject);
                           }
                       );
    }

    public function supportsExport($subject, ExportTarget $target): bool
    {
        return Iterator::create($this->exporters)
                       ->any(
                           static function (Exporter $exporter) use ($subject, $target) {
                               return $exporter->supportsExport($subject, $target);
                           }
                       );
    }
}
