<?php

namespace App\Model\Export\Target;

use App\Model\Export\FileExportTarget;
use Arriva\Abt\Value\File;

class CsvFile implements FileExportTarget
{
    /** @var File */
    private $file;

    /**
     * @param File $file
     */
    public function __construct(File $file)
    {
        $this->file = $file;
    }

    public function getFile(): File
    {
        return $this->file;
    }
}
