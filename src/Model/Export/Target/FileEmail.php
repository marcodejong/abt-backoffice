<?php

namespace App\Model\Export\Target;

use App\Model\Export\ExportTarget;
use App\Model\Export\FileExportTarget;
use App\Service\Mailer\Data\Body;
use App\Service\Mailer\Data\Identity;
use App\Service\Mailer\Data\Subject;

class FileEmail implements ExportTarget
{
    /** @var Identity */
    private $sender;

    /** @var Identity */
    private $receiver;

    /** @var Subject */
    private $subject;

    /** @var Body */
    private $body;

    /** @var FileExportTarget[] */
    private $fileTargets;

    /**
     * @param Identity           $sender
     * @param Identity           $receiver
     * @param Subject            $subject
     * @param Body               $body
     * @param FileExportTarget[] $fileTargets
     */
    public function __construct(Identity $sender, Identity $receiver, Subject $subject, Body $body, array $fileTargets = [])
    {
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->subject = $subject;
        $this->body = $body;
        $this->fileTargets = $fileTargets;
    }

    public function getSender(): Identity
    {
        return $this->sender;
    }

    public function getReceiver(): Identity
    {
        return $this->receiver;
    }

    public function getSubject(): Subject
    {
        return $this->subject;
    }

    public function getBody(): Body
    {
        return $this->body;
    }

    /**
     * @return FileExportTarget[]
     */
    public function getFileTargets(): array
    {
        return $this->fileTargets;
    }
}
