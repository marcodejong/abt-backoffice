<?php

namespace App\Model\Export;

use Arriva\Abt\Value\File;

interface FileExportTarget extends ExportTarget
{
    public function getFile(): File;
}
