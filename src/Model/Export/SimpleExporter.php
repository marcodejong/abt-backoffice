<?php

namespace App\Model\Export;

use App\Model\Export\Result\ExportResult;
use App\Model\Export\Result\FailureExportResult;
use Arriva\Abt\Utility\Iterator;
use SolidPhp\ValueObjects\Type\Type;

/**
 * Class SimpleExporter
 *
 * A base class for the most common type of Exporter; one that supports
 * a fixed set of subject and target types.
 *
 */
abstract class SimpleExporter implements Exporter
{
    public function supportsSubject($subject): bool
    {
        return Iterator::create($this->getSupportedSubjectTypes())
            ->any(
                static function (Type $supportedSubjectType) use ($subject) {
                    return $supportedSubjectType->isInstance($subject);
                }
            );
    }

    public function supportsExport($subject, ExportTarget $target): bool
    {
        return $this->supportsSubject($subject) &&
            Iterator::create($this->getSupportedTargetTypes(Type::of($subject)))
            ->any(
                static function (Type $supportedTargetType) use ($target) {
                    return $supportedTargetType->isInstance($target);
                }
            );
    }

    abstract protected function doExport($subject, $target): ExportResult;

    public function export($subject, ExportTarget $target): ExportResult
    {
        if ($this->supportsExport($subject, $target)) {
            return $this->doExport($subject, $target);
        }

        return new FailureExportResult(sprintf('%s does not support subject %s with target %s', $this, Type::of($subject), Type::of($target)));
    }

    public function __toString()
    {
        return (string)Type::of($this);
    }

}
