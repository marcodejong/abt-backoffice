<?php

namespace App\Model\Export;

/**
 * Interface ExportTarget
 *
 * This interface specifies that the implementing class serves
 * as an export target.
 *
 */
interface ExportTarget
{

}
