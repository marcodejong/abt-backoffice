<?php

namespace App\Model\Export;

use SolidPhp\ValueObjects\Type\Type;

/**
 * Trait SingleTypeExporterTrait
 *
 * This Trait gives the using class the ability to easily specify
 * that it's an Exporter that supports a single subject type and a
 * single target type.
 * @see Exporter
 */
trait SingleTypeExporterTrait /* implements Exporter */
{
    /** @var Type */
    protected $subjectType;

    /** @var Type */
    protected $targetType;

    /**
     * @param Type $subjectType
     * @param Type $targetType
     */
    protected function setSupport(Type $subjectType, Type $targetType): void
    {
        $this->subjectType = $subjectType;
        $this->targetType = $targetType;
    }

    /**
     * @return Type[]|iterable
     */
    public function getSupportedSubjectTypes(): array
    {
        return [$this->subjectType];
    }

    /**
     * @param Type $subjectType
     *
     * @return Type[]|array|iterable
     */
    public function getSupportedTargetTypes(Type $subjectType): array
    {
        return [$this->targetType];
    }
}
