<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\VatRate;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;

/**
 * @package App\Manager
 */
class VatRateManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        CoreConfigManager $configManager
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
    }

    public function addVatRate(VatRate $vatRate): void
    {
        $this->entityManager->addEntity($vatRate);
        $this->entitySynchronizer->flush();
    }

    public function removeVatRate(VatRate $vatRate): void
    {
        $this->entityManager->removeEntity($vatRate);
        $this->entitySynchronizer->flush();
    }
}
