<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @package App\Manager
 */
class LocaleManager
{
    /** @var string */
    private $defaultLocale;

    /** @var string[] */
    private $languageCodes;

    /** @var string[] */
    private $locales;

    /** @var RequestStack */
    private $requestStack;

    public function __construct(string $defaultLocale, array $locales, RequestStack $requestStack)
    {
        $locales = $this->convertSeparatorsToUnderscores($locales);
        $this->assertLocales($locales);

        $this->locales = $locales;
        $this->languageCodes = \array_map([__CLASS__, 'getLanguageCodeFromLocale'], $this->locales);
        $this->defaultLocale = $defaultLocale;
        $this->requestStack = $requestStack;
    }

    public static function getLanguageCodeFromLocale(string $locale): string
    {
        $locale = \str_replace('-', '_', $locale);
        [$languageCode] = \explode('_', $locale);

        return $languageCode;
    }

    /**
     * @return null|string
     */
    public function getCurrentLocale(): ?string
    {
        if ($this->requestStack->getCurrentRequest()) {
            return $this->requestStack->getCurrentRequest()->getLocale();
        }

        return null;
    }

    /**
     * @return string[]
     */
    public function getAvailableLanguageCodes(): array
    {
        return $this->languageCodes;
    }

    /**
     * @return string[]
     */
    public function getAvailableLocales(): array
    {
        return $this->locales;
    }

    public function getDefaultLocale(): string
    {
        return $this->defaultLocale;
    }

    /**
     * Maps a Symfony 2 char Symfony locale to a PHP locale with regions for example en -> en_EN
     *
     * @param string $languageCode
     * @throws \LogicException when locale isn't available
     * @return string
     */
    public function getLocaleForLanguageCode(string $languageCode): string
    {
        $availableLanguageCodes = $this->getAvailableLanguageCodes();
        if (!\in_array($languageCode, $availableLanguageCodes, false)) {
            throw new \LogicException(
                \sprintf(
                    'Used locale: %s is not available, because it is not configured as available. Available locales are: [%s]',
                    $languageCode,
                    \implode(', ', $availableLanguageCodes)
                )
            );
        }

        foreach ($this->locales as $locale) {
            if (0 === \strpos($locale, $languageCode)) {
                return $locale;
            }
        }
    }

    /**
     * @param array $locales
     * @throws \DomainException
     * @return void
     */
    private function assertLocales(array $locales): void
    {
        if (empty($locales)) {
            throw new \DomainException(
                'Locales should contain at least 1 locale value of the format Lang_Region like: en_GB or nl_BE'
            );
        }

        if (\count($locales) !== \count(\array_unique($locales))) {
            throw new \DomainException(
                'Locales should only contain unique values of the format lang_REGION like: en_GB or nl_BE'
            );
        }

        $processedLanguageCodes = [];
        foreach ($locales as $locale) {
            $this->assertLocale($locale);

            $languageCode = self::getLanguageCodeFromLocale($locale);
            if (\in_array($languageCode, $processedLanguageCodes, false)) {
                throw new \DomainException(
                    sprintf(
                        'There are more locales for the same language %s and this is not supported',
                        $languageCode
                    )
                );
            }
            $processedLanguageCodes[] = $languageCode;
        }
    }

    /**
     * @param string $locale
     * @throws \DomainException
     * @return void
     */
    private function assertLocale(string $locale): void
    {
        [$languageCode, $regionCode] = \explode('_', $locale);
        if (2 !== \strlen($languageCode) || 2 !== \strlen($regionCode)) {
            throw new \DomainException(
                sprintf(
                    'Invalid locale: %s. format must be xx_XX (like en_GB, nl_NL or nl_BE)',
                    $locale
                )
            );
        }
    }

    /**
     * @param string[] $locales
     * @return string[]
     */
    private function convertSeparatorsToUnderscores(array $locales): array
    {
        return \array_map(
            function (string $locale): string {
                return \str_replace('-', '_', $locale);
            },
            $locales
        );
    }
}
