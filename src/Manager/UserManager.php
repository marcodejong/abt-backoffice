<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use App\Util\IterableFunctions;
use App\Entity\User;
use Arriva\Abt\ORM\Doctrine\DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Assist\Utility\DateTime\DateTimeServiceInterface;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @package App\Manager
 */
class UserManager
{
    /** @var array */
    private $options;

    /** @var DateTimeServiceInterface */
    private $dateTimeService;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface */
    private $constraintFactory;

    public function __construct(
        array $options,
        DateTimeServiceInterface $dateTimeService,
        EntityManagerInterface $entityManager,
        DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface $constraintFactory
    ) {
        $this->options = $this->configureResolver(new OptionsResolver())->resolve($options);
        $this->dateTimeService = $dateTimeService;
        $this->entityManager = $entityManager;
        $this->constraintFactory = $constraintFactory;
    }

    private function configureResolver(OptionsResolver $resolver): OptionsResolver
    {
        $resolver
            ->setRequired(
                [
                    'failedLoginAttemptsBlockThreshold',
                    'failedLoginAttemptsBlockDuration',
                    'userPasswordValidity',
                ]
            )
            ->setAllowedTypes('failedLoginAttemptsBlockThreshold', 'int')
            ->setAllowedTypes('failedLoginAttemptsBlockDuration', 'string')
            ->setAllowedTypes('userPasswordValidity', 'string');

        return $resolver;
    }

    /**
     * @param string $username
     * @throws \LogicException
     * @return null|User
     *
     */
    public function getUserByUserName(string $username): ?User
    {
        $user = null;
        $constraint = $this->constraintFactory->createFromClassNameAndCriteria(
            User::class,
            Criteria::create()->where(Criteria::expr()->eq('username', $username))
        );
        $entities = IterableFunctions::iterable_to_array($this->entityManager->getEntities($constraint));
        if (1 === \count($entities)) {
            $user = $entities[0];
        }
        $this->assertUser($user);

        /** @var User $user */

        return $user;
    }

    public function isBlockedForTooManyInvalidLoginAttempts(User $user): bool
    {
        return (
            $user->getLatestInvalidLoginStreak() >= $this->options['failedLoginAttemptsBlockThreshold'] &&
            $this->isUserAccountTemporallyBlocked($user)
        );
    }

    public function isUserAccountTemporallyBlocked(User $user): bool
    {
        if (!$user->getLatestInvalidLoginTime()) {
            return false;
        }

        $interval = \DateInterval::createFromDateString($this->options['failedLoginAttemptsBlockDuration']);
        $isBlockedUntil = new \DateTime(
            $user->getLatestInvalidLoginTime()->format('Y-m-d H:i:s'),
            $user->getLatestInvalidLoginTime()->getTimezone()
        );
        $isBlockedUntil->add($interval);
        $now = $this->dateTimeService->now();

        return $now < $isBlockedUntil;
    }

    public function isPasswordExpired(User $user): bool
    {
        $lastChange = $user->getLatestPasswordChangeAt() ?? $user->getCreatedAt();

        $interval = \DateInterval::createFromDateString($this->options['userPasswordValidity']);
        $mustBeChangeFor = clone $lastChange;
        $mustBeChangeFor->add($interval);
        $now = $this->dateTimeService->now();

        return $now > $mustBeChangeFor;
    }

    public function resetInvalidLoginStreak(User $user): void
    {
        if ($user->getLatestInvalidLoginStreak() > 0) {
            $user->setLatestInvalidLoginStreak(0);
            $this->entityManager->addEntity($user);
        }
    }

    public function updateInvalidLogin(User $user): void
    {
        $user->setLatestInvalidLoginStreak($user->getLatestInvalidLoginStreak() + 1);
        $user->setLatestInvalidLoginTime($this->dateTimeService->now());
        $this->entityManager->addEntity($user);
    }

    /**
     * @param null|UserInterface $user
     * @throws \LogicException when $user is not an instance of User
     * @return void
     */
    private function assertUser(UserInterface $user = null): void
    {
        if (null !== $user && !$user instanceof User) {
            throw new \LogicException('Accept only instance of: ' . User::class);
        }
    }
}
