<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\PropositionGroupRepositoryInterface;

/**
 * @package App\Manager
 */
class PropositionGroupManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var PropositionGroupRepositoryInterface */
    private $propositionGroupRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        PropositionGroupRepositoryInterface $propositionGroupRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->propositionGroupRepository = $propositionGroupRepository;
    }

    public function getAllPropositionGroups(): array
    {
        return $this->propositionGroupRepository->getPropositionGroups();
    }

    public function getAllVisiblePropositionGroups(): array
    {
        return array_filter(
            $this->getAllPropositionGroups(),
            function (PropositionGroup $propositionGroup) {
                return !$propositionGroup->isHidden();
            }
        );
    }

    public function addPropositionGroup(PropositionGroup $propositionGroup) :void
    {
        $this->entityManager->addEntity($propositionGroup);
        $this->entitySynchronizer->flush();
    }

    public function removePropositionGroup(PropositionGroup $propositionGroup): void
    {
        $this->entityManager->removeEntity($propositionGroup);
        $this->entitySynchronizer->flush();
    }
}
