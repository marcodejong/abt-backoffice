<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\ValidityPeriod;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\ValidityPeriodRepositoryInterface;

/**
 * @package App\Manager
 */
class ValidityPeriodManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var ValidityPeriodRepositoryInterface */
    private $validityPeriodRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        ValidityPeriodRepositoryInterface $validityPeriodRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->validityPeriodRepository = $validityPeriodRepository;
    }

    public function getAllValidityPeriods(): array
    {
        return $this->validityPeriodRepository->getValidityPeriods();
    }

    public function addValidityPeriod(ValidityPeriod $validityPeriod): void
    {
        $this->entityManager->addEntity($validityPeriod);
        $this->entitySynchronizer->flush();
    }

    public function removeValidityPeriod(ValidityPeriod $validityPeriod): void
    {
        $this->entityManager->removeEntity($validityPeriod);
        $this->entitySynchronizer->flush();
    }
}
