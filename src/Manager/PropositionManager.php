<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\PropositionRepositoryInterface;

/**
 * @package App\Manager
 */
class PropositionManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var PropositionRepositoryInterface */
    private $propositionRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        PropositionRepositoryInterface $propositionRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->propositionRepository = $propositionRepository;
    }

    /**
     * @return Proposition[]
     */
    public function getAllPropositions(): array
    {
        return $this->propositionRepository->getAllPropositions();
    }

    public function addProposition(Proposition $proposition) :void
    {
        $this->entityManager->addEntity($proposition);
        $this->entitySynchronizer->flush();
    }

    public function removeProposition(Proposition $proposition): void
    {
        $this->entityManager->removeEntity($proposition);
        $this->entitySynchronizer->flush();
    }
}
