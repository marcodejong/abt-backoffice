<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\CustomerGroup;
use Arriva\Abt\Entity\CustomerType;
use Arriva\Abt\ORM\Doctrine\DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface;
use Arriva\Abt\ORM\Repository\CustomerRepositoryInterface;
use Arriva\Abt\Entity\Customer;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\CustomerTypeRepositoryInterface;
use Arriva\Abt\Value\CustomerType as CustomerTypeValue;
use Doctrine\Common\Collections\Criteria;

/**
 * @package App\Manager
 */
class CustomerManager
{
    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /** @var CustomerTypeRepositoryInterface */
    private $customerTypeRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface */
    private $constraintFactory;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        CustomerTypeRepositoryInterface $customerTypeRepository,
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface $constraintFactory
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerTypeRepository = $customerTypeRepository;
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->constraintFactory = $constraintFactory;
    }

    /**
     * @return Customer[]
     */
    public function getOrganisationCustomers(): array
    {
        return $this->customerRepository->getOrganisationCustomers();
    }

    public function addCustomer(Customer $customer): void
    {
        $this->entityManager->addEntity($customer);
        $this->entitySynchronizer->flush();
    }

    public function findCustomerById(int $id): ?Customer
    {
        return $this->customerRepository->findCustomerById($id);
    }

    /**
     * @param string $term
     * @return Customer[]
     */
    public function findCustomersBySearchTerm(string $term): array
    {
        return $this->customerRepository->findCustomersBySearchTerm($term);
    }

    /**
     * @return CustomerType[]
     */
    public function getCustomerTypes(): array
    {
        $constraint = $this->constraintFactory->createFromClassNameAndCriteria(CustomerType::class, Criteria::create());

        return $this->entityManager->getEntities($constraint);
    }

    public function getCustomerTypeByValue(CustomerTypeValue $value): ?CustomerType
    {
        return $this->customerTypeRepository->getCustomerTypeByCode($value->getCode());
    }

    public function addCustomerGroup(CustomerGroup $customerGroup): void
    {
        $this->entityManager->addEntity( $customerGroup);
        $this->entitySynchronizer->flush();
    }

    public function removeCustomerGroup(CustomerGroup $customerGroup): void
    {
        $this->entityManager->removeEntity($customerGroup);
        $this->entitySynchronizer->flush();
    }
}
