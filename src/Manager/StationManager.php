<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Station;
use Arriva\Abt\ORM\Doctrine\DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @package App\Manager
 */
class StationManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface */
    private $constraintFactory;

    /** @var null|array */
    private $cache;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface $constraintFactory
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->constraintFactory = $constraintFactory;
    }

    public function getStationByCode(int $code): ?Station
    {
        $stations = $this->getAllStations();
        if (\array_key_exists($code, $stations)) {
            return $stations[$code];
        }

        return null;
    }

    /**
     * @return Station[]
     */
    public function getAllStations(): array
    {
        if (!$this->cache) {
            $constraint = $this->constraintFactory->createFromClassNameAndCriteria(
                Station::class,
                Criteria::create()
            );
            $this->cache = [];
            /** @var Station $station */
            foreach ($this->entityManager->getEntities($constraint) as $station) {
                $this->cache[$station->getCode()] = $station;
            }
        }

        return $this->cache;
    }

    public function addStation(Station $station) :void
    {
        $this->entityManager->addEntity($station);
        $this->entitySynchronizer->flush();
    }

    public function removeStation(Station $station): void
    {
        $this->entityManager->removeEntity($station);
        $this->entitySynchronizer->flush();
    }
}
