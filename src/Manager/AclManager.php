<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Assist\Entity\AclRole;
use Arriva\Assist\ORM\Repository\AclRoleRepositoryInterface;

/**
 * @package App\Manager
 */
class AclManager
{
    /** @var AclRoleRepositoryInterface */
    private $aclRoleRepository;

    public function __construct(AclRoleRepositoryInterface $aclRoleRepository)
    {
        $this->aclRoleRepository = $aclRoleRepository;
    }

    public function findAclRoleById(int $id): ?AclRole
    {
        return $this->aclRoleRepository->findAclRoleById($id);
    }

    public function hasPrivilegeForAclResource(
        AclRole $aclRole,
        string $privilege,
        string $resource,
        string $rule = 'allow'
    ): bool {
        return $this->aclRoleRepository->hasPrivilegeForAclResource($aclRole, $privilege, $resource, $rule);
    }
}
