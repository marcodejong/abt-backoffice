<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Zone;
use Arriva\Abt\Entity\ZoneRegion;
use Arriva\Abt\ORM\Doctrine\DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @package App\Manager
 */
class ZoneManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface */
    private $constraintFactory;

    /** @var null|array */
    private $cache;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface $constraintFactory
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->constraintFactory = $constraintFactory;
    }

    public function getZoneByCode(int $code): ?Zone
    {
        $zones = $this->getAllZones();
        if (\array_key_exists($code, $zones)) {
            return $zones[$code];
        }

        return null;
    }

    /**
     * @return Zone[]
     */
    public function getAllZones(): array
    {
        if (!$this->cache) {
            $constraint = $this->constraintFactory->createFromClassNameAndCriteria(
                Zone::class,
                Criteria::create()
            );
            $this->cache = [];
            /** @var Zone $zone */
            foreach ($this->entityManager->getEntities($constraint) as $zone) {
                $this->cache[$zone->getCode()] = $zone;
            }
        }

        return $this->cache;
    }

    public function addZone(Zone $zone): void
    {
        $this->entityManager->addEntity($zone);
        $this->entitySynchronizer->flush();
    }

    public function removeZone(Zone $zone): void
    {
        $this->entityManager->removeEntity($zone);
        $this->entitySynchronizer->flush();
    }

    public function removeZoneRegion(ZoneRegion $zoneRegion): void
    {
        $this->entityManager->removeEntity($zoneRegion);
        $this->entitySynchronizer->flush();
    }

    public function addZones($zones)
    {
        foreach ($zones as $zone) {
            $this->addZone($zone);
        }
    }
}
