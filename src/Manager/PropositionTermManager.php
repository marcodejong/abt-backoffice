<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Configuration\PropositionTerm\AbstractPropositionTerm;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\PropositionTermRepositoryInterface;

/**
 * @package App\Manager
 */
class PropositionTermManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var PropositionTermRepositoryInterface */
    private $propositionTermRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        PropositionTermRepositoryInterface $propositionTermRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->propositionTermRepository = $propositionTermRepository;
    }

    public function getAllPropositionTerms(): array
    {
        return $this->propositionTermRepository->getPropositionTerms();
    }

    public function addPropositionTerm(AbstractPropositionTerm $propositionTerm) :void
    {
        $this->entityManager->addEntity($propositionTerm);
        $this->entitySynchronizer->flush();
    }

    public function removePropositionTerm(AbstractPropositionTerm $propositionTerm): void
    {
        $this->entityManager->removeEntity($propositionTerm);
        $this->entitySynchronizer->flush();
    }
}
