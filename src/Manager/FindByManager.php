<?php

namespace App\Manager;

use Arriva\Abt\ORM\Doctrine\DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Doctrine\Common\Collections\Criteria;

class FindByManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface */
    private $constraintFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
        DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface $constraintFactory
    ) {
        $this->entityManager = $entityManager;
        $this->constraintFactory = $constraintFactory;
    }

    public function findBy(string $entity, Criteria $criteria): iterable
    {
        $constraint = $this->constraintFactory->createFromClassNameAndCriteria($entity, $criteria);

        return $this->entityManager->getEntities($constraint);
    }
}
