<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\EmailTemplate;
use Arriva\Abt\ORM\Doctrine\DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @package App\Manager
 */
class EmailTemplateManager
{
    public const EMAIL_TEMPLATE_SEND_REFUND_CREDIT_INVOICE_ROR_PRODUCT = 'sendRefundCreditInvoiceRorProduct';
    public const EMAIL_TEMPLATE_SEND_CARD_EXCHANGE_CONFIRMATION = 'sendCardExchangeConfirmation';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface */
    private $constraintFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface $constraintFactory
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->constraintFactory = $constraintFactory;
    }

    public function getEmailTemplateByName(string $name): ?EmailTemplate
    {
        $constraint = $this->constraintFactory->createFromClassNameAndCriteria(
            EmailTemplate::class,
            Criteria::create()->where(Criteria::expr()->eq('name', $name))
        );
        $entities = $this->entityManager->getEntities($constraint);

        return !empty($entities) ? \reset($entities) : null;
    }
    
    public function addEmailTemplate(EmailTemplate $emailTemplate): void
    {
        $this->entityManager->addEntity($emailTemplate);
        $this->entitySynchronizer->flush();
    }

    public function removeEmailTemplate(EmailTemplate $emailTemplate): void
    {
        $this->entityManager->removeEntity($emailTemplate);
        $this->entitySynchronizer->flush();
    }
}
