<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\SqillsCoreConfig;
use Arriva\Abt\ORM\Doctrine\DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @package App\Manager
 */
class CoreConfigManager
{
    /** @var string[] */
    private const MAP_ENVIRONMENT = [
        'dev' => 'development',
        'prod' => 'production',
    ];

    /** @var string */
    private const SEPARATOR = '/';

    /** @var string */
    private $environment;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface */
    private $constraintFactory;

    public function __construct(
        string $environment,
        EntityManagerInterface $entityManager,
        DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface $constraintFactory
    ) {
        $this->setEnvironment($environment);
        $this->entityManager = $entityManager;
        $this->constraintFactory = $constraintFactory;
    }

    public function getConfigValue(string $key, string $environment = null): ?string
    {
        $environment = $environment ?? $this->environment;
        $config = $this->getConfig($key, $environment);
        if (null === $config) {
            throw new \DomainException(
                \sprintf('Config with key: %s for environment %s not found', $key, $environment)
            );
        }

        return $config->getValue() ?? $config->getDefaultValue();
    }

    private function getConfig(string $key, string $environment): ?SqillsCoreConfig
    {
        [$category, $name] = $this->parseKey($key);

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('environment', $environment))
            ->andWhere(Criteria::expr()->eq('category', $category))
            ->andWhere(Criteria::expr()->eq('name', $name));
        $constraint = $this->constraintFactory->createFromClassNameAndCriteria(SqillsCoreConfig::class, $criteria);
        $entities = $this->entityManager->getEntities($constraint);

        return !empty($entities) ? \reset($entities) : null;
    }

    private function setEnvironment(string $environment): void
    {
        if (\array_key_exists(\strtolower($environment), self::MAP_ENVIRONMENT)) {
            $environment = self::MAP_ENVIRONMENT[$environment];
        }

        $this->environment = $environment;
    }

    private function parseKey(string $key): array
    {
        if (false === \strpos($key, self::SEPARATOR)) {
            throw new \DomainException('Category should be defined, $key should be in form of name/category');
        }

        return \explode(self::SEPARATOR, $key);
    }
}
