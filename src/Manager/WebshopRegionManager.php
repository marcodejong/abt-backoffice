<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\WebshopRegion;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\WebshopRegionRepositoryInterface;

/**
 * @package App\Manager
 */
class WebshopRegionManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var WebshopRegionRepositoryInterface */
    private $webshopRegionRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        WebshopRegionRepositoryInterface $webshopRegionRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->webshopRegionRepository = $webshopRegionRepository;
    }

    public function getAllWebshopRegions(): array
    {
        return $this->webshopRegionRepository->getWebshopRegions();
    }

    public function addWebshopRegion(WebshopRegion $webshopRegion): void
    {
        $this->entityManager->addEntity($webshopRegion);
        $this->entitySynchronizer->flush();
    }

    public function removeWebshopRegion(WebshopRegion $webshopRegion): void
    {
        $this->entityManager->removeEntity($webshopRegion);
        $this->entitySynchronizer->flush();
    }
}
