<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Region;
use Arriva\Abt\Entity\WebshopRegion;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\RegionRepositoryInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @package App\Manager
 */
class RegionManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var RegionRepositoryInterface */
    private $regionRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        RegionRepositoryInterface $regionRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->regionRepository = $regionRepository;
    }

    /**
     * @param string $code
     * @return Region
     */
    public function findRegionByCode(string $code): ?Region
    {
        return $this->regionRepository->findRegionByCode($code);
    }

    public function findRegionsByCodes(array $codes): ArrayCollection
    {
        $regions = new ArrayCollection();
        foreach ($codes as $code) {
            $region = $this->findRegionByCode($code);
            if ($region instanceof Region) {
                $regions->add($region);
            }
        }
        return $regions;
    }

    /**
     * @return Region[]
     */
    public function getRegions(): array
    {
        return $this->regionRepository->getRegions();
    }

    public function addRegion(Region $region): void
    {
        $this->entityManager->addEntity($region);
        $this->entitySynchronizer->flush();
    }

    public function removeRegion(Region $region): void
    {
        $this->entityManager->removeEntity($region);
        $this->entitySynchronizer->flush();
    }

    public function addWebshopRegion(WebshopRegion $webshopRegion): void
    {
        $this->entityManager->addEntity($webshopRegion);
        $this->entitySynchronizer->flush();
    }

    public function removeWebshopRegion(WebshopRegion $webshopRegion): void
    {
        $this->entityManager->removeEntity($webshopRegion);
        $this->entitySynchronizer->flush();
    }

    public function findRegionById($id): Region
    {
        return $this->regionRepository->findRegionById($id);
    }
}
