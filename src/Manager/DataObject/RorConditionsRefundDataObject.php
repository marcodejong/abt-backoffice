<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager\DataObject;

use Arriva\Abt\Entity\RorConditions;

/**
 * @package App\Manager\DataObject
 */
class RorConditionsRefundDataObject
{
    /** @var RorConditions */
    private $rorConditions;

    /** @var string */
    private $refundAmount;

    /** @var \DateTimeInterface */
    private $endDate;

    public function __construct(RorConditions $rorConditions, string $refundAmount, \DateTimeInterface $endDate)
    {
        $this->rorConditions = $rorConditions;
        $this->refundAmount = $refundAmount;
        $this->endDate = $endDate;
    }

    public function getRorConditions(): RorConditions
    {
        return $this->rorConditions;
    }

    public function getRefundAmount(): string
    {
        return $this->refundAmount;
    }

    public function getEndDate(): \DateTimeInterface
    {
        return $this->endDate;
    }
}
