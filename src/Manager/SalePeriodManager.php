<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\SalePeriod;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\SalePeriodRepositoryInterface;

/**
 * @package App\Manager
 */
class SalePeriodManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var SalePeriodRepositoryInterface */
    private $salePeriodRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        SalePeriodRepositoryInterface $salePeriodRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->salePeriodRepository = $salePeriodRepository;
    }

    public function getAllSalePeriods(): array
    {
        return $this->salePeriodRepository->getSalePeriods();
    }

    public function addSalePeriod(SalePeriod $salePeriod): void
    {
        $this->entityManager->addEntity($salePeriod);
        $this->entitySynchronizer->flush();
    }

    public function removeSalePeriod(SalePeriod $salePeriod): void
    {
        $this->entityManager->removeEntity($salePeriod);
        $this->entitySynchronizer->flush();
    }
}
