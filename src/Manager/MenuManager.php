<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Menu;
use Arriva\Abt\ORM\Repository\MenuRepositoryInterface;

/**
 * @package App\Manager
 */
class MenuManager
{
    /** @var MenuRepositoryInterface */
    private $menuRepository;

    /**
     * @param MenuRepositoryInterface $menuRepository
     */
    public function __construct(MenuRepositoryInterface $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    public function findOneByModuleControllerAction(string $module, string $controller, string $action): ?Menu
    {
        return $this->menuRepository->findOneByModuleControllerAction($module, $controller, $action);
    }

    /**
     * @param string $location
     * @return Menu[]
     */
    public function findVisibleTopLevelMenuItemsByLocation(string $location): array
    {
        return $this->menuRepository->findVisibleTopLevelMenuItemsByLocation($location);
    }
}
