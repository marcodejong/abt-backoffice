<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\PricePeriod;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\PricePeriodRepositoryInterface;

/**
 * @package App\Manager
 */
class PricePeriodManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var PricePeriodRepositoryInterface */
    private $pricePeriodRespository;


    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        PricePeriodRepositoryInterface $pricePeriodRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->pricePeriodRespository = $pricePeriodRepository;
    }

    public function addPricePeriod(PricePeriod $pricePeriod): void
    {
        $this->entityManager->addEntity($pricePeriod);
        $this->entitySynchronizer->flush();
    }

    public function removePricePeriod(PricePeriod $pricePeriod): void
    {
        $this->entityManager->removeEntity($pricePeriod);
        $this->entitySynchronizer->flush();
    }

    public function getPricePeriodByName(string $name): ?PricePeriod
    {
        return $this->pricePeriodRespository->getPricePeriodByName($name);
    }

    public function getAllPricePeriods(): array
    {
        return $this->pricePeriodRespository->getAllPricePeriods();
    }
}
