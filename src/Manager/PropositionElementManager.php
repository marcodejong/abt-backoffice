<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\PropositionElementRepositoryInterface;

/**
 * @package App\Manager
 */
class PropositionElementManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var PropositionElementRepositoryInterface */
    private $propositionElementRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        PropositionElementRepositoryInterface $propositionElementRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->propositionElementRepository = $propositionElementRepository;
    }

    public function getAllPropositionElements(): array
    {
        return $this->propositionElementRepository->getPropositionElements();
    }

    public function getPropositionElementByDescription(string $description): ?PropositionElement
    {
        return $this->propositionElementRepository->getPropositionElementByDescription($description);
    }

    public function addPropositionElement(PropositionElement $propositionElement): void
    {
        $this->entityManager->addEntity($propositionElement);
        $this->entitySynchronizer->flush();
    }

    public function removePropositionElement(PropositionElement $propositionElement): void
    {
        $this->entityManager->removeEntity($propositionElement);
        $this->entitySynchronizer->flush();
    }
}
