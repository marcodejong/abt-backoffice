<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Modality;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\ModalityRepositoryInterface;

/**
 * @package App\Manager
 */
class ModalityManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var ModalityRepositoryInterface */
    private $modalityRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        ModalityRepositoryInterface $modalityRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->modalityRepository = $modalityRepository;
    }
    /**
     * @return Modality[]
     */
    public function getAllModalities(): array
    {
        return $this->modalityRepository->getModalities();
    }

    public function addModality(Modality $modality): void
    {
        $this->entityManager->addEntity($modality);
        $this->entitySynchronizer->flush();
    }

    public function removeModality(Modality $modality): void
    {
        $this->entityManager->removeEntity($modality);
        $this->entitySynchronizer->flush();
    }
}
