<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Import;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;

/**
 * @package App\Manager
 */
class ImportManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;


    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
    }

    public function addImport(Import $import) :void
    {
        $this->entityManager->addEntity($import);
        $this->entitySynchronizer->flush();
    }

    public function removeImport(Import $import): void
    {
        $this->entityManager->removeEntity($import);
        $this->entitySynchronizer->flush();
    }
}
