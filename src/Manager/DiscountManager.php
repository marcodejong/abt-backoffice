<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Discount;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;

/**
 * @package App\Manager
 */
class DiscountManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
    }

    public function addDiscount(Discount $discount) :void
    {
        $this->entityManager->addEntity($discount);
        $this->entitySynchronizer->flush();
    }

    public function removeDiscount(Discount $discount): void
    {
        $this->entityManager->removeEntity($discount);
        $this->entitySynchronizer->flush();
    }
}
