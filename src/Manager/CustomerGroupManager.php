<?php

namespace App\Manager;

use Arriva\Abt\Entity\CustomerGroup;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\CustomerGroupRepositoryInterface;

/**
 * @package App\Manager
 */
class CustomerGroupManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var CustomerGroupRepositoryInterface */
    private $customerGroupRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        CustomerGroupRepositoryInterface $customerGroupRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->customerGroupRepository = $customerGroupRepository;
    }
    /**
     * @return CustomerGroup[]
     */
    public function getAllCustomerGroups(): array
    {
        return $this->customerGroupRepository->getAllCustomerGroups();
    }

    public function addCustomerGroup(CustomerGroup $customerGroup): void
    {
        $this->entityManager->addEntity($customerGroup);
        $this->entitySynchronizer->flush();
    }

    public function removeCustomerGroup(CustomerGroup $customerGroup): void
    {
        $this->entityManager->removeEntity($customerGroup);
        $this->entitySynchronizer->flush();
    }
}
