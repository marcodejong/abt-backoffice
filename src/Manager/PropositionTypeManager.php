<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\PropositionType;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\PropositionTypeRepositoryInterface;

/**
 * @package App\Manager
 */
class PropositionTypeManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var PropositionTypeRepositoryInterface */
    private $propositionTypeRepository;


    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        PropositionTypeRepositoryInterface $propositionTypeRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->propositionTypeRepository = $propositionTypeRepository;
    }

    /**
     * @return PropositionType[]
     */
    public function getAllPropositionTypes(): array
    {
        return $this->propositionTypeRepository->getPropositionTypes();
    }


    public function addPropositionType(PropositionType $propositionType): void
    {
        $this->entityManager->addEntity($propositionType);
        $this->entitySynchronizer->flush();
    }

    public function removePropositionType(PropositionType $propositionType): void
    {
        $this->entityManager->removeEntity($propositionType);
        $this->entitySynchronizer->flush();
    }
}
