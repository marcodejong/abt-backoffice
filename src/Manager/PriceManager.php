<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Manager;

use Arriva\Abt\Entity\Price;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Arriva\Abt\ORM\EntityManager\EntitySynchronizerInterface;
use Arriva\Abt\ORM\Repository\PriceRepositoryInterface;

/**
 * @package App\Manager
 */
class PriceManager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EntitySynchronizerInterface */
    private $entitySynchronizer;

    /** @var PriceRepositoryInterface */
    private $priceRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        EntitySynchronizerInterface $entitySynchronizer,
        PriceRepositoryInterface $priceRepository
    ) {
        $this->entityManager = $entityManager;
        $this->entitySynchronizer = $entitySynchronizer;
        $this->priceRepository = $priceRepository;
    }

    public function addPrice(Price $price) :void
    {
        $this->entityManager->addEntity($price);
        $this->entitySynchronizer->flush();
    }

    public function removePrice(Price $price): void
    {
        $this->entityManager->removeEntity($price);
        $this->entitySynchronizer->flush();
    }

    public function getPriceById($id) : ?Price
    {
        return $this->priceRepository->getPriceById($id);
    }
}
