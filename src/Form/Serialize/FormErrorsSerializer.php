<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\Serialize;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\Exception\InvalidArgumentException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @package App\Form\Serialize
 */
class FormErrorsSerializer
{
    /** @var TranslatorInterface */
    private $translator;

    /** @var string */
    private $translationDomain;

    public function __construct(TranslatorInterface $translator, string $translationDomain = 'messages')
    {
        $this->translator = $translator;
        $this->translationDomain = $translationDomain;
    }

    /**
     * @param FormInterface $form
     * @param null|string $translationDomain
     * @throws InvalidArgumentException
     * @return array
     */
    public function serializeFormErrors(FormInterface $form, ?string $translationDomain = null): array
    {
        $errors = ['form' => [], 'fields' => []];
        foreach ($form->getErrors() as $error) {
            $errors['form'][$error->getMessage()] = $this->getErrorMessage($error, $translationDomain);
        }
        $errors['fields'] = $this->serialize($form, $translationDomain);

        return $errors;
    }

    /**
     * @param FormInterface $form
     * @param null|string $translationDomain
     * @throws InvalidArgumentException
     * @return array
     */
    private function serialize(FormInterface $form, ?string $translationDomain = null): array
    {
        $result = [];
        if ($form instanceof Form) {
            /** @var string $name */
            /** @var FormInterface $child */
            foreach ($form->getIterator() as $name => $child) {
                if ($child instanceof Form) {
                    /** @var FormError $error */
                    foreach ($child->getErrors() as $error) {
                        if (!\array_key_exists($name, $result)) {
                            $result[$name] = [];
                        }
                        $result[$name][$error->getMessage()] = $this->getErrorMessage($error, $translationDomain);
                    }
                    if (\count($child->getIterator()) > 0) {
                        $result[$name] = $this->serialize($child);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param FormError $error
     * @param null|string $translationDomain
     * @throws InvalidArgumentException
     * @return string
     */
    private function getErrorMessage(FormError $error, ?string $translationDomain = null): string
    {
        $domain = $translationDomain ?? $this->translationDomain;
        if (null !== $error->getMessagePluralization()) {
            return $this->translator->transChoice(
                $error->getMessageTemplate(),
                $error->getMessagePluralization(),
                $error->getMessageParameters(),
                $domain
            );
        }

        return $this->translator->trans($error->getMessageTemplate(), $error->getMessageParameters(), $domain);
    }
}
