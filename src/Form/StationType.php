<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\StationDataObject;
use Arriva\Abt\Entity\Region;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class StationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'code',
                IntegerType::class,
                 [
                    'label' => 'form.administration.field.code.label',
                    'required' => true,
                ]
            )
            ->add(
                'regions',
                EntityType::class,
                [
                    'label' => 'form.administration.field.regions.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => Region::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'attr' => ['style' => 'display:none'],
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => StationDataObject::class]);
    }
}
