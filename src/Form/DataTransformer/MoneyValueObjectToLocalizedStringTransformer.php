<?php

namespace App\Form\DataTransformer;

use Arriva\Abt\Utility\Currency;
use Arriva\Assist\Utility\Money\Money as AssistMoney;
use Arriva\Abt\Utility\Money as AbtMoney;
use Symfony\Component\Form\Extension\Core\DataTransformer\MoneyToLocalizedStringTransformer;

class MoneyValueObjectToLocalizedStringTransformer extends MoneyToLocalizedStringTransformer
{
    /**
     * @var string
     */
    private $moneyClass;

    public function __construct(
        ?int $scale = 2,
        ?bool $grouping = true,
        ?int $roundingMode = self::ROUND_HALF_UP,
        ?int $divisor = 1,
        ?string $moneyClass = AbtMoney::class
    ) {
        parent::__construct($scale, $grouping, $roundingMode, $divisor);

        if (AbtMoney::class !== $moneyClass && AssistMoney::class !== $moneyClass) {
            throw new \RuntimeException(
                sprintf(
                    'The transaformer only supports the following money classes: "%s".',
                    implode('", "', [AssistMoney::class, AbtMoney::class])
                )
            );
        }
        $this->moneyClass = $moneyClass;
    }

    public function transform($value)
    {
        if ($value instanceof AssistMoney || $value instanceof AbtMoney) {
            $value = $value->getAmount();
        }

        return parent::transform($value);
    }

    public function reverseTransform($value)
    {
        $value = parent::reverseTransform($value);

        return null === $value ? $value : $this->moneyClass::amount($value, Currency::EUR());
    }
}
