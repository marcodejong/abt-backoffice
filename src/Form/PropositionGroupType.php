<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\PropositionGroupDataObject;
use Arriva\Abt\Entity\Modality;
use Arriva\Abt\Entity\PropositionCategory;
use Arriva\Abt\Entity\PropositionType;
use Arriva\Abt\Entity\Region;
use Arriva\Abt\Entity\StarValue;
use Arriva\Abt\Entity\VatRate;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class PropositionGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'propositionType',
                EntityType::class,
                [
                    'label' => 'form.administration.field.proposition-type.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => PropositionType::class,
                    'choice_label' => 'name',
                    'required' => true,
                ]
            )
            ->add(
                'category',
                ChoiceType::class,
                [
                    'label' => 'form.administration.field.proposition-category.label',
                    'required' => true,
                    'choices' => [
                        "Regulier" => PropositionCategory::REGULAR()->getId(),
                        "Ster" => PropositionCategory::STAR()->getId(),
                        "Traject" => PropositionCategory::ROUTE()->getId(),
                    ],
                ]
            )
            ->add(
                'vatRate',
                EntityType::class,
                [
                    'label' => 'form.administration.field.vat-rate.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => VatRate::class,
                    'choice_label' => 'name',
                    'required' => true,
                ]
            )
            ->add(
                'termsLink',
                TextType::class,
                [
                    'label' => 'form.administration.field.termsLink.label',
                    'required' => true,
                ]
            )
            ->add(
                'description',
                CKEditorType::class,
                [
                    'label' => 'form.administration.field.description.label',
                    'required' => true
                ]
            )
            ->add(
                'order',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.order.label',
                    'required' => true,
                ]
            )
            ->add(
                'starValues',
                ChoiceType::class,
                [
                    'label' => 'form.administration.field.stars.label',
                    'placeholder' => 'form.element.select.options.first',
                    'required' => false,
                    'choices' => StarValue::instances(),
                    'multiple' => true,
                    'choice_label' => 'id',
                    'attr' => [
                        'class' => 'multiselect',
                    ],
                ]
            )
            ->add(
                'salesEnabled',
                CheckboxType::class,
                [
                    'label' => 'form.administration.field.sales-enabled-webshop.label',
                    'required' => false,

                ]
            )
            ->add(
                'regions',
                EntityType::class,
                [
                    'label' => 'form.administration.field.regions.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => Region::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'attr' => ['style' => 'display:none'],
                    'required' => true,
                ]
            )
            ->add(
                'modalities',
                EntityType::class,
                [
                    'label' => 'form.administration.field.modalities.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => Modality::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => false,
                    'attr' => ['style' => 'display:none'],
                ]
            );
    }

    public function configureOptions(
        OptionsResolver $resolver
    ): void {
        $resolver->setDefaults(['data_class' => PropositionGroupDataObject::class]);
    }
}
