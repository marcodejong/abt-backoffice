<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class CurrencyPriceType extends AbstractType implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [FormEvents::PRE_SUBMIT => ['onPreSubmit', 9999]];
    }

    public function onPreSubmit(FormEvent $event)
    {
        if (null !== $value = $event->getData()) {
            $event->setData(\str_replace('.', '', $value));
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventSubscriber($this);
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        if ('0' === $view->vars['value']) {
            $decimalString = \str_repeat('0', $options['decimal_precision']);
            $view->vars['value'] = '0' . $options['decimal_separator_localized'] . $decimalString;
        }
        $view->vars['currency_symbol'] = $options['currency_symbol'];
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefined('currency_symbol')
            ->setDefault('currency_symbol', '€')
            ->setAllowedTypes('currency_symbol', 'string');
    }

    public function getParent(): string
    {
        return FloatType::class;
    }
}
