<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\RegionDataObject;
use Arriva\Abt\Entity\WebshopRegion;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class RegionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'invalid_message' => 'form.validator.name.notemptyinvalid',
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'code',
                TextType::class,
                [
                    'invalid_message' => 'form.validator.code.notemptyinvalid',
                    'label' => 'form.administration.field.code.label',
                    'required' => true,
                ]
            )
            ->add(
                'webshopRegion',
                EntityType::class,
                [
                    'required' => false,
                    'label' => 'form.administration.field.webshop-region.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => WebshopRegion::class,
                    'choice_label' => 'name',
                ]
            )
            ->add(
                'costUnitName',
                TextType::class,
                [
                    'label' => 'form.administration.field.cost-unit.label',
                    'required' => true,
                ]
            )
            ->add(
                'costUnitCode',
                TextType::class,
                [
                    'label' => 'form.administration.field.cost-unit-code.label',
                    'required' => true,
                ]
            )
            ->add(
                'concessionId',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.concession-id.label',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => RegionDataObject::class]);
    }
}
