<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\PropositionDataObject;
use Arriva\Abt\Entity\Configuration\PropositionTerm\AbstractPropositionTerm;
use Arriva\Abt\Entity\CustomerGroup;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\Entity\SalePeriod;
use Arriva\Abt\Entity\ValidityPeriod;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class PropositionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'propositionGroup',
                EntityType::class,
                [
                    'label' => 'form.administration.field.proposition-group.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => PropositionGroup::class,
                    'query_builder' => function(EntityRepository $repository) {
                        return $repository->createQueryBuilder('pg')->andWhere('pg.hidden = 0');
                    },
                    'choice_label' => 'name',
                    'required' => true,
                ]
            )
            ->add(
                'validityPeriod',
                EntityType::class,
                [
                    'label' => 'form.administration.field.validity-period.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => ValidityPeriod::class,
                    'choice_label' => 'name',
                    'required' => true,
                ]
            )
            ->add(
                'salePeriod',
                EntityType::class,
                [
                    'label' => 'form.administration.field.sale-period.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => SalePeriod::class,
                    'choice_label' => 'name',
                    'required' => true,
                ]
            )
            ->add(
                'propositionTerm',
                EntityType::class,
                [
                    'label' => 'form.administration.field.proposition-term.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => AbstractPropositionTerm::class,
                    'choice_label' => 'name',
                    'required' => true,
                ]
            )
            ->add(
                'customerGroups',
                EntityType::class,
                [
                    'label' => 'form.administration.field.customer-groups.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => CustomerGroup::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => true,
                    'attr' => ['style' => 'display:none']
                ]
            )
            ->add(
                'propositionElements',
                EntityType::class,
                [
                    'label' => 'form.administration.field.proposition-elements.label',
                    'placeholder' => 'form.element.select.options.first',
                    'class' => PropositionElement::class,
                    'choice_label' => 'description',
                    'multiple' => true,
                    'required' => true,
                    'attr' => ['style' => 'display:none']
                ]
            )
            ->add(
                'tAccount',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.t-account.label',
                    'required' => false,
                ]
            )
            ->add(
                'paymentByInstallmentsAllowed',
                CheckboxType::class,
                [
                    'label' => 'form.administration.field.payment-by-installments-allowed.label',
                    'required' => false,
                ]
            )
            ->add(
                'refundPeriod',
                ChoiceType::class,
                [
                    'label' => 'form.administration.field.refund-period.label',
                    'required' => false,
                    'choices' => [
                        "Jaar/Jaren" => "year",
                        "Maand/Maanden" => "month",
                    ],
                    'expanded' => true,
                    'attr' => ['style' => 'display:none'],
                ]
            )
            ->add(
                'refundPeriodValue',
                IntegerType::class,
                [
                    'required' => false,
                ]
            );
    }

    public function configureOptions(
        OptionsResolver $resolver
    ): void {
        $resolver->setDefaults(['data_class' => PropositionDataObject::class]);
    }
}
