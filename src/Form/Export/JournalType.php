<?php


namespace App\Form\Export;


use App\Form\DatePickerType;
use Symfony\Component\Form\FormBuilderInterface;

class JournalType extends ExportType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->get('exportType')->setData('journal');

        $builder
            ->add(
                'date',
                DatePickerType::class,
                [
                    'label' => 'form.administration.export.field.date.label',
                    'required' => true,
                ]
            );
    }
}