<?php


namespace App\Form\Export;


use App\Form\DataObject\ExportDataObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExportType extends AbstractType
{
    /** @var TranslatorInterface */
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'exportType',
                ChoiceType::class,
                [
                    'label' => 'form.administration.field.type.label',
                    'required' => true,
                    'choices' => $this->getExportChoices(),
                    'attr' => ['class' => 'typeSelector']
                ]
            );
    }

    protected function getExportChoices(): array
    {
        return [
            $this->translator->trans('form.administration.export.select') => '',
            $this->translator->trans('form.administration.export.tariff-configuration') => 'tariff-configuration',
            $this->translator->trans('form.administration.export.debtor-receivables') => 'debtor-receivables',
            $this->translator->trans('form.administration.export.journal') => 'journal',
            $this->translator->trans('form.administration.export.monthly-journal') => 'monthly-journal',
            $this->translator->trans('form.administration.export.journal-transactions') => 'journal-transactions',
            $this->translator->trans('form.administration.export.invoiced-transactions') => 'invoiced-transactions',
        ];
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => ExportDataObject::class]);
    }
}