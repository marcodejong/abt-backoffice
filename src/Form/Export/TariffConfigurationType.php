<?php


namespace App\Form\Export;


use App\Model\Report\TariffConfiguration\TariffConfigurationType as Configuration;
use Arriva\Abt\Entity\PricePeriod;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class TariffConfigurationType extends ExportType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->get('exportType')->setData('tariff-configuration');

        $builder
            ->add(
                'configurationType',
                ChoiceType::class,
                [
                    'label' => 'form.administration.export.tariff-configuration.type.label',
                    'choices' => $this->getConfigurationChoices(),
                ]
            )
            ->add(
                'pricePeriod',
                EntityType::class,
                [
                    'label' => 'form.administration.export.tariff-configuration.price-period.label',
                    'class' => PricePeriod::class,
                    'choice_label' => 'name',
                ]
            );
    }

    private function getConfigurationChoices(): array
    {
        return [
            $this->translator->trans('form.administration.export.tariff-configuration.propositions') => Configuration::PROPOSITION_EXPORT,
            $this->translator->trans('form.administration.export.tariff-configuration.regular-tariffs') => Configuration::REGULAR_TARIFFS,
            $this->translator->trans('form.administration.export.tariff-configuration.route-tariffs') => Configuration::ROUTE_TARIFFS,
            $this->translator->trans('form.administration.export.tariff-configuration.star-tariffs') => Configuration::STAR_TARIFFS,
        ];
    }
}