<?php


namespace App\Form\Export;


use App\Form\DatePickerType;
use Symfony\Component\Form\FormBuilderInterface;

class JournalTransactionsType extends ExportType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->get('exportType')->setData('journal-transactions');

        $builder
            ->add(
                'date',
                DatePickerType::class,
                [
                    'label' => 'form.administration.export.field.date.label',
                    'required' => true,
                ]
            );
    }
}