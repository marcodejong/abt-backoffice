<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\CustomerGroupDataObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class CustomerGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'code',
                TextType::class,
                [
                    'label' => 'form.administration.field.code.label',
                    'required' => true,
                ]
            )
            ->add(
                'ageFrom',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.agefrom.label',
                    'required' => true,
                ]
            )
            ->add(
                'ageTo',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.ageto.label',
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => CustomerGroupDataObject::class]);
    }
}
