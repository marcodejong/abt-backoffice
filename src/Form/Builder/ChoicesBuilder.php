<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\Builder;

/**
 * @package App\Form\Builder
 */
class ChoicesBuilder
{
    /** @var null|callable */
    private $mapCallback;

    /** @var null|callable */
    private $sortCallback;

    /** @var null|array */
    private $firstOption;

    /** @var bool */
    private $reversed = false;

    public function __construct()
    {
        $this->sortCallback = function (string $nameA, string $nameB) {
            return \strcasecmp($nameA, $nameB);
        };
    }

    public static function create(): ChoicesBuilder
    {
        return new self();
    }

    /**
     * The $mapCallback should return an indexed array with for each item an associative array
     * with and id and name key.
     *
     * @param callable $mapCallback
     * @return ChoicesBuilder
     */
    public function map(callable $mapCallback): ChoicesBuilder
    {
        $this->mapCallback = $mapCallback;

        return $this;
    }

    public function sort(callable $sortCallback): ChoicesBuilder
    {
        $this->sortCallback = $sortCallback;

        return $this;
    }

    public function removeSort(): ChoicesBuilder
    {
        $this->sortCallback = null;

        return $this;
    }

    public function addFirstOption(string $name, ?string $id = null): ChoicesBuilder
    {
        $this->firstOption = ['name' => $name, 'id' => $id];

        return $this;
    }

    public function removeEmptyFirstOption(): ChoicesBuilder
    {
        $this->firstOption = null;

        return $this;
    }

    public function setReversed(bool $reversed): ChoicesBuilder
    {
        $this->reversed = $reversed;

        return $this;
    }

    /**
     * @param array $items
     * @throws \RuntimeException
     * @return array
     */
    public function build(array $items): array
    {
        if (!$this->mapCallback) {
            throw new \LogicException(
                \sprintf(
                    'A map callback function should be provided call %s first',
                    __CLASS__ . '::map'
                )
            );
        }

        $items = \array_map($this->mapCallback, $items);
        if ($this->sortCallback) {
            \usort(
                $items,
                function (array $optionA, array $optionB) {
                    return \call_user_func($this->sortCallback, $optionA['name'], $optionB['name']);
                }
            );
        }

        if ($this->firstOption) {
            \array_unshift($items, ['id' => $this->firstOption['id'], 'name' => $this->firstOption['name']]);
        }

        return $this->reversed ? \array_column($items, 'name', 'id') : \array_column($items, 'id', 'name');
    }
}
