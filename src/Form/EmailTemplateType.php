<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\EmailTemplateDataObject;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class EmailTemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label' => 'form.administration.field.description.label',
                    'required' => true,
                ]
            )
            ->add(
                'emailSender',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'form.administration.email-template.field.email_sender.label',
                ]
            )
            ->add(
                'subject',
                TextType::class,
                [
                    'label' => 'form.administration.field.email-subject.label',
                    'required' => true,
                ]
            )
            ->add(
                'body',
                CKEditorType::class,
                [
                    'label' => 'form.administration.field.email_body.label',
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => EmailTemplateDataObject::class]);
    }
}
