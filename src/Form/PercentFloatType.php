<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * @package App\Form
 */
class PercentFloatType extends FloatType
{
    protected function preSubmitMessageId(string $value): ?string
    {
        if (!\is_numeric($value)) {
            return 'form.validator.percentage.notdigits';
        }

        $parts = \explode($this->options['decimal_separator_localized'], $value);
        $number = $parts[0];
        $decimal = array_key_exists(1, $parts) ? $parts[1] : '0';
        if ((int)$number > 100 || \strlen($decimal) > $this->options['decimal_precision']) {
            return 'form.validator.percentage.invalid-value';
        }

        return null;
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['key_code_to_replace'] = (',' === $options['decimal_separator_localized'] ? 190 : 188);
        $view->vars['mask_pattern'] = $this->getMaskPattern($options['decimal_precision']);
        $view->vars['attr']['class'] = \array_key_exists('class', $view->vars['attr'])
            ? \trim($view->vars['attr']['class']) . ' percent-float'
            : 'percent-float';
    }

    private function getMaskPattern(int $decimalPrecision): string
    {
        return \sprintf(
            "^(0)|(100)|([1-9][0-9])|([1-9])|([1-9][0-9]{0,1}[\\%s\\%s]{1}[0-9]{0,%s}$|)",
            ',',
            '.',
            $decimalPrecision
        );
    }
}
