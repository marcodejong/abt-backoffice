<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Configuration\RouteItem;
use Arriva\Abt\Entity\Configuration\StarItem;
use Arriva\Abt\Entity\PricePeriod;
use Arriva\Abt\Entity\Price;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Entity\StarValue;
use Arriva\Abt\Entity\Station;

/**
 * @package App\Form\DataObject
 */
class PriceDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var float */
    public $amount;

    /** @var PropositionElement */
    public $propositionElement;

    /** @var PricePeriod */
    public $pricePeriod;

    /** @var Station|null */
    public $fromStation;

    /** @var Station|null */
    public $toStation;

    /** @var StarValue|null */
    public $starValue;

    public static function fromPrice(Price $price): PriceDataObject
    {
        $priceDataObject = new self();
        $priceDataObject->id = $price->getId();
        $priceDataObject->amount = $price->getAmount();
        $priceDataObject->pricePeriod = $price->getPricePeriod();
        $pricedItem = $price->getItem();
        $priceDataObject->propositionElement = $pricedItem->getPropositionElement();
        if($pricedItem instanceof StarItem) {
            $priceDataObject->starValue = $pricedItem->getStarValue();
        } else if ($pricedItem instanceof RouteItem) {
            $priceDataObject->fromStation = $pricedItem->getFromStation();
            $priceDataObject->toStation = $pricedItem->getToStation();
        }

        return $priceDataObject;
    }

    public function toPrice(Price $price): void
    {
        $price->setAmount($this->amount);
        $price->setPricePeriod($this->pricePeriod);
        $pricedItem = $price->getItem();
        $pricedItem->setPropositionElement($this->propositionElement);
        if($pricedItem instanceof StarItem) {
            $pricedItem->setStarValue($this->starValue);

        } else if ($pricedItem instanceof RouteItem) {
            $pricedItem->setFromStation($this->fromStation);
            $pricedItem->setToStation($this->toStation);
        }
    }
}
