<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

/**
 * @package App\Form\DataObject
 */
final class ObjectStates
{
    public const STATE_NEW = 'new';
    public const STATE_MODIFIED = 'modified';
}
