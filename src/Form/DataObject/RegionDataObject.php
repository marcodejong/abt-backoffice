<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\WebshopRegion;
use Arriva\Abt\Entity\Region;
use Arriva\Abt\Value\CostUnitCode;

/**
 * @package App\Form\DataObject
 */
class RegionDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $code;

    /** @var string */
    public $costUnitName;

    /** @var string */
    public $costUnitCode;

    /** @var int */
    public $concessionId;

    /** @var WebshopRegion */
    public $webshopRegion;

    public static function fromRegion(Region $region): RegionDataObject
    {
        $regionDataObject = new self();
        $regionDataObject->id = $region->getId();
        $regionDataObject->name = $region->getName();
        $regionDataObject->code = $region->getCode();
        $regionDataObject->costUnitName = $region->getCostUnitName();
        $regionDataObject->costUnitCode = $region->getValueCostUnitCode()->getValue();
        $regionDataObject->concessionId = $region->getConcessionId();
        $regionDataObject->webshopRegion = $region->getWebshopRegion();

        return $regionDataObject;
    }

    public function toRegion(Region $region): void
    {
        $region->setName($this->name);
        $region->setCode($this->code);
        $region->setCostUnitName($this->costUnitName);
        $region->setValueCostUnitCode(CostUnitCode::of($this->costUnitCode));
        $region->setWebshopRegion($this->webshopRegion);
        $region->setConcessionId($this->concessionId);

    }
}
