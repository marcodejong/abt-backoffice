<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Configuration\PropositionTerm\AbstractPropositionTerm;
use Arriva\Abt\Entity\Configuration\PropositionTerm\FlexiblePropositionTerm;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Money;
use Arriva\Abt\Value\Date;
use Arriva\Abt\Value\Duration\Duration;
use Arriva\Abt\Value\Duration\FixedEndDateDuration;
use Arriva\Abt\Value\Duration\IntervalDuration;

/**
 * @package App\Form\DataObject
 */
class PropositionTermDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $code;

    /** @var string */
    public $name;

    /** @var int */
    public $days;

    /** @var int */
    public $weeks;

    /** @var int */
    public $months;

    /** @var int */
    public $years;

    /** @var bool */
    public $permanent;

    /** @var \DateTimeInterface */
    public $endDate;

    /** @var float */
    public $fromAmount;

    /** @var int */
    public $subscriptionReminder1;

    /** @var int */
    public $subscriptionReminder2;

    /** @var int */
    public $subscriptionReminder3;

    public static function fromPropositionTerm(AbstractPropositionTerm $propositionTerm): PropositionTermDataObject
    {
        $propositionTermDataObject = new self();
        $propositionTermDataObject->id = $propositionTerm->getId();
        $propositionTermDataObject->code = $propositionTerm->getCode();
        $propositionTermDataObject->name = $propositionTerm->getName();

        $propositionTermDataObject->fromAmount = $propositionTerm->getFromMoneyAmount()->getAmount();
        $propositionTermDataObject->subscriptionReminder1 = $propositionTerm->getSubscriptionReminder1();
        $propositionTermDataObject->subscriptionReminder2 = $propositionTerm->getSubscriptionReminder2();
        $propositionTermDataObject->subscriptionReminder3 = $propositionTerm->getSubscriptionReminder3();
        $propositionTermDataObject->permanent = $propositionTerm->isPermanent();

        if ($propositionTerm instanceof FlexiblePropositionTerm) {
            $duration = $propositionTerm->getDuration();
            if ($duration instanceof IntervalDuration) {
                $propositionTermDataObject->days = $duration->getDays();
                $propositionTermDataObject->weeks = $duration->getWeeks();
                $propositionTermDataObject->months = $duration->getMonths();
                $propositionTermDataObject->years = $duration->getyears();
            }

            if ($duration instanceof FixedEndDateDuration) {
                $propositionTermDataObject->endDate = $duration->getEndDate()->getAsDateTime();
            }
        }

        return $propositionTermDataObject;
    }

    public function toPropositionTerm(AbstractPropositionTerm $propositionTerm): void
    {
        $propositionTerm->setCode($this->code);
        $propositionTerm->setName($this->name);
        $propositionTerm->setFromMoneyAmount($this->getFromMoneyAmount());
        $propositionTerm->setSubscriptionReminder1($this->subscriptionReminder1);
        $propositionTerm->setSubscriptionReminder2($this->subscriptionReminder2);
        $propositionTerm->setSubscriptionReminder3($this->subscriptionReminder3);

        if ($propositionTerm instanceof FlexiblePropositionTerm) {
            $this->toFlexiblePropositionTerm($propositionTerm);
        }
    }

    public function toNewPropositionTerm(): AbstractPropositionTerm
    {
        $propositionTerm = new FlexiblePropositionTerm(
            $this->getDuration(),
            $this->permanent,
            $this->getFromMoneyAmount());

        $this->toPropositionTerm($propositionTerm);

        return $propositionTerm;
    }

    public function toFlexiblePropositionTerm(FlexiblePropositionTerm $propositionTerm): void
    {
        $propositionTerm->setDuration($this->getDuration());
        $propositionTerm->setPermanent($this->permanent);
    }

    private function getDuration(): Duration
    {
        if ($this->endDate) {
            $duration = new FixedEndDateDuration(Date::fromDateTime($this->endDate));
        }

        if ($this->days || $this->weeks || $this->months || $this->years) {
            $duration = new IntervalDuration(
                $this->days ?: 0,
                $this->weeks ?: 0,
                $this->months ?: 0,
                $this->years ?: 0);
        }

        if (isset($duration)) {
            return $duration;
        } else {
            throw new \DomainException('FlexiblePropositionTerm must have either an endDate or at least one of days, weeks, months, years');
        }
    }

    private function getFromMoneyAmount(): Money
    {
        return Money::amount($this->fromAmount, Currency::EUR());
    }
}
