<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\CustomerGroup;

/**
 * @package App\Form\DataObject
 */
class CustomerGroupDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var string */
    public $code;

    /** @var string */
    public $name;

    /** @var int */
    public $ageFrom;

    /** @var int */
    public $ageTo;

    public static function fromCustomerGroup(CustomerGroup $customerGroup): CustomerGroupDataObject
    {
        $customerGroupDataObject = new self();
        $customerGroupDataObject->code = $customerGroup->getCode();
        $customerGroupDataObject->name = $customerGroup->getName();
        $customerGroupDataObject->ageFrom = $customerGroup->getAgeFrom();
        $customerGroupDataObject->ageTo = $customerGroup->getAgeTo();

        return $customerGroupDataObject;
    }

    public function toCustomerGroup(CustomerGroup $customerGroup): void
    {
        $customerGroup->setCode($this->code);
        $customerGroup->setName($this->name);
        $customerGroup->setAgeFrom($this->ageFrom);
        $customerGroup->setAgeTo($this->ageTo);

    }
}
