<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\EmailTemplate;

/**
 * @package App\Form\DataObject
 */
class EmailTemplateDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var string */
    public $id;

    /** @var string
     */
    public $name;

    /** @var string */
    public $description;

    /** @var string */
    public $emailSender;

    /** @var string */
    public $subject;

    /** @var string */
    public $body;

    public static function fromEmailTemplate(EmailTemplate $emailTemplate): EmailTemplateDataObject
    {
        $dataObject = new self();
        $dataObject->id = $emailTemplate->getId();
        $dataObject->name = $emailTemplate->getName();
        $dataObject->description = $emailTemplate->getDescription();
        $dataObject->emailSender = $emailTemplate->getEmailSender();
        $dataObject->subject = $emailTemplate->getSubject();
        $dataObject->body = $emailTemplate->getBody();

        return $dataObject;
    }

    public function toEmailTemplate(EmailTemplate $emailTemplate): void
    {
        $emailTemplate->setName($this->name);
        $emailTemplate->setDescription($this->description);
        $emailTemplate->setEmailSender($this->emailSender);
        $emailTemplate->setSubject($this->subject);
        $emailTemplate->setBody($this->body);

    }
}
