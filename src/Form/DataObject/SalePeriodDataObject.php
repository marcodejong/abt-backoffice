<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Value\Date;
use Arriva\Abt\Entity\SalePeriod;

/**
 * @package App\Form\DataObject
 */
class SalePeriodDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string
     */
    public $name;

    /** @var \DateTimeInterface */
    public $fromDate;

    /** @var \DateTimeInterface */
    public $toDate;

    public static function fromSalePeriod(SalePeriod $salePeriod): SalePeriodDataObject
    {
        $dataObject = new self();
        $dataObject->id = $salePeriod->getId();
        $dataObject->name = $salePeriod->getName();
        $dataObject->fromDate = $salePeriod->getFromDate()->getDateTime();
        $dataObject->toDate = $salePeriod->getToDate()->getDateTime();

        return $dataObject;
    }

    public function toSalePeriod(SalePeriod $salePeriod): void
    {
        $salePeriod->setName($this->name);
        $salePeriod->setFromDate(Date::fromDateTime($this->fromDate));
        $salePeriod->setToDate(Date::fromDateTime($this->toDate));

    }
}
