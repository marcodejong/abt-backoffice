<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Region;
use Arriva\Abt\Entity\Zone;
use Doctrine\Common\Collections\Collection;


/**
 * @package App\Form\DataObject
 */
class ZoneDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var boolean */
    public $active;

    /** @var int */
    public $code;

    /** @var Region[]|Collection */
    public $regions;


    public static function fromZone(Zone $zone): ZoneDataObject
    {
        $zoneDataObject = new self();
        $zoneDataObject->id = $zone->getId();
        $zoneDataObject->name = $zone->getName();
        $zoneDataObject->code = $zone->getCode();
        $zoneDataObject->active = $zone->getActive();
        $zoneDataObject->regions = $zone->getRegions();
        return $zoneDataObject;
    }

    public function toZone(Zone $zone): void
    {
        $zone->setName($this->name);
        $zone->setCode($this->code);
        $zone->setActive($this->active);
        $zone->setRegions($this->regions);

    }
}
