<?php


namespace App\Form\DataObject;


use App\Form\Value\ExportFormType;
use App\Model\Report\TariffConfiguration\TariffConfigurationType;
use Arriva\Abt\Entity\PricePeriod;

class ExportDataObject
{
    /** @var ExportFormType|null */
    public $exportType;

    /** @var TariffConfigurationType|null */
    public $configurationType;

    /** @var PricePeriod|null */
    public $pricePeriod;

    /** @var \DateTime|null */
    public $date;

    public function getExportType(): ?ExportFormType
    {
        return $this->exportType;
    }

    public function getConfigurationType(): ?TariffConfigurationType
    {
        return $this->configurationType;
    }

    public function getPricePeriod(): ?PricePeriod
    {
        return $this->pricePeriod;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setExportType(?string $exportType): void
    {
        $this->exportType = $exportType ? ExportFormType::instance($exportType) : null;
    }

    public function setConfigurationType(?string $configurationType): void
    {
        $this->configurationType = $configurationType ? TariffConfigurationType::instance($configurationType) : null;
    }

    public function setPricePeriod(?PricePeriod $pricePeriod): void
    {
        $this->pricePeriod = $pricePeriod;
    }

    public function setDate(?\DateTime $date): void
    {
        $this->date = $date ?: '';
    }
}