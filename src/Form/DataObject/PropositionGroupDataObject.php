<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Modality;
use Arriva\Abt\Entity\PropositionCategory;
use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\Entity\PropositionType;
use Arriva\Abt\Entity\Region;
use Arriva\Abt\Entity\StarValue;
use Arriva\Abt\Entity\StarValueSet;
use Arriva\Abt\Entity\VatRate;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @package App\Form\DataObject
 */
class PropositionGroupDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $category;

    /** @var string */
    public $termsLink;

    /** @var string */
    public $description;

    /** @var int */
    public $order;

    /** @var StarValue[] */
    public $starValues;

    /** @var bool */
    public $salesEnabled;

    /** @var VatRate */
    public $vatRate;

    /** @var PropositionType */
    public $propositionType;

    /** @var ArrayCollection|Region[] */
    public $regions;

    /** @var ArrayCollection|Modality[] */
    public $modalities;

    public function __construct()
    {
        $this->starValues = [];
        $this->regions = new ArrayCollection();
        $this->modalities = new ArrayCollection();
    }

    public static function fromPropositionGroup(PropositionGroup $propositionGroup): PropositionGroupDataObject
    {
        $propositionGroupDataObject = new self();
        $propositionGroupDataObject->name = $propositionGroup->getName();
        $propositionGroupDataObject->category = $propositionGroup->getCategory()->getId();
        $propositionGroupDataObject->termsLink = $propositionGroup->getTermsLink();
        $propositionGroupDataObject->description = $propositionGroup->getDescription();
        $propositionGroupDataObject->order = $propositionGroup->getOrder();
        $propositionGroupDataObject->starValues = $propositionGroup->getStarValues()->toArray();
        $propositionGroupDataObject->salesEnabled = $propositionGroup->getSalesEnabled();
        $propositionGroupDataObject->vatRate = $propositionGroup->getVatRate();
        $propositionGroupDataObject->propositionType = $propositionGroup->getPropositionType();
        $propositionGroupDataObject->regions = $propositionGroup->getRegions();
        $propositionGroupDataObject->modalities = $propositionGroup->getModalities();

        return $propositionGroupDataObject;
    }

    /**
     * @param PropositionGroup $propositionGroup
     * @throws \ConfigurationException
     */
    public function toPropositionGroup(PropositionGroup $propositionGroup): void
    {
        $propositionGroup->setName($this->name);
        $propositionGroup->setCategory(PropositionCategory::instance($this->category));
        $propositionGroup->setTermsLink($this->termsLink);
        $propositionGroup->setDescription($this->description);
        $propositionGroup->setOrder($this->order);
        $propositionGroup->setStarValues(new StarValueSet($this->starValues));
        $propositionGroup->setSalesEnabled($this->salesEnabled);
        $propositionGroup->setVatRate($this->vatRate);
        $propositionGroup->setPropositionType($this->propositionType);
        $propositionGroup->setRegions($this->regions);
        $propositionGroup->setModalities($this->modalities);
    }
}
