<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Discount;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Value\Discount\DiscountType;
use Arriva\Abt\Value\Discount\DiscountCode;
use Arriva\Abt\Value\Date;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @package App\Form\DataObject
 */
class DiscountDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $code;

    /** @var string */
    public $type;

    /** @var \DateTimeInterface */
    public $startDate;

    /** @var \DateTimeInterface */
    public $endDate;

    /** @var bool */
    public $percentage;

    /** @var int */
    public $amount;

    /** @var ArrayCollection|Proposition[] */
    public $propositions;

    /**
     * DiscountDataObject constructor.
     */
    public function __construct()
    {
        $this->propositions = new ArrayCollection();
    }

    public static function fromDiscount(Discount $discount): DiscountDataObject
    {
        $discountDataObject = new self();
        $discountDataObject->id = $discount->getId();
        $discountDataObject->code = $discount->getCode()->getCode();
        $discountDataObject->type = $discount->getType()->getId();
        $discountDataObject->startDate = $discount->getStartDate()->getAsDateTime();
        $discountDataObject->endDate = $discount->getEndDate()->getAsDateTime();
        $discountDataObject->percentage = $discount->isPercentage();
        $discountDataObject->amount = $discount->getAmount();
        $discountDataObject->propositions = $discount->getPropositions();

        return $discountDataObject;
    }

    public function toDiscount(Discount $discount): void
    {
        $discount->setCode(new DiscountCode($this->code));
        $type = $this->type === 'UNIQUE' ? DiscountType::UNIQUE() : DiscountType::GENERIC();
        $discount->setType($type);
        $discount->setStartDate(Date::fromDateTime($this->startDate));
        $discount->setEndDate(Date::fromDateTime($this->endDate));
        $discount->setPercentage($this->percentage);
        $discount->setAmount($this->amount);
        $discount->setPropositions($this->propositions);
    }
}
