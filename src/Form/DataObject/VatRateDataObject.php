<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\VatRate;

/**
 * @package App\Form\DataObject
 */
class VatRateDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $code;

    /** @var float */
    public $percentage;

    public static function fromVatRate(VatRate $vatRate): VatRateDataObject
    {
        $dataObject = new self();
        $dataObject->id = $vatRate->getId();
        $dataObject->name = $vatRate->getName();
        $dataObject->code = $vatRate->getCode();
        $dataObject->percentage = $vatRate->getPercentage();

        return $dataObject;
    }

    public function toVatRate(VatRate $vatRate): void
    {
        $vatRate->setName($this->name);
        $vatRate->setCode($this->code);
        $vatRate->setPercentage($this->percentage);

    }
}
