<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\PropositionType;

/**
 * @package App\Form\DataObject
 */
class PropositionTypeDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string
     */
    public $name;

    /** @var string */
    public $description;

    /** @var int */
    public $order;

    public static function fromPropositionType(PropositionType $propositionType): PropositionTypeDataObject
    {
        $dataObject = new self();
        $dataObject->id = $propositionType->getId();
        $dataObject->name = $propositionType->getName();
        $dataObject->description = $propositionType->getDescription();
        $dataObject->order = $propositionType->getOrder();

        return $dataObject;
    }

    public function toPropositionType(PropositionType $propositionType): void
    {
        $propositionType->setName($this->name);
        $propositionType->setDescription($this->description);
        $propositionType->setOrder($this->order);

    }
}
