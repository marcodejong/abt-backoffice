<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;


/**
 * @package App\Form\DataObject
 */
class FileDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    private $file;

    public function setFile($file): void
    {
        $this->file = $file;
    }

    public function getFile() {
        return $this->file;
    }
}
