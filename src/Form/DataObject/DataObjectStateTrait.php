<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

/**
 * @package App\Form\DataObject
 */
trait DataObjectStateTrait
{
    /** @var null|string */
    private $state;

    /**
     * @throws \LogicException
     * @return bool
     */
    public function isNewObject(): bool
    {
        $this->assertState();

        return ObjectStates::STATE_NEW === $this->state;
    }

    /**
     * @throws \LogicException
     * @return bool
     */
    public function isModifiedObject(): bool
    {
        $this->assertState();

        return ObjectStates::STATE_MODIFIED === $this->state;
    }

    public function markAsNewObject(): void
    {
        $this->state = ObjectStates::STATE_NEW;
    }

    public function markAsModifiedObject(): void
    {
        $this->state = ObjectStates::STATE_MODIFIED;
    }

    /**
     * @throws \LogicException
     */
    private function assertState(): void
    {
        if (null === $this->state) {
            throw new \LogicException(
                'State is not explicitly set so can not determine if this is a new or modified object'
            );
        }
    }
}
