<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Modality;

/**
 * @package App\Form\DataObject
 */
class ModalityDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var string
     */
    public $name;

    public static function fromModality(Modality $modality): ModalityDataObject
    {
        $dataObject = new self();
        $dataObject->name = $modality->getName();

        return $dataObject;
    }

    public function toModality(Modality $modality): void
    {
        $modality->setName($this->name);

    }
}
