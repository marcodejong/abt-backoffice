<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\WebshopRegion;

/**
 * @package App\Form\DataObject
 */
class WebshopRegionDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var string */
    public $name;

    /** @var int */
    public $externalId;

    /** @var string */
    public $externalName;

    public static function fromWebshopRegion(WebshopRegion $webshopRegion): WebshopRegionDataObject
    {
        $webshopRegionDataObject = new self();
        $webshopRegionDataObject->name = $webshopRegion->getName();
        $webshopRegionDataObject->externalId = $webshopRegion->getExternalId();
        $webshopRegionDataObject->externalName = $webshopRegion->getExternalName();

        return $webshopRegionDataObject;
    }

    public function toWebshopRegion(WebshopRegion $webshopRegion): void
    {
        $webshopRegion->setName($this->name);
        $webshopRegion->setExternalId($this->externalId);
        $webshopRegion->setExternalName($this->externalName);

    }
}
