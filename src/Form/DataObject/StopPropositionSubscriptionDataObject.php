<?php

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Subscription;
use Arriva\Abt\Utility\Currency;
use Arriva\Abt\Utility\Money;

class StopPropositionSubscriptionDataObject
{
    /** @var \DateTimeImmutable */
    public $endDate;

    /** @var Money */
    public $creditAmount;

    /** @var Subscription */
    public $subscription;

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
        $this->endDate = new \DateTimeImmutable();
        $this->creditAmount = Money::zero(Currency::EUR());
    }
}
