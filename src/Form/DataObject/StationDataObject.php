<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Region;
use Arriva\Abt\Entity\Station;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @package App\Form\DataObject
 */
class StationDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var string */
    public $name;

    /** @var int */
    public $code;

    /** @var Region[]|Collection */
    public $regions;

    public function __construct()
    {
        $this->regions = new ArrayCollection();
    }

    public static function fromStation(Station $station): StationDataObject
    {
        $stationDataObject = new self();
        $stationDataObject->name = $station->getName();
        $stationDataObject->code = $station->getCode();
        $stationDataObject->regions = $station->getRegions();

        return $stationDataObject;
    }

    public function toStation(Station $station): void
    {
        $station->setName($this->name);
        $station->setCode($this->code);
        $station->setRegions($this->regions);
    }

}
