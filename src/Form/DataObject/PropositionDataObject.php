<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\Configuration\PropositionTerm\AbstractPropositionTerm;
use Arriva\Abt\Entity\CustomerGroup;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Entity\PropositionElement;
use Arriva\Abt\Entity\PropositionGroup;
use Arriva\Abt\Entity\SalePeriod;
use Arriva\Abt\Entity\ValidityPeriod;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @package App\Form\DataObject
 */
class PropositionDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $tAccount;

    /** @var bool */
    public $paymentByInstallmentsAllowed;

    /** @var string */
    public $refundPeriod;

    /** @var int */
    public $refundPeriodValue;

    /** @var ArrayCollection|CustomerGroup[] */
    public $customerGroups;

    /** @var ArrayCollection|PropositionElement[] */
    public $propositionElements;

    /** @var PropositionGroup */
    public $propositionGroup;

    /** @var SalePeriod */
    public $salePeriod;

    /** @var ValidityPeriod */
    public $validityPeriod;

    /** @var AbstractPropositionTerm */
    public $propositionTerm;

    public function __construct()
    {
        $this->customerGroups = new ArrayCollection();
        $this->propositionElements = new ArrayCollection();
    }

    public static function fromProposition(Proposition $proposition): PropositionDataObject
    {
        $propositionDataObject = new self();
        $propositionDataObject->id = $proposition->getId();
        $propositionDataObject->name = $proposition->getName();
        $propositionDataObject->tAccount = $proposition->getTAccount();
        $propositionDataObject->paymentByInstallmentsAllowed = $proposition->isPaymentByInstallmentsAllowed();
        $propositionDataObject->refundPeriod = $proposition->getRefundPeriod();
        $propositionDataObject->refundPeriodValue = $proposition->getRefundPeriodValue();
        $propositionDataObject->customerGroups = $proposition->getCustomerGroups();
        $propositionDataObject->propositionElements = $proposition->getPropositionElements();
        $propositionDataObject->propositionGroup = $proposition->getPropositionGroup();
        $propositionDataObject->propositionTerm = $proposition->getPropositionTerm();
        $propositionDataObject->salePeriod = $proposition->getSalePeriod();
        $propositionDataObject->validityPeriod = $proposition->getValidityPeriod();

        return $propositionDataObject;
    }

    public function toProposition(Proposition $proposition): void
    {
        $proposition->setName($this->name);
        $proposition->setTAccount($this->tAccount);
        $proposition->setPaymentByInstallmentsAllowed($this->paymentByInstallmentsAllowed);
        $this->refundPeriodValue
            ? $proposition->setRefundPeriod($this->refundPeriod)
            : $proposition->setRefundPeriod(null);
        $proposition->setRefundPeriodValue($this->refundPeriodValue);
        $proposition->setCustomerGroups($this->customerGroups);
        $proposition->setPropositionElements($this->propositionElements);
        $proposition->setPropositionGroup($this->propositionGroup);
        $proposition->setPropositionTerm($this->propositionTerm);
        $proposition->setSalePeriod($this->salePeriod);
        $proposition->setValidityPeriod($this->validityPeriod);
    }

    public function toNewProposition(): Proposition
    {
        $proposition = new Proposition(
            $this->name,
            $this->paymentByInstallmentsAllowed,
            $this->propositionGroup,
            $this->propositionTerm,
            $this->validityPeriod,
            $this->salePeriod
        );

        $proposition->setTAccount($this->tAccount);
        $proposition->setRefundPeriod($this->refundPeriod);
        $proposition->setRefundPeriodValue($this->refundPeriodValue);
        $proposition->setCustomerGroups($this->customerGroups);
        $proposition->setPropositionElements($this->propositionElements);

        return $proposition;
    }
}
