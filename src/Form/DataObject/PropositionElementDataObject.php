<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Entity\PropositionElement;

/**
 * @package App\Form\DataObject
 */
class PropositionElementDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $description;

    /** @var string */
    public $peCode;

    public static function fromPropositionElement(PropositionElement $propositionElement): PropositionElementDataObject
    {
        $dataObject = new self();
        $dataObject->id = $propositionElement->getId();
        $dataObject->name = $propositionElement->getName();
        $dataObject->peCode = $propositionElement->getPeCode();
        $dataObject->description = $propositionElement->getDescription();
        return $dataObject;
    }

    public function toPropositionElement(PropositionElement $propositionElement): void
    {
        $propositionElement->setName($this->name);
        $propositionElement->setPeCode($this->peCode);
        $propositionElement->setDescription($this->description);

    }
}
