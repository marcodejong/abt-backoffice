<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

/**
 * @package App\Form\DataObject
 */
interface DataObjectStateInterface
{
    public function isNewObject(): bool;

    public function isModifiedObject(): bool;
}
