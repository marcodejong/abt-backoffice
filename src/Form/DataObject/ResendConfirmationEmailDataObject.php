<?php

namespace App\Form\DataObject;

/**
 * Class ResendConfirmationEmailDataObject
 * @package App\Form\DataObject
 */
class ResendConfirmationEmailDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var string */
    public $email;

}
