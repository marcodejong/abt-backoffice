<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Value\Date;
use Arriva\Abt\Entity\PricePeriod;

/**
 * @package App\Form\DataObject
 */
class PricePeriodDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string
     */
    public $name;

    /** @var \DateTimeInterface */
    public $fromDate;

    /** @var \DateTimeInterface */
    public $toDate;

    public static function fromPricePeriod(PricePeriod $pricePeriod): PricePeriodDataObject
    {
        $dataObject = new self();
        $dataObject->id = $pricePeriod->getId();
        $dataObject->name = $pricePeriod->getName();
        $dataObject->fromDate = $pricePeriod->getFromDate()->getDateTime();
        $dataObject->toDate = $pricePeriod->getToDate()->getDateTime();

        return $dataObject;
    }

    public function toPricePeriod(PricePeriod $pricePeriod): void
    {
        $pricePeriod->setName($this->name);
        $pricePeriod->setFromDate(Date::fromDateTime($this->fromDate));
        $pricePeriod->setToDate(Date::fromDateTime($this->toDate));

    }
}
