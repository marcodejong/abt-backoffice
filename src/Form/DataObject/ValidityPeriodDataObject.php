<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form\DataObject;

use Arriva\Abt\Value\Date;
use Arriva\Abt\Entity\ValidityPeriod;

/**
 * @package App\Form\DataObject
 */
class ValidityPeriodDataObject implements DataObjectStateInterface
{
    use DataObjectStateTrait;

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var \DateTimeInterface */
    public $fromDate;

    /** @var \DateTimeInterface */
    public $toDate;

    public static function fromValidityPeriod(ValidityPeriod $validityPeriod): ValidityPeriodDataObject
    {
        $dataObject = new self();
        $dataObject->id = $validityPeriod->getId();
        $dataObject->name = $validityPeriod->getName();
        $dataObject->fromDate = $validityPeriod->getFromDate()->getDateTime();
        $dataObject->toDate = $validityPeriod->getToDate()->getDateTime();

        return $dataObject;
    }

    public function toValidityPeriod(ValidityPeriod $validityPeriod): void
    {
        $validityPeriod->setName($this->name);
        $validityPeriod->setFromDate(Date::fromDateTime($this->fromDate));
        $validityPeriod->setToDate(Date::fromDateTime($this->toDate));

    }
}
