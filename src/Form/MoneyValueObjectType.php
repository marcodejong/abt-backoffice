<?php

namespace App\Form;

use App\Form\DataTransformer\MoneyValueObjectToLocalizedStringTransformer;
use Arriva\Abt\Utility\Currency;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;

class MoneyValueObjectType extends MoneyType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addViewTransformer(new MoneyValueObjectToLocalizedStringTransformer(
                $options['scale'],
                $options['grouping'],
                $options['rounding_mode'],
                $options['divisor']
            ))
        ;
    }
}
