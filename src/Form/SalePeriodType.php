<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\SalePeriodDataObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class SalePeriodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'fromDate',
                DatePickerType::class,
                [
                    'label' => 'form.administration.field.startdate.label',
                    'required' => true,
                ]
            )
            ->add(
                'toDate',
                DatePickerType::class,
                [
                    'label' => 'form.administration.field.enddate.label',
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => SalePeriodDataObject::class]);
    }
}
