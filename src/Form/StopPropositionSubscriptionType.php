<?php

namespace App\Form;

use App\Form\DataObject\StopPropositionSubscriptionDataObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StopPropositionSubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'endDate',
                DatePickerType::class,
                [
                    'label' => 'New end date',
                    'required' => true,
                ]
            )
            ->add(
                'creditAmount',
                MoneyValueObjectType::class,
                [
                    'label' => 'Credit amount',
                    'attr' => [
                        'class' => 'money',
                    ],
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => StopPropositionSubscriptionDataObject::class,
                'translation_domain' => 'customer-service',
            ]
        );
    }
}
