<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\PropositionTermDataObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class PropositionTermType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'code',
                TextType::class,
                [
                    'label' => 'form.administration.field.code.label',
                    'required' => true,
                ]
            )
            ->add(
                'days',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.days.label',
                    'required' => false
                ]
            )
            ->add(
                'weeks',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.weeks.label',
                    'required' => false
                ]
            )
            ->add(
                'months',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.months.label',
                    'required' => false
                ]
            )
            ->add(
                'years',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.years.label',
                    'required' => false
                ]
            )
            ->add(
                'endDate',
                DatePickerType::class,
                [
                    'label' => 'form.administration.proposition-term.field.end-date.label',
                    'required' => false,
                    'maxDate' => '31-12-2050',
                    'minDate' => 'today'
                ]
            )
            ->add(
                'permanent',
                CheckboxType::class,
                [
                    'label' => 'form.administration.proposition-term.field.permanent.label',
                    'required' => false
                ]
            )
            ->add(
                'fromAmount',
                CurrencyPriceType::class,
                [
                    'label' => 'form.administration.field.payment-by-installments.payment-by-installment-from.label',
                    'required' => true,
                ]
            )
            ->add(
                'subscriptionReminder1',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.subscriptionreminder1.label',
                    'required' => false,
                ]
            )
            ->add(
                'subscriptionReminder2',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.subscriptionreminder2.label',
                    'required' => false,
                ]
            )
            ->add(
                'subscriptionReminder3',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.subscriptionreminder3.label',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => PropositionTermDataObject::class]);
    }
}
