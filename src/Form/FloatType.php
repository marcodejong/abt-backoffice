<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Exception\InvalidArgumentException;
use Symfony\Component\Translation\TranslatorInterface;

class FloatType extends AbstractType
{
    /** @var TranslatorInterface */
    protected $translator;

    /** @var array */
    protected $options = [];

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->options = $options;
        $builder->addModelTransformer(
            new CallbackTransformer(
                function ($value) {
                    return (string)$value;
                },
                function ($value) {
                    return (float)\str_replace(',', '.', $value);
                }
            )
        );
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'preSubmit']);
    }

    /**
     * @param FormEvent $event
     * @throws InvalidArgumentException
     * @return void
     */
    public function preSubmit(FormEvent $event): void
    {
        $value = \str_replace(',', '.', $event->getData());
        if ('' === $value && false === $this->options['required']) {
            return;
        }

        if ($messageId = $this->preSubmitMessageId($value)) {
            $messageParams = ['decimalPrecision' => $this->options['decimal_precision']];
            $event->getForm()->addError(
                new FormError($this->translator->trans($messageId, $messageParams, 'validators'))
            );
        }
    }

    protected function preSubmitMessageId(string $value): ?string
    {
        if (!\is_numeric($value)) {
            return 'form.validator.float.notdigits';
        }

        $parts = \explode($this->options['decimal_separator_localized'], $value);
        $decimal = array_key_exists(1, $parts) ? $parts[1] : '0';
        if (\strlen($decimal) > $this->options['decimal_precision']) {
            return 'form.validator.float.invalid-value';
        }

        return null;
    }

    /**
     * @param OptionsResolver $resolver
     * @throws UndefinedOptionsException
     * @throws AccessException
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefined(
                [
                    'decimal_precision',
                    'decimal_separator_localized',
                    'group_separator_localized',
                ]
            )
            ->setAllowedTypes('decimal_precision', 'int')
            ->setAllowedTypes('decimal_separator_localized', 'string')
            ->setAllowedValues('decimal_separator_localized', [',', '.'])
            ->setAllowedTypes('group_separator_localized', 'string')
            ->setAllowedValues('group_separator_localized', ['.', ','])
            ->setDefaults(
                [
                    'decimal_precision' => 2,
                    'decimal_separator_localized' => ',',
                    'group_separator_localized' => '.',
                ]
            );
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['decimal_separator_localized'] = $options['decimal_separator_localized'];
        $view->vars['group_separator_localized'] = $options['group_separator_localized'];
        $view->vars['decimal_precision'] = $options['decimal_precision'];
        $view->vars['attr']['class'] = \array_key_exists('class', $view->vars['attr'])
            ? \trim($view->vars['attr']['class']) . ' float'
            : 'float';
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
