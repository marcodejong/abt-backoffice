<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\WebshopRegionDataObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class WebshopRegionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'form.administration.field.name.label',
                    'required' => true,
                ]
            )
            ->add(
                'externalId',
                IntegerType::class,
                [
                    'label' => 'form.administration.field.external-id.label',
                    'required' => false,
                ]
            )
            ->add(
                'externalName',
                TextType::class,
                [
                    'label' => 'form.administration.field.external-name.label',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => WebshopRegionDataObject::class]);
    }
}
