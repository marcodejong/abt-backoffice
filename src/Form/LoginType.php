<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\Builder\ChoicesBuilder;
use App\Manager\LocaleManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @package App\Form
 */
class LoginType extends AbstractType
{
    /** @var string */
    private $csrfTokenId;

    /** @var TranslatorInterface */
    private $translator;

    /** @var LocaleManager */
    private $localeManager;

    public function __construct(string $csrfTokenId, TranslatorInterface $translator, LocaleManager $localeManager)
    {
        $this->csrfTokenId = $csrfTokenId;
        $this->translator = $translator;
        $this->localeManager = $localeManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $languageChoices = ChoicesBuilder::create()
            ->map(
                function (string $locale) {
                    return [
                        'id' => $locale,
                        'name' => $this->translator->trans('common.language.' . $locale, [], 'messages', $locale),
                    ];
                }
            )
            ->build($this->localeManager->getAvailableLanguageCodes());

        $builder
            ->add(
                '_username',
                TextType::class,
                [
                    'label' => 'form.common.field.username.label',
                    'required' => true,
                    'attr' => [
                        'id' => 'userName',
                        'autofocus' => true,
                    ],
                ]
            )
            ->add(
                '_password',
                PasswordType::class,
                [
                    'label' => 'form.common.field.password.label',
                    'required' => true,
                    'attr' => [
                        'id' => 'password',
                    ],
                ]
            )
            ->add(
                '_locale',
                ChoiceType::class,
                [
                    'choices' => $languageChoices,
                    'choice_translation_domain' => false,
                    'label' => 'form.common.field.language.label',
                    'required' => true,
                    'attr' => [
                        'id' => 'languageSelection',
                    ],
                ]
            )
            ->add(
                'login',
                SubmitType::class,
                [
                    'label' => 'common.authentication.form.login.submit',
                    'attr' => [
                        'id' => 'login',
                    ],
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     * @throws AccessException
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'attr' => ['id' => 'LoginForm'],
                'csrf_protection' => true,
                'csrf_field_name' => '_csrf_token',
                'csrf_token_id' => $this->csrfTokenId,
            ]
        );
    }
}
