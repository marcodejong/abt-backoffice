<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Form\DataObject\DiscountDataObject;
use Arriva\Abt\Entity\Proposition;
use Arriva\Abt\Value\Discount\DiscountType AS Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class DiscountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'code',
                TextType::class,
                [
                    'label' => 'form.administration.discount.field.code.label',
                    'required' => true,
                    'attr' => ['style' => 'width:100%']
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => 'form.administration.discount.field.type.label',
                    'required' => true,
                    'choices' => [
                        'Generiek' => Type::GENERIC()->getId(),
                        'Uniek' => Type::UNIQUE()->getId(),
                    ],
                    'attr' => ['style' => 'width:100%']
                ]
            )
            ->add(
                'startDate',
                DatePickerType::class,
                [
                    'label' => 'form.administration.discount.field.start-date.label',
                    'required' => true,
                ]
            )
            ->add(
                'endDate',
                DatePickerType::class,
                [
                    'label' => 'form.administration.discount.field.end-date.label',
                    'required' => true,
                ]
            )
            ->add(
                'percentage',
                ChoiceType::class,
                [
                    'label' => 'form.administration.discount.field.percentage.label',
                    'required' => true,
                    'choices' => [
                        'form.administration.discount.field.percentage.choice.euro' => false,
                        'form.administration.discount.field.percentage.choice.percentage' => true
                    ],
                    'attr' => ['style' => 'width:125px']
                ]
            )
            ->add(
                'amount',
                CurrencyPriceType::class,
                [
                    'label' => 'form.administration.discount.field.amount.label',
                    'required' => true,
                    'attr' => ['style' => 'width:125px; max-width:125px; margin-left:0'],
                    'label_attr' => ['style' => 'height:2em']
                ]
            )
            ->add(
                'propositions',
                EntityType::class,
                [
                    'label' => 'form.administration.discount.field.propositions.label',
                    'required' => false,
                    'placeholder' => 'form.element.select.options.first',
                    'class' => Proposition::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'attr' => ['style' => 'display:none'],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => DiscountDataObject::class]);
    }
}
