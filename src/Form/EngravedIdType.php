<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use App\Validator\Constraints\EngravedId;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EngravedIdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer(
            new CallbackTransformer(
                function ($value) {
                    $value = $value ?: '3528';
                    $parts = \str_split($value, 4);

                    return [
                        'part1' => array_key_exists(0, $parts) ? $parts[0] : '',
                        'part2' => array_key_exists(1, $parts) ? $parts[1] : '',
                        'part3' => array_key_exists(2, $parts) ? $parts[2] : '',
                        'part4' => array_key_exists(3, $parts) ? $parts[3] : '',
                    ];
                },
                function ($value) {
                    return \implode('', $value);
                }
            )
        );

        $builder->add('part1', TextType::class, $this->getPartOptions('first'));
        $builder->add('part2', TextType::class, $this->getPartOptions());
        $builder->add('part3', TextType::class, $this->getPartOptions());
        $builder->add('part4', TextType::class, $this->getPartOptions('last'));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'error_bubbling' => false,
                'constraints' => [new EngravedId()],
            ]
        );
    }

    private function getPartOptions(string $class = null): array
    {
        return [
            'attr' => [
                'class' => 'multitext' . (null !== $class ? ' ' . $class : ''),
                'maxlength' => 4,
            ],
        ];
    }
}
