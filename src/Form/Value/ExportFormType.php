<?php


namespace App\Form\Value;


use Arriva\Abt\Utility\Enum;

class ExportFormType extends Enum
{
    const DEBTOR_RECEIVABLES = 'debtor-receivables';
    const INVOICED_TRANSACTIONS = 'invoiced-transactions';
    const JOURNAL = 'journal';
    const JOURNAL_TRANSACTIONS = 'journal-transactions';
    const MONTHLY_JOURNAL = 'monthly-journal';
    const TARIFF_CONFIGURATION = 'tariff-configuration';

    public static function DEBTOR_RECEIVABLES(): self
    {
        return self::define(self::DEBTOR_RECEIVABLES);
    }

    public static function INVOICED_TRANSACTIONS(): self
    {
        return self::define(self::INVOICED_TRANSACTIONS);
    }

    public static function JOURNAL(): self
    {
        return self::define(self::JOURNAL);
    }

    public static function JOURNAL_TRANSACTIONS(): self
    {
        return self::define(self::JOURNAL_TRANSACTIONS);
    }

    public static function MONTHLY_JOURNAL(): self
    {
        return self::define(self::MONTHLY_JOURNAL);
    }

    public static function TARIFF_CONFIGURATION(): self
    {
        return self::define(self::TARIFF_CONFIGURATION);
    }
}