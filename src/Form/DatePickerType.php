<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Form
 */
class DatePickerType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     * @throws UndefinedOptionsException
     * @throws AccessException
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefined(
                [
                    'showButtonPanel',
                    'showWeek',
                    'changeMonth',
                    'changeYear',
                    'firstDay',
                    'prevText',
                    'nextText',
                    'dateFormat',
                    'showAnim',
                    'minDate',
                    'maxDate',
                ]
            )
            ->setAllowedTypes('showButtonPanel', 'bool')
            ->setAllowedTypes('showWeek', 'bool')
            ->setAllowedTypes('changeMonth', 'bool')
            ->setAllowedTypes('changeYear', 'bool')
            ->setAllowedTypes('firstDay', 'int')
            ->setAllowedTypes('prevText', 'string')
            ->setAllowedTypes('nextText', 'string')
            ->setAllowedTypes('dateFormat', 'string')
            ->setAllowedTypes('showAnim', 'string')
            ->setAllowedTypes('minDate', [\DateTimeInterface::class, 'string', 'null'])
            ->setAllowedTypes('maxDate', [\DateTimeInterface::class, 'string', 'null'])
            ->setDefaults(
                [
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'html5' => false,
                    'showButtonPanel' => false,
                    'showWeek' => true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'firstDay' => 1,
                    'prevText' => '<',
                    'nextText' => '>',
                    'dateFormat' => 'dd-mm-yy',
                    'showAnim' => 'slide',
                    'minDate' => null,
                    'maxDate' => null,
                ]
            );
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['attr']['class'] = \array_key_exists('class', $view->vars['attr'])
            ? \trim($view->vars['attr']['class']) . ' datepicker' : 'datepicker';

        $datePickerOptions = new \StdClass();
        $datePickerOptions->showButtonPanel = $options['showButtonPanel'];
        $datePickerOptions->showWeek = $options['showWeek'];
        $datePickerOptions->changeMonth = $options['changeMonth'];
        $datePickerOptions->changeYear = $options['changeYear'];
        $datePickerOptions->firstDay = $options['firstDay'];
        $datePickerOptions->prevText = $options['prevText'];
        $datePickerOptions->nextText = $options['nextText'];
        $datePickerOptions->dateFormat = $options['dateFormat'];
        $datePickerOptions->showAnim = $options['showAnim'];
        $datePickerOptions->minDate = $this->getDate($options['minDate']);
        $datePickerOptions->maxDate = $this->getDate($options['maxDate']);

        $view->vars['date_picker_options'] = $datePickerOptions;
    }

    private function getDate($date): ?string
    {
        return $date instanceof \DateTimeInterface ? $date->format('d-m-Y') : $date;
    }

    public function getParent(): string
    {
        return DateType::class;
    }
}
