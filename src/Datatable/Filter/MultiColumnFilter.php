<?php


namespace App\Datatable\Filter;


use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MultiColumnFilter extends TextFilter
{
    /** @var string[] */
    protected $columns = [];

    public function addAndExpression(Andx $andExpr, QueryBuilder $qb, $searchField, $searchValue, $searchTypeOfField, &$parameterCounter)
    {
        $orExpr = new Orx();
        $parameterCounter++;

        foreach($this->columns as $column) {
            $orExpr->add($qb->expr()->like($column, '?'.$parameterCounter));
            $qb->setParameter($parameterCounter, '%'.$searchValue.'%');
        }

        return $andExpr->add($orExpr);
    }


    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(array(
            'columns' => []
        ));

        $resolver->setAllowedTypes('columns', 'array');

        return $this;
    }

    /**
     * @return string[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param string[] $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }
}