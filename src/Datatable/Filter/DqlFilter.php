<?php


namespace App\Datatable\Filter;


use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DqlFilter extends TextFilter
{
    /** @var string */
    protected $dql;

    /** @var string|null */
    protected $fieldType;

    /** @var string[] */
    protected $parameters = [];

    public function addAndExpression(Andx $andExpr, QueryBuilder $qb, $searchField, $searchValue, $searchTypeOfField, &$parameterCounter)
    {
        $this->replaceParameters($qb, $parameterCounter);
        $this->getExpression($andExpr, $qb, $this->searchType, $this->dql, $searchValue, $this->fieldType ?? $searchTypeOfField, $parameterCounter);

        return $andExpr;
    }


    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(array(
            'dql' => '',
            'parameters' => [],
            'field_type' => null
        ));

        $resolver->setAllowedTypes('dql', 'string');
        $resolver->setAllowedTypes('parameters', 'array');
        $resolver->setAllowedTypes('field_type', ['string', 'null']);

        return $this;
    }

    public function getDql(): string
    {
        return $this->dql;
    }

    public function setDql(string $dql): void
    {
        $this->dql = $dql;
    }

    public function getFieldType(): ?string
    {
        return $this->fieldType;
    }

    public function setFieldType(?string $fieldType): void
    {
        $this->fieldType = $fieldType;
    }

    /**
     * @return string[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param string[] $parameters
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }

    private function replaceParameters(QueryBuilder $qb, &$parameterCounter)
    {
        foreach($this->parameters as $parameter => $value) {
            $parameterCounter++;
            $this->dql = str_replace(':'.$parameter, '?'.$parameterCounter, $this->dql);
            $qb->setParameter($parameterCounter, $value);
        }

        return $parameterCounter;
    }
}