<?php

namespace App\Datatable\Filter;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Andx;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;

/**
 * Class EngravedIdFilter
 * @package App\Datatable\Filter
 */
class EngravedIdFilter extends TextFilter
{
    //-------------------------------------------------
    // FilterInterface
    //-------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function getTemplate()
    {
        return '@SgDatatables/filter/input.html.twig';
    }

    /**
     * {@inheritdoc}
     */
    public function addAndExpression(Andx $andExpr, QueryBuilder $qb, $searchField, $searchValue, $searchTypeOfField, &$parameterCounter)
    {
        return $this->getExpression($andExpr, $qb, $this->searchType, $searchField, str_replace('.', '', $searchValue), 'string', $parameterCounter);
    }

    //-------------------------------------------------
    // Helper
    //-------------------------------------------------

    /**
     * Returns the type for the <input> element.
     *
     * @return string
     */
    public function getType()
    {
        return 'text';
    }
}
