<?php
namespace App\Datatable\Filter;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Andx;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
/**
 * Class IntegerFilter
 *
 * @package Sg\DatatablesBundle\Datatable\Filter
 */
class IntegerFilter extends TextFilter
{

    /**
     * {@inheritdoc}
     */
    public function addAndExpression(Andx $andExpr, QueryBuilder $qb, $searchField, $searchValue, $searchTypeOfField, &$parameterCounter)
    {
        if($searchTypeOfField === 'integer' || $searchTypeOfField === 'decimal') {
            $searchTypeOfField = 'string';
        }
        return $this->getExpression($andExpr, $qb, $this->searchType, $searchField, $searchValue, $searchTypeOfField, $parameterCounter);
    }

    /**
     * Returns the type for the <input> element.
     *
     * @return string
     */
    public function getType()
    {
        return 'text';
    }
}
