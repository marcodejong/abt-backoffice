<?php


namespace App\Datatable\Filter;


use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatePickerFilter extends DateRangeFilter
{
    /** @var string */
    protected $dateFormat;

    /**
     * {@inheritdoc}
     */
    public function addAndExpression(Andx $andExpr, QueryBuilder $qb, $searchField, $searchValue, $searchTypeOfField, &$parameterCounter)
    {
        $date = new \DateTimeImmutable($searchValue);
        $dateEnd = (new \DateTimeImmutable($searchValue))->setTime(23,59,59);

        $andExpr = $this->getBetweenAndExpression($andExpr, $qb, $searchField, $date->format($this->dateFormat), $dateEnd->format($this->dateFormat), $parameterCounter);
        $parameterCounter += 2;

        return $andExpr;
    }

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('date_format', 'Y-m-d');
        $resolver->setAllowedTypes('date_format', 'string');

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate()
    {
        return 'datatable/filter/datepicker.html.twig';
    }

    public function getDateFormat(): string
    {
        return $this->dateFormat;
    }

    public function setDateFormat(string $dateFormat): void
    {
        $this->dateFormat = $dateFormat;
    }
}