<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatable\Column;

/**
 * @package App\Datatable\Column
 */
class StringableEntityValueColumn extends AbstractColumn
{
    protected function transformValueForView($value)
    {
        return (string)$value;
    }
}
