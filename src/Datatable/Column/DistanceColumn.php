<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatable\Column;

/**
 * @package App\Datatable\Column
 */
class DistanceColumn extends AbstractColumn
{
    public function getCellContentTemplate(): string
    {
        return 'datatable/column/distance.html.twig';
    }

    protected function transformValueForView($value)
    {
        if (!$value) {
            return $value;
        }

        return ['value' => $value, 'unit' => 'km'];
    }
}
