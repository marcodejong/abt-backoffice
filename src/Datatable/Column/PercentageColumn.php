<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatable\Column;

/**
 * @package App\Datatable\Column
 */
class PercentageColumn extends AbstractColumn
{
    public function getCellContentTemplate(): string
    {
        return 'datatable/column/percentage.html.twig';
    }

    protected function transformValueForView($value)
    {
        return $value;
    }
}
