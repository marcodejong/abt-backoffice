<?php

namespace App\Datatable\Column;

use Arriva\Abt\Utility\Money;
use Sg\DatatablesBundle\Datatable\Editable\EditableInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Datatable\Column
 */
class MoneyColumn extends AbstractColumn
{
    /**
     * A NumberFormatter instance.
     * A required option.
     *
     * @var \NumberFormatter
     */
    protected $formatter;

    /**
     * Set formatter.
     *
     * @param \NumberFormatter $formatter
     *
     * @return $this
     */
    public function setFormatter(\NumberFormatter  $formatter)
    {
        $this->formatter = $formatter;

        return $this;
    }

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setRequired('formatter');

        $resolver->setAllowedTypes('formatter', array('object'));

        $resolver->setAllowedValues('formatter', function ($formatter) {
            if (!$formatter instanceof \NumberFormatter) {
                return false;
            }

            return true;
        });

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function renderPostCreateDatatableJsContent()
    {
        if ($this->editable instanceof EditableInterface) {
            return $this->twig->render(
                '@SgDatatables/column/column_post_create_dt.js.twig',
                array(
                    'column_class_editable_selector' => $this->getColumnClassEditableSelector(),
                    'editable_options' => $this->editable,
                    'entity_class_name' => $this->getEntityClassName(),
                    'column_dql' => $this->dql,
                    'original_type_of_field' => $this->getOriginalTypeOfField(),
                    'datatable_name' => $this->datatableName
                )
            );
        }

        return null;
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function transformValueForView($data)
    {
        if ($data instanceof Money) {
            return $this->formatter->formatCurrency($data->getAmount(), $data->getCurrency()->getId());
        }
    }
}
