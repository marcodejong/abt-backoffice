<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatable\Column;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Datatable\Column
 */
class ListColumn extends AbstractColumn
{
    /** @var string */
    private $listSeparator;

    public function setListSeparator(string $listSeparator): void
    {
        $this->listSeparator = $listSeparator;
    }

    public function getCellContentTemplate(): string
    {
        return 'datatable/column/list.html.twig';
    }

    protected function transformValueForView($value)
    {
        if (!$value) {
            return $value;
        }

        return \explode($this->listSeparator, $value);
    }


    public function configureOptions(OptionsResolver $resolver): self
    {
        $resolver
            ->setDefined(['list_separator'])
            ->setAllowedTypes('list_separator', 'string')
            ->setDefaults(['list_separator' => '|||']);
        parent::configureOptions($resolver);

        return $this;
    }
}
