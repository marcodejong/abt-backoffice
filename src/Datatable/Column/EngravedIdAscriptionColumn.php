<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatable\Column;

/**
 * @package App\Datatable\Column
 */
class EngravedIdAscriptionColumn extends AbstractColumn
{
    public function getCellContentTemplate(): string
    {
        return 'datatable/column/engraved_id_ascription.html.twig';
    }

    protected function transformValueForView($value)
    {
        if (!$value) {
            return null;
        }

        [$engravedId, $ascription] = \explode(',', $value);

        return [
            'engravedId' => $engravedId,
            'ascription' => $ascription,
        ];
    }
}
