<?php

namespace App\Datatable\Column;

use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Helper;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @package App\Datatable\Column
 */
class EmbeddableVatAmountColumn extends VirtualColumn
{
    /**
     * A NumberFormatter instance.
     * A required option.
     *
     * @var \NumberFormatter
     */
    protected $formatter;

    /**
     * Use NumberFormatter::formatCurrency instead NumberFormatter::format to format the value.
     * Default: false
     *
     * @var bool
     */
    protected $useFormatCurrency;

    /**
     * The currency code.
     * Default: null => NumberFormatter::INTL_CURRENCY_SYMBOL is used
     *
     * @var null|string
     */
    protected $currency;


    /**
     * Set formatter.
     *
     * @param \NumberFormatter $formatter
     *
     * @return $this
     */
    public function setFormatter(\NumberFormatter  $formatter)
    {
        $this->formatter = $formatter;

        return $this;
    }


    /**
     * Set useFormatCurrency.
     *
     * @param bool $useFormatCurrency
     *
     * @return $this
     */
    public function setUseFormatCurrency($useFormatCurrency)
    {
        $this->useFormatCurrency = $useFormatCurrency;

        return $this;
    }


    /**
     * Set currency.
     *
     * @param null|string $currency
     *
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }


    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setRequired('formatter');

        $resolver->setDefaults(
            array(
                'use_format_currency' => false,
                'currency' => null,
            )
        );

        $resolver->setAllowedTypes('formatter', array('object'));
        $resolver->setAllowedTypes('use_format_currency', array('bool'));
        $resolver->setAllowedTypes('currency', array('null', 'string'));

        $resolver->setAllowedValues('formatter', function ($formatter) {
            if (!$formatter instanceof \NumberFormatter) {
                return false;
            }

            return true;
        });

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function renderSingleField(array &$row)
    {
        $path = Helper::getDataPropertyPath($this->data);

        if ($this->accessor->isReadable($row, $path)) {
            $this->accessor->setValue($row, $path, $this->formatData(
                $this->accessor->getValue($row, $path))
            );
        }

        return $this;
    }

    /**
     * @param mixed $data
     * @return bool|string
     */
    private function formatData($data)
    {
        if (true === $this->useFormatCurrency) {
            if (false === is_float($data)) {
                $data = (float) $data;
            }

            if (null === $this->currency) {
                $this->currency = $this->formatter->getSymbol(\NumberFormatter::INTL_CURRENCY_SYMBOL);
            }

            $data = $this->formatter->formatCurrency($data, $this->currency);
        } else {
            // expected number (int or float), other values will be converted to a numeric value
            $data = $this->formatter->format($data);
        }

        return $data;
    }

}
