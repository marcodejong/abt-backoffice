<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatable\Column;

/**
 * @package App\Datatable\Column
 */
class DaysColumn extends AbstractColumn
{
    protected function transformValueForView($value)
    {
        if (!$value) {
            return $value;
        }

        $discountDays = \explode(',', $value);
        $result = '';
        foreach ($discountDays as $discountDay) {
            $result .= \strftime('%a', \strtotime('last sunday + ' . $discountDay . ' day')) . '. ';
        }

        return $result;
    }
}
