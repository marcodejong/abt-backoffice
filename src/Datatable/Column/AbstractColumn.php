<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Datatable\Column;

use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Helper;

/**
 * @package App\Datatable\Column
 */
abstract class AbstractColumn extends Column
{
    /**
     * @param array &$row
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @return $this
     */
    public function renderSingleField(array &$row): self
    {
        $path = Helper::getDataPropertyPath($this->data);
        $currentValue = $this->transformValueForView($this->accessor->getValue($row, $path));
        if ($this->isEditableContentRequired($row)) {
            $content = $this->renderCustomTemplate($currentValue, $row[$this->editable->getPk()]);
        } else {
            $content = $this->renderCustomTemplate($currentValue);
        }
        $this->accessor->setValue($row, $path, $content);

        return $this;
    }

    /**
     * @param array &$row
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @return $this
     */
    public function renderToMany(array &$row): self
    {
        $value = null;
        $path = Helper::getDataPropertyPath($this->data, $value);
        $entries = $this->accessor->getValue($row, $path);

        foreach (\array_keys($entries) as $key) {
            $currentPath = $path . '[' . $key . ']' . $value;
            $currentObjectPath = Helper::getPropertyPathObjectNotation($path, $key, $value);
            $currentValue = $this->transformValueForView($this->accessor->getValue($row, $currentPath));
            if ($this->isEditableContentRequired($row)) {
                $newValue = $this->renderCustomTemplate(
                    $currentValue,
                    $row[$this->editable->getPk()],
                    $currentObjectPath
                );
            } else {
                $newValue = $this->renderCustomTemplate($currentValue);
            }

            $this->accessor->setValue($row, $currentPath, $newValue);
        }

        return $this;
    }

    /**
     * @param null|string $data
     * @param null|string $pk
     * @param null|string $path
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @return string
     */
    private function renderCustomTemplate($data, $pk = null, $path = null): string
    {
        return $this->twig->render(
            $this->getCellContentTemplate(),
            [
                'data' => $data,
                'column_class_editable_selector' => $this->getColumnClassEditableSelector(),
                'pk' => $pk,
                'path' => $path,
            ]
        );
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    abstract protected function transformValueForView($value);
}
