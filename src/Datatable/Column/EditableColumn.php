<?php


namespace App\Datatable\Column;


use Sg\DatatablesBundle\Datatable\Editable\EditableInterface;

class EditableColumn extends AbstractColumn
{
    protected function transformValueForView($value)
    {
        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function renderPostCreateDatatableJsContent()
    {
        if ($this->editable instanceof EditableInterface) {
            return $this->twig->render(
                '@SgDatatables/column/column_post_create_dt.js.twig',
                array(
                    'column_class_editable_selector' => $this->getColumnClassEditableSelector(),
                    'editable_options' => $this->editable,
                    'entity_class_name' => $this->getEntityClassName(),
                    'column_dql' => $this->dql,
                    'original_type_of_field' => $this->getOriginalTypeOfField(),
                    'datatable_name' => $this->datatableName
                )
            );
        }

        return null;
    }

}