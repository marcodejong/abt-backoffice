<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Webshop\ProductSelection\Filter;

/**
 * @package App\Webshop\ProductSelection\Filter
 */
class ProductSelectionFilter implements FilterInterface
{
    /** @var array */
    private $data = [];

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function get(string $name, $default = null)
    {
        return $default;
    }
}
