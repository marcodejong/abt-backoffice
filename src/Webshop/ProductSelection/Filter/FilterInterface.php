<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Webshop\ProductSelection\Filter;

/**
 * @package App\Webshop\ProductSelection\Filter
 */
interface FilterInterface
{
    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function get(string $name, $default = null);
}
