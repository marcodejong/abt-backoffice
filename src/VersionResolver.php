<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App;

/**
 * @package App
 */
class VersionResolver
{
    /** @var string */
    private $kernelProjectDir;

    /** @var string */
    private $kernelEnvironment;

    public function __construct(string $kernelProjectDir, string $kernelEnvironment)
    {
        $this->kernelProjectDir = $kernelProjectDir;
        $this->kernelEnvironment = $kernelEnvironment;
    }

    public function getVersion(): string
    {
        $versionHintFileContents = '';
        foreach ($this->getVersionHintFilePaths() as $versionHintFilePath) {
            if (\file_exists($versionHintFilePath)) {
                $versionHintFileContents = $this->getContentsFromVersionHintFile($versionHintFilePath);
            }
        }

        return \sprintf('%s (%s)', $versionHintFileContents, $this->mapEnvironment($this->kernelEnvironment));
    }

    public function getVersionTimestamp(): string
    {
        $version = (string)\time();
        foreach ($this->getVersionHintFilePaths() as $versionHintFilePath) {
            if (\file_exists($versionHintFilePath)) {
                $version = (string)\filemtime($versionHintFilePath);
            }
        }

        return $version;
    }

    /**
     * @return string[]
     */
    private function getVersionHintFilePaths(): array
    {
        return [
            $this->kernelProjectDir . '/TAG',
            $this->kernelProjectDir . '/REVISION',
            $this->kernelProjectDir . '/.git',
        ];
    }

    private function mapEnvironment(string $environment): string
    {
        $map = [
            'dev' => 'development',
            'prod' => 'production',
            'ci' => 'continuous integration',
            'accept' => 'acceptance',
            'uat' => 'user acceptance test',
        ];

        return \array_key_exists(\strtolower($environment), $map) ? $map[\strtolower($environment)] : $environment;
    }

    private function getContentsFromVersionHintFile(string $versionHintFilePath): string
    {
        $content = '';
        if (\is_file($versionHintFilePath)) {
            $content = \file_get_contents($versionHintFilePath);
        } elseif (\is_dir($versionHintFilePath) && substr($versionHintFilePath, -3) === 'git') {
            $content = \substr(\file_get_contents($versionHintFilePath . '/HEAD'), 16);
        }

        return $content;
    }
}
