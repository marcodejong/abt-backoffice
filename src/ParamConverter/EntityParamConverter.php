<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\ParamConverter;

use Arriva\Abt\ORM\Doctrine\DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface;
use Arriva\Abt\ORM\EntityManager\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * @package App\ParamConverter
 */
class EntityParamConverter implements ParamConverterInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface */
    private $constraintFactory;

    /** @var null|CamelCaseToSnakeCaseNameConverter */
    private $converter;

    public function __construct(
        EntityManagerInterface $entityManager,
        DoctrineEntityConstraintFromClassNameAndCriteriaFactoryInterface $constraintFactory
    ) {
        $this->entityManager = $entityManager;
        $this->constraintFactory = $constraintFactory;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @throws NotFoundHttpException
     * @throws \ReflectionException
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $paramName = $configuration->getName();
        $class = $configuration->getClass();
        $shortName = (new \ReflectionClass($class))->getShortName();
        $attributeName = $this->getConverter()->denormalize($shortName) . 'Id';
        $attributeValue = $request->get($attributeName);
        if (!$attributeValue && !$configuration->isOptional()) {
            throw new NotFoundHttpException(
                \sprintf(
                    'For entity %s used as param with name: %s could no identifier value be found in request data by key: %s',
                    $class,
                    $paramName,
                    $attributeName
                )
            );
        }

        if (!$attributeValue) {
            return false;
        }
        $queryBuilder = $this->constraintFactory->createQueryBuilderForEntity($class, 'object');
        $queryBuilder->where('object.id = :idValue')->setParameter('idValue', $attributeValue);
        $constraint = $this->constraintFactory->createDoctrineQueryEntityConstraint($queryBuilder->getQuery());
        $entities = $this->entityManager->getEntities($constraint);
        if (!$configuration->isOptional() && 1 !== \count($entities)) {
            throw new NotFoundHttpException(
                \sprintf(
                    'Required param: %s with entity %s with identifier: %s %s',
                    $paramName,
                    $class,
                    $attributeValue,
                    0 === \count($entities) ? 'was not found' : 'has more than 1 result'
                )
            );
        }

        $object = null;
        if (1 === \count($entities)) {
            $object = $entities[0];
        }
        $request->attributes->set($paramName, $object);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        $class = $configuration->getClass();
        if (!$class) {
            return false;
        }

        return 0 === \strpos($class, 'App\Entity') || 0 === \strpos($class, 'Arriva\Abt\Entity')
            || 0 === \strpos($class, 'Arriva\Reporting\Entity');
    }

    private function getConverter(): CamelCaseToSnakeCaseNameConverter
    {
        if (!$this->converter) {
            $this->converter = new CamelCaseToSnakeCaseNameConverter();
        }

        return $this->converter;
    }
}
