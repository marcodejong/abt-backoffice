<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * This param converted will convert request values which have the format value1/value2/value3 into an array like:
 * ['value1', 'value2', 'value3']. Only when the controller/action method signature declare the corresponding parameter
 * as an array
 *
 * @package App\ParamConverter
 */
class ArrayParamConverter implements ParamConverterInterface
{
    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @throws \ReflectionException
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $requestValue = $request->get($configuration->getName());
        if ($requestValue) {
            $parameter = $this->getReflectionParameterFromControllerStringAndParameterByName(
                $request->get('_controller'),
                $configuration->getName()
            );
            if ($parameter && null !== $parameter->getType() && 'array' === $parameter->getType()->getName()) {
                $value = \explode('/', $requestValue);
                $request->attributes->set($configuration->getName(), $value);
            }
        }

        return false;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return true;
    }

    /**
     * @param string $controllerString
     * @param string $parameterName
     * @throws \ReflectionException
     * @return null|\ReflectionParameter
     */
    private function getReflectionParameterFromControllerStringAndParameterByName(
        string $controllerString,
        string $parameterName
    ): ?\ReflectionParameter {
        $reflectionMethod = $this->getReflectionMethodFromControllerString($controllerString);
        if ($reflectionMethod) {
            return $this->getParameterByName($reflectionMethod, $parameterName);
        }

        return null;
    }

    /**
     * @param string $controllerString
     * @throws \ReflectionException
     * @return null|\ReflectionMethod
     */
    private function getReflectionMethodFromControllerString(string $controllerString): ?\ReflectionMethod
    {
        $controllerString = \str_replace('::', ':', $controllerString);
        if (false !== \strpos($controllerString, ':')) {
            [$controller, $action] = \explode(':', $controllerString);
            $reflectionClass = new \ReflectionClass($controller);

            return $reflectionClass->getMethod($action);
        }

        return null;
    }

    private function getParameterByName(\ReflectionMethod $reflectionMethod, string $paramName): ?\ReflectionParameter
    {
        foreach ($reflectionMethod->getParameters() as $parameter) {
            if ($paramName === $parameter->getName()) {
                return $parameter;
            }
        }

        return null;
    }
}
