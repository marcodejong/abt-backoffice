<?php

namespace App\DataFixtures;

use App\Test\Faker\Provider\DateProvider;
use App\Test\Faker\Provider\FixedUuidProvider;
use App\Test\Faker\Provider\IbanNumberProvider;
use App\Test\Faker\Provider\MoneyProvider;
use App\Test\Faker\Provider\NalTypesProvider;
use App\Test\Fixture\Loader\AliceFixtureLoader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Generator;
use Nelmio\Alice\Faker\Provider\AliceProvider;
use Nelmio\Alice\Loader\NativeLoader;

/**
 * Class TestFixtures
 *
 * This fixture loaded loads the test fixtures defined in <root>/tests/fixtures/abt
 */
class TestFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $fakerGenerator = new Generator();
        $fakerGenerator->addProvider(new AliceProvider());
        $fakerGenerator->addProvider(new DateProvider($fakerGenerator));
        $fakerGenerator->addProvider(new MoneyProvider($fakerGenerator));
        $fakerGenerator->addProvider(new NalTypesProvider($fakerGenerator));
        $fakerGenerator->addProvider(new IbanNumberProvider($fakerGenerator));
        $fakerGenerator->addProvider(new FixedUuidProvider($fakerGenerator));

        $fixtureLoader = new AliceFixtureLoader(new NativeLoader($fakerGenerator), __DIR__ . '/../../tests/fixtures/abt');

        foreach ($fixtureLoader->loadObjects() as $object) {
            // only add the object to the object manager if its class is managed by the object manager
            if (!$manager->getMetadataFactory()->isTransient(\get_class($object))) {
                $manager->persist($object);
            }
        }

        $manager->flush();
    }
}
