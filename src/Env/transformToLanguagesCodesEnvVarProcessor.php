<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Env;

use App\Manager\LocaleManager;
use Symfony\Component\DependencyInjection\EnvVarProcessorInterface;

/**
 * @package App\Env
 */
class transformToLanguagesCodesEnvVarProcessor implements EnvVarProcessorInterface
{
    public function getEnv($prefix, $name, \Closure $getEnv): string
    {
        $languageCodes = \array_map([LocaleManager::class, 'getLanguageCodeFromLocale'], $getEnv($name));

        return \implode('|', $languageCodes);
    }

    public static function getProvidedTypes(): array
    {
        return ['transformToLanguagesCodes' => 'array'];
    }
}
