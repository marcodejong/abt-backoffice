<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Formatter;

use Arriva\Assist\Entity\AddressInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AddressFormatter
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function formatAddress(AddressInterface $address): string
    {
        $result = '';
        if (null !== $address->getPostOfficeBox()) {
            $result .= \sprintf(
                '%s %s',
                $this->translator->trans('general.address_formatter.post_office_box'),
                $address->getPostOfficeBox()
            );
        } else {
            $result .= \sprintf(
                '%s %s',
                $address->getStreet(),
                $address->getHouseNumber() . $address->getHouseNumberExtension()
            );
        }

        $result .= \sprintf(
            ', %s %s, %s',
            $address->getPostcode(),
            $address->getCity(),
            $address->getCountryCode()
        );

        return $result;
    }
}
