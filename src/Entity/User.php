<?php

namespace App\Entity;

use Arriva\Assist\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * This class has to exist when re-using the ASSIST Symfony authentication
 * The user is stored in the session as serialized data and this class is used in the data.
 *
 * @ORM\Entity
 */
class User extends BaseUser implements UserInterface
{
    public function getRoles(): array
    {
        return [$this->getAclRole()->getName()];
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials(): void
    {
    }
}
