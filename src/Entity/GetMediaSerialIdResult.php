<?php

namespace App\Entity;

class GetMediaSerialIdResult implements \JsonSerializable
{
    const ERROR_MESSAGE_CODE_NOT_FOUND = 'not-found';
    const ERROR_MESSAGE_CODE_UNKNOWN = 'unknown-error';

    /**
     * @var bool|null
     */
    private $success;

    /**
     * @var string|null
     */
    private $mediaSerialId;

    /**
     * @var string|null
     */
    private $errorMessageCode;

    public function getSuccess(): ?bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    public function getMediaSerialId(): ?string
    {
        return $this->mediaSerialId;
    }

    public function setMediaSerialId(string $mediaSerialId): self
    {
        $this->mediaSerialId = $mediaSerialId;

        return $this;
    }


    public function getErrorMessageCode(): ?string
    {
        return $this->errorMessageCode;
    }

    public function setErrorMessageCode(string $errorMessageCode): self
    {
        $this->errorMessageCode = $errorMessageCode;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'success' => $this->getSuccess(),
            'mediaSerialId' => $this->getMediaSerialId(),
            'errorMessageCode' => $this->getErrorMessageCode(),
        ];
    }
}
