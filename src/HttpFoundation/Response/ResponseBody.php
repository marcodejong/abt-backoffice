<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\HttpFoundation\Response;

/**
 * @package App\HttpFoundation\Response
 */
class ResponseBody
{
    /** @var array */
    private $data;

    /**
     * @param array $data
     * @param array $errors
     */
    public function __construct(array $data, array $errors = [])
    {
        $this->data = \array_merge(
            ['success' => empty($errors), 'errors' => empty($errors) ? new \stdClass() : $errors],
            $data
        );
    }

    public function getData(): array
    {
        return $this->data;
    }
}
