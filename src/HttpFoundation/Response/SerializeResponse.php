<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * @package App\HttpFoundation\Response
 */
class SerializeResponse extends Response
{
    /** @var ResponseBody */
    private $responseBody;

    /**
     * @param ResponseBody $responseBody
     * @param int $status
     * @param array $headers
     * @throws \InvalidArgumentException
     */
    public function __construct(ResponseBody $responseBody, int $status = 200, array $headers = [])
    {
        parent::__construct('', $status, $headers);
        $this->responseBody = $responseBody;
    }

    public function getData(): array
    {
        return $this->responseBody->getData();
    }
}
