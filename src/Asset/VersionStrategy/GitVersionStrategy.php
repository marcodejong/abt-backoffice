<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Asset\VersionStrategy;

use App\VersionResolver;
use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;

/**
 * @package App\Asset\VersionStrategy
 */
class GitVersionStrategy implements VersionStrategyInterface
{
    /** @var VersionResolver */
    private $versionResolver;

    public function __construct(VersionResolver $versionResolver)
    {
        $this->versionResolver = $versionResolver;
    }

    public function getVersion($path): string
    {
        return $this->versionResolver->getVersionTimestamp();
    }

    public function applyVersion($path): string
    {
        $version = $this->getVersion($path);

        return !empty($version) ? \sprintf('%s?v=%s', $path, $version) : $path;
    }
}
