<?php


namespace App\Service\FileGenerator\Builder;


use App\Exception\UnsupportedSubtypeException;
use App\Model\Report\Journal\JournalReport;
use App\Model\Report\Journal\Summary\DiscountSummary;
use App\Model\Report\Journal\Summary\MoneysPayableSummary;
use App\Model\Report\Journal\Summary\MoneysReceivableSummary;
use App\Model\Report\Journal\Summary\MoneysReceivedSummary;
use App\Model\Report\Journal\Summary\RefundSummary;
use App\Model\Report\Journal\Summary\SalesSummary;
use App\Model\Report\Journal\Summary\TransactionSummary;
use App\Service\FileGenerator\Entity\JournalReport\DataRow;
use App\Service\FileGenerator\Entity\JournalReport\Header;
use App\Service\FileGenerator\Entity\JournalReport\HeaderRow;
use App\Service\FileGenerator\Entity\JournalReport\JournalReportDocument;
use App\Service\FileGenerator\Entity\JournalReport\TableData;
use App\Service\FileGenerator\Entity\DocumentData;
use Arriva\Abt\Utility\Money;
use SolidPhp\ValueObjects\Type\Type;

class JournalReportPdfDataBuilder
{
    /** @var JournalReport */
    private $report;

    public function setJournalReport(JournalReport $report): self
    {
        $this->report = $report;

        return $this;
    }

    public function buildJournalReportData(): DocumentData
    {
        return new DocumentData(
            $this->buildDocument()
        );
    }

    private function buildDocument(): JournalReportDocument
    {
        return new JournalReportDocument(
            $this->buildTableData()
        );
    }

    private function buildTableData(): TableData
    {
        return new TableData(
            $this->buildHeaderRow(),
            $this->buildDataRows()
        );
    }

    private function buildHeaderRow(): HeaderRow
    {
        $headerRow = new HeaderRow();
        $headerRow->addHeader(new Header('Regelnr'));
        $headerRow->addHeader(new Header('Afdeling'));
        $headerRow->addHeader(new Header('Jaar'));
        $headerRow->addHeader(new Header('Maand'));
        $headerRow->addHeader(new Header('Dagboek'));
        $headerRow->addHeader(new Header('Datum'));
        $headerRow->addHeader(new Header('Omschrijving'));
        $headerRow->addHeader(new Header('Rekening'));
        $headerRow->addHeader(new Header('Bedrag incl BTW'));
        $headerRow->addHeader(new Header('BTW code'));
        $headerRow->addHeader(new Header('BTW bedrag'));
        $headerRow->addHeader(new Header('KPL'));
        $headerRow->addHeader(new Header('KDR'));
        $headerRow->addHeader(new Header('Aantal'));

        return $headerRow;
    }

    /**
     * @return DataRow[]
     */
    private function buildDataRows(): array
    {
        $dataRows = [];
        foreach($this->report->getTransactionSummaries() as $summary) {
            switch (true) {
                case $summary instanceof SalesSummary:
                    $dataRows[] = $this->buildSalesSummaryRow($summary);
                    break;
                case $summary instanceof RefundSummary:
                    $dataRows[] = $this->buildRefundSummaryRow($summary);
                    break;
                case $summary instanceof MoneysReceivedSummary:
                    $dataRows[] = $this->buildMoneysReceivedSummaryRow($summary);
                    break;
                case $summary instanceof MoneysPayableSummary:
                    $dataRows[] = $this->buildMoneysPayableSummaryRow($summary);
                    break;
                case $summary instanceof MoneysReceivableSummary:
                    $dataRows[] = $this->buildMoneysReceivableSummaryRow($summary);
                    break;
                case $summary instanceof DiscountSummary:
                    $dataRows[] = $this->buildDiscountSummaryRow($summary);
                    break;
                default:
                    throw new UnsupportedSubtypeException(Type::of(TransactionSummary::class), $summary);
            }
        }

        return $dataRows;
    }

    private function buildSalesSummaryRow(SalesSummary $summary): DataRow
    {
        $date = $this->report->getPeriod()->getStartDate();

        $dataRow = new DataRow();
        $dataRow->addCellData('ABT');
        $dataRow->addCellData($date->getYearObject()->getNumber());
        $dataRow->addCellData($date->getMonthObject()->getNumber());
        $dataRow->addCellData('');
        $dataRow->addCellData($date->format('d-m-Y'));
        $dataRow->addCellData('ABT');
        $dataRow->addCellData($summary->getTAccount()->getValue());
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getAmountWithVat()->multiply(-1)));
        $dataRow->addCellData($summary->getVatCode()->getValue());
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getVatAmount()));
        $dataRow->addCellData($summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '');
        $dataRow->addCellData($summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '');
        $dataRow->addCellData($summary->getNrOfTransactions());

        return $dataRow;
    }

    private function buildRefundSummaryRow(RefundSummary $summary): DataRow
    {
        $dataRow = new DataRow();
        $dataRow->addCellData('ABT');
        $dataRow->addCellData('');
        $dataRow->addCellData('');
        $dataRow->addCellData('');
        $dataRow->addCellData($this->report->getPeriod()->getStartDate()->format('d-m-Y'));
        $dataRow->addCellData('ABT');
        $dataRow->addCellData($summary->getTAccount()->getValue());
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getAmountWithVat()));
        $dataRow->addCellData($summary->getVatCode()->getValue());
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getVatAmount()->multiply(-1)));
        $dataRow->addCellData($summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '');
        $dataRow->addCellData($summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '');
        $dataRow->addCellData($summary->getNrOfTransactions());

        return $dataRow;
    }

    private function buildMoneysReceivedSummaryRow(MoneysReceivedSummary $summary): DataRow
    {
        $date = $this->report->getPeriod()->getStartDate();

        $dataRow = new DataRow();
        $dataRow->addCellData('ABT');
        $dataRow->addCellData($date->getYearObject()->getNumber());
        $dataRow->addCellData($date->getMonthObject()->getNumber());
        $dataRow->addCellData('');
        $dataRow->addCellData($date->format('d-m-Y'));
        $dataRow->addCellData(sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')));
        $dataRow->addCellData($summary->getTAccount()->getValue());
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getAmountWithVat()));
        $dataRow->addCellData('');
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getVatAmount()));
        $dataRow->addCellData('');
        $dataRow->addCellData('');
        $dataRow->addCellData($summary->getNrOfTransactions());

        return $dataRow;
    }

    private function buildMoneysPayableSummaryRow(MoneysPayableSummary $summary): DataRow
    {
        $date = $this->report->getPeriod()->getStartDate();

        $dataRow = new DataRow();
        $dataRow->addCellData('ABT');
        $dataRow->addCellData($date->getYearObject()->getNumber());
        $dataRow->addCellData($date->getMonthObject()->getNumber());
        $dataRow->addCellData('');
        $dataRow->addCellData($date->format('d-m-Y'));
        $dataRow->addCellData(sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')));
        $dataRow->addCellData($summary->getTAccount()->getValue());
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getAmountWithVat()->multiply(-1)));
        $dataRow->addCellData('');
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getVatAmount()->multiply(-1)));
        $dataRow->addCellData($summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '');
        $dataRow->addCellData($summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '');
        $dataRow->addCellData($summary->getNrOfTransactions());

        return $dataRow;
    }

    private function buildMoneysReceivableSummaryRow(MoneysReceivableSummary $summary): DataRow
    {
        $date = $this->report->getPeriod()->getStartDate();

        $dataRow = new DataRow();
        $dataRow->addCellData('ABT');
        $dataRow->addCellData($date->getYearObject()->getNumber());
        $dataRow->addCellData($date->getMonthObject()->getNumber());
        $dataRow->addCellData('');
        $dataRow->addCellData($date->format('d-m-Y'));
        $dataRow->addCellData(sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')));
        $dataRow->addCellData($summary->getTAccount()->getValue());
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getAmountWithVat()->multiply(-1)));
        $dataRow->addCellData('');
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getVatAmount()));
        $dataRow->addCellData($summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '');
        $dataRow->addCellData($summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '');
        $dataRow->addCellData($summary->getNrOfTransactions());

        return $dataRow;
    }

    private function buildDiscountSummaryRow(DiscountSummary $summary): DataRow
    {
        $date = $this->report->getPeriod()->getStartDate();

        $dataRow = new DataRow();
        $dataRow->addCellData('ABT');
        $dataRow->addCellData($date->getYearObject()->getNumber());
        $dataRow->addCellData($date->getMonthObject()->getNumber());
        $dataRow->addCellData('');
        $dataRow->addCellData($date->format('d-m-Y'));
        $dataRow->addCellData(sprintf('%s %s', $summary->getLabel(), $date->format('d-m-Y')));
        $dataRow->addCellData(sprintf(
            '%s %s',
            $summary->getGroupTAccount()->getValue(),
            $summary->getTAccount()->getValue()
        ));
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getAmountWithVat()));
        $dataRow->addCellData($summary->getVatCode()->getValue());
        $dataRow->addCellData($this->formatMoney($summary->getAmount()->getVatAmount()));
        $dataRow->addCellData($summary->getCostCenter() ? $summary->getCostCenter()->getValue() : '');
        $dataRow->addCellData($summary->getCostUnitCode() ? $summary->getCostUnitCode()->getValue() : '');
        $dataRow->addCellData($summary->getNrOfTransactions());

        return $dataRow;
    }

    private function formatMoney(Money $money): string
    {
        return number_format(
            (float)$money->getAmount(),
            2,
            ',',
            ''
        );
    }
}