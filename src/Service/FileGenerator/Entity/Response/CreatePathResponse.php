<?php


namespace App\Service\FileGenerator\Entity\Response;


class CreatePathResponse implements CreateResponse
{
    /** @var string */
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }
}