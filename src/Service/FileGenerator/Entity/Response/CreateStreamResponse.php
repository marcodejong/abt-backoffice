<?php


namespace App\Service\FileGenerator\Entity\Response;


use Psr\Http\Message\StreamInterface;

class CreateStreamResponse implements CreateResponse
{
    /** @var StreamInterface */
    private $stream;

    public function __construct(StreamInterface $stream)
    {
        $this->stream = $stream;
    }

    public function getStream(): StreamInterface
    {
        return $this->stream;
    }
}