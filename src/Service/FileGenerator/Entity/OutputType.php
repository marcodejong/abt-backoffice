<?php


namespace App\Service\FileGenerator\Entity;


use Arriva\Abt\Utility\Enum;

class OutputType extends Enum
{
    const TYPE_JSON = 'json';
    const TYPE_STREAM = 'stream';
    const TYPE_AMPQ = 'ampq';

    public static function JSON(): self
    {
        return self::define(self::TYPE_JSON);
    }

    public static function STREAM(): self
    {
        return self::define(self::TYPE_STREAM);
    }

    public static function AMPQ(): self
    {
        return self::define(self::TYPE_AMPQ);
    }
}