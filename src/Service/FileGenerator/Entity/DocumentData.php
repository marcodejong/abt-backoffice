<?php


namespace App\Service\FileGenerator\Entity;


class DocumentData implements Data
{
    /** @var Document */
    private $document;

    public function __construct(Document $document)
    {
        $this->document = $document;
    }
}