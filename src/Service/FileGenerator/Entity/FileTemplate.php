<?php


namespace App\Service\FileGenerator\Entity;


use Arriva\Abt\Utility\Enum;

class FileTemplate extends Enum
{
    const ABT_INVOICE = 'abt-invoice';
    const ABT_TRANSACTION_SPECIFICATION = 'abt-transaction-specification';
    const ABT_MONTHLY_JOURNAL_REPORT = 'abt-monthly-journal-report';

    public static function ABT_INVOICE(): self
    {
        return self::define(self::ABT_INVOICE);
    }

    public static function ABT_TRANSACTION_SPECIFICATION(): self
    {
        return self::define(self::ABT_TRANSACTION_SPECIFICATION);
    }

    public static function ABT_MONTHLY_JOURNAL_REPORT(): self
    {
        return self::define(self::ABT_MONTHLY_JOURNAL_REPORT);
    }
}