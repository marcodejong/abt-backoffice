<?php


namespace App\Service\FileGenerator\Entity\JournalReport;


class HeaderRow
{
    /** @var Header[] */
    private $headers;

    /**
     * HeaderRow constructor.
     * @param Header[] $headers
     */
    public function __construct(array $headers = [])
    {
        $this->headers = $headers;
    }

    public function addHeader(Header $header): void
    {
        $this->headers[] = $header;
    }
}