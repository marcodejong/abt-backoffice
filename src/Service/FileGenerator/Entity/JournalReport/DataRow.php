<?php


namespace App\Service\FileGenerator\Entity\JournalReport;


class DataRow
{
    /** @var mixed[] */
    private $cellData;

    /**
     * DataRow constructor.
     * @param mixed[] $cellData
     */
    public function __construct(array $cellData = [])
    {
        $this->cellData = $cellData;
    }

    public function addCellData($cellData): void
    {
        $this->cellData[] = $cellData;
    }
}