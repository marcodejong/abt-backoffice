<?php


namespace App\Service\FileGenerator\Entity\JournalReport;


class Header
{
    /** @var string */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }
}