<?php


namespace App\Service\FileGenerator\Entity\JournalReport;


use App\Service\FileGenerator\Entity\Document;

class JournalReportDocument implements Document
{
    /** @var TableData */
    private $tableData;

    public function __construct(TableData $tableData)
    {
        $this->tableData = $tableData;
    }
}