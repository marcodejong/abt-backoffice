<?php


namespace App\Service\FileGenerator\Entity\JournalReport;


class TableData
{
    /** @var HeaderRow */
    private $headerRow;

    /** @var DataRow[] */
    private $dataRows;

    /**
     * TableData constructor.
     * @param HeaderRow $headerRow
     * @param DataRow[] $dataRows
     */
    public function __construct(HeaderRow $headerRow, array $dataRows)
    {
        $this->headerRow = $headerRow;
        $this->dataRows = $dataRows;
    }
}