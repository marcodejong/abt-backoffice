<?php


namespace App\Service\FileGenerator\Entity\Request;


use App\Service\FileGenerator\Entity\Data;
use App\Service\FileGenerator\Entity\File;
use App\Service\FileGenerator\Entity\OutputType;

class CreateRequest
{
    /** @var string */
    private $output;

    /** @var File[] */
    private $files;

    /** @var Data */
    private $data;

    /**
     * CreateRequest constructor.
     * @param OutputType $output
     * @param File[] $files
     * @param Data $data
     */
    public function __construct(OutputType $output, array $files, Data $data)
    {
        $this->output = $output->getId();
        $this->files = $files;
        $this->data = $data;
    }
}