<?php


namespace App\Service\FileGenerator\Entity;


class File
{
    const TYPE_PDF = 'pdf';
    const TYPE_CSV = 'csv';
    const TYPE_XLS = 'xls';

    /** @var string */
    private $type;

    /** @var string|null */
    private $template;

    private function __construct(string $type, ?string $template = null)
    {
        $this->type = $type;
        $this->template = $template;
    }

    public static function createPdfType(FileTemplate $template): self
    {
        return new self(self::TYPE_PDF, $template->getId());
    }

    public static function createCsvType(): self
    {
        return new self(self::TYPE_CSV);
    }

    public static function createXlsType(): self
    {
        return new self(self::TYPE_XLS);
    }

    public static function createFromString(string $type, ?FileTemplate $template = null): self
    {
        switch (true) {
            case $type === self::TYPE_PDF:
                if (!$template) {
                    throw new \DomainException(sprintf('PDF file requires a FileTemplate, non given'));
                }
                return self::createPdfType($template);
            case $type === self::TYPE_CSV:
                return self::createCsvType();
            case $type === self::TYPE_XLS:
                return self::createXlsType();
            default:
                throw new \DomainException(sprintf('Type "%s" is not allowed', $type));
        }
    }

    public function isValid(): bool
    {
        $validTemplate = true;
        if ($this->type === self::TYPE_PDF) {
            $validTemplate = InvoiceTemplate::isValid($this->template);
        }

        $validType = in_array($this->type, [self::TYPE_PDF, self::TYPE_CSV, self::TYPE_XLS]);

        return $validTemplate && $validType;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }
}
