<?php


namespace App\Service\FileGenerator;


use App\Service\Exception\Factory\GuzzleServiceExceptionFactory;
use App\Service\FileGenerator\Entity\Request\CreateRequest;
use App\Service\FileGenerator\Entity\Response\CreatePathResponse;
use App\Service\FileGenerator\Entity\Response\CreateResponse;
use App\Service\FileGenerator\Entity\Response\CreateStreamResponse;
use Exception;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileGeneratorClient
{
    /** @var GuzzleClientInterface */
    private $httpClient;

    /** @var SerializerInterface */
    private $serializer;

    /** @var GuzzleServiceExceptionFactory */
    private $exceptionFactory;


    public function __construct(
        GuzzleClientInterface $httpClient,
        SerializerInterface $serializer,
        GuzzleServiceExceptionFactory $exceptionFactory
    ) {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
        $this->exceptionFactory = $exceptionFactory;
    }

    public function createFile(CreateRequest $request): CreateResponse
    {
        try {
            $response = $this->httpClient->request(
                Request::METHOD_POST,
                "/create",
                [
                    RequestOptions::BODY => $this->serializer->serialize($request, 'json'),
                    RequestOptions::HEADERS => [
                        'Content-Type' => 'application/json',
                    ],
                ]
            );

            if ($response->getStatusCode() === Response::HTTP_OK
                && $this->isJsonResponse($response)
                && ($responseObject = $this->deserializeResponse($response, CreatePathResponse::class))
                && $responseObject instanceof CreatePathResponse
            ) {
                return $responseObject;
            }

            if ($response->getStatusCode() === Response::HTTP_OK
                && $this->isStreamResponse($response)
            ) {
                return new CreateStreamResponse($response->getBody());
            }

            throw $this->getInvalidResponseException($response);
        } catch (GuzzleException | Exception $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }

    /**
     * @param ResponseInterface $response
     *
     * @param string $expectedType
     *
     * @return object|null Will be $expectedType, or null if the response shape does not match the type
     */
    private function deserializeResponse(ResponseInterface $response, string $expectedType)
    {
        $responseBody = (string)$response->getBody();
        $deserializedObject = $this->serializer->deserialize($responseBody, $expectedType, 'json');

        if ($deserializedObject instanceof $expectedType) {
            return $deserializedObject;
        }

        return null;
    }

    private function getInvalidResponseException(ResponseInterface $response): RuntimeException
    {
        return new RuntimeException(sprintf('Invalid Invoice Generator response: %s', (string)$response->getBody()));
    }

    private function isJsonResponse(ResponseInterface $response): bool
    {
        return $response->getHeaderLine('Content-Type') === 'application/json';
    }

    private function isStreamResponse(ResponseInterface $response): bool
    {
        return in_array(
            $response->getHeaderLine('Content-Type'),
            ['application/vnd.ms-excel', 'application/pdf', 'text/csv; charset=utf-8']
        );
    }
}