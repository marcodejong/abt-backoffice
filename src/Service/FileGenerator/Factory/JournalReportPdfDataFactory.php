<?php


namespace App\Service\FileGenerator\Factory;


use App\Model\Report\Journal\JournalReport;
use App\Service\FileGenerator\Builder\JournalReportPdfDataBuilder;
use App\Service\FileGenerator\Entity\DocumentData;

class JournalReportPdfDataFactory
{
    /** @var JournalReportPdfDataBuilder */
    private $journalReportPdfDataBuilder;

    public function __construct(JournalReportPdfDataBuilder $journalReportPdfDataBuilder)
    {
        $this->journalReportPdfDataBuilder = $journalReportPdfDataBuilder;
    }

    public function create(JournalReport $report): DocumentData
    {
        return $this->journalReportPdfDataBuilder
            ->setJournalReport($report)
            ->buildJournalReportData();
    }
}