<?php

namespace App\Service;

use Arriva\Abt\Value\EncryptedMediaSerialId;
use Arriva\Abt\Value\MediaSerialId;

interface MediaSerialIdService
{
    public function encryptMediaSerialId(MediaSerialId $mediaSerialId): EncryptedMediaSerialId;

    public function decryptMediaSerialId(EncryptedMediaSerialId $encryptedMediaSerialId): MediaSerialId;
}
