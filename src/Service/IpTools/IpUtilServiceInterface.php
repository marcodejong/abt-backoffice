<?php

namespace App\Service\IpTools;

interface IpUtilServiceInterface
{
    /**
     * Checks if the given IP addresses matches any of the ranges
     *
     * @param string $ip An IP4v/IPv6 address to match
     * @param string[] $ranges An array of CIDR specification strings to match against.
     *                         A CIDR specification can be a specific IP address or a subnet notation (a.b.c.d/x)
     *
     * @return bool True if the given IP address matches at least one of the ranges
     */
    public function ipMatchesAny(string $ip, array $ranges): bool;
}