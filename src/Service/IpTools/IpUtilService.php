<?php

namespace App\Service\IpTools;

use IPSet\IPSet;

class IpUtilService implements IpUtilServiceInterface
{
    public function ipMatchesAny(string $ip, array $ranges): bool
    {
        return (new IPSet($ranges))->match($ip);
    }
}