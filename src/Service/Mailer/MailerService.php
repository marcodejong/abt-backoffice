<?php

namespace App\Service\Mailer;

use App\Service\Mailer\Data\Mail;
use App\Service\Mailer\Result\SendMail\SendMailResult;

interface MailerService
{
    public function sendMail(Mail $mail): SendMailResult;
}
