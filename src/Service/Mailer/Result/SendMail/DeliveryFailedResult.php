<?php

namespace App\Service\Mailer\Result\SendMail;

use App\Service\Mailer\Data\Mail;
use Arriva\Abt\Value\EmailAddress;

class DeliveryFailedResult implements SendMailResult
{
    /** @var Mail */
    private $mail;

    /** @var EmailAddress[] */
    private $failedAddresses;

    /**
     * @param Mail           $mail
     * @param EmailAddress[] $failedAddresses
     */
    public function __construct(Mail $mail, array $failedAddresses)
    {
        $this->mail = $mail;
        $this->failedAddresses = $failedAddresses;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }

    /**
     * @return EmailAddress[]
     */
    public function getFailedAddresses(): array
    {
        return $this->failedAddresses;
    }

    public function __toString()
    {
        return sprintf('Mail delivery failed to %s', implode(', ', $this->getFailedAddresses()));
    }
}
