<?php

namespace App\Service\Mailer\Result\SendMail;

class OkResult implements SendMailResult
{
    public function __toString(): string
    {
        return 'Delivery succeeded';
    }

}
