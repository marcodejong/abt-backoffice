<?php

namespace App\Service\Mailer;

use App\Service\Mailer\Data\Mail;
use App\Service\Mailer\Result\SendMail\DeliveryFailedResult;
use App\Service\Mailer\Result\SendMail\OkResult;
use App\Service\Mailer\Result\SendMail\SendMailResult;
use Arriva\Abt\Value\EmailAddress;
use Swift_Attachment;
use Swift_Mailer;
use Swift_Message;

class SwiftMailerService implements MailerService
{
    /** @var Swift_Mailer */
    private $swiftMailer;

    public function __construct(Swift_Mailer $swiftMailer)
    {
        $this->swiftMailer = $swiftMailer;
    }

    public function sendMail(Mail $mail): SendMailResult
    {
        $failedRecipients = [];

        if (!$this->swiftMailer->getTransport()->ping()) {
            $this->swiftMailer->getTransport()->stop();
            $this->swiftMailer->getTransport()->start();
        }

        $result = $this->swiftMailer->send($this->createMessage($mail), $failedRecipients);

        if ($result === 1) {
            return new OkResult();
        }

        return new DeliveryFailedResult($mail, array_map([EmailAddress::class, 'fromString'], $failedRecipients));
    }

    private function createMessage(Mail $mail): Swift_Message
    {
        $message = new Swift_Message(
            $mail->getSubject()->getValue(),
            $mail->getBody()->getValue(),
            'text/html'
        );

        $sender = $mail->getSender();
        $receiver = $mail->getReceiver();

        $message->setFrom($sender->getEmailAddress()->getEmailAddressString(), (string)$sender->getDisplayName());
        $message->setTo($receiver->getEmailAddress()->getEmailAddressString(), (string)$receiver->getDisplayName());

        foreach ($mail->getAttachments() as $attachment) {
            $message->attach(Swift_Attachment::fromPath($attachment->getPath(), $attachment->getContentType()));
        }

        return $message;
    }
}
