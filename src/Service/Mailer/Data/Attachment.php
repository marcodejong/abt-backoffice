<?php

namespace App\Service\Mailer\Data;

class Attachment
{
    /** @var string */
    private $path;

    /** @var string */
    private $contentType;

    /**
     * @param string $path
     * @param string $contentType
     */
    public function __construct(string $path, string $contentType)
    {
        $this->path = $path;
        $this->contentType = $contentType;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

}
