<?php

namespace App\Service\Mailer\Data;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;

class Mail
{
    /** @var Identity */
    private $sender;

    /** @var Identity */
    private $receiver;

    /** @var Subject */
    private $subject;

    /** @var Body */
    private $body;

    /** @var Collection|Selectable|Attachment[] */
    private $attachments;

    /**
     * @param Identity $sender
     * @param Identity $receiver
     * @param Subject  $subject
     * @param Body     $body
     */
    public function __construct(Identity $sender, Identity $receiver, Subject $subject, Body $body)
    {
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->subject = $subject;
        $this->body = $body;

        $this->attachments = new ArrayCollection();
    }

    public function getSender(): Identity
    {
        return $this->sender;
    }

    public function getReceiver(): Identity
    {
        return $this->receiver;
    }

    public function getSubject(): Subject
    {
        return $this->subject;
    }

    public function getBody(): Body
    {
        return $this->body;
    }

    /**
     * @return Collection|Selectable|Attachment[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(Attachment $attachment): void
    {
        $this->attachments->add($attachment);
    }

    public function __toString()
    {
        return sprintf(
            'Mail from %s to %s, subject %s, %s',
            $this->getSender(),
            $this->getReceiver(),
            $this->getSubject(),
            $this->attachments->count() > 0 ? sprintf('%d attachments', $this->attachments->count()) : 'no attachments'
        );
    }
}
