<?php

namespace App\Service\Mailer\Data;

use SolidPhp\ValueObjects\Value\SingleValueObjectTrait;

class Body
{
    use SingleValueObjectTrait;
}
