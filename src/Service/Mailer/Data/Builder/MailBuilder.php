<?php

namespace App\Service\Mailer\Data\Builder;

use App\Service\Mailer\Data\Attachment;
use App\Service\Mailer\Data\Body;
use App\Service\Mailer\Data\Identity;
use App\Service\Mailer\Data\Mail;
use App\Service\Mailer\Data\Subject;

class MailBuilder
{
    /** @var Identity|null */
    private $sender;

    /** @var Identity|null */
    private $receiver;

    /** @var Subject|null */
    private $subject;

    /** @var Body|null */
    private $body;

    /** @var Attachment[] */
    private $attachments;

    public function setSender(Identity $sender): void
    {
        $this->sender = $sender;
    }

    public function setReceiver(Identity $receiver): void
    {
        $this->receiver = $receiver;
    }

    public function setSubject(Subject $subject): void
    {
        $this->subject = $subject;
    }

    public function setBody(Body $body): void
    {
        $this->body = $body;
    }

    public function addAttachment(Attachment $attachment): void
    {
        $this->attachments[] = $attachment;
    }

    public function getMail(): Mail
    {
        $mailObject = new Mail(
            $this->sender,
            $this->receiver,
            $this->subject,
            $this->body
        );

        foreach ($this->attachments as $attachment) {
            $mailObject->addAttachment($attachment);
        }

        return $mailObject;
    }
}
