<?php

namespace App\Service\Mailer\Data;

use SolidPhp\ValueObjects\Value\SingleValueObjectTrait;

class Subject
{
    use SingleValueObjectTrait;
}
