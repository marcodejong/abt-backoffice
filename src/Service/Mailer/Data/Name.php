<?php

namespace App\Service\Mailer\Data;

use SolidPhp\ValueObjects\Value\SingleValueObjectTrait;

class Name
{
    use SingleValueObjectTrait;
}
