<?php

namespace App\Service\Mailer\Data;

use Arriva\Abt\Value\EmailAddress;
use SolidPhp\ValueObjects\Value\ValueObjectTrait;

final class Identity
{
    use ValueObjectTrait;

    /** @var EmailAddress */
    private $emailAddress;

    /** @var Name|null */
    private $displayName;

    /**
     * @param Name|null    $displayName
     * @param EmailAddress $emailAddress
     */
    protected function __construct(EmailAddress $emailAddress, ?Name $displayName = null)
    {
        $this->emailAddress = $emailAddress;
        $this->displayName = $displayName;
    }

    public static function of(EmailAddress $emailAddress, ?Name $name = null): self
    {
        return self::getInstance($emailAddress, $name);
    }

    /**
     * This method parses an e-mail string that optionally includes a display name
     * of the format often used by mail clients:
     *   email@address.com
     *   <email@address.com>
     *   John Doe <email@address.com>
     *   "John Doe" <email@address.com>
     *
     * @param string $string
     *
     * @return Identity
     */
    public static function ofString(string $string): self
    {
        $matches = [];

        if (preg_match('/\"?(\b[^\"<>]+\b)\"?\s*<([^"\'<>\s]+@[^"<>\s]+\.[^"\'<>\s]+)>/', $string, $matches) === 1) {
            $name = $matches[1] ? Name::of($matches[1]) : null;
            $emailAddress = EmailAddress::fromString($matches[2]);

            return self::getInstance($emailAddress, $name);
        }

        if (preg_match('/([^"\'<>\s]+@[^"<>\s]+\.[^"\'<>\s]+)/', $string, $matches) === 1) {
            return self::getInstance(EmailAddress::fromString($matches[1]));
        }

        throw new \DomainException(sprintf('Unsupported identity string "%s"', $string));
    }

    public function getDisplayName(): ?Name
    {
        return $this->displayName;
    }

    public function getEmailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    public function toString(): string
    {
        return $this->displayName ? sprintf('"%s" <%s>', $this->displayName, $this->emailAddress) : sprintf('%s', $this->emailAddress);
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
