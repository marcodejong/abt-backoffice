<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Service\Exception\Factory;

use App\Service\Exception\BadRequestException;
use App\Service\Exception\BadResponseException;
use App\Service\Exception\ConnectionException;
use App\Service\Exception\ServiceExceptionInterface;
use App\Service\Exception\UnknownException;
use GuzzleHttp\Exception as GuzzleExceptions;

/**
 * @package App\Service\Exception\Factory
 */
class GuzzleServiceExceptionFactory
{
    public function createExceptionFromPrevious(\Exception $exception): ServiceExceptionInterface
    {
        switch (\get_class($exception)) {
            case GuzzleExceptions\BadResponseException::class:
            case GuzzleExceptions\TooManyRedirectsException::class:
            case GuzzleExceptions\ServerException::class:
                return new BadResponseException($exception->getMessage(), $exception->getCode(), $exception);

            case GuzzleExceptions\ClientException::class:
                return new BadRequestException($exception->getMessage(), $exception->getCode(), $exception);

            case GuzzleExceptions\ConnectException::class:
                return new ConnectionException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return new UnknownException($exception->getMessage(), $exception->getCode(), $exception);
    }
}
