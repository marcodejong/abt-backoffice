<?php

namespace App\Service\Exception;

class CardInfoNotFoundException extends \RuntimeException
{
}
