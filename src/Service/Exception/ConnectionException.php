<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Service\Exception;

/**
 * @package App\Service\Exception
 */
class ConnectionException extends \RuntimeException implements ServiceExceptionInterface
{
}
