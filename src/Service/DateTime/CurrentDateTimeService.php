<?php

namespace App\Service\DateTime;

use DateTimeImmutable;

class CurrentDateTimeService implements DateTimeServiceInterface
{
    use DateUtilitiesTrait;

    public function now(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}
