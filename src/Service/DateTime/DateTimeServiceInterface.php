<?php

namespace App\Service\DateTime;

use DateTimeImmutable;

interface DateTimeServiceInterface
{
    public function now(): DateTimeImmutable;


    public function getDatePart(DateTimeImmutable $source): DateTimeImmutable;
}
