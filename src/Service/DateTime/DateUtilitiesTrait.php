<?php

namespace App\Service\DateTime;

use DateTimeImmutable;

trait DateUtilitiesTrait
{
    public function getDatePart(DateTimeImmutable $source): DateTimeImmutable
    {
        return new DateTimeImmutable($source->format('Y-m-d\T\1\2\:\0\0\:\0\0\+\0\0\:\0\0'));
    }
}
