<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Service\AssistRestApi;

use App\Manager\LocaleManager;
use App\Service\Exception\Factory\GuzzleServiceExceptionFactory;
use Arriva\Assist\Entity\Card;
use Arriva\Assist\Entity\Invoice;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\Request;

/**
 * @package App\Service\AssistRestApi
 */
class AssistApiRestClient
{
    /** @var GuzzleClientInterface */
    private $httpClient;

    /** @var GuzzleServiceExceptionFactory */
    private $exceptionFactory;

    /** @var LocaleManager */
    private $localeManager;

    public function __construct(
        GuzzleClientInterface $httpClient,
        GuzzleServiceExceptionFactory $exceptionFactory,
        LocaleManager $localeManager
    ) {
        $this->httpClient = $httpClient;
        $this->exceptionFactory = $exceptionFactory;
        $this->localeManager = $localeManager;
    }

    public function sendInvoice(Invoice $invoice): void
    {
        try {
            $this->httpClient->request(
                Request::METHOD_GET,
                \sprintf('/%s/api/email/send-invoice', $this->localeManager->getCurrentLocale() ?? 'nl'),
                [RequestOptions::QUERY => ['invoice_id' => $invoice->getId()]]
            );
        } catch (GuzzleException $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }

    public function sendCardReplaceNotificationWithoutInvoice(
        Card $card,
        int $reasonAdministrativeId,
        \DateTimeInterface $dateEffective,
        ?Invoice $invoice = null
    ): void {
        try {
            $uri = \sprintf(
                '/%s/api/email/send-card-exchange-notification-email',
                $this->localeManager->getCurrentLocale() ?? 'nl'
            );
            $params = [
                'card_id' => $card->getId(),
                'reason_administrative_id' => $reasonAdministrativeId,
                'date_effective' => $dateEffective->format('Y-m-d'),
            ];
            if ($invoice) {
                $params['invoice_id'] = $invoice->getId();
            }
            $this->httpClient->request(
                Request::METHOD_GET,
                $uri,
                [RequestOptions::QUERY => $params]
            );
        } catch (GuzzleException $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }
}
