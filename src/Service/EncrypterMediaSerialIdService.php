<?php

namespace App\Service;

use Arriva\Abt\Utility\EncrypterInterface;
use Arriva\Abt\Value\EncryptedMediaSerialId;
use Arriva\Abt\Value\MediaSerialId;

class EncrypterMediaSerialIdService implements MediaSerialIdService
{
    /** @var EncrypterInterface */
    private $encrypter;

    /**
     * @param EncrypterInterface $encrypter
     */
    public function __construct(EncrypterInterface $encrypter)
    {
        $this->encrypter = $encrypter;
    }

    public function encryptMediaSerialId(MediaSerialId $mediaSerialId): EncryptedMediaSerialId
    {
        return EncryptedMediaSerialId::of($this->encrypter->encrypt($mediaSerialId->getValue()));
    }

    public function decryptMediaSerialId(EncryptedMediaSerialId $encryptedMediaSerialId): MediaSerialId
    {
        return MediaSerialId::of($this->encrypter->decrypt($encryptedMediaSerialId->getValue()));
    }
}
