<?php


namespace App\Service\CustomerIdService;


use App\Repository\CustomerIdentityRepositoryInterface;
use Arriva\Reporting\Entity\CustomerIdentity;

class CustomerIdentityService
{
    /** @var CustomerIdentityRepositoryInterface */
    private $customerIdentityRepository;

    public function __construct(CustomerIdentityRepositoryInterface $customerIdentityRepository)
    {
        $this->customerIdentityRepository = $customerIdentityRepository;
    }

    public function getAssistIdByIdmId(int $idmId): ?int
    {
        $customerIdentity = $this->customerIdentityRepository->findByIdmId($idmId);

        return $customerIdentity ? $customerIdentity->getAssistCustomerId() : null;
    }

    public function getCustomerIdentityByIdmId(int $idmId): ?CustomerIdentity
    {
        return $this->customerIdentityRepository->findByIdmId($idmId);
    }
}
