<?php


namespace App\Service\ProductApi;


use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Request;


class ProductApiRestClient
{
    /** @var ClientInterface */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function buildModel(): int
    {
        try {
            $result = $this->client->request(
                Request::METHOD_GET,
                '/build-model'
            );

            return $result->getStatusCode();
        } catch (GuzzleException $exception) {
            return $exception->getCode();
        }

    }

    public function getBuildModelStatus(): int
    {
        try {
            $result = $this->client->request(
                Request::METHOD_GET,
                '/build-model-status'
            );

            return $result->getStatusCode();
        } catch (GuzzleException $exception) {
            return $exception->getCode();
        }

    }
}