<?php

namespace App\Service\Tls\Client\Proposition;

use Arriva\Abt\Value\EngravedId;

interface PropositionInformationServiceInterface
{
    public function getInstances(EngravedId $engravedId): array;

}
