<?php

namespace App\Service\Tls\Client\Proposition\Builder\Information;

use App\Service\DateTime\DateTimeServiceInterface;
use Arriva\Abt\Value\EngravedId;
use SDOA\Model\Proposition\Entity\Information\InformationRequestType;
use SDOA\Model\Proposition\Entity\Information\MediaReferenceType;
use SDOA\Model\Proposition\Entity\Information\PropositionInformationRequest;

class PropositionInformationRequestBuilder
{
    private const NAMESPACE = 'http://cbo.translink.nl/1.0/PropositionInformationService';

    /** @var EngravedId */
    private $engravedId;

    /** @var DateTimeServiceInterface */
    private $dateTimeService;

    /** @var string */
    private $version;

    /** @var int */
    private $participantId;

    public function __construct(DateTimeServiceInterface $dateTimeService, string $version, int $participantId)
    {
        $this->dateTimeService = $dateTimeService;
        $this->version = $version;
        $this->participantId = $participantId;
    }

    public function setEngravedId(EngravedId $engravedId): PropositionInformationRequestBuilder
    {
        $this->engravedId = $engravedId;

        return $this;
    }

    public function buildRequest(): PropositionInformationRequest
    {
        return new PropositionInformationRequest($this->buildBaseRequest());
    }

    private function buildBaseRequest(): PropositionInformationRequest
    {
        if (!$this->engravedId) {
            throw new \RuntimeException('You have to set an engraved ID');
        }

        return new PropositionInformationRequest([
            'Version' => $this->version,
            'ParticipantID' => $this->participantId,
            'Request' => [$this->buildBodyRequest()],
        ]);
    }

    private function buildBodyRequest(): InformationRequestType
    {
        return new InformationRequestType([
            'ReportDateTime' => $this->dateTimeService->now()->format('Y-m-d\TH:i:s'),
            'RequestReason' => 'InfoRequest',
            'MediaReference' => new MediaReferenceType([
                'CSC' => new \SoapVar([
                    'EngravedID' => new \SoapVar(
                        $this->engravedId->getEngravedIdString(),
                        XSD_STRING,
                        null,
                        null,
                        'EngravedID',
                        self::NAMESPACE
                    ),
                ], SOAP_ENC_OBJECT),
            ]),
        ]);
    }
}
