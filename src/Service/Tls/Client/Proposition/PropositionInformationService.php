<?php

namespace App\Service\Tls\Client\Proposition;

use App\Service\Tls\Client\Proposition\Builder\Information\PropositionInformationRequestBuilder;
use Arriva\Abt\Value\EngravedId;
use Tls\Proposition\Api\Client\PropositionService\Information\InformationServiceInterface;

class PropositionInformationService implements PropositionInformationServiceInterface
{
    /** @var InformationServiceInterface */
    private $informationService;

    /** @var PropositionInformationRequestBuilder */
    private $requestBuilder;

    public function __construct(
        InformationServiceInterface $informationService,
        PropositionInformationRequestBuilder $requestBuilder
    ) {
        $this->informationService = $informationService;
        $this->requestBuilder = $requestBuilder;
    }

    public function getInstances(EngravedId $engravedId): array
    {
        $info = $this->informationService->getPropositionInformation(
            $this->requestBuilder->setEngravedId($engravedId)->buildRequest()
        );

        $instances = [];
        foreach ($info->getAllResponses() as $response) {
            foreach ($response->getAllInstancess() as $instance) {
                $instances = is_array($instance->Instance) ? array_merge($instances, $instance->Instance) : [];
            }
        }

        return $instances;
    }

}
