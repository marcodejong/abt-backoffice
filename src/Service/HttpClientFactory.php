<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Service;

use App\Monolog\HttpClientMessageFormatter;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Log\LoggerInterface;

/**
 * @package App\Service
 */
class HttpClientFactory
{
    public static function createHttpClient(array $config, LoggerInterface $logger, string $logLevel): ClientInterface
    {
        $handler = HandlerStack::create();
        $handler->push(Middleware::log($logger, new HttpClientMessageFormatter($logLevel)), $logLevel);

        $config['handler'] = $handler;

        return new Client($config);
    }
}
