<?php

namespace App\Service\LocalLinkmanagerService;

use App\Service\Exception\CardInfoNotFoundException;
use App\Service\LocalLinkmanagerService\Entity\CardInfoResponse;
use App\Service\LocalLinkmanagerService\OAuth\Token;
use GuzzleHttp\ClientInterface as HttpClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Predis\ClientInterface as RedisClientInterface;

class LocalLinkmanagerService
{
    const CACHE_KEY_TOKEN = 'local_linkmanager.oauth.access_token';

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var RedisClientInterface
     */
    private $redisClient;

    /**
     * @var string
     */
    private $oauthClientId;

    /**
     * @var string
     */
    private $oauthClientSecret;

    /**
     * Local Linkmanager only allows client_credentials for now, so this is static
     * @var string
     */
    private $oauthGrantType = 'client_credentials';

    public function __construct(
        HttpClientInterface $httpClient,
        RedisClientInterface $redisClient,
        string $oauthClientId,
        string $oauthClientSecret
    ) {
        $this->httpClient = $httpClient;
        $this->redisClient = $redisClient;
        $this->oauthClientId = $oauthClientId;
        $this->oauthClientSecret = $oauthClientSecret;
    }

    /**
     * @param string $engravedId
     * @return null|CardInfoResponse
     * @throws CardInfoNotFoundException When card info could not be found
     * @throws BadResponseException When there was an error with the Local Linkmanager service
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCardInfo($engravedId): ?CardInfoResponse
    {
        $accessToken = $this->getAccessToken();
        try {
            $response = $this->httpClient->request(
                'GET',
                sprintf('/api/v1/card/%s', $engravedId),
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => sprintf('Bearer %s', $accessToken->getToken()),
                    ],
                ]
            );

            if ($response->getStatusCode() === 200) {
                return CardInfoResponse::fromJsonString((string)$response->getBody());
            }
        } catch (ClientException $exception) {
            if ($exception->getResponse() && $exception->getResponse()->getStatusCode() === 404) {
                throw new CardInfoNotFoundException(
                    sprintf('Card info for card with engraved ID "%s" not found.', $engravedId)
                );
            }

            throw $exception;
        }

        return null;
    }

    private function getAccessToken(): Token
    {
        if (!$this->redisClient->exists(self::CACHE_KEY_TOKEN)) {
            $this->redisClient->set(self::CACHE_KEY_TOKEN, serialize($this->retrieveAccessToken()));
        }

        $token = unserialize($this->redisClient->get(self::CACHE_KEY_TOKEN));

        if ($token->isExpired()) {
            $token = $this->retrieveAccessToken();
            $this->redisClient->set(self::CACHE_KEY_TOKEN, serialize($token));
        }

        return $token;
    }

    private function retrieveAccessToken(): ?Token
    {
        $response = $this->httpClient->request(
            'POST',
            '/oauth/v2/token',
            [
                'json' => [
                    'grant_type' => $this->oauthGrantType,
                    'client_id' => $this->oauthClientId,
                    'client_secret' => $this->oauthClientSecret,
                ],
            ]
        );
        if ($response->getStatusCode() === 200
            && ($responseBody = json_decode((string)$response->getBody()))
            && false !== $responseBody
            && isset($responseBody->access_token, $responseBody->expires_in)
        ) {
            return new Token(
                $responseBody->access_token,
                (new \DateTime())->add(new \DateInterval(sprintf('PT%sS', $responseBody->expires_in)))
            );
        }

        return null;
    }
}
