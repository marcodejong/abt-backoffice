<?php

namespace App\Service\LocalLinkmanagerService\OAuth;

class Token
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var \DateTime
     */
    private $expireDate;

    public function __construct(string $token, \DateTime $expireDate)
    {
        $this->token = $token;
        $this->expireDate = $expireDate;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getExpireDate(): \DateTime
    {
        return $this->expireDate;
    }

    public function isExpired(): bool
    {
        return $this->expireDate->getTimestamp() - time() < 0;
    }
}
