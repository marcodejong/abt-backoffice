<?php

namespace App\Service\LocalLinkmanagerService\Entity;

class CardInfoResponse
{
    /**
     * @var string
     */
    private $cardId;

    /**
     * @var string
     */
    private $engravedId;

    /**
     * @var int
     */
    private $generationNumber;

    /**
     * @var \DateTime
     */
    private $registrationEndDateTime;

    public function __construct(
        string $cardId,
        string $engravedId,
        int $generationNumber,
        \DateTime $registrationEndDateTime
    ) {
        $this->cardId = $cardId;
        $this->engravedId = $engravedId;
        $this->generationNumber = $generationNumber;
        $this->registrationEndDateTime = $registrationEndDateTime;
    }

    public static function fromJsonString(string $jsonString): self
    {
        $jsonObject = json_decode($jsonString);
        if (false === $jsonObject) {
            throw new \InvalidArgumentException('Could not decode JSON string');
        }

        return new self(
            $jsonObject->card_id,
            $jsonObject->engraved_id,
            $jsonObject->generation_number,
            \DateTime::createFromFormat(\DateTime::ATOM, $jsonObject->registration_end_date_time)
        );
    }

    public function getCardId(): string
    {
        return $this->cardId;
    }

    public function getEngravedId(): string
    {
        return $this->engravedId;
    }

    public function getGenerationNumber(): int
    {
        return $this->generationNumber;
    }

    public function getRegistrationEndDateTime(): \DateTime
    {
        return $this->registrationEndDateTime;
    }
}
