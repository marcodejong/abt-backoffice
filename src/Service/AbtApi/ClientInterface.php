<?php

namespace App\Service\AbtApi;

use App\Service\AbtApi\Entity\Request\CancelAndCreditSubscriptionRequest;
use App\Service\AbtApi\Entity\Request\CreateNalCardProductInstancesRequest;
use App\Service\AbtApi\Entity\Request\CreatePropositionElementInstancesRequest;
use App\Service\AbtApi\Entity\Request\DeliverOrderRequest;
use App\Service\AbtApi\Entity\Request\SendOrderConfirmationEmailRequest;
use App\Service\AbtApi\Entity\Request\SetTransactionOverrideRequest;
use App\Service\AbtApi\Entity\Response\CancelAndCreditSubscriptionResponse;
use App\Service\AbtApi\Entity\Response\CreateNalCardProductInstancesResponse;
use App\Service\AbtApi\Entity\Response\CreatePropositionElementInstancesResponse;
use App\Service\AbtApi\Entity\Response\DeliverOrderResponse;
use App\Service\AbtApi\Entity\Response\SendOrderConfirmationEmailResponse;
use App\Service\AbtApi\Entity\Response\SetTransactionOverrideResponse;

interface ClientInterface
{
    public function sendOrderConfirmationEmail(SendOrderConfirmationEmailRequest $request): SendOrderConfirmationEmailResponse;

    public function createPropositionElementInstances(CreatePropositionElementInstancesRequest $request): CreatePropositionElementInstancesResponse;

    public function createNalCardProductInstances(CreateNalCardProductInstancesRequest $request): CreateNalCardProductInstancesResponse;

    public function setTransactionOverride(SetTransactionOverrideRequest $request): SetTransactionOverrideResponse;

    public function deliverOrder(DeliverOrderRequest $request): DeliverOrderResponse;

    public function cancelAndCreditSubscription(CancelAndCreditSubscriptionRequest $request): CancelAndCreditSubscriptionResponse;
}
