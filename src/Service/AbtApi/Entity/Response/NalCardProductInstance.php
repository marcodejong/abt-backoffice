<?php

namespace App\Service\AbtApi\Entity\Response;

class NalCardProductInstance
{
    /** @var bool */
    private $success;

    /** @var int */
    private $resultCode;

    /** @var string */
    private $resultMessage;

    /** @var int */
    private $id;

    /** @var string */
    private $contractTariff;

    /** @var string */
    private $engravedId;

    public function __construct(
        bool $success,
        int $resultCode,
        string $resultMessage,
        int $id,
        string $contractTariff,
        string $engravedId
    ) {
        $this->success = $success;
        $this->resultCode = $resultCode;
        $this->contractTariff = $contractTariff;
        $this->resultMessage = $resultMessage;
        $this->id = $id;
        $this->engravedId = $engravedId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function isSuccessful(): bool
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getResultMessage(): string
    {
        return $this->resultMessage;
    }

    /**
     * @return int
     */
    public function getResultCode(): int
    {
        return $this->resultCode;
    }

    /**
     * @return string
     */
    public function getContractTariff(): string
    {
        return $this->contractTariff;
    }

    /**
     * @return string
     */
    public function getEngravedId(): string
    {
        return $this->engravedId;
    }

}
