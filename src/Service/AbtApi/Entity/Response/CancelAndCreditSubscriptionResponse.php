<?php

namespace App\Service\AbtApi\Entity\Response;

class CancelAndCreditSubscriptionResponse implements ResponseInterface
{
    /**
     * @var bool|null
     */
    public $success;

    /**
     * @var string|null
     */
    public $message;
}
