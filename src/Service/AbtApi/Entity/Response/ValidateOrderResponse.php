<?php

namespace App\Service\AbtApi\Entity\Response;

class ValidateOrderResponse implements ResponseInterface, ValidatedResponseInterface
{
    use ValidatedResponseTrait;

    public function __construct(?array $errors = null)
    {
        if ($errors) {
            $this->setErrors($errors);
        }
    }
}
