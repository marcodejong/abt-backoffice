<?php

namespace App\Service\AbtApi\Entity\Response;

class Error
{
    /** @var null|string */
    private $code;

    /** @var null|string|float|int */
    private $invalidValue;

    /** @var null|string */
    private $propertyPath;

    /** @var null|string */
    private $message;

    /** @var null|array */
    private $satisfiableBy;

    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return float|int|null|string
     */
    public function getInvalidValue()
    {
        return $this->invalidValue;
    }

    public function getPropertyPath(): ?string
    {
        return $this->propertyPath;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getSatisfiableBy(): ?array
    {
        return $this->satisfiableBy;
    }
}
