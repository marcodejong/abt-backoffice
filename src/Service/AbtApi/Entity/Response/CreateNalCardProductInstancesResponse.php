<?php

namespace App\Service\AbtApi\Entity\Response;

class CreateNalCardProductInstancesResponse implements ResponseInterface, ValidatedResponseInterface
{
    use ValidatedResponseTrait;

    private $nalCardProductInstances = [];

    public function __construct(?array $errors = null)
    {
        if ($errors) {
            $this->setErrors($errors);
        }
    }

    /**
     * @return NalCardProductInstance[]
     */
    public function getPropositionElementInstances(): array
    {
        return $this->nalCardProductInstances;
    }

    /**
     * @return NalCardProductInstance[]
     */
    public function getFailedNalCardProductInstances(): array
    {
        return array_filter($this->nalCardProductInstances, function(NalCardProductInstance $nalCardProductInstance) {
            return false === $nalCardProductInstance->isSuccessful();
        });
    }

    /**
     * @return NalCardProductInstance[]
     */
    public function getSucceededNalCardProductInstances(): array
    {
        return array_filter($this->nalCardProductInstances, function(NalCardProductInstance $nalCardProductInstance) {
            return true === $nalCardProductInstance->isSuccessful();
        });
    }

    public function nalCardProductInstancesPartiallySucceeded(): bool
    {
        return !empty($this->getFailedNalCardProductInstances())
            && !empty($this->getSucceededNalCardProductInstances());
    }

}
