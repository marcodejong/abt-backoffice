<?php

namespace App\Service\AbtApi\Entity\Response;

/**
 * Trait ValidatedResponseTrait
 *
 * This Trait ensures that the using class fulfills the contract for ValidatedResponseInterface.
 * Using classes can use `setErrors` to mark the response as being invalid
 */
trait ValidatedResponseTrait /* implements ValidatedResponseInterface */
{
    /** @var null|Error[] */
    private $errors = [];

    /**
     * @param Error[] $errors
     */
    private function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @return null|Error[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function isValid(): bool
    {
        return empty($this->errors);
    }

}
