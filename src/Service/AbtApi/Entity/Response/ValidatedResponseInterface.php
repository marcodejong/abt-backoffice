<?php

namespace App\Service\AbtApi\Entity\Response;

interface ValidatedResponseInterface extends ResponseInterface
{
    /**
     * @return Error[]|null
     */
    public function getErrors(): array;

    public function isValid(): bool;
}
