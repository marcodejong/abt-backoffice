<?php

namespace App\Service\AbtApi\Entity\Response\Error;

interface ErrorInterface
{
    public function getCode(): string;

    public function getMessage(): string;
}
