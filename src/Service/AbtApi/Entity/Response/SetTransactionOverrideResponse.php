<?php


namespace App\Service\AbtApi\Entity\Response;


class SetTransactionOverrideResponse
{
    /** @var string|null */
    private $message;

    /**
     * SetTransactionOverrideResponse constructor.
     * @param string|null $message
     */
    public function __construct(?string $message)
    {
        $this->message = $message;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }
}