<?php

namespace App\Service\AbtApi\Entity\Response;

class CreatePropositionElementInstancesResponse implements ResponseInterface, ValidatedResponseInterface
{
    use ValidatedResponseTrait;

    private $propositionElementInstances = [];

    public function __construct(?array $errors = null)
    {
        if ($errors) {
            $this->setErrors($errors);
        }
    }

    /**
     * @return PropositionElementInstance[]
     */
    public function getPropositionElementInstances(): array
    {
        return $this->propositionElementInstances;
    }

    /**
     * @return PropositionElementInstance[]
     */
    public function getFailedPropositionElementInstances(): array
    {
        return array_filter($this->propositionElementInstances, function(PropositionElementInstance $propositionElementInstance) {
            return false === $propositionElementInstance->isSuccessful();
        });
    }

    /**
     * @return PropositionElementInstance[]
     */
    public function getSucceededPropositionElementInstances(): array
    {
        return array_filter($this->propositionElementInstances, function(PropositionElementInstance $propositionElementInstance) {
            return true === $propositionElementInstance->isSuccessful();
        });
    }

    public function propositionElementInstancesPartiallySucceeded(): bool
    {
        return !empty($this->getFailedPropositionElementInstances())
            && !empty($this->getSucceededPropositionElementInstances());
    }

}
