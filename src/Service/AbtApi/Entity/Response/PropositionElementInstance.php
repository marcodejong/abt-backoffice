<?php

namespace App\Service\AbtApi\Entity\Response;

class PropositionElementInstance
{
    /** @var bool */
    private $success;

    /** @var int */
    private $resultCode;

    /** @var string */
    private $resultMessage;

    /** @var int */
    private $id;

    /** @var int */
    private $retailerRefID;

    /** @var string */
    private $peDescription;
    /**
     * @var string
     */
    private $peCode;

    /** @var string */
    private $engravedId;

    public function __construct(
        bool $success,
        int $resultCode,
        string $resultMessage,
        int $id,
        int $retailerRefID,
        ?string $peDescription,
        string $peCode,
        string $engravedId
    ) {
        $this->success = $success;
        $this->resultCode = $resultCode;
        $this->retailerRefID = $retailerRefID;
        $this->peDescription = $peDescription;
        $this->peCode = $peCode;
        $this->resultMessage = $resultMessage;
        $this->id = $id;
        $this->engravedId = $engravedId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function isSuccessful(): bool
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getResultMessage(): string
    {
        return $this->resultMessage;
    }

    /**
     * @return int
     */
    public function getResultCode(): int
    {
        return $this->resultCode;
    }

    /**
     * @return int
     */
    public function getRetailerRefID(): int
    {
        return $this->retailerRefID;
    }

    /**
     * @return string
     */
    public function getPeCode(): string
    {
        return $this->peCode;
    }

    /**
     * @return string
     */
    public function getPeDescription(): string
    {
        return $this->peDescription;
    }

    /**
     * @return string
     */
    public function getEngravedId(): string
    {
        return $this->engravedId;
    }

}
