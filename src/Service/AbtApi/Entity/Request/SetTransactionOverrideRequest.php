<?php


namespace App\Service\AbtApi\Entity\Request;


class SetTransactionOverrideRequest
{
    public const TYPE__NO_OVERRIDE = 'no-override';
    public const TYPE__PRICE = 'price';
    public const TYPE__CREDIT = 'credit';
    public const TYPE__REMOVE = 'remove';

    /** @var int */
    private $transactionId;

    /** @var string */
    private $overrideType;

    /** @var float|null */
    private $priceBeforeRating;

    /** @var float|null */
    private $priceAfterRating;

    public function __construct(
        int $transactionId,
        string $overrideType,
        ?float $priceBeforeRating = null,
        ?float $priceAfterRating = null
    ) {
        $this->transactionId = $transactionId;
        $this->overrideType = $overrideType;
        $this->priceBeforeRating = $priceBeforeRating;
        $this->priceAfterRating = $priceAfterRating;
    }

    /**
     * @param float|null $priceBeforeRating
     * @return SetTransactionOverrideRequest
     */
    public function setPriceBeforeRating($priceBeforeRating)
    {
        $this->priceBeforeRating = $priceBeforeRating;

        return $this;
    }

    /**
     * @param float|null $priceAfterRating
     * @return SetTransactionOverrideRequest
     */
    public function setPriceAfterRating($priceAfterRating)
    {
        $this->priceAfterRating = $priceAfterRating;

        return $this;
    }
}