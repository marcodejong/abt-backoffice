<?php

namespace App\Service\AbtApi\Entity\Request;

class SendOrderConfirmationEmailRequest
{
    /** @var Order */
    private $order;

    /**
     * @var string
     */
    private $emailAddress;

    public function __construct(Order $order, string $emailAddress)
    {
        $this->order = $order;
        $this->emailAddress = $emailAddress;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

}
