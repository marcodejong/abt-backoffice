<?php

namespace App\Service\AbtApi\Entity\Request;

class CreateNalCardProductInstancesRequest
{
    /** @var string */
    private $orderPublicId;

    /** @var NalCardProductInstance[] */
    private $nalCardProductInstances = [];

    public function __construct(string $orderPublicId, array $nalCardProductInstances)
    {
        $this->orderPublicId = $orderPublicId;
        $this->nalCardProductInstances = $nalCardProductInstances;
    }

    /**
     * @return string
     */
    public function getOrderPublicId(): string
    {
        return $this->orderPublicId;
    }

    /**
     * @return NalCardProductInstance[]
     */
    public function getNalCardProductInstances(): array
    {
        return $this->nalCardProductInstances;
    }

}
