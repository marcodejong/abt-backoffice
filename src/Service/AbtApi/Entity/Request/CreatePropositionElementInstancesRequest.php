<?php

namespace App\Service\AbtApi\Entity\Request;

class CreatePropositionElementInstancesRequest
{
    /** @var string */
    private $orderPublicId;

    /** @var PropositionElementInstance[] */
    private $propositionElementInstances = [];

    public function __construct(string $orderPublicId, array $propositionElementInstance)
    {
        $this->orderPublicId = $orderPublicId;
        $this->propositionElementInstances = $propositionElementInstance;
    }

    /**
     * @return string
     */
    public function getOrderPublicId(): string
    {
        return $this->orderPublicId;
    }

    /**
     * @return PropositionElementInstance[]
     */
    public function getPropositionElementInstances(): array
    {
        return $this->propositionElementInstances;
    }

}
