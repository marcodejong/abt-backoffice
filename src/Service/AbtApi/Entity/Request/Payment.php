<?php

namespace App\Service\AbtApi\Entity\Request;

class Payment
{
    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILED = 'failed';
    public const STATUS_PENDING = 'pending';
    public const STATUS_CANCELLED = 'cancelled';

    /** @var null|string */
    private $status;

    /** @var null|string */
    private $pspReference;

    /** @var null|string */
    private $paymentMethod;

    /** @var null|string */
    private $issuer;

    /** @var null|float */
    private $amountPaid;

    /** @var null|string */
    private $accountNumber;

    public function __construct(
        ?string $status,
        ?string $pspReference,
        ?string $paymentMethod,
        ?string $issuer,
        ?float $amountPaid,
        ?string $accountNumber
    ) {
        $this->status = $status;
        $this->pspReference = $pspReference;
        $this->paymentMethod = $paymentMethod;
        $this->issuer = $issuer;
        $this->amountPaid = $amountPaid;
        $this->accountNumber = $accountNumber;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    public function getPspReference(): ?string
    {
        return $this->pspReference;
    }

    public function setPspReference(?string $pspReference): void
    {
        $this->pspReference = $pspReference;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?string $paymentMethod): void
    {
        $this->paymentMethod = $paymentMethod;
    }

    public function getIssuer(): ?string
    {
        return $this->issuer;
    }

    public function setIssuer(?string $issuer): void
    {
        $this->issuer = $issuer;
    }

    public function getAmountPaid(): ?float
    {
        return $this->amountPaid;
    }

    public function setAmountPaid(?float $amountPaid): void
    {
        $this->amountPaid = $amountPaid;
    }

    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    public function setAccountNumber(?string $accountNumber): void
    {
        $this->accountNumber = $accountNumber;
    }
}
