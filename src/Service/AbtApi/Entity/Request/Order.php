<?php

namespace App\Service\AbtApi\Entity\Request;

class Order
{
    /** @var OrderItem[] */
    private $items;

    /** @var string|null */
    private $id;

    /** @var Payment|null */
    private $payment;

    /**
     * @param OrderItem[]  $items
     * @param string|null  $id
     * @param Payment|null $payment
     */
    private function __construct(array $items, ?string $id, ?Payment $payment = null)
    {
        $this->items = $items;
        $this->id = $id;
        $this->payment = $payment;
    }

    public static function createDraftOrder(array $items): self
    {
        return new self($items, null, null);
    }

    public static function createUpdateOrder(string $id, Payment $payment): self
    {
        return new self([], $id, $payment);
    }

    public static function createAcceptedOrder(string $id): self
    {
        return new self([], $id);
    }

    /**
     * @return OrderItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }
}
