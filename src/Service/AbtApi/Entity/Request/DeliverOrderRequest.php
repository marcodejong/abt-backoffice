<?php


namespace App\Service\AbtApi\Entity\Request;


class DeliverOrderRequest
{
    /** @var string */
    private $publicId;

    public function __construct(string $publicId)
    {
        $this->publicId = $publicId;
    }
}