<?php

namespace App\Service\AbtApi\Entity\Request;

use Arriva\Abt\Utility\Money;

class CancelAndCreditSubscriptionRequest
{
    /** @var string */
    public $subscriptionPublicId;

    /** @var \DateTimeImmutable */
    public $endDate;

    /** @var Money */
    public $creditAmount;

    public function __construct(string $subscriptionPublicId, \DateTimeInterface $endDate, Money $creditAmount)
    {
        $this->subscriptionPublicId = $subscriptionPublicId;
        $this->endDate = ($endDate instanceof \DateTime) ? \DateTimeImmutable::createFromMutable($endDate) : $endDate;
        $this->creditAmount = $creditAmount;
    }
}
