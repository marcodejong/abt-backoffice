<?php

namespace App\Service\AbtApi\Entity\Request;

class OrderItem
{
    /** @var string */
    private $productConfigurationCode;

    /** @var string */
    private $startDate;

    /** @var string */
    private $engravedId;

    public function __construct(string $productConfigurationCode, string $startDate, string $engravedId)
    {
        $this->productConfigurationCode = $productConfigurationCode;
        $this->startDate = $startDate;
        $this->engravedId = $engravedId;
    }

    public function getProductConfigurationCode(): string
    {
        return $this->productConfigurationCode;
    }

    public function getStartDate(): string
    {
        return $this->startDate;
    }

    public function getEngravedId(): string
    {
        return $this->engravedId;
    }
}
