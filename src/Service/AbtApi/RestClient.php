<?php

namespace App\Service\AbtApi;

use App\Service\AbtApi\Entity\Request\CancelAndCreditSubscriptionRequest;
use App\Service\AbtApi\Entity\Request\CreateNalCardProductInstancesRequest;
use App\Service\AbtApi\Entity\Request\CreatePropositionElementInstancesRequest;
use App\Service\AbtApi\Entity\Request\DeliverOrderRequest;
use App\Service\AbtApi\Entity\Request\SendOrderConfirmationEmailRequest;
use App\Service\AbtApi\Entity\Request\SetTransactionOverrideRequest;
use App\Service\AbtApi\Entity\Response;
use App\Service\AbtApi\Entity\Response\CancelAndCreditSubscriptionResponse;
use App\Service\AbtApi\Entity\Response\CreateNalCardProductInstancesResponse;
use App\Service\AbtApi\Entity\Response\CreatePropositionElementInstancesResponse;
use App\Service\AbtApi\Entity\Response\DeliverOrderResponse;
use App\Service\AbtApi\Entity\Response\SetTransactionOverrideResponse;
use Arriva\Idm\Bundle\Exception\Factory\ServiceExceptionFactoryInterface;
use Exception;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\HttpFoundation\Request as HttpRequest;

class RestClient implements ClientInterface
{
    /** @var GuzzleClientInterface */
    private $httpClient;

    /** @var SerializerInterface */
    private $serializer;

    /** @var ServiceExceptionFactoryInterface */
    private $exceptionFactory;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        GuzzleClientInterface $httpClient,
        SerializerInterface $serializer,
        ServiceExceptionFactoryInterface $exceptionFactory,
        LoggerInterface $logger
    ) {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
        $this->exceptionFactory = $exceptionFactory;
        $this->logger = $logger;
    }

    public function sendOrderConfirmationEmail(SendOrderConfirmationEmailRequest $request): Response\SendOrderConfirmationEmailResponse
    {
        try {
            $response = $this->httpClient->request(
                HttpRequest::METHOD_POST,
                '/api/email/confirm-order',
                [
                    RequestOptions::BODY => $this->serializer->serialize($request, 'json'),
                ]
            );
            if ($response->getStatusCode() === HttpResponse::HTTP_OK
                && ($responseObject = $this->deserializeResponse($response, Response\SendOrderConfirmationEmailResponse::class))
                && $responseObject instanceof Response\SendOrderConfirmationEmailResponse
            ) {
                return $responseObject;
            }

            throw $this->getInvalidResponseException($response);
        } catch (GuzzleException | Exception $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }

    public function createPropositionElementInstances(CreatePropositionElementInstancesRequest $request
    ): CreatePropositionElementInstancesResponse {
        try {
            $response = $this->httpClient->request(
                HttpRequest::METHOD_POST,
                '/api/order/proposition-element-instances/create',
                [
                    RequestOptions::BODY => $this->serializer->serialize($request, 'json'),
                ]
            );
            if ($response->getStatusCode() === HttpResponse::HTTP_OK
                && ($responseObject = $this->deserializeResponse($response, Response\CreatePropositionElementInstancesResponse::class))
                && $responseObject instanceof Response\CreatePropositionElementInstancesResponse
            ) {
                return $responseObject;
            }

            throw $this->getInvalidResponseException($response);
        } catch (GuzzleException | Exception $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }

    public function createNalCardProductInstances(CreateNalCardProductInstancesRequest $request
    ): CreateNalCardProductInstancesResponse {
        try {
            $response = $this->httpClient->request(
                HttpRequest::METHOD_POST,
                '/api/order/nal-card-product-instances/create',
                [
                    RequestOptions::BODY => $this->serializer->serialize($request, 'json'),
                ]
            );
            if ($response->getStatusCode() === HttpResponse::HTTP_OK
                && ($responseObject = $this->deserializeResponse($response, Response\CreateNalCardProductInstancesResponse::class))
                && $responseObject instanceof Response\CreateNalCardProductInstancesResponse
            ) {
                return $responseObject;
            }

            throw $this->getInvalidResponseException($response);
        } catch (GuzzleException | Exception $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }

    public function setTransactionOverride(SetTransactionOverrideRequest $request): SetTransactionOverrideResponse
    {
        try {
            $response = $this->httpClient->request(
                HttpRequest::METHOD_POST,
                '/api/transactions/override',
                [
                    RequestOptions::BODY => $this->serializer->serialize($request, 'json'),
                ]
            );
            if ($response->getStatusCode() === HttpResponse::HTTP_OK
                && ($responseObject = $this->deserializeResponse($response, SetTransactionOverrideResponse::class))
                && $responseObject instanceof SetTransactionOverrideResponse
            ) {
                return $responseObject;
            }

            throw $this->getInvalidResponseException($response);
        } catch (GuzzleException | Exception $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }

    public function deliverOrder(DeliverOrderRequest $request): DeliverOrderResponse
    {
        try {
            $response = $this->httpClient->request(
                HttpRequest::METHOD_POST,
                '/api/order/deliver',
                [
                    RequestOptions::BODY => $this->serializer->serialize($request, 'json'),
                ]
            );
            if ($response->getStatusCode() === HttpResponse::HTTP_OK
                && ($responseObject = $this->deserializeResponse($response, DeliverOrderResponse::class))
                && $responseObject instanceof DeliverOrderResponse
            ) {
                return $responseObject;
            }

            throw $this->getInvalidResponseException($response);
        } catch (GuzzleException | Exception $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }

    public function cancelAndCreditSubscription(CancelAndCreditSubscriptionRequest $request): CancelAndCreditSubscriptionResponse
    {
        try {
            $response = $this->httpClient->request(
                HttpRequest::METHOD_POST,
                '/api/subscriptions/cancel-credit',
                [
                    RequestOptions::BODY => $this->serializer->serialize($request, 'json'),
                ]
            );
            if ($response->getStatusCode() === HttpResponse::HTTP_OK
                && ($responseObject = $this->deserializeResponse($response, CancelAndCreditSubscriptionResponse::class))
                && $responseObject instanceof CancelAndCreditSubscriptionResponse
            ) {
                return $responseObject;
            }

            throw $this->getInvalidResponseException($response);
        } catch (GuzzleException | Exception $exception) {
            throw $this->exceptionFactory->createExceptionFromPrevious($exception);
        }
    }

    /**
     * @param ResponseInterface $response
     *
     * @param string            $expectedType
     *
     * @return object|null Will be one of the $acceptedTypes, or null if the response shape matches none of the types
     */
    private function deserializeResponse(ResponseInterface $response, string $expectedType)
    {
        $responseBody = (string)$response->getBody();
        $deserializedObject = $this->serializer->deserialize($responseBody, $expectedType, 'json');

        if ($deserializedObject instanceof $expectedType) {
            return $deserializedObject;
        }

        return null;
    }

    private function getInvalidResponseException(ResponseInterface $response): RuntimeException
    {
        return new RuntimeException(sprintf('Invalid ABT API response: %s', (string)$response->getBody()));
    }
}
