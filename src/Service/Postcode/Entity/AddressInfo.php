<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Service\Postcode\Entity;

/**
 * @package App\Service\Postcode\Entity
 */
class AddressInfo
{
    /** @var string */
    private $street;

    /** @var int */
    private $houseNumber;

    /** @var string|null */
    private $houseNumberAddition;

    /** @var int */
    private $postalCodeNumber;

    /** @var string */
    private $postalCodeLetters;

    /** @var string */
    private $city;

    /** @var string */
    private $community;

    /** @var string */
    private $province;

    /** @var float */
    private $latitude;

    /** @var float */
    private $longitude;

    /** @var string */
    private $purpose;

    /** @var int */
    private $surface;

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    public function getHouseNumber(): int
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(int $houseNumber): void
    {
        $this->houseNumber = $houseNumber;
    }

    public function getHouseNumberAddition(): ?string
    {
        return $this->houseNumberAddition;
    }

    public function setHouseNumberAddition(?string $houseNumberAddition): void
    {
        $this->houseNumberAddition = $houseNumberAddition;
    }

    public function getPostalCodeNumber(): int
    {
        return $this->postalCodeNumber;
    }

    public function setPostalCodeNumber(int $postalCodeNumber): void
    {
        $this->postalCodeNumber = $postalCodeNumber;
    }

    public function getPostalCodeLetters(): string
    {
        return $this->postalCodeLetters;
    }

    public function setPostalCodeLetters(string $postalCodeLetters): void
    {
        $this->postalCodeLetters = $postalCodeLetters;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getCommunity(): string
    {
        return $this->community;
    }

    public function setCommunity(string $community): void
    {
        $this->community = $community;
    }

    public function getProvince(): string
    {
        return $this->province;
    }

    public function setProvince(string $province): void
    {
        $this->province = $province;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getPurpose(): string
    {
        return $this->purpose;
    }

    public function setPurpose(string $purpose): void
    {
        $this->purpose = $purpose;
    }

    public function getSurface(): int
    {
        return $this->surface;
    }

    public function setSurface(int $surface): void
    {
        $this->surface = $surface;
    }
}
