<?php
/*
 * (c) Sqills Projects B.V. 2018 <php-dev-enschede@sqills.com>
 */

namespace App\Service\Postcode;

use App\Service\Postcode\Entity\AddressByPostalCodeDetailed;
use App\Service\Postcode\Entity\AddressInfo;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @package App\Service\Postcode
 */
class RestPostcodeService
{
    private const ACTION_ADDRESS_BY_POSTCODE_DETAILED = 'addressByPostalCodeDetailed';

    /** @var string */
    private $apiKey;

    /** @var ClientInterface */
    private $client;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(string $apiKey, Client $client, SerializerInterface $serializer)
    {
        $this->apiKey = $apiKey;
        $this->client = $client;
        $this->serializer = $serializer;
    }

    public function getAddressInfoByPostcode(
        string $postcode,
        int $houseNumber,
        ?string $houseNumberExtension = null
    ): ?AddressInfo {
        $queryParams = [
            'postal_code' => $postcode,
            'house_number' => $houseNumber,
        ];
        if ($houseNumberExtension) {
            $queryParams['house_number_addition'] = $houseNumberExtension;
        }
        try {
            $response = $this->client->request(
                Request::METHOD_GET,
                $this->getUrl(self::ACTION_ADDRESS_BY_POSTCODE_DETAILED),
                [RequestOptions::QUERY => $queryParams]
            );

            return $this->convertToAddressInfo(
                $this->serializer->deserialize(
                    (string)$response->getBody(),
                    AddressByPostalCodeDetailed::class,
                    'json'
                )
            );
        } catch (GuzzleException $exception) {
        }

        return null;
    }

    private function getUrl(string $action): string
    {
        return sprintf(
            '/postalcode/local/%s/%s.json',
            $this->apiKey,
            $action
        );
    }

    private function convertToAddressInfo(AddressByPostalCodeDetailed $addressByPostcodeDetailed): AddressInfo
    {
        $addressInfo = new AddressInfo();
        $addressInfo->setStreet($addressByPostcodeDetailed->getStreet());
        $addressInfo->setHouseNumber($addressByPostcodeDetailed->getHouseNumber());
        $addressInfo->setHouseNumberAddition($addressByPostcodeDetailed->getHouseNumberAddition());
        $addressInfo->setCity($addressByPostcodeDetailed->getCity());
        $addressInfo->setProvince($addressByPostcodeDetailed->getProvince());
        $addressInfo->setCommunity($addressByPostcodeDetailed->getCommunity());
        $addressInfo->setPurpose($addressByPostcodeDetailed->getPurpose());
        $addressInfo->setLongitude($addressByPostcodeDetailed->getLongitude());
        $addressInfo->setLatitude($addressByPostcodeDetailed->getLatitude());
        $addressInfo->setPostalCodeNumber($addressByPostcodeDetailed->getPostalCodeNumber());
        $addressInfo->setPostalCodeLetters($addressByPostcodeDetailed->getPostalCodeLetters());
        $addressInfo->setSurface($addressByPostcodeDetailed->getSurface());

        return $addressInfo;
    }
}
