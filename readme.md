# Arriva ABT backoffice #

## Authentication ##

To authenticate at the ABT backoffice, login at the ASSIST Symfony application and make sure you're 
using the same cookie configuration for both applications.

Make sure these environment variables are equal in both applications:

- `SESSION_PREFIX`
- `SESSION_COOKIE_DOMAIN`

For ABT backoffice, set the `APP_ASSIST_BASE_URL` variable so you're redirected to the correct login page.

## Run ##

```
$ docker-compose up
```

## Test ##

```
$ make test
```
