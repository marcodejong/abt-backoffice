.PHONY: build push

SHELL=/bin/bash

REGISTRY ?= 084202542620.dkr.ecr.eu-central-1.amazonaws.com
VERSION=$(shell git rev-parse --short HEAD)
IMAGE_NAME=$(REGISTRY)/arriva-abt-backoffice
HASH_TAG=$(IMAGE_NAME):$(VERSION)
PHP_VERSION=7.2
##

ssh_auth_socket := $(shell readlink -f $(SSH_AUTH_SOCK))

ifeq ($(ssh_auth_socket),)
ssh_auth_socket_flag =
ssh_auth_env_flag =
else
ssh_auth_socket_flag = -v $(ssh_auth_socket):/ssh-agent
ssh_auth_env_flag = -e SSH_AUTH_SOCK=/ssh-agent
endif
##
COMPOSER=docker run -i --rm -v $$PWD:/workspace  -e ASSETS_BASE_URL="https://" -e APP_ENV=ci -v $$HOME/.ssh/known_hosts:/root/.ssh/known_hosts -v $$(readlink -f $$SSH_AUTH_SOCK):/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent $(REGISTRY)/php-composer:$(PHP_VERSION)

all: build push

build:
	$(COMPOSER) install -n --optimize-autoloader --no-scripts
	docker build -f Dockerfile -t $(HASH_TAG) .

##
test: build
	docker run \
		-i \
		--rm  \
		--env-file .env.ci \
		-v ${PWD}/reports:/app/reports \
		--tmpfs /app/var/cache \
		${HASH_TAG} \
		phing test
##

push:
	docker push $(HASH_TAG)
