#!/bin/sh
#
# Source environment script
# Usage:
#
#   source_env command
#
if [ -n "${CONSUL_HOST}" ]; then
  cmd="envconsul -log-level=${CONSUL_LOG_LEVEL} -consul-addr=${CONSUL_HOST} -prefix=${CONSUL_PREFIX} -once"
else
  cmd="exec"
fi

eval "${cmd} exec $@"
